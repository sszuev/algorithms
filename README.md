# Algorithms

This is a collection of algorithmic questions. An attempt to systematize the knowledge, that may be required at a coding
interview.

## [Arrays and Strings](src/main/java/com/gitlab/sszuev/questions/arrays_and_strings/CONTENT.md)

- **T1**: How to find the missing number in an integer array of 1 to 100
- **T2**: How to find the largest and smallest number from an integer array
- **T3**: How to find all pairs in an integer array that sum is a given number
- **T4**: How to reverse an array
- **T5**: How to remove duplicates from an array without using the Java Collection API
- **T6**: How to check if two strings are anagrams
- **T7**: Find the first non-repeating character in a string (3 ways)
- **T8**: How to find duplicate characters in a string
- **T9**: How to find all permutations of a string
- **T10**: How to reverse words in a string phrase
- **T11**: How to check if strings are rotations of each other
- **T12**: How to check is the given string is a palindrome
- **H1**: (hackerrank) How to calculate cost of transforming the given `3x3` matrix to Magic Square
- **H1**: (hackerrank) Absolute Permutation
- **L3**: (leetcode) Two Sum
- **L8**: (leetcode) Repeated DNA Sequences
- **L13**: (leetcode) Greatest Common Divisor of Strings
- **L14**: (leetcode) Container With Most Water
- **L20**: (leetcode) Maximum Average Subarray
- **L21**: (leetcode) Find Pivot Index
- **L31**: (leetcode) Merge Strings Alternately
- **L34**: (leetcode) Kids With the Greatest Number of Candies
- **L36**: (leetcode) Is Substring
- **L39**: (leetcode) Reverse Words in a String
- **L43**: (leetcode) Increasing Triplet Subsequence
- **L44**: (leetcode) Maximum Number of Vowels in a Substring of Given Length
- **L47**: (leetcode) Decode String
- **L50**: (leetcode) Non-overlapping Intervals
- **L58**: (leetcode) Minimum Number of Arrows to Burst Balloons
- **L59**: (leetcode) Find the Highest Altitude
- **L69**: (leetcode) Can Place Flowers
- **L70**: (leetcode) Longest Subarray of 1's After Deleting One Element
- **L71**: (leetcode) Max Consecutive Ones III
- **L75**: (leetcode) Product of Array Except Self
- **L78**: (leetcode) Reverse Vowels of a String
- **L87**: (leetcode) String Compression
- **L89**: (leetcode) Move Zeroes
- **L96**: (leetcode) Latest Time You Can Obtain After Replacing Characters
- **L100**: (leetcode) Summary Ranges

## [Linked List](src/main/java/com/gitlab/sszuev/questions/linked_list/CONTENT.md)

- **T1**: How to find the middle element of a linked list in single pass
- **T2**: How to find if a linked list contains a loop
- **T3**: How to reverse a linked list using recursion and iteration
- **T4**: Remove duplicates from an unsorted linked list
- **T5**: How to find length of a singly linked list using Loop and recursion
- **T6**: How to find the k-th node from the end of a linked list
- **T7**: Add two numbers represented by linked lists
- **L4**: (leetcode): Add Two Numbers represented by linked lists
- **Q2**: Delete Middle Node: Implement an algorithm to delete a node in the middle of a singly linked list, given only
  access to that node
- **Q6**: Palindrome: Implement a function to check if a linked list is a palindrome
- **L23**: (leetcode) Delete the Middle Node of a Linked List
- **L51**: (leetcode) Odd Even Linked List
- **L74**: (leetcode) Maximum Twin Sum of a Linked List
- **L91**: (leetcode) Reverse Linked List
- **L102**: (leetcode) Linked List Cycle

## [Stacks and Queues](src/main/java/com/gitlab/sszuev/questions/staks_and_queues/CONTENT.md)

- **Q1**: Three in One: Describe how you could use a single array to implement three stacks
- **Q4**: Queue via Stacks: Implement a MyQueue class which implements a queue using two stacks
- **L15**: (leetcode) Removing Stars From a String
- **L26**: (leetcode) Number of Recent Calls
- **L30**: (leetcode; topics: Array, Stack, Monotonic Stack) Daily Temperatures
- **L37**: (leetcode; topics: Array, Stack, Simulation) Asteroid collision
- **L55**: (leetcode) Online Stock Span
- **L56**: (leetcode) Dota2 Senate
- **L73**: (leetcode) Maximum Subsequence Score
- **L81**: (leetcode) Total Cost to Hire K Workers
- **L101**: (leetcode) Valid Parentheses

## [Trees and Graphs](src/main/java/com/gitlab/sszuev/questions/trees_and_graphs/CONTENT.md)

- **T1**: Binary Tree: Pre order traversal Algorithms
- **T2**: Binary Tree: In order traversal Algorithms
- **T3**: Binary Tree: Post order traversal Algorithms
- **T4**: Binary Tree: Print all leaves
- **T5**: Binary Tree: Count all leaves
- **T6**: BST (Binary Search Tree) implementation
- **Q1**: Route Between Nodes:
  Given a directed graph, design an algorithm to find out whether there is a route between two nodes
- **Q2**: Minimal Tree: Given a sorted (increasing order) array with unique integer elements,
  write an algorithm to create a binary search tree with minimal height.
- **L11**: (leetcode) Count Nodes Equal to Average of Subtree
- **L16**: (leetcode) Leaf-Similar Trees
- **L17**: (leetcode) Keys and Rooms
- **L25**: (leetcode) Binary Tree Right Side View
- **L27**: (leetcode) Search in a Binary Search Tree
- **L28**: (leetcode) Nearest Exit from Entrance in Maze
- **L32**: (leetcode) Implement Trie (Prefix Tree)
- **L40**: (leetcode) Smallest Number in Infinite Set
- **L42**: (leetcode) Maximum Depth of Binary Tree
- **L46**: (leetcode) Count Good Nodes in Binary Tree
- **L48**: (leetcode) Number of provinces
- **L57**: (leetcode) Search Suggestions System
- **L61**: (leetcode) Lowest Common Ancestor of a Binary Tree
- **L77**: (leetcode) Reorder Routes to Make All Paths Lead to the City Zero
- **L79**: (leetcode) Path Sum III
- **L80**: (leetcode) Delete Node in a BST
- **L82**: (leetcode) Rotting Oranges
- **L83**: (leetcode) Longest ZigZag Path in a Binary Tree
- **L84**: (leetcode) Maximum Level Sum of a Binary Tree
- **L85**: (leetcode) Evaluate Division
- **L98**: (leetcode) Minimum Size Subarray Sum

## [Hash tables](src/main/java/com/gitlab/sszuev/questions/hash_table/CONTENT.md)

- **L22**: (leetcode) Find the Difference of Two Arrays
- **L24**: (leetcode) Letter Combinations of a Phone Number
- **L35**: (leetcode) Determine if Two Strings Are Close
- **L45**: (leetcode) Equal Row and Column Pairs
- **L53**: (leetcode) Max Number of K-Sum Pairs
- **L90**: (leetcode) Unique Number of Occurrences
- **L97**: (leetcode) Majority Element
- **L99**: (leetcode) Ransom Note

## [Bit Manipulation](src/main/java/com/gitlab/sszuev/questions/bit_manipulation/CONTENT.md)

- **Q2**: Binary to String: Given a real number between `0` and `1` (e.g., `0.72`) that is passed in as a double, print
  the binary representation.
  If the number cannot be represented accurately in binary with at most `32` characters, print `"ERROR"`
- **Q4**: Next Number: Given a positive integer, print the next smallest and the next largest number
  that have the same number of `1` bits in their binary representation.
- **Q6**: Conversion: Write a function to determine the number of bits you would need to flip to convert integer A to
  integer B.
- **Q7**: Pairwise Swap: Write a program to swap odd and even bits in an integer with as few instructions as possible
  (e.g., bit `0` and bit `1` are swapped, bit `2` and bit `3` are swapped, and so on).
- **L7**: (leetcode) Divide Two Integers: Given two integers dividend and divisor, divide two integers without using
  multiplication, division, and mod operator.
- **L10**: (leetcode) Reverse bits of a given 32-bit unsigned integer.
- **L18**: (leetcode) Minimum Flips to Make a OR b Equal to c.
- **L33**: (leetcode) Find single number: given a non-empty array of integers nums, every element appears twice except for one.
- **L72**: (leetcode) Counting Bits
- **L95**: (leetcode) Number of 1 Bits

## [Sorting and Searching](src/main/java/com/gitlab/sszuev/questions/sorting_and_searching/CONTENT.md)

- **T1**: Bubble (+ Improved) sort
- **T2**: Iterative Quicksort
- **T3**: Insert sort
- **T4**: Merge sort
- **T5**: Bucket sort
- **T6**: Counting sort
- **T7**: Radix sort
- **Q1**: Sorted Merge: You are given two sorted arrays, A and B, where A has a large enough buffer at the end to hold
  B.
  Write a method to merge B into A in sorted order.
- **Q5**: Sparse Search: Given a sorted array of strings that is interspersed with empty strings,
  write a method to find the location of a given string.
- **L9**: (leetcode) Kth Largest Element in an Array.
- **L18**: (leetcode) Koko Eating Bananas
- **L38**: (leetcode) Guess Number Higher or Lower
- **L76**: (leetcode) Successful Pairs of Spells and Potions
- **L86**: (leetcode) Find Peak Element

## [Recursion and Dynamic Programming](src/main/java/com/gitlab/sszuev/questions/recursion_and_dynamic_programming/CONTENT.md)

- **Q7**: Permutations without Duplicates: Write a method to compute all permutations of a string of unique characters.
- **Q8**: Permutations with Duplicates: Write a method to compute all permutations of a string whose characters are not
  necessarily unique.
- **Q14**: Boolean Evaluation: Given a boolean expression consisting of the symbols `0`(_FALSE_), `1` (_TRUE_), `&`(
  _AND_), `|`(_OR_), `^`(_XOR_),
  and a desired boolean result value `result` (_TRUE_ or _FALSE_),
  implement a function to count the number of ways of parenthesizing the expression such that it evaluates to `result`.
- **L5**: (leetcode) Climbing stairs.
- **L6**: (leetcode) N-th Tribonacci Number.
- **L29**: (leetcode) Unique path (a robot on an m x n grid)
- **L41**: (leetcode) Min Cost Climbing Stairs
- **L49**: (leetcode) Longest Common Subsequence
- **L52**: (leetcode) House Robber
- **L54**: (leetcode) Combination Sum III
- **L56**: (leetcode) Edit Distance
- **L60**: (leetcode) Best Time to Buy and Sell Stock with Transaction Fee
- **L88**: (leetcode) Domino and Tromino Tiling
- **L92**: (leetcode) Pascal's Triangle
- **L93**: (leetcode) Best Time to Buy and Sell Stock
- **L94**: (leetcode) Divisor Game

## [Misc](src/main/java/com/gitlab/sszuev/questions/misc/CONTENT.md)

- **T1**: How to swap two Integers without using temporary variable
- **T2**: How to Check if two Rectangles overlap
- **T3**: Vending Machine
- **H4**: (hackerrank) Birthday Cake Candles
- **L1**: (leetcode) Roman to Integer
- **L2**: (leetcode) Integer to Roman
- **L2**: (leetcode) Find Positive Integer Solution for a Given Equation

## References

- https://leetcode.com
- https://www.hackerrank.com/
- https://habr.com/ru/company/gms/blog/515200/
- https://dev.by/news/50-chasto-zadavaemykh-voprosov-po-programmirovaniyu-dlya-podgotovki-k-tekhnicheskomu-sobesedovaniyu
- Cracking the coding interview, 6th edition, Gayle Laakmann McDowell
