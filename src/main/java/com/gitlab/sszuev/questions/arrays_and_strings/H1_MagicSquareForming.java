package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * We define a magic square to be an {@code n x n} matrix of distinct positive integers
 * from {@code 1} to {@code n ^ 2} where the sum of any row, column, or diagonal of length {@code n}
 * is always equal to the same number: the magic constant.
 * You will be given a {@code 3 x 3} matrix {@code s} of integers in the inclusive range {@code 1, 9}.
 * We can convert any digit {@code a} to any other digit {@code b} in the range {@code [1, 9]} at cost of {@code |a - b|}.
 * Given {@code s}, convert it into a magic square at minimal cost.
 * Print matrix cost on a new line.
 * <b>Note</b>: The resulting magic square must contain distinct integers in the inclusive range {@code [1, 9]}
 * <p>
 * <b>Example</b>:
 * <pre>{@code
 * $s = [[5, 3, 4], [1, 5, 8], [6, 4, 2]]
 * }</pre>
 * The matrix looks like this:
 * <pre>{@code
 * 5 3 4
 * 1 5 8
 * 6 4 2
 * }</pre>
 * We can convert it to the following magic square:
 * <pre>{@code
 * 8 3 4
 * 1 5 9
 * 6 7 2
 * }</pre>
 * This took three replacements at a cost of {@code |5 - 8| + |8 - 9| + |4 - 7| = 7}.
 * <p>
 * <b>Constraints</b>: {@code s[i][j] e [1,9]}
 *
 * @see <a href="https://en.wikipedia.org/wiki/Magic_square">wiki: Magic Square</a>
 * @see <a href="https://www.hackerrank.com/challenges/magic-square-forming/problem">Hackerrank: Magic Square Froming</a>
 */
public class H1_MagicSquareForming {

    static int[][] a10 = new int[][]{new int[]{2, 9, 4}, new int[]{7, 5, 3}, new int[]{6, 1, 8}};
    static int[][] a20 = new int[][]{a10[2], a10[1], a10[0]};
    static int[][] a11 = rotate(a10);
    static int[][] a12 = rotate(a11);
    static int[][] a13 = rotate(a12);
    static int[][] a21 = rotate(a20);
    static int[][] a22 = rotate(a21);
    static int[][] a23 = rotate(a22);

    static List<int[][]> magics = List.of(a10, a11, a12, a13, a20, a21, a22, a23);

    private static int calcCost(int[][] s) {
        int res = Integer.MAX_VALUE;
        for (int[][] it : magics) {
            int x = sum(abs(minus(s, it)));
            if (x < res) {
                res = x;
            }
        }
        return res;
    }

    private static int[][] rotate(int[][] matrix) {
        int[] row1 = new int[]{matrix[0][2], matrix[1][2], matrix[2][2]};
        int[] row2 = new int[]{matrix[0][1], matrix[1][1], matrix[2][1]};
        int[] row3 = new int[]{matrix[0][0], matrix[1][0], matrix[2][0]};
        return new int[][]{row1, row2, row3};
    }

    private static int[][] abs(int[][] matrix) {
        int[] row1 = new int[]{Math.abs(matrix[0][0]), Math.abs(matrix[0][1]), Math.abs(matrix[0][2])};
        int[] row2 = new int[]{Math.abs(matrix[1][0]), Math.abs(matrix[1][1]), Math.abs(matrix[1][2])};
        int[] row3 = new int[]{Math.abs(matrix[2][0]), Math.abs(matrix[2][1]), Math.abs(matrix[2][2])};
        return new int[][]{row1, row2, row3};
    }

    private static int sum(int[][] matrix) {
        return matrix[0][0] + matrix[0][1] + matrix[0][2] +
                matrix[1][0] + matrix[1][1] + matrix[1][2] +
                matrix[2][0] + matrix[2][1] + matrix[2][2];
    }

    private static int[][] minus(int[][] left, int[][] right) {
        int[] row1 = new int[]{left[0][0] - right[0][0], left[0][1] - right[0][1], left[0][2] - right[0][2]};
        int[] row2 = new int[]{left[1][0] - right[1][0], left[1][1] - right[1][1], left[1][2] - right[1][2]};
        int[] row3 = new int[]{left[2][0] - right[2][0], left[2][1] - right[2][1], left[2][2] - right[2][2]};
        return new int[][]{row1, row2, row3};
    }

    @Test
    public void testCalcCost() {
        int[][] given = parse("5 3 4\n1 5 8\n6 4 2");
        int res = calcCost(given);
        Assertions.assertEquals(7, res);
    }

    private static int[][] parse(@SuppressWarnings("SameParameterValue") String given) {
        return Arrays.stream(given.split("\n"))
                .map(s -> Arrays.stream(s.split("\\s"))
                        .mapToInt(Integer::parseInt)
                        .toArray()).toArray(int[][]::new);
    }
}
