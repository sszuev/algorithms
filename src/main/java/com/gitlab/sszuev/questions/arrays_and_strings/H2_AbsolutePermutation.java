package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * We define {@code P} to be a permutation of the first {@code n} natural numbers in the range {@code [1, n]}.
 * Let {@code pos[i]} denote the value at position {@code i} in permutation using {@code P} using {@code 1}-based indexing.
 * {@code P} is considered to be an absolute permutation if {@code |pos[i] - i| = k} holds true for every {@code i e [1, n]}.
 * Given {@code i} and {@code k}, print the lexicographically smallest absolute permutation {@code P}.
 * If no absolute permutation exists, print {@code -1}.
 * <p>
 * <b>Example</b>:
 * <pre>{@code
 * n = 4
 * k = 2
 * }</pre>
 * Create an array of elements from {@code 1} to {@code n}, {@code pos = [1,2,3,4]}.
 * Using {@code 1} based indexing, create a permutation where every {@code |pos[i] - i| = k}.
 * It can be rearranged to {@code pos = [3,4,1,2]} so that all the absolute differences equal {@code 2}:
 * <pre>{@code
 * pos[i]   i   |pos[i] - i|
 * 3        1   2
 * 4        2   2
 * 1        3   2
 * 2        4   2
 * }</pre>
 * <p><b>Constraints</b>: {@code 1 <= n <= 100_000}, {@code 0 <= k <= n}
 * <p>
 * <b>Sample input</b>:
 * <pre>{@code
 * 2 1 n = 2, k = 1
 * 3 0 n = 3, k = 0
 * 3 2 n = 3, k = 2
 * }</pre>
 * <b>Sample output</b>:
 * <pre>{@code
 * 2 1
 * 1 2 3
 * -1
 * }</pre>
 *
 * @see <a href="https://www.hackerrank.com/challenges/absolute-permutation/problem">Hackerrank: Absolute Permutation</a>
 * @see T9_Permutation
 */
public class H2_AbsolutePermutation {

    private static List<Integer> calcAbsolutePermutation(int n, int k) {
        List<Integer> res = new ArrayList<>(Collections.nCopies(n, -1));
        Set<Integer> seen = new HashSet<>();
        if (k == 0) {
            for (int i = 1; i <= n; i++) {
                res.set(i - 1, i);
            }
            return res;
        }

        for (int i = 1; i <= n; i++) {
            int lowValue = i - k;
            int upValue = i + k;

            if (lowValue > 0 && !seen.contains(lowValue)) {
                res.set(i - 1, lowValue);
                seen.add(lowValue);
            } else if (upValue <= n && !seen.contains(upValue)) {
                res.set(i - 1, upValue);
                seen.add(upValue);
            } else {
                return Collections.singletonList(-1);
            }
        }
        return res;
    }

    @Test
    public void testN4K2() {
        List<Integer> res = calcAbsolutePermutation(4, 2);
        System.out.println(res);
        Assertions.assertEquals(List.of(3, 4, 1, 2), res);
    }

    @Test
    public void testN2K1() {
        List<Integer> res = calcAbsolutePermutation(2, 1);
        System.out.println(res);
        Assertions.assertEquals(List.of(2, 1), res);
    }

    @Test
    public void testN3K0() {
        List<Integer> res = calcAbsolutePermutation(3, 0);
        System.out.println(res);
        Assertions.assertEquals(List.of(1, 2, 3), res);
    }

    @Test
    public void testN6K0() {
        List<Integer> res = calcAbsolutePermutation(6, 0);
        System.out.println(res);
        Assertions.assertEquals(List.of(1, 2, 3, 4, 5, 6), res);
    }

    @Test
    public void testN3K2() {
        List<Integer> res = calcAbsolutePermutation(3, 2);
        System.out.println(res);
        Assertions.assertEquals(List.of(-1), res);

        Set<Integer> x = new TreeSet<>();
        x.add(2);
        x.add(44);
        System.out.println(x.iterator().next());
    }
}
