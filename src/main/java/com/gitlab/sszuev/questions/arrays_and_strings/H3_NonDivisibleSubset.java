package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * @see <a href="https://www.hackerrank.com/challenges/non-divisible-subset/problem">Hackerrank: Non-Divisible Subset</a>
 */
public class H3_NonDivisibleSubset {

    public static int _2_nonDivisibleSubset(int k, List<Integer> s) {
        int[] frequencyRems = new int[k];
        int tempRem;

        for (Integer integer : s) {
            // remainder of each number in s
            tempRem = integer % k;
            // add one counter at that position
            frequencyRems[tempRem]++;
        }

        // if we have more than one case of s[i]% k we set it to one
        // as per modulos rules we can just have one
        if (frequencyRems[0] > 1) frequencyRems[0] = 1;

        // same here, as per modulos rules we just can have one
        if (k % 2 == 0 && frequencyRems[k / 2] > 1) frequencyRems[k / 2] = 1;

        // as third case we stay with the biggest by setting the
        // the counter of the smallest to 0
        for (int i = 1; i < k; i++) {
            if (frequencyRems[i] > frequencyRems[k - i]) {
                frequencyRems[k - i] = 0;
            } else if (k % 2 != 0) {
                frequencyRems[i] = 0;
            }
        }

        int result = 0;

        for (int i : frequencyRems) {
            result += i;
        }
        return result;
    }

    public static int nonDivisibleSubset(int k, List<Integer> s) {
        if (k < 1 || k > 100) {
            throw new IllegalArgumentException();
        }
        if (s.size() < 1 || s.size() > 100_000) {
            throw new IllegalArgumentException();
        }
        int res;
        int[] array = new int[k];

        for (int si : s) {
            if (si < 1 || si > 1_000_000_000) {
                throw new IllegalArgumentException();
            }
            array[si % k] += 1;
        }

        if (array[0] == 0) {
            res = 0;
        } else {
            res = 1;
        }
        for (int i = 1; i < k / 2 + 1; i++) {
            if (i != k - i) {
                res += Math.max(array[i], array[k - i]);
            } else {
                res += 1;
            }
        }
        return res;
    }

    public static int _1_nonDivisibleSubset(int k, List<Integer> s) {
        if (k < 1 || k > 100) {
            throw new IllegalArgumentException();
        }
        if (s.size() < 1 || s.size() > 100_000) {
            throw new IllegalArgumentException();
        }
        int res = 0;
        int[] array = new int[k];

        for (int si : s) {
            if (si < 1 || si > 1_000_000_000) {
                throw new IllegalArgumentException();
            }
            array[si % k]++;
        }
        if (array[0] > 0) {
            res++;
        }
        for (int i = 1; i < k; i++) {
            if (array[i] == 0) {
                continue;
            }
            if (i + i == k) {
                res++;
            } else {
                res += Math.max(array[i], array[k - i]);
                array[i] = 0;
                array[k - i] = 0;
            }
        }
        return res;
    }

    public static int _nonDivisibleSubset(int k, List<Integer> s) {
        if (k < 1 || k > 100) {
            throw new IllegalArgumentException();
        }
        if (s.size() < 1 || s.size() > 100_000) {
            throw new IllegalArgumentException();
        }
        Map<Integer, Set<Integer>> res = new HashMap<>();
        for (int i = 0; i < s.size(); i++) {
            int si = s.get(i);
            if (si < 1 || si > 1_000_000_000) {
                throw new IllegalArgumentException();
            }
            for (int j = i + 1; j < s.size(); j++) {
                int sj = s.get(j);
                int d = (sj + si) % k;
                if (d != 0) {
                    put(res, si, sj);
                    put(res, sj, si);
                }
            }
        }
        for (int i = 0; i < s.size(); i++) {
            int si = s.get(i);
            for (int j = i + 1; j < s.size(); j++) {
                int sj = s.get(j);
                int d = (sj + si) % k;
                if (d != 0) {
                    continue;
                }
                res.forEach((n, v) -> {
                    if (v.contains(si)) {
                        v.remove(sj);
                    }
                });
            }
        }
        return res.values().stream().mapToInt(Set::size).max().orElse(0);
    }

    public static int nonDivisibleSubsetBruteForce(int k, List<Integer> s) {
        if (k < 1 || k > 100) {
            throw new IllegalArgumentException();
        }
        if (s.size() < 1 || s.size() > 100_000) {
            throw new IllegalArgumentException();
        }
        Map<Integer, Set<Integer>> res = new HashMap<>();
        for (int i = 0; i < s.size(); i++) {
            int si = s.get(i);
            if (si < 1 || si > 1_000_000_000) {
                throw new IllegalArgumentException();
            }
            for (int j = 0; j < s.size(); j++) {
                if (j == i) {
                    continue;
                }
                int sj = s.get(j);
                if ((sj + si) % k != 0) {
                    put(res, si, sj);
                    put(res, sj, si);
                }
            }
        }
        for (int i = 0; i < s.size(); i++) {
            int si = s.get(i);
            for (int j = 0; j < s.size(); j++) {
                if (i == j) {
                    continue;
                }
                int sj = s.get(j);
                int d = (sj + si) % k;
                if (d != 0) {
                    continue;
                }
                res.forEach((n, v) -> {
                    if (v.contains(si)) {
                        v.remove(sj);
                    }
                });
            }
        }
        return res.values().stream().mapToInt(Set::size).max().orElse(0);
    }

    private static void put(Map<Integer, Set<Integer>> res, int k, int v) {
        res.computeIfAbsent(k, x -> {
            Set<Integer> t = new HashSet<>();
            t.add(k);
            return t;
        }).add(v);
    }

    @Test
    public void test_k3_19_10_12_24_25_22() {
        int x = nonDivisibleSubsetBruteForce(4, List.of(19, 10, 12, 24, 25, 22));
        Assertions.assertEquals(3, x);
    }

    @Test
    public void test_k4_1_7_2_4() {
        int x = nonDivisibleSubsetBruteForce(4, List.of(1, 7, 2, 4));
        Assertions.assertEquals(3, x);
    }

    @Test
    public void test_k5_1_7_2_4_42_103_9_111() {
        List<Integer> array = List.of(1, 7, 2, 4, 42, 103, 9, 111);
        int k = 5;
        int actual = nonDivisibleSubset(k, array);
        Assertions.assertEquals(5, actual);
    }

    @Test
    public void test_k1_2_39() {
        List<Integer> array = List.of(2, 39);
        int actual = nonDivisibleSubset(1, array);
        Assertions.assertEquals(0, actual);
    }

    @Test
    public void test_k42_10_100_1000() {
        List<Integer> array = List.of(10, 100, 1000);
        int k = 42;
        int actual = nonDivisibleSubset(k, array);
        Assertions.assertEquals(3, actual);
    }

    @Test
    public void test_k42_100000_42_420_27_1_9() {
        List<Integer> array = List.of(100_000, 42, 420, 27, 1, 9);
        int k = 42;
        int actual = nonDivisibleSubset(k, array);
        Assertions.assertEquals(5, actual);
    }

    @Test
    public void test_k1_1_42() {
        List<Integer> array = List.of(1, 42);
        int actual = nonDivisibleSubset(1, array);
        Assertions.assertEquals(0, actual);
    }

    @Test
    public void test_k1_42() {
        List<Integer> array = List.of(42);
        int actual = nonDivisibleSubset(1, array);
        Assertions.assertEquals(0, actual);
    }

    @Test
    public void test_k2_2_18_19_35_4() {
        List<Integer> array = List.of(18, 19, 35, 4);
        int actual = nonDivisibleSubset(2, array);
        System.out.println(nonDivisibleSubsetBruteForce(2, array));
        Assertions.assertEquals(2, actual);
    }

    @Test
    public void testRandom() {
        Random r = new Random();
        int n = 10;
        int maxSize = 21;
        int maxItem = 42;
        int k = 42;
        for (int i = 0; i < n; i++) {
            Set<Integer> numbers = new HashSet<>();
            int size = r.nextInt(maxSize) + 1;
            for (int j = 1; j <= size; j++) {
                numbers.add(r.nextInt(maxItem) + 1);
            }
            if (numbers.isEmpty()) {
                return;
            }
            List<Integer> array = new ArrayList<>(numbers);
            System.out.println("size = " + array.size());
            for (int q = 1; q <= k; q++) {
                int expected = nonDivisibleSubsetBruteForce(q, array);
                System.out.println("k = " + q + ", expected = " + expected);
                int actual = nonDivisibleSubset(q, array);
                Assertions.assertEquals(expected, actual, "array = " + array + ", k = " + q);
            }
        }
    }
}
