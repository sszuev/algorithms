package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * You are given a sorted unique integer array `nums`.
 * A range `[a,b]` is the set of all integers from `a` to `b` (inclusive).
 * Return the smallest sorted list of ranges that cover all the numbers in the array exactly.
 * That is, each element of `nums` is covered by exactly one of the ranges,
 * and there is no integer `x` such that `x` is in one of the ranges but not in nums.
 * Each range `[a,b]` in the list should be output as:
 * - `"a->b" if a != b`
 * - `"a" if a == b`
 * <p>
 * Constraints:
 * - `0 <= nums.length <= 20`
 * - `-2^31 <= nums[i] <= 2^31 - 1`
 * - All the values of `nums` are unique
 * - `nums` is sorted in ascending order
 *
 * @see <a href="https://leetcode.com/problems/summary-ranges">228. Summary Ranges</a>
 */
public class L100_SummaryRanges {
    public List<String> summaryRanges(int[] nums) {
        return calcSummaryRanges(nums);
    }

    public List<String> calcSummaryRanges(int[] nums) {
        if (nums.length == 0) return List.of();
        List<String> res = new ArrayList<>();
        int min = 0;
        int max = 0;
        for (int i = 1; i < nums.length; i++) {
            int num = nums[i];
            if (nums[max] + 1 == num) {
                max++;
            } else {
                int a = nums[min];
                int b = nums[max];
                if (min == max) {
                    res.add(String.valueOf(a));
                } else {
                    res.add(a + "->" + b);
                }
                min = i;
                max = min;
            }
        }
        int a = nums[min];
        int b = nums[max];
        if (min == max) {
            res.add(String.valueOf(a));
        } else {
            res.add(a + "->" + b);
        }
        return res;
    }

    @Test
    void test1() {
        List<String> res = summaryRanges(new int[]{0, 1, 2, 4, 5, 7});
        Assertions.assertEquals(
                List.of("0->2", "4->5", "7"),
                res
        );
    }

    @Test
    void test2() {
        List<String> res = summaryRanges(new int[]{0, 2, 3, 4, 6, 8, 9});
        Assertions.assertEquals(
                List.of("0", "2->4", "6", "8->9"),
                res
        );
    }
}
