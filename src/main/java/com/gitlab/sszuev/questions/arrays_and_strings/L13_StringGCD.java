package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Greatest Common Divisor of Strings
 * <p>
 * For two strings {@code s} and {@code t},
 * we say "t divides s" if and only if {@code s = t + ... + t} (i.e., {@code t} is concatenated with itself one or more times).
 * <p>
 * Given two strings str1 and str2, return the largest string x such that x divides both str1 and str2.
 *
 * @see <a href="https://leetcode.com/problems/greatest-common-divisor-of-strings">1071. Greatest Common Divisor of Strings</a>
 */
@SuppressWarnings("unused")
public class L13_StringGCD {

    public String gcdOfStrings(String str1, String str2) {
        return findStringsGCD(str1, str2);
    }

    private static String findStringsGCD(String str1, String str2) {
        if (str1.isEmpty() || str2.isEmpty()) {
            return "";
        }
        char[] left = str1.toCharArray();
        char[] right = str2.toCharArray();
        List<Character> prefix = new ArrayList<>();
        for (int i = 0; i < Math.min(left.length, right.length); i++) {
            if (left[i] != right[i]) {
                break;
            }
            prefix.add(left[i]);
        }
        if (prefix.isEmpty()) {
            return "";
        }
        String res = "";
        for (int i = 1; i <= prefix.size(); i++) {
            List<Character> candidate = prefix.subList(0, i);
            boolean isDivisorOfLeft = isDivisor(left, candidate);
            boolean isDivisorOfRight = isDivisor(right, candidate);
            if (isDivisorOfLeft && isDivisorOfRight) {
                res = toString(candidate);
            }
        }
        return res;
    }

    private static String toString(List<Character> chars) {
        StringBuilder sb = new StringBuilder();
        chars.forEach(sb::append);
        return sb.toString();
    }

    private static boolean isDivisor(char[] string, List<Character> candidate) {
        if (candidate.size() > string.length) {
            return false;
        }
        if (string.length % candidate.size() != 0) {
            return false;
        }
        for (int i = 0; i <= string.length - candidate.size(); i += candidate.size()) {
            for (int j = 0; j < candidate.size(); j++) {
                char cs = string[i + j];
                char cc = candidate.get(j);
                if (cc != cs) {
                    return false;
                }
            }
        }
        return true;
    }

    @Test
    public void testABCDEF_ABC(){
        Assertions.assertEquals("", findStringsGCD("ABCDEF", "ABC"));
    }

    @Test
    public void testABCABC_ABC() {
        Assertions.assertEquals("ABC", findStringsGCD("ABCABC", "ABC"));
    }

    @Test
    public void testABABAB_ABAB() {
        Assertions.assertEquals("AB", findStringsGCD("ABABAB", "ABAB"));
    }

    @Test
    public void testXXX_ABAB() {
        Assertions.assertEquals("", findStringsGCD("XXX", "ABAB"));
    }
}
