package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Container With Most Water
 * <p>
 * You are given an integer array height of length n.
 * There are n vertical lines drawn such that the two endpoints of the ith line are (i, 0) and (i, height[i]).
 * Find two lines that together with the x-axis form a container, such that the container contains the most water.
 * Return the maximum amount of water a container can store.
 * Notice that you may not slant the container.
 * <p>
 * Constraints:
 * n == height.length;
 * 2 <= n <= 105;
 * 0 <= height[i] <= 104;
 *
 * @see <a href="https://leetcode.com/problems/container-with-most-water">11. Container With Most Water</a>
 */
@SuppressWarnings("unused")
public class L14_ContainerWithMostWater {

    public int maxArea(int[] height) {
        return calcMaxArea(height);
    }

    private static int calcMaxArea(int[] height) {
        int res = 0;
        int i = 0;
        int j = height.length - 1;
        while (i < j) {
            res = Math.max(res, calcArea(height, i, j));
            int left = height[i];
            int right = height[j];
            if (left < right) {
                i++;
            } else {
                j--;
            }
        }
        return res;
    }

    private static int calcArea(int[] array, int i, int j) {
        int x = array[i];
        int y = array[j];
        return (j - i) * Math.min(x, y);
    }

    @Test
    public void test1_8_6_2_5_4_8_3_7() {
        int actual = calcMaxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7});
        Assertions.assertEquals(49, actual);
    }

    @Test
    public void test1_8_6_2_5_4_8_25_7() {
        int actual = calcMaxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 25, 7});
        Assertions.assertEquals(49, actual);
    }

    @Test
    public void test1_1() {
        int actual = calcMaxArea(new int[]{1, 1});
        Assertions.assertEquals(1, actual);
    }
}
