package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You are given an integer array nums consisting of n elements, and an integer k.
 * Find a contiguous subarray whose length is equal to k that has the maximum average value and return this value.
 * Any answer with a calculation error less than 10^-5 will be accepted.
 * <p>
 * Constraints:
 * 1) n == nums.length;
 * 2) 1 <= k <= n <= 10^5
 * 3) -10^4 <= nums[i] <= 10^4
 *
 * @see <a href="https://leetcode.com/problems/maximum-average-subarray-i">643. Maximum Average Subarray I</a>
 */
public class L20_MaximumAverageSubarray {

    public double findMaxAverage(int[] nums, int k) {
        return maxAverage(nums, k);
    }

    public static double maxAverage(int[] array, int k) {
        if (array.length < k) {
            throw new IllegalArgumentException();
        }

        int sum = 0;
        for (int i = 0; i < k; i++) {
            sum += array[i];
        }
        int max = sum;
        for (int i = k; i < array.length; i++) {
            sum = sum - array[i - k] + array[i];
            max = Math.max(max, sum);
        }
        return ((double) max) / k;
    }


    @Test
    public void test1_12_m5_m6_50_3() {
        Assertions.assertEquals(12.75, findMaxAverage(new int[]{1, 12, -5, -6, 50, 3}, 4), 0.00001);
    }

    @Test
    public void test5() {
        Assertions.assertEquals(5.0, findMaxAverage(new int[]{5}, 1), 0.00001);
    }
}
