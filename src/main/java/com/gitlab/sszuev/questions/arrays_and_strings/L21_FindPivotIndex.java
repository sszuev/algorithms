package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given an array of integers nums, calculate the pivot index of this array.
 * The pivot index is the index where
 * the sum of all the numbers strictly to the left of the index is equal to the sum of all the numbers
 * strictly to the index's right.
 * If the index is on the left edge of the array,
 * then the left sum is 0 because there are no elements to the left.
 * This also applies to the right edge of the array.
 * Return the leftmost pivot index. If no such index exists, return -1.
 * <p>
 * Constraints:
 * 1) 1 <= nums.length <= 10^4
 * 2) -1000 <= nums[i] <= 1000
 *
 * @see <a href="https://leetcode.com/problems/find-pivot-index">724. Find Pivot Index</a>
 */
public class L21_FindPivotIndex {

    public int pivotIndex(int[] nums) {
        return findPivotIndex(nums);
    }

    private static int findPivotIndex(int[] nums) {
        int[] prefixSums = new int[nums.length + 1];
        for (int i = 1; i < prefixSums.length; i++) {
            prefixSums[i] = prefixSums[i - 1] + nums[i - 1];
        }
        for (int i = 0; i < nums.length; i++) {
            int sumLeft = prefixSums[i];
            int sumRight = prefixSums[nums.length] - prefixSums[i + 1];
            if (sumRight == sumLeft) {
                return i;
            }
        }
        return -1;
    }

    @Test
    void test1_7_3_6_5_6() {
        Assertions.assertEquals(3, pivotIndex(new int[]{1, 7, 3, 6, 5, 6}));
    }

    @Test
    void test1_2_3() {
        Assertions.assertEquals(-1, pivotIndex(new int[]{1, 2, 3}));
    }

    @Test
    void test2_1_m1() {
        Assertions.assertEquals(0, pivotIndex(new int[]{2, 1, -1}));
    }
}
