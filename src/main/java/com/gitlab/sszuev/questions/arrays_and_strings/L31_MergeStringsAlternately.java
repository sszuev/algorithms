package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You are given two strings {@code word1} and {@code word2}.
 * Merge the strings by adding letters in alternating order, starting with {@code word1}.
 * If a string is longer than the other, append the additional letters onto the end of the merged string.
 * Return the merged string.
 * Constraints:
 * {@code 1 <= word1.length, word2.length <= 100}, {@code word1} and {@code word2} consist of lowercase English letters.
 *
 * @see <a href="https://leetcode.com/problems/merge-strings-alternately">1768. Merge Strings Alternately</a>
 */
public class L31_MergeStringsAlternately {
    public String mergeAlternately(String word1, String word2) {
        return doMergeAlternately(word1, word2);
    }

    private static String doMergeAlternately(String left, String right) {
        StringBuilder res = new StringBuilder();
        char[] leftChars = left.toCharArray();
        char[] rightChars = right.toCharArray();
        for (int i = 0; i < Math.max(leftChars.length, rightChars.length); i++) {
            if (i < leftChars.length && i < rightChars.length) {
                res.append(leftChars[i]);
                res.append(rightChars[i]);
            } else if (i < leftChars.length) {
                res.append(leftChars[i]);
            } else {
                res.append(rightChars[i]);
            }
        }
        return res.toString();
    }

    @Test
    public void test1() {
        String actual = mergeAlternately("abc", "pqr");
        Assertions.assertEquals("apbqcr", actual);
    }

    @Test
    public void test2() {
        String actual = mergeAlternately("ab", "pqrs");
        Assertions.assertEquals("apbqrs", actual);
    }

    @Test
    public void test3() {
        String actual = mergeAlternately("abcd", "pq");
        Assertions.assertEquals("apbqcd", actual);
    }
}
