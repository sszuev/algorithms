package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * There are n kids with candies.
 * You are given an integer array of candies,
 * where each {@code candies[i]} represents the number of candies the {@code i}th kid has,
 * and an integer {@code extraCandies}, denoting the number of extra candies that you have.
 * Return a boolean array result of length n, where {@code result[i]} is {@code true} if,
 * after giving the {@code i}th kid all the {@code extraCandies},
 * they will have the greatest number of candies among all the kids, or {@code false} otherwise.
 * Note that multiple kids can have the greatest number of candies.
 * <p>
 * Constraints: {@code n == candies.length}, {@code 2 <= n <= 100}, {@code 1 <= candies[i] <= 100}, {@code 1 <= extraCandies <= 50}
 *
 * @see <a href="https://leetcode.com/problems/kids-with-the-greatest-number-of-candies">1431. Kids With the Greatest Number of Candies</a>
 */
public class L34_KidsWithTheGreatestNumberOfCandies {
    public List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
        return calcKidsWithCandies(candies, extraCandies);
    }

    private static List<Boolean> calcKidsWithCandies(int[] candies, int extraCandies) {
        int max = 0;
        for (int candy : candies) {
            if (candy >= max) {
                max = candy;
            }
        }
        List<Boolean> res = new ArrayList<>(candies.length);
        for (int candy : candies) {
            res.add(candy + extraCandies >= max);
        }
        return res;
    }

    @Test
    void test1() {
        List<Boolean> actual = kidsWithCandies(new int[]{2, 3, 5, 1, 3}, 3);
        Assertions.assertEquals(List.of(true, true, true, false, true), actual);
    }

    @Test
    void test2() {
        List<Boolean> actual = kidsWithCandies(new int[]{4, 2, 1, 1, 2}, 1);
        Assertions.assertEquals(List.of(true, false, false, false, false), actual);
    }

    @Test
    void test3() {
        List<Boolean> actual = kidsWithCandies(new int[]{12, 1, 12}, 10);
        Assertions.assertEquals(List.of(true, false, true), actual);
    }
}
