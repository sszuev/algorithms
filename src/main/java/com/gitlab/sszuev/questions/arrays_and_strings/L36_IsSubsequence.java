package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given two strings {@code s} and {@code t}, return `true` if {@code s} is a subsequence of {@code t}, or `false` otherwise.
 * <p>
 * A subsequence of a string is a new string
 * that is formed from the original string by deleting some (can be none) of the characters
 * without disturbing the relative positions of the remaining characters.
 * (i.e., "ace" is a subsequence of "abcde" while "aec" is not).
 * <p>
 * Constraints: `0 <= s.length <= 100`, `0 <= t.length <= 10^4`, `s` and `t` consist only of lowercase English letters.
 *
 * @see <a href="https://leetcode.com/problems/is-subsequence">392. Is Subsequence</a>
 */
public class L36_IsSubsequence {

    public boolean isSubsequence(String s, String t) {
        return testIsSubsequence(s.toCharArray(), t.toCharArray());
    }

    private static boolean testIsSubsequence(char[] s, char[] t) {
        int i = 0;
        int j = 0;
        while (i < s.length && j < t.length) {
            if (s[i] == t[j]) {
                i++;
                j++;
                continue;
            }
            j++;
        }
        return i == s.length;
    }

    @Test
    void test1() {
        Assertions.assertTrue(isSubsequence("abc", "ahbgdc"));
    }

    @Test
    void test2() {
        Assertions.assertFalse(isSubsequence("axc", "ahbgdc"));
    }
}
