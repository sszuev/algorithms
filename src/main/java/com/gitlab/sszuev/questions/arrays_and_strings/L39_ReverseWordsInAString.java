package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Given an input string s, reverse the order of the words.
 * A word is defined as a sequence of non-space characters.
 * The words in s will be separated by at least one space.
 * Return a string of the words in reverse order concatenated by a single space.
 * <p>
 * Note that s may contain leading or trailing spaces or multiple spaces between two words.
 * The returned string should only have a single space separating the words.
 * Do not include any extra spaces.
 * <p>
 * Constraints:
 * {@code 1 <= s.length <= 10^4},
 * {@code s} contains English letters (upper-case and lower-case), digits, and spaces {@code ' '}.
 * There is at least one word in {@code s}.
 *
 * @see <a href="https://leetcode.com/problems/reverse-words-in-a-string">151. Reverse Words in a String</a>
 */
public class L39_ReverseWordsInAString {

    @Test
    void test1() {
        Assertions.assertEquals("blue is sky the", reverseWords("the sky is blue"));
    }

    @Test
    void test2() {
        Assertions.assertEquals("world hello", reverseWords("  hello world  "));
    }

    @Test
    void test3() {
        Assertions.assertEquals("example good a", reverseWords("a good   example"));
    }

    public String reverseWords(String s) {
        String[] parts = s.split(" ");
        return IntStream.range(0, parts.length)
                .mapToObj(it -> parts[parts.length - it - 1])
                .filter(it -> !it.isEmpty())
                .collect(Collectors.joining(" "));
    }
}
