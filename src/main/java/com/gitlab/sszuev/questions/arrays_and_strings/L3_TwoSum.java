package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * You can return the answer in any order.
 * <p>
 * Constraints:
 * <p>
 * {@code 2 <= nums.length <= 10^4}, {@code -10^9 <= nums[i] <= 10^9}, {@code -10^9 <= target <= 10^9}.
 * <p>
 * Only one valid answer exists.
 *
 * @see <a href="https://leetcode.com/problems/two-sum">1. Two Sum</a>
 */
@SuppressWarnings("unused")
public class L3_TwoSum {

    public int[] twoSum(int[] nums, int target) {
        if (nums.length < 2 || nums.length > 10_000) {
            throw new IllegalArgumentException();
        }
        checkN(target);
        Map<Integer, Integer> res = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int n = nums[i];
            checkN(n);
            Integer j = res.get(n);
            if (j != null) {
                return new int[]{i, j};
            }
            int x = target - n;
            res.put(x, i);
        }
        throw new RuntimeException("no solution");
    }

    public int[] twoSum_BF(int[] nums, int target) {
        if (nums.length < 2 || nums.length > 10_000) {
            throw new IllegalArgumentException();
        }
        checkN(target);
        for (int i = 0; i < nums.length; i++) {
            int n = nums[i];
            checkN(n);
            for (int j = i + 1; j < nums.length; j++) {
                int other = nums[j];
                checkN(n);
                int res = other + n;
                if (res == target) {
                    return new int[]{i, j};
                }
            }
        }
        throw new RuntimeException("no solution");
    }

    private static void checkN(int n) {
        if (n > 1_000_000_000 || n < -1_000_000_000) {
            throw new IllegalArgumentException();
        }
    }

    @Test
    public void test2x7x11x15t9() {
        assertArrayEquals(
                new int[]{0, 1},
                twoSum(new int[]{2, 7, 11, 15}, 9)
        );
    }

    @Test
    public void test3x2x4t6() {
        assertArrayEquals(
                new int[]{1, 2},
                twoSum(new int[]{3, 2, 4}, 6)
        );
    }

    @Test
    public void test3x3t6() {
        assertArrayEquals(
                new int[]{0, 1},
                twoSum(new int[]{3, 3}, 6)
        );
    }

    private static void assertArrayEquals(int[] expected, int[] actual) {
        Assertions.assertTrue(
                Arrays.equals(expected, actual) || Arrays.equals(new int[]{expected[1], expected[0]}, actual),
                "expected: " + Arrays.toString(expected) + ", actual: " + Arrays.toString(actual));
    }
}
