package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given an integer array nums, return true if there exists a triple of indices (i, j, k)
 * such that i < j < k and nums[i] < nums[j] < nums[k].
 * If no such indices exist, return false.
 * <p>
 * Constraints: `1 <= nums.length <= 5 * 10^5`, `-2^31 <= nums[i] <= 2^31 - 1`
 *
 * @see <a href="https://leetcode.com/problems/increasing-triplet-subsequence">334. Increasing Triplet Subsequence</a>
 */
public class L43_IncreasingTripletSubsequence {

    public boolean increasingTriplet(int[] nums) {
        return containsIncreasingTriplet(nums);
    }

    private static boolean containsIncreasingTriplet(int[] nums) {
        int a = Integer.MAX_VALUE;
        int b = Integer.MAX_VALUE;
        for (int num : nums) {
            if (a >= num) {
                a = num;
            } else if (b >= num) {
                b = num;
            } else {
                return true;
            }
        }
        return false;
    }

    @Test
    void test1() {
        Assertions.assertTrue(increasingTriplet(new int[]{1, 2, 3, 4, 5}));
    }

    @Test
    void test2() {
        Assertions.assertFalse(increasingTriplet(new int[]{5, 4, 3, 2, 1}));
    }

    @Test
    void test3() {
        Assertions.assertTrue(increasingTriplet(new int[]{2, 1, 5, 0, 4, 6}));
    }

    @Test
    void test4() {
        Assertions.assertFalse(increasingTriplet(new int[]{
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}));
    }
}
