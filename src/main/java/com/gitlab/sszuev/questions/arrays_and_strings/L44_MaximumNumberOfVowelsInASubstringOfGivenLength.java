package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given a string s and an integer k, return the maximum number of vowel letters in any substring of s with length k.
 * Vowel letters in English are 'a', 'e', 'i', 'o', and 'u'.
 * <p>
 * Constraints: `1 <= s.length <= 10^5`; s consists of lowercase English letters; `1 <= k <= s.length`
 *
 * @see <a href="https://leetcode.com/problems/maximum-number-of-vowels-in-a-substring-of-given-length">1456. Maximum Number of Vowels in a Substring of Given Length</a>
 */
public class L44_MaximumNumberOfVowelsInASubstringOfGivenLength {

    public int maxVowels(String s, int k) {
        return calcMaxVowels(s.toCharArray(), k);
    }

    private static final byte[] VOWEL = new byte[]{1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0};

    private static int calcMaxVowels(char[] s, int k) {
        if (s.length == k) {
            return numOfVowels(s, 0, s.length);
        }
        int max = 0;
        int num = 0;
        for (int i = 0; i < s.length - k + 1; i++) {
            if (i == 0) {
                num = numOfVowels(s, i, i + k);
            } else {
                if (VOWEL[s[i - 1] - 97] == 1) {
                    num--;
                }
                if (i + k <= s.length && VOWEL[s[i + k - 1] - 97] == 1) {
                    num++;
                }
            }
            max = Math.max(max, num);
        }
        return max;
    }

    private static int numOfVowels(char[] str, int startInclusive, int endExclusive) {
        int count = 0;
        for (int i = startInclusive; i < endExclusive; i++) {
            if (VOWEL[str[i] - 97] == 1) {
                count++;
            }
        }
        return count;
    }

    @Test
    void test1() {
        int actual = maxVowels("abciiidef", 3);
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test2() {
        int actual = maxVowels("aeiou", 2);
        Assertions.assertEquals(2, actual);
    }

    @Test
    void test3() {
        int actual = maxVowels("leetcode", 3);
        Assertions.assertEquals(2, actual);
    }

    @Test
    void test4() {
        int actual = maxVowels("weallloveyou", 7);
        Assertions.assertEquals(4, actual);
    }

    @Test
    void test5() {
        int actual = maxVowels("novowels", 1);
        Assertions.assertEquals(1, actual);
    }
}
