package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given an encoded string, return its decoded string.
 * The encoding rule is: `k[encoded_string]`,
 * where the `encoded_string` inside the square brackets is being repeated exactly `k` times.
 * Note that `k` is guaranteed to be a positive integer.
 * You may assume that the input string is always valid;
 * there are no extra white spaces, square brackets are well-formed, etc.
 * Furthermore, you may assume that the original data
 * does not contain any digits and that digits are only for those repeat numbers, `k`.
 * For example, there will not be input like `3a` or `2[4]`.
 * The test cases are generated so that the length of the output will never exceed 10^5.
 * <p>
 * Constraints:
 * `1 <= s.length <= 30`;
 * `s` consists of lowercase English letters, digits, and square brackets '[]';
 * `s` is guaranteed to be a valid input;
 * All the integers in `s` are in the range `[1, 300]`.
 *
 * @see <a href="https://leetcode.com/problems/decode-string/">394. Decode String</a>
 */
public class L47_DecodeString {

    public String decodeString(String s) {
        return decodeString(s.toCharArray());
    }

    private static String decodeString(char[] s) {
        StringBuilder res = new StringBuilder();
        decodeString(s, res, 0, s.length);
        return res.toString();
    }

    private static int decodeString(char[] s, StringBuilder sb, int start, int end) {
        StringBuilder d = new StringBuilder(3);
        int i = start;
        for (; i < end; i++) {
            char c = s[i];
            if (isDigit(c)) {
                d.append(c);
            } else if ('[' == c) {
                StringBuilder tmp = new StringBuilder();
                i = decodeString(s, tmp, i + 1, end);
                int num = Integer.parseInt(d.toString());
                sb.append(String.valueOf(tmp).repeat(Math.max(0, num)));
                d = new StringBuilder(3);
            } else if (']' == c) {
                return i;
            } else {
                sb.append(c);
            }
        }
        return i;
    }

    private static boolean isDigit(char c) {
        return c > 47 && c < 58;
    }

    @Test
    void test1() {
        String actual = decodeString("3[a]2[bc]");
        Assertions.assertEquals("aaabcbc", actual);
    }

    @Test
    void test2() {
        String actual = decodeString("3[a2[c]]");
        Assertions.assertEquals("accaccacc", actual);
    }

    @Test
    void test3() {
        String actual = decodeString("2[abc]3[cd]ef");
        Assertions.assertEquals("abcabccdcdcdef", actual);
    }

    @Test
    void test4() {
        String actual = decodeString("xxx");
        Assertions.assertEquals("xxx", actual);
    }

}
