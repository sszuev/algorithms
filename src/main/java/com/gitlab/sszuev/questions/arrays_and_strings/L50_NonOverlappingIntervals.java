package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Given an array of `intervals` intervals where `intervals[i] = [starti, endi]`,
 * return the minimum number of intervals you need to remove to make the rest of the intervals non-overlapping.
 * <p>
 * Constraints: `1 <= intervals.length <= 10^5`; `intervals[i].length == 2`; `-5 * 10^4 <= starti < endi <= 5 * 10^4`
 *
 * @see <a href="https://leetcode.com/problems/non-overlapping-intervals">435. Non-overlapping Intervals</a>
 */
public class L50_NonOverlappingIntervals {

    public int eraseOverlapIntervals(int[][] intervals) {
        return countErasingOverlapIntervals(intervals);
    }

    private static int countErasingOverlapIntervals(int[][] intervals) {
        int res = 0;
        Arrays.sort(intervals, Comparator.comparingInt(o -> o[0]));
        for (int i = 0, j = 1; i < intervals.length - 1 && j < intervals.length; ) {
            int[] left = intervals[i];
            int[] right = intervals[j];
            if (isOverlap(left, right)) {
                res++;
                if (right[1] > left[1]) {
                    j++;
                } else {
                    i = j;
                    j++;
                }
            } else {
                i = j;
                j++;
            }
        }
        return res;
    }

    private static boolean isOverlap(int[] left, int[] right) {
        return !(left[1] <= right[0] || right[1] <= left[0]);
    }

    @Test
    void test1() {
        int[][] intervals = new int[][]{{1, 2}, {2, 3}, {3, 4}, {1, 3}};
        int actual = eraseOverlapIntervals(intervals);
        Assertions.assertEquals(1, actual);
    }

    @Test
    void test2() {
        int[][] intervals = new int[][]{{1, 2}, {1, 2}, {1, 2}};
        int actual = eraseOverlapIntervals(intervals);
        Assertions.assertEquals(2, actual);
    }

    @Test
    void test3() {
        int[][] intervals = new int[][]{{1, 2}, {2, 3}};
        int actual = eraseOverlapIntervals(intervals);
        Assertions.assertEquals(0, actual);
    }

    @Test
    void test4() {
        int[][] intervals = new int[][]{{1, 2}, {1, 3}, {3, 5}, {4, 6}, {5, 7}, {7, 8}, {7, 8}};
        int actual = eraseOverlapIntervals(intervals);
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test5() {
        int[][] intervals = new int[][]{{1, 100}, {11, 22}, {1, 11}, {2, 12}};
        int actual = eraseOverlapIntervals(intervals);
        Assertions.assertEquals(2, actual);
    }
}
