package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;

/**
 * There are some spherical balloons taped onto a flat wall that represents the XY-plane.
 * The balloons are represented as a 2D integer array points
 * where `points[i] = [xstart, xend]` denotes a balloon whose horizontal diameter stretches between `xstart` and `xend`.
 * You do not know the exact y-coordinates of the balloons.
 * Arrows can be shot up directly vertically (in the positive y-direction) from different points along the x-axis.
 * A balloon with `xstart` and `xend` is burst by an arrow shot at `x`
 * if `xstart <= x <= xend`.
 * There is no limit to the number of arrows that can be shot.
 * A shot arrow keeps traveling up infinitely, bursting any balloons in its path.
 * Given the array points, return the minimum number of arrows that must be shot to burst all balloons.
 * <p>
 * Constraints:
 * 1) `1 <= points.length <= 10^5`
 * 2) `points[i].length == 2`
 * 3) `-2^31 <= xstart < xend <= 2^31 - 1`
 *
 * @see <a href="https://leetcode.com/problems/minimum-number-of-arrows-to-burst-balloons">452. Minimum Number of Arrows to Burst Balloons</a>
 */
public class L58_MinimumNumberOfArrowsToBurstBalloons {
    public int findMinArrowShots(int[][] points) {
        return calcMinArrowShots(points);
    }

    private static int calcMinArrowShots(int[][] points) {
        Arrays.sort(points, Comparator.comparingInt(a -> a[1]));
        int res = 1;
        int[] d = points[0];
        for (int[] current : points) {
            if (current[0] <= d[1]) {
                d[0] = Math.max(current[0], d[0]);
            } else {
                d = current;
                res++;
            }
        }
        return res;
    }

    @Test
    void test1() {
        int actual = findMinArrowShots(new int[][]{{10, 16}, {2, 8}, {1, 6}, {7, 12}});
        Assertions.assertEquals(2, actual);
    }

    @Test
    void test2() {
        int actual = findMinArrowShots(new int[][]{{1, 2}, {3, 4}, {5, 6}, {7, 8}});
        Assertions.assertEquals(4, actual);
    }

    @Test
    void test3() {
        int actual = findMinArrowShots(new int[][]{{1, 2}, {2, 3}, {3, 4}, {4, 5}});
        Assertions.assertEquals(2, actual);
    }
}
