package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * There is a biker going on a road trip.
 * The road trip consists of `n + 1` points at different altitudes.
 * The biker starts his trip on point `0` with altitude equal `0`.
 * You are given an integer array `gain` of length `n`
 * where `gain[i]` is the net gain in altitude between points `i` and `i + 1` for all `(0 <= i < n)`.
 * Return the highest altitude of a point.
 * <p>
 * Constraints:
 * 1) `n == gain.length`
 * 2) `1 <= n <= 100`
 * 3) `-100 <= gain[i] <= 100`
 *
 * @see <a href="https://leetcode.com/problems/find-the-highest-altitude">1732. Find the Highest Altitude</a>
 */
public class L59_FindHighestAltitude {

    public int largestAltitude(int[] gain) {
        return calcLargestAltitude(gain);
    }

    private static int calcLargestAltitude(int[] gain) {
        int altitude = 0;
        int max = altitude;
        for (int j : gain) {
            altitude += j;
            max = Math.max(max, altitude);
        }
        return max;
    }

    @Test
    void test1() {
        int actual = largestAltitude(new int[]{-5, 1, 5, 0, -7});
        Assertions.assertEquals(1, actual);
    }

    @Test
    void test2() {
        int actual = largestAltitude(new int[]{-4, -3, -2, -1, 4, 3, 2});
        Assertions.assertEquals(0, actual);
    }
}
