package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You have a long flowerbed in which some of the plots are planted, and some are not.
 * However, flowers cannot be planted in adjacent plots.
 * Given an integer array flowerbed containing 0's and 1's,
 * where 0 means empty and 1 means not empty, and an integer n,
 * return true if n new flowers can be planted in the flowerbed
 * without violating the no-adjacent-flowers rule and false otherwise.
 *
 * @see <a href="https://leetcode.com/problems/can-place-flowers">605. Can Place Flowers</a>
 */
public class L69_CanPlaceFlowers {
    public boolean canPlaceFlowers(int[] flowerbed, int n) {
        return calcCanPlaceFlowers(flowerbed, n);
    }

    @Test
    void test1() {
        Assertions.assertTrue(canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 1));
    }

    @Test
    void test2() {
        Assertions.assertFalse(canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 2));
    }

    @Test
    void test3() {
        Assertions.assertTrue(canPlaceFlowers(new int[]{0, 0, 1, 0, 1}, 1));
    }

    @Test
    void test4() {
        Assertions.assertTrue(canPlaceFlowers(new int[]{0,0,1,0,0}, 1));
    }

    @Test
    void test5() {
        Assertions.assertTrue(canPlaceFlowers(new int[]{0,0,0,0,1}, 2));
    }

    static boolean calcCanPlaceFlowers(int[] flowerbed, int n) {
        if (n == 0) {
            return true;
        }
        if (flowerbed.length == 0) {
            return false;
        }
        if (flowerbed.length == 1) {
            return n == 1 && flowerbed[0] == 0;
        }
        if (flowerbed.length == 2) {
            return n == 1 && flowerbed[0] == 0 && flowerbed[1] == 0;
        }
        int i = 0;
        while (i < flowerbed.length) {
            while (i < flowerbed.length && flowerbed[i] == 1) {
                i++;
            }
            if (i == flowerbed.length - 1) {
                break;
            } else if (i == flowerbed.length - 2) {
                if (flowerbed[i + 1] == 0) {
                    n--;
                }
                break;
            } else {
                if (flowerbed[i + 2] == 0 || i == 0) {
                    if (flowerbed[i + 1] == 0) {
                        n--;
                    }
                    if (n == 0) {
                        break;
                    }
                    if (i == 0) {
                        i++;
                    } else {
                        i += 2;
                    }
                    continue;
                }
            }
            i++;
        }
        return n == 0;
    }
}
