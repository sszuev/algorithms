package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given a binary array nums, you should delete one element from it.
 * Return the size of the longest non-empty subarray containing only 1's in the resulting array.
 * Return 0 if there is no such subarray.
 *
 * @see <a href="https://leetcode.com/problems/longest-subarray-of-1s-after-deleting-one-element">1493. Longest Subarray of 1's After Deleting One Element</a>
 */
public class L70_LongestSubarrayOfOnesAfterDeletingOneElement {
    public int longestSubarray(int[] nums) {
        return calcLongestSubarray(nums);
    }

    private static int calcLongestSubarray(int[] nums) {
        int res = 0;
        int left = 0;
        int right = 0;
        int zero = 0;
        while (right < nums.length) {
            if (nums[right] == 0) {
                zero++;
            }
            while (zero > 1) {
                if (nums[left] == 0) {
                    zero--;
                }
                left++;
            }
            res = Math.max(res, right - left);
            right++;
        }
        if (res == nums.length) {
            res--;
        }
        return res;
    }

    @Test
    void test1() {
        Assertions.assertEquals(3, longestSubarray(new int[]{1, 1, 0, 1}));
    }

    @Test
    void test2() {
        Assertions.assertEquals(5, longestSubarray(new int[]{0, 1, 1, 1, 0, 1, 1, 0, 1}));
    }
}
