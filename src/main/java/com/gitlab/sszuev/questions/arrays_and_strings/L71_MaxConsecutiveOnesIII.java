package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given a binary array nums and an integer k,
 * return the maximum number of consecutive 1's in the array if you can flip at most k 0's.
 *
 * @see <a href="https://leetcode.com/problems/max-consecutive-ones-iii">1004. Max Consecutive Ones III</a>
 */
public class L71_MaxConsecutiveOnesIII {
    public int longestOnes(int[] nums, int k) {
        return calcLongestOnes(nums, k);
    }

    public static int calcLongestOnes(int[] nums, int k) {
        int res = 0;
        int left = 0;
        int right = 0;
        int zeros = 0;
        while (right < nums.length) {
            if (nums[right] == 0) {
                zeros++;
            }
            while (zeros > k) {
                if (nums[left] == 0) {
                    zeros--;
                }
                left++;
            }
            right++;
            res = Math.max(res, right - left);
        }
        return res;
    }

    @Test
    void test1() {
        Assertions.assertEquals(6, longestOnes(new int[]{1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0}, 2));
    }

    @Test
    void test2() {
        Assertions.assertEquals(10, longestOnes(new int[]{0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1}, 3));
    }
}
