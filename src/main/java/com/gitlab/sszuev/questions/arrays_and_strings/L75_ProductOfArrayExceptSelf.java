package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given an integer array nums, return an array `answer` such that `answer[i]` is equal to the product
 * of all the elements of `nums` except `nums[i]`.
 * The product of any prefix or suffix of `nums` is guaranteed to fit in a 32-bit integer.
 * You must write an algorithm that runs in `O(n)` time and **without using the division operation**.
 * <p>
 * Constraints:
 * - `2 <= nums.length <= 10^5`
 * - `-30 <= nums[i] <= 30`
 * - The product of any prefix or suffix of `nums` is guaranteed to fit in a 32-bit integer.
 *
 * @see <a href="https://leetcode.com/problems/product-of-array-except-self">238. Product of Array Except Self</a>
 */
public class L75_ProductOfArrayExceptSelf {

    public int[] productExceptSelf(int[] nums) {
        return calcProductExceptSelf(nums);
    }

    public static int[] calcProductExceptSelf(int[] array) {
        int[] prefixes = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            int p;
            if (i == 0) {
                p = 1;
            } else if (i == 1) {
                p = array[0];
            } else {
                p = array[i - 1] * prefixes[i - 1];
            }
            prefixes[i] = p;
        }

        int[] suffixes = new int[array.length];
        for (int i = array.length - 1; i >= 0; i--) {
            int p;
            if (i == array.length - 1) {
                p = 1;
            } else if (i == array.length - 2) {
                p = array[array.length - 1];
            } else {
                p = array[i + 1] * suffixes[i + 1];
            }
            suffixes[i] = p;
        }
        for (int i = 0; i < array.length; i++) {
            array[i] = prefixes[i] * suffixes[i];
        }
        return array;
    }

    @Test
    void test1() {
        int[] actual = productExceptSelf(new int[]{1, 2, 3, 4});
        Assertions.assertArrayEquals(new int[]{24, 12, 8, 6}, actual);
    }

    @Test
    void test2() {
        int[] actual = productExceptSelf(new int[]{-1, 1, 0, -3, 3});
        Assertions.assertArrayEquals(new int[]{0, 0, 9, 0, 0}, actual);
    }
}
