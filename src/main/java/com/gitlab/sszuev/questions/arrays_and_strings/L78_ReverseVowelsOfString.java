package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Given a string `s`, reverse only all the vowels in the string and return it.
 * The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both lower and upper cases, more than once.
 * <p>
 * Constraints:
 * - `1 <= s.length <= 3 * 10^5`
 * - `s` consist of printable ASCII characters.
 *
 * @see <a href="https://leetcode.com/problems/reverse-vowels-of-a-string">345. Reverse Vowels of a String</a>
 */
public class L78_ReverseVowelsOfString {

    public String reverseVowels(String s) {
        return calcReverseVowels(s);
    }

    private static final Set<Character> VOWELS = Set.of('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');

    private static String calcReverseVowels(String s) {
        List<Character> vowels = new ArrayList<>();
        for (char c: s.toCharArray()) {
            if (VOWELS.contains(c)) {
                vowels.add(c);
            }
        }
        StringBuilder sb = new StringBuilder();
        for (char c: s.toCharArray()) {
            if (VOWELS.contains(c)) {
                sb.append(vowels.remove(vowels.size() - 1));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    @Test
    void test1() {
        Assertions.assertEquals("AceCreIm", reverseVowels("IceCreAm"));
    }

    @Test
    void test2() {
        Assertions.assertEquals("leotcede", reverseVowels("leetcode"));
    }
}
