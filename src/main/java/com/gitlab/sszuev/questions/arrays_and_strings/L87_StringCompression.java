package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * Given an array of characters chars, compress it using the following algorithm:
 * Begin with an empty string `s`. For each group of consecutive repeating characters in chars:
 * If the group's length is 1, append the character to s.
 * Otherwise, append the character followed by the group's length.
 * The compressed string s should not be returned separately,
 * but instead, be stored in the input character array chars.
 * Note that group lengths that are 10 or longer will be split into multiple characters in chars.
 * After you are done modifying the input array, return the new length of the array.
 * You must write an algorithm that uses only constant extra space.
 * <p>
 * Constraints:
 * - `1 <= chars.length <= 2000`
 * - `chars[i]` is a lowercase English letter, uppercase English letter, digit, or symbol.
 *
 * @see <a href="https://leetcode.com/problems/string-compression">443. String Compression</a>
 */
public class L87_StringCompression {

    public int compress(char[] chars) {
        return calcCompress(chars);
    }

    private static int calcCompress(char[] chars) {
        if (chars.length == 0) return 0;
        if (chars.length == 1) return 1;

        int index = 0;
        int count = 1;
        char c = chars[0];
        for (int i = 0; i < chars.length - 1; i++) {
            if (i != chars.length - 1 && chars[i] == chars[i + 1]) {
                c = chars[i];
                count++;
            } else {
                // flush
                chars[index++] = c;
                if (count != 1) {
                    for (char d : toChars(count)) {
                        chars[index++] = d;
                    }
                }
                c = chars[i + 1];
                count = 1;
            }
        }
        chars[index++] = c;
        if (count != 1) {
            for (char d : toChars(count)) {
                chars[index++] = d;
            }
        }
        return index;
    }

    private static char[] toChars(int i) {
        return String.valueOf(i).toCharArray();
    }

    @Test
    void test1() {
        // Input: chars = ["a","a","b","b","c","c","c"]
        // Output: Return 6, and the first 6 characters of the input array should be: ["a","2","b","2","c","3"]
        // Explanation: The groups are "aa", "bb", and "ccc". This compresses to "a2b2c3".
        char[] input = new char[] {'a', 'a', 'b', 'b', 'c', 'c', 'c'};
        char[] expected = new char[] {'a', '2', 'b', '2', 'c', '3'};
        int res = compress(input);
        Assertions.assertEquals(6, res);
        Assertions.assertArrayEquals(expected, Arrays.copyOf(input, res));
    }

    @Test
    void test2() {
        // Input: chars = ["a"]
        // Output: Return 1, and the first character of the input array should be: ["a"]
        // Explanation: The only group is "a", which remains uncompressed since it's a single character.
        char[] input = new char[] {'a'};
        char[] expected = new char[] {'a'};
        int res = compress(input);
        Assertions.assertEquals(1, res);
        Assertions.assertArrayEquals(expected, Arrays.copyOf(input, res));
    }

    @Test
    void test3() {
        // Input: chars = ["a","b","b","b","b","b","b","b","b","b","b","b","b"]
        // Output: Return 4, and the first 4 characters of the input array should be: ["a","b","1","2"].
        // Explanation: The groups are "a" and "bbbbbbbbbbbb". This compresses to "ab12".
        char[] input = new char[] {'a','b','b','b','b','b','b','b','b','b','b','b','b'};
        char[] expected = new char[] {'a', 'b', '1', '2'};
        int res = compress(input);
        Assertions.assertEquals(4, res);
        Assertions.assertArrayEquals(expected, Arrays.copyOf(input, res));
    }

    @Test
    void test4() {
        char[] input = new char[] {'a','b','c'};
        char[] expected = new char[] {'a', 'b', 'c'};
        int res = compress(input);
        Assertions.assertEquals(3, res);
        Assertions.assertArrayEquals(expected, Arrays.copyOf(input, res));
    }
}
