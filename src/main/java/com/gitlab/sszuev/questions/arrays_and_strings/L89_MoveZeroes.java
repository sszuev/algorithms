package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given an integer array `nums`,
 * move all `0`'s to the end of it while maintaining the relative order of the non-zero elements.
 * Note that you must do this in-place without making a copy of the array.
 * <p>
 * Constraints:
 * - `1 <= nums.length <= 10^4`
 * - `-2^31 <= nums[i] <= 2^31 - 1`
 *
 * @see <a href="https://leetcode.com/problems/move-zeroes">283. Move Zeroes</a>
 */
public class L89_MoveZeroes {

    public void moveZeroes(int[] nums) {
        calcMoveZeroes(nums);
    }

    private static void calcMoveZeroes(int[] nums) {
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                int tmp = nums[i];
                nums[i] = nums[index];
                nums[index] = tmp;
                index++;
            }
        }
    }

    @Test
    void test1() {
        int[] given = new int[]{0, 1, 0, 3, 12};
        int[] expected = new int[]{1, 3, 12, 0, 0};
        moveZeroes(given);
        Assertions.assertArrayEquals(expected, given);
    }

    @Test
    void test2() {
        int[] given = new int[]{0};
        int[] expected = new int[]{0};
        moveZeroes(given);
        Assertions.assertArrayEquals(expected, given);
    }
}
