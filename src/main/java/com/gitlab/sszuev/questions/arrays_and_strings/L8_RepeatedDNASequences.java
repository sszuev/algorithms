package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The DNA sequence is composed of a series of nucleotides abbreviated as 'A', 'C', 'G', and 'T'.
 * For example, "ACGAATTCCG" is a DNA sequence.
 * When studying DNA, it is useful to identify repeated sequences within the DNA.
 * Given a string s that represents a DNA sequence,
 * return all the 10-letter-long sequences (substrings) that occur more than once in a DNA molecule.
 * You may return the answer in any order.
 * <p>
 * Constraints: {@code 1 <= s.length <= 10^5}, s[i] is either 'A', 'C', 'G', or 'T'.
 *
 * @see <a href="https://leetcode.com/problems/repeated-dna-sequences/">187. Repeated DNA Sequences</a>
 */
public class L8_RepeatedDNASequences {

    public static List<String> findRepeatedDnaSequences(String s) {
        if (s.isEmpty() || s.length() > 100_000) {
            throw new IllegalArgumentException();
        }
        if (s.length() <= 10) {
            return List.of();
        }
        return findRepeatedDnaSequences(ListBytes.fromString(s, 0, s.length()));
    }

    private static final byte ONE = 1;
    private static final byte TWO = 2;

    public static List<String> findRepeatedDnaSequences(ListBytes string) {
        Map<ListBytes, Byte> seen = new HashMap<>();
        List<String> res = new ArrayList<>();
        for (int i = 0; i < string.size() - 9; i++) {
            ListBytes t = string.substring(i, 10);
            Byte has = seen.get(t);
            if (has == null) {
                seen.put(t, ONE);
            } else {
                if (has == 1) {
                    String a = t.toString();
                    res.add(a);
                    seen.put(t, TWO);
                }
            }
        }
        return res;
    }

    public static class ListBytes {
        private final int start;
        private final int length;
        private final byte[] bytes;
        private String string;
        private int hashCode;

        private ListBytes(byte[] bytes, int start, int length) {
            this.start = start;
            this.length = length;
            this.bytes = bytes;
        }

        public static ListBytes fromString(String str, int start, int length) {
            byte[] bytes = str.getBytes(StandardCharsets.US_ASCII);
            return new ListBytes(bytes, start, length);
        }

        public ListBytes substring(int startInclusive, int length) {
            return new ListBytes(bytes, startInclusive, length);
        }

        public int size() {
            return length;
        }

        @Override
        public String toString() {
            if (string == null) {
                string = new String(bytes, start, length, StandardCharsets.US_ASCII);
            }
            return string;
        }

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            ListBytes other = (ListBytes) o;
            if (start == other.start && length == other.length) {
                return true;
            }
            return Arrays.equals(bytes, start, start + length,
                    other.bytes, other.start, other.start + other.length);
        }

        @Override
        public int hashCode() {
            if (hashCode != 0) {
                return hashCode;
            }
            int res = 1;
            for (int i = start; i < start + length; i++) {
                byte element = bytes[i];
                res = 31 * res + element;
            }
            return hashCode = res;
        }
    }

    @Test
    public void testAAAAACCCCCAAAAACCCCCCAAAAAGGGTTT() {
        Assertions.assertEquals(
                List.of("AAAAACCCCC", "CCCCCAAAAA"),
                findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT")
                        .stream().sorted().collect(Collectors.toList())
        );
    }

    @Test
    public void testAAAAAAAAAAAAA() {
        Assertions.assertEquals(
                List.of("AAAAAAAAAA"),
                findRepeatedDnaSequences("AAAAAAAAAAAAA")
                        .stream().sorted().collect(Collectors.toList())
        );
    }

    @Test
    public void testAAAAAAAAAA() {
        Assertions.assertEquals(
                List.of(),
                findRepeatedDnaSequences("AAAAAAAAAA")
                        .stream().sorted().collect(Collectors.toList())
        );
    }

    @Test
    public void testAAAAAAAAAAA() {
        Assertions.assertEquals(
                List.of("AAAAAAAAAA"),
                findRepeatedDnaSequences("AAAAAAAAAAA")
                        .stream().sorted().collect(Collectors.toList())
        );
    }

}
