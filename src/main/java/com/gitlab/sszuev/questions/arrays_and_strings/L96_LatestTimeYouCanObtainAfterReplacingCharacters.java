package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * You are given a string s representing a 12-hour format time where some of the digits (possibly none) are replaced with a "?".
 * 12-hour times are formatted as "HH:MM", where HH is between 00 and 11,
 * and MM is between 00 and 59.
 * The earliest 12-hour time is 00:00, and the latest is 11:59.
 * You have to replace all the "?" characters in s with digits such that the time
 * we obtain by the resulting string is a valid 12-hour format time and is the latest possible.
 * Return the resulting string.
 *
 * @see <a href="https://leetcode.com/problems/latest-time-you-can-obtain-after-replacing-characters">3114. Latest Time You Can Obtain After Replacing Characters</a>
 */
public class L96_LatestTimeYouCanObtainAfterReplacingCharacters {
    public String findLatestTime(String s) {
        return calcLatestTime(s);
    }

    private static String calcLatestTime(String s) {
        char[] time = s.toCharArray();
        char[] hours = Arrays.copyOf(time, 2);
        char[] minutes = Arrays.copyOfRange(time, 3, time.length);
        StringBuilder sb = new StringBuilder();
        if (hours[1] == '?') {
            if (hours[0] == '1' || hours[0] == '?') {
                sb.append("11");
            } else if (hours[0] == '0') {
                sb.append("09");
            } else {
                throw new IllegalArgumentException();
            }
        } else {
            if (hours[0] == '?') {
                if (hours[1] == '1') {
                    sb.append("11");
                } else if (hours[1] == '0') {
                    sb.append("10");
                } else {
                    sb.append('0').append(hours[1]);
                }
            } else {
                sb.append(hours[0]).append(hours[1]);
            }
        }
        sb.append(":");
        if (minutes[1] == '?') {
            if (minutes[0] == '?') {
                sb.append("59");
            } else {
                sb.append(minutes[0]).append('9');
            }
        } else {
            if (minutes[0] == '?') {
                sb.append('5').append(minutes[1]);
            } else {
                sb.append(minutes[0]).append(minutes[1]);
            }
        }
        return sb.toString();
    }

    @Test
    void test1() {
        // Input: s = "1?:?4"
        // Output: "11:54"
        String actual = findLatestTime("1?:?4");
        Assertions.assertEquals("11:54", actual);
    }

    @Test
    void test2() {
        // Input: s = "0?:5?"
        // Output: "09:59"
        String actual = findLatestTime("0?:5?");
        Assertions.assertEquals("09:59", actual);
    }

    @Test
    void test3() {
        String actual = findLatestTime("?0:40");
        Assertions.assertEquals("10:40", actual);
    }

    @Test
    void test4() {
        String actual = findLatestTime("?1:?6");
        Assertions.assertEquals("11:56", actual);
    }
}
