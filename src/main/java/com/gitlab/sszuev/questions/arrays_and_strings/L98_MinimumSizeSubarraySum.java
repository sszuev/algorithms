package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given an array of positive integers nums and a positive integer target, return the minimal length of a
 * subarray whose sum is greater than or equal to target.
 * If there is no such subarray, return 0 instead.
 * <p>
 * Constraints:
 * - `1 <= target <= 10^9`
 * - `1 <= nums.length <= 10^5`
 * - `1 <= nums[i] <= 10^4`
 *
 * @see <a href="https://leetcode.com/problems/minimum-size-subarray-sum/">209. Minimum Size Subarray Sum</a>
 */
public class L98_MinimumSizeSubarraySum {
    public int minSubArrayLen(int target, int[] nums) {
        return calcMinSubArrayLen(target, nums);
    }

    private static int calcMinSubArrayLen(int target, int[] nums) {
        int res = Integer.MAX_VALUE;
        int indexLeft = 0;
        int indexRight = 1;
        int sum = nums[indexLeft];
        if (sum >= target) {
            return 1;
        }
        while (indexRight < nums.length) {
            sum += nums[indexRight];
            indexRight++;
            if (sum >= target) {
                res = Math.min(res, indexRight - indexLeft);
                sum -= nums[indexLeft];
                indexLeft++;
                sum -= nums[--indexRight];
            }
        }
        if (res == Integer.MAX_VALUE) return 0;
        return res;
    }

    @Test
    void test1() {
        int[] nums = new int[]{2, 3, 1, 2, 4, 3};
        int res = minSubArrayLen(7, nums);
        Assertions.assertEquals(2, res);
    }

    @Test
    void test2() {
        int[] nums = new int[]{1, 4, 4};
        int res = minSubArrayLen(4, nums);
        Assertions.assertEquals(1, res);
    }

    @Test
    void test3() {
        int[] nums = new int[]{1, 1, 1, 1, 1, 1, 1, 1};
        int res = minSubArrayLen(11, nums);
        Assertions.assertEquals(0, res);
    }

    @Test
    void test4() {
        int[] nums = new int[]{10, 2, 3};
        int res = minSubArrayLen(6, nums);
        Assertions.assertEquals(1, res);
    }
}
