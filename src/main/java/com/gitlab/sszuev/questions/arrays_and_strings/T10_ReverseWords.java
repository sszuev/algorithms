package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <b>How to reverse words in String?</b>
 *
 * @see <a href='https://www.java67.com/2015/06/how-to-reverse-words-in-string-java.html'>How to reverse words in String Java?</a>
 */
public class T10_ReverseWords {

    @Test
    public void testMethod1() {
        Assertions.assertEquals("Ccc Bbb A", reverseWords1("A Bbb Ccc"));
    }

    @Test
    public void testMethod2() {
        Assertions.assertEquals("Ccc Bbb A", reverseWords2("A Bbb Ccc"));
    }

    public static String reverseWords1(String sentence) {
        List<String> words = Arrays.asList(sentence.trim().split("\\s"));
        Collections.reverse(words);
        return String.join(" ", words);
    }

    public static String reverseWords2(String line) {
        if (line.trim().isEmpty()) {
            return line;
        }
        StringBuilder res = new StringBuilder();
        String[] sa = line.trim().split("\\s");
        for (int i = sa.length - 1; i >= 0; i--) {
            res.append(sa[i]);
            res.append(' ');
        }
        return res.toString().trim();
    }
}
