package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * <b>How to check if strings are rotations of each other?</b>
 *
 * @see <a href='https://www.java67.com/2017/07/string-rotation-in-java-write-program.html'>How to check if strings are rotations of each other or not</a>
 */
public class T11_Rotation {

    @Test
    public void testTrue() {
        Assertions.assertTrue(isRotation("IndiaUSAEngland", "USAEnglandIndia"));
        Assertions.assertTrue(isRotation("USAEnglandIndia", "IndiaUSAEngland"));
    }

    @Test
    public void testFalse() {
        Assertions.assertFalse(isRotation("IndiaUSAEngland", "IndiaEnglandUSA"));
        Assertions.assertFalse(isRotation("IndiaEnglandUSA", "IndiaUSAEngland"));
    }

    public static boolean isRotation(String original, String rotation) {
        return original.length() == rotation.length() && (original + original).contains(rotation);
    }
}
