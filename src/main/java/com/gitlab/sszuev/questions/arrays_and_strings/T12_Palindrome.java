package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

/**
 * <b>How to Check is given String is a Palindrome?</b>
 * <a href='https://www.java67.com/2015/06/how-to-check-is-string-is-palindrome-in.html'>How to Check is given String is a Palindrome in Java using Recursion</a>
 */
public class T12_Palindrome {

    @Test
    public void testStandard() {
        test("khgggk3432е62309г90ererуке", T12_Palindrome::isPalindromeStandard);
    }

    @Test
    public void testFast() {
        test("kh ererуке", T12_Palindrome::isPalindromeStraightforward);
    }

    @Test
    public void testWithRecursion() {
        test("abc", T12_Palindrome::isPalindromeWithRecursion);
    }

    private static void test(String s, Predicate<String> test) {
        String p1 = new StringBuilder(s).reverse().append("A").append(s).toString();
        String p2 = s + new StringBuffer(s).reverse();
        System.out.println(p1);
        System.out.println(p2);
        Assertions.assertTrue(test.test(p1));
        Assertions.assertTrue(test.test(p2));
        Assertions.assertFalse(test.test(s));
    }

    public static boolean isPalindromeStandard(String input) {
        return input.equals(new StringBuilder(input).reverse().toString());
    }

    public static boolean isPalindromeStraightforward(String input) {
        for (int i = 0; i < input.length() / 2; i++) {
            if (input.charAt(i) != input.charAt(input.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPalindromeWithRecursion(String input) {
        return input.equals(reverseWithRecursion(input));
    }

    /**
     * Java method to reverse String using recursion
     *
     * @param input {@code String}
     * @return reversed String of input
     */
    public static String reverseWithRecursion(String input) {
        if (input == null || input.isEmpty()) {
            return input;
        }
        return input.charAt(input.length() - 1) + reverseWithRecursion(input.substring(0, input.length() - 1));
    }
}
