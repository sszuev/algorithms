package com.gitlab.sszuev.questions.arrays_and_strings;

import java.util.Arrays;
import java.util.BitSet;

/**
 * <b>How to Find Missing Number on Integer Array of 1 to 100?</b>
 * <p>
 * Let's understand the problem statement, we have numbers from 1 to 100 that are put into an integer array,
 * what's the best way to find out which number is missing?
 * If Interviewer especially mentions 1 to 100 then you can apply the above trick about the sum of the series as shown below as well.
 * If it has more than one missing element that you can use BitSet class, of course only if your interviewer allows it.
 * 1) Sum of the series: Formula: n (n+1)/2( but only work for one missing number)
 * 2) Use BitSet, if an array has more than one missing elements.
 *
 * @see <a href='https://javarevisited.blogspot.com/2014/11/how-to-find-missing-number-on-integer-array-java.html'>How to Find Missing Number on Integer Array of 1 to 100</a>
 */
public class T1_FindMissing {

    public static void main(String[] args) {
        // one missing number
        printMissingNumber(new int[]{1, 2, 3, 4, 6}, 6);

        // two missing number
        printMissingNumber(new int[]{1, 2, 3, 4, 6, 7, 9, 8, 10}, 10);

        // three missing number
        printMissingNumber(new int[]{1, 2, 3, 4, 6, 9, 8}, 10);

        // four missing number
        printMissingNumber(new int[]{1, 2, 3, 4, 9, 8}, 10);
    }

    /**
     * A general method to find missing values from an integer array in Java.
     * This method will work even if array has more than one missing element.
     */
    private static void printMissingNumber(int[] numbers, int count) {
        int missingCount = count - numbers.length;
        BitSet bitSet = new BitSet(count);

        for (int number : numbers) {
            bitSet.set(number - 1);
        }

        System.out.printf("Missing numbers in integer array %s, with total number %d is %n", Arrays.toString(numbers), count);
        int lastMissingIndex = 0;

        for (int i = 0; i < missingCount; i++) {
            lastMissingIndex = bitSet.nextClearBit(lastMissingIndex);
            System.out.println(++lastMissingIndex);
        }
    }
}
