package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * <b>How to Find all Pairs in Array of Integers Whose sum is Equal to a Given Number</b>
 *
 * @see <a href='https://javarevisited.blogspot.com/2014/08/how-to-find-all-pairs-in-array-of-integers-whose-sum-equal-given-number-java.html'>How to Find all Pairs in Array of Integers Whose sum is Equal to a Given Number</a>
 */
public class T3_EqualSumPairs {

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void test1(Solutions solution) {
        Assertions.assertEquals(createExpected(-11, 20), findPairs(solution, 9, 12, 14, 17, 15, 19, 20, -11));
    }

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void test2(Solutions solution) {
        Assertions.assertEquals(createExpected(-1, 10, 2, 7, 4, 5), findPairs(solution, 9, 2, 4, 7, 5, 9, 10, -1));
    }

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void test3(Solutions solution) {
        Assertions.assertEquals(createExpected(2, 5, 4, 3), findPairs(solution, 7, 2, 4, 3, 5, 7, 8, 9));
    }

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void test4(Solutions solution) {
        Assertions.assertEquals(createExpected(2, 5, 4, 3, 3, 4, -2, 9), findPairs(solution, 7, 2, 4, 3, 5, 6, -2, 4, 7, 8, 9));
    }

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void test5(Solutions solution) {
        Assertions.assertEquals(createExpected(11, 0), findPairs(solution, 11, 0, 2, 13, 5, 3, 1, 11, 14, 3));
    }

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void test6(Solutions solution) {
        Assertions.assertEquals(createExpected(7, 5, 11, 1), findPairs(solution, 12, 10, 5, 0, 3, 14, 7, 13, 1, 4, 11));
    }

    private Set<IntPair> findPairs(Solutions solution, int sum, int... array) {
        System.out.println("Array to test : " + Arrays.toString(array));
        System.out.println("Sum : " + sum);
        Set<IntPair> res = new HashSet<>();
        solution.findPairs(array, sum, (a, b) -> res.add(new IntPair(a, b)));
        System.out.printf("Pairs of numbers from an array whose sum equals %d : %s%n", sum, res);
        return res;
    }

    private Set<IntPair> createExpected(int... values) {
        if (values.length == 0 || values.length % 2 != 0) {
            throw new IllegalArgumentException();
        }
        Set<IntPair> res = new HashSet<>();
        for (int i = 0; i < values.length; i += 2) {
            res.add(new IntPair(values[i], values[i + 1]));
        }
        return res;
    }

    enum Solutions {
        BRUTE_FORCE {
            /**
             * Prints all pair of integer values from given array whose sum is is equal to given number.
             * Complexity of this solution is {@code O(n^2)}
             * {@inheritDoc}
             */
            @Override
            void findPairs(int[] array, int sum, IntBiConsumer consumer) {
                for (int i = 0; i < array.length; i++) {
                    int first = array[i];
                    for (int j = i + 1; j < array.length; j++) {
                        int second = array[j];
                        if ((first + second) == sum) {
                            consumer.accept(first, second);
                        }
                    }
                }
            }
        },
        USE_TWO_POINTERS {
            @Override
            void findPairs(int[] numbers, int k, IntBiConsumer consumer) {
                if (numbers.length < 2) {
                    return;
                }
                Arrays.sort(numbers);
                int left = 0;
                int right = numbers.length - 1;
                while (left < right) {
                    int sum = numbers[left] + numbers[right];
                    if (sum == k) {
                        consumer.accept(numbers[left], numbers[right]);
                        left = left + 1;
                        right = right - 1;
                    } else if (sum < k) {
                        left = left + 1;
                    } else {
                        right = right - 1;
                    }
                }
            }
        },

        USE_SET {
            @Override
            void findPairs(int[] numbers, int n, IntBiConsumer consumer) {
                if (numbers.length < 2) {
                    return;
                }
                Set<Integer> set = new HashSet<>(numbers.length);
                for (int value : numbers) {
                    int target = n - value;
                    // if target number is not in set then add
                    if (!set.contains(target)) {
                        set.add(value);
                    } else {
                        consumer.accept(value, target);
                    }
                }
            }
        };

        abstract void findPairs(int[] array, int sum, IntBiConsumer consumer);
    }

    interface IntBiConsumer {
        void accept(int a, int b);
    }

    static class IntPair {
        final int a;
        final int b;

        IntPair(int a, int b) {
            this.a = Math.min(a, b);
            this.b = Math.max(a, b);
        }

        public static IntPair of(int a, int b) {
            return new IntPair(a, b);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            IntPair other = (IntPair) o;
            return a == other.a && b == other.b;
        }

        @Override
        public int hashCode() {
            return Objects.hash(a, b);
        }

        @Override
        public String toString() {
            return String.format("(%d, %d)", a, b);
        }
    }
}
