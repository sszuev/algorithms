package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * <b>How to Reverse an Array</b>
 *
 * @see <a href='https://javarevisited.blogspot.com/2013/03/how-to-reverse-array-in-java-int-String-array-example.html'>How to Reverse an Array in Java - Integer and String Array Example</a>
 */
public class T4_ReverseArray {

    @Test
    public void test1() {
        test(new int[]{1, 1, 2, 2, 3, 4, 5, 2}, new int[]{2, 5, 4, 3, 2, 2, 1, 1});
    }

    @Test
    public void test2() {
        test(new int[]{1, 1, 1}, new int[]{1, 1, 1});
    }

    private void test(int[] array, int[] expected) {
        System.out.println("Before : " + Arrays.toString(array));
        reverse(array);
        System.out.println("After : " + Arrays.toString(array));
        Assertions.assertArrayEquals(expected, array);
    }

    public static void reverse(final int[] array) {
        if (array == null) {
            return;
        }
        reverse(array, 0, array.length);
    }

    /**
     * Reverses the order of the given array in the given range.
     * This method does nothing for a {@code null} input array.
     *
     * @param array               the array to reverse, may be {@code null}
     * @param startIndexInclusive the starting index. Undervalue (&lt;0) is promoted to 0, overvalue (&gt;array.length) results in no
     *                            change.
     * @param endIndexExclusive   elements up to endIndex-1 are reversed in the array. Undervalue (&lt; start index) results in no
     *                            change. Overvalue (&gt;array.length) is demoted to array length.
     */
    public static void reverse(final int[] array, final int startIndexInclusive, final int endIndexExclusive) {
        if (array == null) {
            return;
        }
        int i = Math.max(startIndexInclusive, 0);
        int j = Math.min(array.length, endIndexExclusive) - 1;
        int tmp;
        while (j > i) {
            tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;
            j--;
            i++;
        }
    }
}
