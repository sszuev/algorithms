package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * <b>How to Remove Duplicates from Array Without Using Java Collection API</b>
 * Created by @ssz on 29.09.2020.
 *
 * @see <a href='https://javarevisited.blogspot.com/2014/01/how-to-remove-duplicates-from-array-java-without-collection-API.html'>How to Remove Duplicates from Array Without Using Java Collection API</a>
 */
public class T5_RemoveDuplicates {

    @Test
    public void test1() {
        test(new int[]{1, 1, 2, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5});
    }

    @Test
    public void test2() {
        test(new int[]{1, 1, 1, 1, 1, 1, 1}, new int[]{1});
    }

    @Test
    public void test3() {
        test(new int[]{1, 2, 3, 4, 5, 6, 7}, new int[]{1, 2, 3, 4, 5, 6, 7});
    }

    @Test
    public void test4() {
        test(new int[]{1, 2, 1, 1, 1, 1, 1}, new int[]{1, 2});
    }

    private void test(int[] given, int[] expected) {
        System.out.println("Array with Duplicates       : " + Arrays.toString(given));
        int[] actual = removeDuplicates(given);
        System.out.println("After removing duplicates   : " + Arrays.toString(actual));
        Assertions.assertArrayEquals(expected, actual);
    }

    /*
     * Method to remove duplicates from array in Java, without using Collection classes e.g. Set or ArrayList.
     * Algorithm for this method is simple,
     * it first sort the array and then compare adjacent objects,
     * leaving out duplicates, which is already in the result.
     */
    public static int[] removeDuplicates(int[] array) {
        // Sorting array to bring duplicates together
        Arrays.sort(array);

        int[] res = new int[array.length];
        int prev = array[0];
        res[0] = prev;

        int j = 1;
        for (int i = 1; i < array.length; i++) {
            int item = array[i];
            if (prev != item) {
                res[j++] = item;
            }
            prev = item;
        }
        if (j != array.length - 1) {
            res = Arrays.copyOf(res, j);
        }
        return res;
    }

}
