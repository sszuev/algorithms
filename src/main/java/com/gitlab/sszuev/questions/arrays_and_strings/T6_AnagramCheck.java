package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Arrays;

/**
 * <b>How to check if two String are Anagram</b>
 *
 * @see <a href='https://javarevisited.blogspot.com/2013/03/Anagram-how-to-check-if-two-string-are-anagrams-example-tutorial.html'>How to check if two String are Anagram</a>
 */
public class T6_AnagramCheck {

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void test1(Solutions solution) {
        Assertions.assertTrue(solution.isAnagram("word", "wrdo"));
        Assertions.assertTrue(solution.isAnagram("bboat", "tboab"));
    }

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void test2(Solutions solution) {
        Assertions.assertFalse(solution.isAnagram("word", "wrda"));
    }

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void test3(Solutions solution) {
        Assertions.assertFalse(solution.isAnagram("ffff", "aaaa"));
        Assertions.assertFalse(solution.isAnagram("aaaa", "ffff"));
    }

    enum Solutions {
        USE_SORT {
            @Override
            boolean checkAnagram(String first, String second) {
                char[] charFromWord = first.toCharArray();
                char[] charFromAnagram = second.toCharArray();
                Arrays.sort(charFromWord);
                Arrays.sort(charFromAnagram);
                return Arrays.equals(charFromWord, charFromAnagram);
            }
        },
        USE_BUILDER {
            @Override
            boolean checkAnagram(String first, String second) {
                StringBuilder res = new StringBuilder(second);
                for (char ch : first.toCharArray()) {
                    int index = res.indexOf(String.valueOf(ch));
                    if (index == -1) {
                        return false;
                    }
                    res.deleteCharAt(index);
                }
                return res.length() == 0;
            }
        },
        FROM_COMMENT {
            @Override
            boolean checkAnagram(String first, String second) {
                // xor gate is special for check occurrences A^B^A == B also A^A == 0
                int xorGateW = 0;
                int xorGateA = 0;
                for (int i = 0; i < first.length(); i++) {
                    xorGateW ^= first.charAt(i);
                    xorGateA ^= second.charAt(i);
                }
                if (xorGateW != 0 || xorGateA != 0)
                    return (xorGateW ^ xorGateA) == 0;
                return false;
            }
        };

        abstract boolean checkAnagram(String first, String second);

        public final boolean isAnagram(String first, String second) {
            if (first.length() != second.length()) return false;
            return checkAnagram(first, second);
        }
    }
}
