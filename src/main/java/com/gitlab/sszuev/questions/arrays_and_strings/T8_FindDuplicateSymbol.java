package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * <b>How to Find Duplicate Characters on String</b>
 *
 * @see <a href='https://www.java67.com/2014/03/how-to-find-duplicate-characters-in-String-Java-program.html'>How to Find Duplicate Characters on String</a>
 */
public class T8_FindDuplicateSymbol {
    @Test
    public void testProgramming() {
        test("Programming", 'r', 'g', 'm');
    }

    @Test
    public void testCombination() {
        test("Combination", 'i', 'n', 'o');
    }

    @Test
    public void testJava() {
        test("Java", 'a');
    }

    private void test(String word, char... expected) {
        List<Character> actual = new ArrayList<>();
        printDuplicateCharacters(word, actual::add);
        Assertions.assertEquals(IntStream.range(0, expected.length)
                .mapToObj(x -> expected[x]).collect(Collectors.toList()), actual);
    }

    /*
     * Find all duplicate characters in a String and print each of them.
     */
    public static void printDuplicateCharacters(String word, Consumer<Character> test) {
        System.out.printf("List of duplicate characters in String '%s' %n", word);
        getMap(word).forEach((key, value) -> {
            char c = (char) key.intValue();
            if (value > 1) {
                System.out.printf("%s : %d %n", c, value);
                test.accept(c);
            }
        });
    }

    public static Map<Integer, Long> getMap(String str) {
        return str.chars().boxed().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

}
