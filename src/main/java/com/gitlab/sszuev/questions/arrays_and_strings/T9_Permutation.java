package com.gitlab.sszuev.questions.arrays_and_strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <b>How to Find All Permutations of String</b>
 * The complexity is {@code O(n*n!)}
 * because loop will run for n times and for each n, we will call permutation method.
 *
 * @see <a href='https://javarevisited.blogspot.com/2015/08/how-to-find-all-permutations-of-string-java-example.html'>How to Find All Permutations of String in Java using Recursion</a>
 */
public class T9_Permutation {

    @Test
    public void testStringArray1234() {
        Collection<String> res = permutation("1234");
        res.forEach(System.out::println);
        Assertions.assertEquals(24, res.size());
        Assertions.assertEquals(24, new HashSet<>(res).size());
    }

    @Test
    public void testABC() {
        Collection<String> res = permutation("ABC");
        res.forEach(System.out::println);
        Assertions.assertEquals(6, res.size());
        Assertions.assertEquals(6, new HashSet<>(res).size());
    }

    @Test
    public void testIntArray1234() {
        List<List<Integer>> res = permutation(List.of(1, 2, 3, 4));
        res.forEach(System.out::println);
        Assertions.assertEquals(24, res.size());
        Assertions.assertEquals(24, new HashSet<>(res).size());
    }

    /**
     * A method exposed to client to calculate permutation of String in Java.
     *
     * @param input {@code String}
     * @return a {@code Collection} of {@code String}s
     */
    public static Collection<String> permutation(String input) {
        Collection<String> res = new LinkedHashSet<>();
        permutation("", input, res);
        return res;
    }

    /*
     * Recursive method which actually prints all permutations of given String,
     * but since we are passing an empty String as current permutation to start with,
     * I have made this method private and didn't exposed it to client.
     */
    private static void permutation(String perm, String word, Collection<String> res) {
        if (word.isEmpty()) {
            res.add(perm);
            return;
        }
        for (int i = 0; i < word.length(); i++) {
            permutation(perm + word.charAt(i), word.substring(0, i) + word.substring(i + 1), res);
        }
    }

    private static <X> List<List<X>> permutation(List<X> givenArray) {
        Map<Set<X>, List<List<X>>> res = new HashMap<>();
        permutation(givenArray, res);
        return res.get(new HashSet<>(givenArray));
    }

    private static <X> void permutation(List<X> givenArray, Map<Set<X>, List<List<X>>> res) {
        if (givenArray.isEmpty()) {
            return;
        }
        if (res.containsKey(new HashSet<>(givenArray))) {
            return;
        }
        if (givenArray.size() == 1) {
            List<X> x = new ArrayList<>();
            x.add(givenArray.get(0));
            res.put(new HashSet<>(x), List.of(x));
            return;
        }
        if (givenArray.size() == 2) {
            List<X> x = new ArrayList<>();
            x.add(givenArray.get(1));
            x.add(givenArray.get(0));
            List<X> y = new ArrayList<>();
            y.add(givenArray.get(0));
            y.add(givenArray.get(1));
            res.put(new HashSet<>(givenArray), List.of(x, y));
            return;
        }
        X item = givenArray.get(0);
        List<X> right = givenArray.subList(1, givenArray.size());
        permutation(right, res);
        List<List<X>> nextPermutations = res.get(new HashSet<>(right));
        List<List<X>> nextRes = new ArrayList<>();
        for (List<X> next : nextPermutations) {
            for (int i = 0; i < givenArray.size(); i++) {
                List<X> r = new ArrayList<>(next);
                r.add(i, item);
                nextRes.add(r);
            }
        }
        res.put(new HashSet<>(givenArray), nextRes);
    }
}
