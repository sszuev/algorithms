[HOME](../../../../../../../../README.md)

- **[Q2](Q2_RealToBinaryString.java)**: Binary to String: Given a real number between `0` and `1` (e.g., `0.72`) that is
  passed in as a double, print the binary representation.
  If the number cannot be represented accurately in binary with at most `32` characters, print `"ERROR"`.
  <details>
    <summary>Hints</summary>  

    - ***143***: To wrap your head around the problem, try thinking about how you'd do it for integers.
    - ***167***: In a number like `.893` (in base `10`), what does each digit signify?
      What then does each digit in `.10010` signify in base `2`?
    - ***173***: A number such as `.893` (in base `10`) indicates `8 * 10 ^-1 + 9 * 10 ^-2 + 3 * 10 ^-3`.
      Translate this system into base `2`.
    - ***269***: How would you get the first digit in `.893`?
      If you multiplied by `10`, you'd shift the values over to get `8.93`. What happens if you multiply by `2`?
    - ***297***: Think about what happens for values that can't be represented accurately in binary.

  </details>


- **[Q4](Q4_NextBinaryNumberWithSameDigits.java)**: Next Number: Given a positive integer, print the next smallest and
  the next largest number
  that have the same number of `1` bits in their binary representation.
  <details>
    <summary>Hints</summary>

    - ***147***: Get Next: Start with a brute force solution for each.

    - ***175***: Get Next: Picture a binary number-something with a bunch of `1` s and `0`s spread out throughout the
      number.
      Suppose you flip a `1` to a `0` and a `0` to a `1`. In what case will the number get bigger? In what case will it
      get smaller?

    - ***242***: Get Next: If you flip a `1` to a `0` and a `0` to a `1`,
      it will get bigger if the `0` -> `1` bit is more significant than the `1` -> `0` bit.
      How can you use this to create the next biggest number (with the same number of `1`s)?

    - ***312***: Get Next: Can you flip a `0` to a `1` to create the next biggest number?

    - ***339***: Get Next: Flipping a `0` to a `1` will create a bigger number.
      The farther right the index is the smaller the bigger number is.
      If we have a number like `1001`, we want to flip the rightmost `0` (to create `1011`).
      But if we have a number like `1010`, we should not flip the rightmost `1`.

    - ***358***: Get Next: We should flip the rightmost non-trailing `0`. The number `1010` would become `1110`.
      Once we've done that, we need to flip a `1` to a `0` to make the number as small as possible,
      but bigger than the original number (`1010`). What do we do? How can we shrink the number?

    - ***375***: Get Next: We can shrink the number by moving all the `1`s
      to the right of the flipped bit as far right as possible (removing a `1` in the process).

    - ***390***: Get Previous: Once you've solved Get Next, try to invert the logic for Get Previous.

  </details>


- **[Q6](Q6_BitsNumberToFlipForConversion.java)**: Conversion: Write a function to determine the number of bits you
  would need to flip to convert integer A to integer B.
  <details>
    <summary>Hints</summary>

    - ***336***: How would you figure out how many bits are different between two numbers?

    - ***369***: Think about what an `XOR` indicates. If you do `a XOR b`, where does the result have `1`s? Where does
      it have `O`s?

  </details>


- **[Q7](Q7_PairwiseBitSwap.java)**: Pairwise Swap: Write a program to swap odd and even bits in an integer with as few
  instructions as possible
  (e.g., bit `0` and bit `1` are swapped, bit `2` and bit `3` are swapped, and so on).
  <details>
    <summary>Hints</summary>

    - ***145***: Swapping each pair means moving the even bits to the left and the odd bits to the right.
      Can you break this problem into parts?
    - ***248***: Can you create a number that represents just the even bits? Then can you shift the even bits over by
      one?
    - ***328***: The value `1010` in binary is `10` in decimal or `OxA` in hex. What will a sequence of `101010...` be
      in hex?
      That is, how do you represent an alternating sequence of `1`s and `O`s with 1s in the odd places?
      How do you do this for the reverse (`1`s in the even spots)?
    - ***355***: Try masks `0xaaaaaaaa` and `0x55555555` to select the even and odd bits.
      Then try shifting the even and odd bits around to create the right number.

  </details>

- **[L7](L7_DivideTwoIntegers.java)**: (leetcode) Given two integers dividend and divisor, divide two integers without
  using multiplication, division, and mod operator.

- **[L10](L10_ReverseBits.java)**: (leetcode) Reverse bits of a given 32-bit unsigned integer.

- **[L18](Q6_BitsNumberToFlipForConversion.java)**: (leetcode) Minimum Flips to Make a OR b Equal to c.

- **[L33](L33_FindSingleNumber.java)**: (leetcode) Single number: given a non-empty array of integers nums, every element appears twice except for one.

- **[L72](L72_CountingBits.java)**: (leetcode) Counting Bits

- **[L95](L95_NumberOf1Bits.java)**: (leetcode) Number of 1 Bits


[HOME](../../../../../../../../README.md)