package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Reverse bits of a given 32-bit unsigned integer.
 * @see <a href="https://leetcode.com/problems/reverse-bits">190. Reverse Bits</a>
 */
public class L10_ReverseBits {

    public static int reverseBits(int x) {
        int res = 0;
        for (int i = 0; i < 16; i++) {
            int right = x & (1 << i);
            int left = x & (1 << (31 - i));

            int R = right << (31 - 2 * i);
            int L = left >>> (31 - 2 * i);
            int RorL = R | L;
            res |= RorL;
        }
        return res;
    }

    @Test
    public void test0b00000010100101000001111010011100() {
        // 43261596 -> 964176192
        // 00000010100101000001111010011100 -> 00111001011110000010100101000000
        Assertions.assertEquals(
                964176192, reverseBits(43261596)
        );
    }

    @Test
    public void test0b11111111111111111111111111111101() {
        // -3 -> -1073741825
        // 11111111111111111111111111111101 -> 10111111111111111111111111111111
        Assertions.assertEquals(
                -1073741825, reverseBits(-3)
        );
    }

}
