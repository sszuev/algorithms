package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given 3 positives numbers a, b and c.
 * Return the minimum flips required in some bits of a and b to make {@code a OR b == c} (bitwise OR operation).
 * Flip operation consists of change any single bit 1 to 0 or
 * change the bit 0 to 1 in their binary representation.
 *
 * @see <a href="https://leetcode.com/problems/minimum-flips-to-make-a-or-b-equal-to-c">1318. Minimum Flips to Make a OR b Equal to c</a>
 */
public class L19_MinimumFlipsForAOrBEqC {

    public int minFlips(int a, int b, int c) {
        return calcMinBitFlips(a, b, c);
    }

    public static int calcMinBitFlips(int a, int b, int c) {
        // a | b = c
        int res = 0;
        for (int i = 0; i < 32; i++) {
            int bC = (c >> i) & 1;
            int bA = (a >> i) & 1;
            int bB = (b >> i) & 1;
            if (bC == 1) {
                if (bA == 0 && bB == 0) {
                    res++;
                }
            } else {
                if (bA == 1 && bB == 1) {
                    res += 2;
                } else if (bA != 0 || bB != 0) {
                    res++;
                }
            }
        }
        return res;
    }

    @Test
    void test2or6eq5() {
        Assertions.assertEquals(3, minFlips(2, 6, 5));
    }

    @Test
    void test4or2eq7() {
        Assertions.assertEquals(1, minFlips(4, 2, 7));
    }

    @Test
    void test1or2eq3() {
        Assertions.assertEquals(0, minFlips(1, 2, 3));
    }
}
