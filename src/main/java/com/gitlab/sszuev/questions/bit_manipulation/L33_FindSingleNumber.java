package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
 * You must implement a solution with a linear runtime complexity and use only constant extra space.
 * Constraints: {@code 1 <= nums.length <= 3 * 10^4}, {@code -3 * 10^4 <= nums[i] <= 3 * 10^4}
 * Each element in the array appears twice except for one element which appears only once.
 *
 * @see <a href="https://leetcode.com/problems/single-number">136. Single Number</a>
 */
public class L33_FindSingleNumber {

    public int singleNumber(int[] nums) {
        return findSingleNumber(nums);
    }

    private static int findSingleNumber(int[] nums) {
        // x = x ^ y ^ y
        int res = nums[0];
        for (int i = 1; i < nums.length; i++) {
            res ^= nums[i];
        }
        return res;
    }

    @Test
    void test1() {
        int actual = singleNumber(new int[]{2, 2, 1});
        Assertions.assertEquals(1, actual);
    }

    @Test
    void test2() {
        int actual = singleNumber(new int[]{4, 1, 2, 1, 2});
        Assertions.assertEquals(4, actual);
    }

    @Test
    void test3() {
        int actual = singleNumber(new int[]{1});
        Assertions.assertEquals(1, actual);
    }
}
