package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given an integer n, return an array `ans` of length `n + 1`
 * such that for each i (0 <= i <= n), `ans[i]` is the number of `1`'s in the binary representation of i.
 * <p>
 * Constraints: 0 <= n <= 10^5
 *
 * @see <a href="https://leetcode.com/problems/counting-bits">338. Counting Bits</a>
 */
public class L72_CountingBits {

    public int[] countBits(int n) {
        return calcCountBits(n);
    }

    private static int[] calcCountBits(int n) {
        int[] res = new int[n + 1];
        for (int i = 0; i < res.length; i++) {
            res[i] = countNumberOfOnes(i);
        }
        return res;
    }

    private static int countNumberOfOnes(int n) {
        int res = 0;
        for (int i = 0; i < 17; i++) {
            if ((n & 1) == 1) {
                res++;
            }
            n = n >> 1;
        }
        return res;
    }

    @Test
    void test1() {
        int[] actual = countBits(2);
        Assertions.assertArrayEquals(new int[]{0, 1, 1}, actual);
    }

    @Test
    void test2() {
        int[] actual = countBits(5);
        Assertions.assertArrayEquals(new int[]{0, 1, 1, 2, 1, 2}, actual);
    }
}
