package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given two integers dividend and divisor, divide two integers without using multiplication, division, and mod operator.
 * The integer division should truncate toward zero, which means losing its fractional part.
 * For example, 8.345 would be truncated to 8, and -2.7335 would be truncated to -2.
 * Return the quotient after dividing dividend by divisor.
 * Note: Assume we are dealing with an environment that could only store integers within the 32-bit signed integer range:
 * {@code [−2^31, 2^31 − 1]}.
 * For this problem, if the quotient is strictly greater than {@code 2^31 - 1},
 * then return {@code 2^31 - 1}, and if the quotient is strictly less than {@code -2^31}, then return {@code -2^31}.
 *
 * @see <a href="https://leetcode.com/problems/divide-two-integers">29. Divide Two Integers</a>
 */
public class L7_DivideTwoIntegers {

    public static int divide(int dividend, int divisor) {
        long res = divide(dividend, (long) divisor);
        if (res < Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }
        if (res > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        }
        return (int) res;
    }

    public static long divide(long dividend, long divisor) {
        if (divisor == 0 || (divisor > Integer.MAX_VALUE || divisor < Integer.MIN_VALUE)) {
            throw new IllegalArgumentException();
        }
        if (dividend > Integer.MAX_VALUE || dividend < Integer.MIN_VALUE) {
            throw new IllegalArgumentException();
        }
        boolean minus = (dividend < 0 && divisor > 0) || (dividend > 0 && divisor < 0);
        if (dividend < 0) {
            dividend = -dividend;
        }
        if (divisor < 0) {
            divisor = -divisor;
        }
        if (divisor == 1) {
            return minus ? -dividend : dividend;
        }
        long quotient = 0;
        /*
         * while (dividend >= divisor) {
         *     dividend = subtract(dividend, divisor);
         *     quotient++;
         * }
         */
        long resQuotient = 0;
        long initDivisor = divisor;
        while (dividend >= divisor) {
            divisor <<= 1;
            if (quotient == 0) {
                quotient = 1;
            }
            quotient <<= 1;
            if (dividend < divisor) {
                quotient >>= 1;
                divisor >>= 1;
                dividend -= divisor;
                divisor = initDivisor;
                resQuotient += quotient;
                quotient = 0;
            }
        }
        resQuotient += quotient;
        return minus ? -resQuotient : resQuotient;
    }

    @Test
    public void test10divide3() {
        Assertions.assertEquals(3, divide(10, 3));
    }

    @Test
    public void test7divideMinus3() {
        Assertions.assertEquals(-2, divide(7, -3));
    }

    @Test
    public void testMinus2147483648divideMinus1() {
        Assertions.assertEquals(2147483647, divide(-2147483648, -1));
    }

    @Test
    public void testMinus2147483648divide1() {
        Assertions.assertEquals(-2147483648, divide(-2147483648, 1));
    }

    @Test
    public void testMinus2147483648divide2() {
        Assertions.assertEquals(-1073741824, divide(-2147483648, 2));
    }
}
