package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Write a function that takes the binary representation of a positive integer and returns the number of
 * set bits it has (also known as the Hamming weight).
 *
 * @see <a href="https://leetcode.com/problems/number-of-1-bits/">191. Number of 1 Bits</a>
 */
public class L95_NumberOf1Bits {

    public int hammingWeight(int n) {
        return calcHammingWeight(n);
    }

    private static int calcHammingWeight(int n) {
        int count = 0;
        for (int i = 0; i < 32; i++) {
            int x = n >> i;
            if ((x & 1) == 1) {
                count++;
            }
        }
        return count;
    }

    @Test
    void test1() {
        Assertions.assertEquals(3, hammingWeight(11));
    }

    @Test
    void test2() {
        Assertions.assertEquals(1, hammingWeight(128));
    }

    @Test
    void test3() {
        Assertions.assertEquals(30, hammingWeight(2147483645));
    }
}
