package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 * Binary to String: Given a real number between {@code 0} and {@code 1} (e.g., {@code 0.72}) that is passed in as a double,
 * print the binary representation.
 * If the number cannot be represented accurately in binary with at most 32 characters, print {@code "ERROR"}.
 * <p>
 * Hints: #143, #167, #173, #269, #297
 * <ul>
 * <li>#143: To wrap your head around the problem, try thinking about how you'd do it for integers.</li>
 * <li>#167: In a number like {@code .893} (in base {@code 10}), what does each digit signify? What then does each digit in {@code .10010} signify in base {@code 2}?</li>
 * <li>#173: A number such as {@code .893} (in base {@code 10}) indicates {@code 8 * 10 ^-1 + 9 * 10 ^-2 + 3 * 10 ^-3}.
 * Translate this system into base {@code 2}</li>
 * <li>#269: How would you get the first digit in {@code .893}?
 * If you multiplied by {@code 10}, you'd shift the values over to get {@code 8.93}. What happens if you multiply by {@code 2}</li>
 * <li>#297: Think about what happens for values that can't be represented accurately in binary.</li>
 * </ul>
 * <p>
 * Created by @ssz on 02.11.2019.
 */
public class Q2_RealToBinaryString {

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test1(Data d) {
        test(d, 0.72);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test2(Data d) {
        test(d, 0.8723);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test4(Data d) {
        test(d, 0.3023);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test6(Data d) {
        test(d, 0.00433023);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test7(Data d) {
        test(d, 0.625);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test8(Data d) {
        test(d, 0.893);
    }

    private void test(Data data, double d) {
        System.out.println(data);
        System.out.println("given: " + d);
        String ex = data.toBinary(d);
        System.out.println("toBinary: " + ex);
        double x = data.fromBinary(ex);
        System.out.println("fromBinary: " + x);
        Assertions.assertEquals(d, x, 0.001);
    }

    enum Data {
        BRUTE_FORCE_V1 {
            @Override
            public String toBinary(final double d) {
                if (d < 0 || d > 1) throw new IllegalArgumentException("[0, 1]:" + d);
                double m = d;
                int e = 0;
                while (m < 1) {
                    m *= 2;
                    e++;
                }
                if (e > 255) { // 2^8 - 1 = 255
                    return "ERROR";
                }
                // 24 cells for rest number (2^24 - 1)
                int max = 1 << 24 - 1;
                while (m < max) {
                    m *= 2;
                }
                int x = ((int) m >> 1);
                return toBinaryString(e, 8) + toBinaryString(x, 24);
            }

            @Override
            public double fromBinary(final String s) {
                if (s.length() != 32)
                    throw new IllegalArgumentException();
                int exp = fromBinaryString(s.substring(0, 8), 8);
                // 14763950
                double m = fromBinaryString(s.substring(8), 24);
                while (m > 2) {
                    m /= 2;
                }
                for (int i = 0; i < exp; i++) {
                    m /= 2;
                }
                return m;
            }

            String toBinaryString(final int n, int b) {
                StringBuilder sb = new StringBuilder();
                for (int i = (b - 1); i >= 0; i--) {
                    sb.append((n & (1 << i)) == 0 ? "0" : "1");
                }
                return sb.toString();
            }

            int fromBinaryString(final String str, int b) {
                int res = 0;
                for (int i = (b - 1); i >= 0; i--) {
                    char c = str.charAt(str.length() - i - 1);
                    if (c == '1') {
                        res |= (1 << i);
                    }
                }
                return res;
            }
        },
        MODIFIED_FROM_BOOK {
            @Override
            public String toBinary(double num) {
                if (num < 0 || num > 1) {
                    throw new IllegalArgumentException("[0, 1]:" + num);
                }
                StringBuilder binary = new StringBuilder();
                while (num > 0) {
                    // Setting a limit on length: 32 characters
                    if (binary.length() >= 32) {
                        return "ERROR";
                    }
                    float r = (float) (num * 2); // differs from book
                    if (r >= 1) {
                        binary.append(1);
                        num = r - 1;
                    } else {
                        binary.append(0);
                        num = r;
                    }
                }
                return binary.toString();
            }

            @Override
            public double fromBinary(String s) {
                double res = 0;
                int d = 2;
                for (char c : s.toCharArray()) {
                    double x = c == '1' ? 1 : 0;
                    res += x / d;
                    d *= 2;
                }
                return res;
            }
        },
        ;

        public abstract String toBinary(double d);

        public abstract double fromBinary(String txt);
    }
}
