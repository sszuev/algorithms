package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 * 5.4 Next Number:
 * Given a positive integer, print the next smallest and the next largest number
 * that have the same number of {@code 1} bits in their binary representation.
 * Hints: #147, #175, #242, #312, #339, #358, #375, #390
 * <p>
 * #147: Get Next: Start with a brute force solution for each.
 * <p>
 * #175: Get Next: Picture a binary number-something with a bunch of {@code 1} s and {@code 0}s spread out throughout the number.
 * Suppose you flip a {@code 1} to a {@code 0} and a {@code 0} to a {@code 1}.
 * In what case will the number get bigger? In what case will it get smaller?
 * <p>
 * #242: Get Next: If you flip a {@code 1} to a {@code 0} and a {@code 0} to a {@code 1},
 * it will get bigger if the {@code 0} -> {@code 1} bit is more significant than the {@code 1} -> {@code 0} bit.
 * How can you use this to create the next biggest number (with the same number of {@code 1}s)?
 * <p>
 * #312: Get Next: Can you flip a {@code 0} to a {@code 1} to create the next biggest number?
 * <p>
 * #339: Get Next: Flipping a {@code 0} to a {@code 1} will create a bigger number.
 * The farther right the index is the smaller the bigger number is.
 * If we have a number like {@code 1001}, we want to flip the rightmost {@code 0} (to create {@code 1011}).
 * But if we have a number like {@code 1010}, we should not flip the rightmost {@code 1}.
 * <p>
 * #358: Get Next: We should flip the rightmost non-trailing {@code 0}.
 * The number {@code 1010} would become {@code 1110}.
 * Once we've done that, we need to flip a {@code 1} to a {@code 0} to make the number as small as possible,
 * but bigger than the original number ({@code 1010}).
 * What do we do? How can we shrink the number?
 * <p>
 * #375: Get Next: We can shrink the number by moving all the {@code 1}s
 * to the right of the flipped bit as far right as possible (removing a {@code 1} in the process).
 * <p>
 * #390: Get Previous: Once you've solved Get Next, try to invert the logic for Get Previous.
 * <p>
 * Created by @ssz on 17.08.2020.
 */
public class Q4_NextBinaryNumberWithSameDigits {

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testNext6(Data data) {
        testNext(6, 9, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testPrev6(Data data) {
        testPrev(6, 5, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test9(Data data) {
        testNext(9, 10, data);
        testPrev(9, 6, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testNext10(Data data) {
        testNext(10, 12, data);
        testPrev(10, 9, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testNext11(Data data) {
        testNext(11, 13, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testPrev11(Data data) {
        testPrev(11, 7, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testNext42(Data data) {
        testNext(42, 44, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testPrev42(Data data) {
        testPrev(42, 41, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testNext123456789(Data data) {
        testNext(123456789, 123456790, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testPrev123456789(Data data) {
        testPrev(123456789, 123456787, data);
    }

    // WARNING: DOES NOT WORK for examples FROM BOOK
    @ParameterizedTest
    @EnumSource(value = Data.class, mode = EnumSource.Mode.EXCLUDE, names = {"FROM_BOOK_ARITHMETIC", "FROM_BOOK_BIT_MANIPULATION"})
    public void test63(Data data) {
        testNext(63, 95, data);
        testPrev(63, -67108864, data);
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test13948(Data data) {
        testNext(13948, 13967, data);
        testPrev(13948, 13946, data);
    }

    private void testNext(int num, int next, Data data) {
        Assertions.assertEquals(next, data.getNext(num));
    }

    private void testPrev(int num, int prev, Data data) {
        Assertions.assertEquals(prev, data.getPrev(num));
    }

    public enum Data {
        MY_BRUTE_FORCE {
            @Override
            int getNext(int n) {
                int count = getCount(n);
                while (n < Integer.MAX_VALUE) {
                    if (getCount(++n) == count) return n;
                }
                throw new IllegalStateException();
            }

            @Override
            int getPrev(int n) {
                int count = getCount(n);
                while (n > Integer.MIN_VALUE) {
                    if (getCount(--n) == count) return n;
                }
                throw new IllegalStateException();
            }
        },

        MY_BIT_MANIPULATION {
            @Override
            int getNext(int n) {
                int i1 = findO1(n);
                int i2 = findRightmost1(n);
                int tail = getTail(n, i1);
                int appender = (2 << i1) ^ (tail >> i2);
                int i3 = i1 + 2;
                return ((n >>> i3) << i3) | appender;
            }

            @Override
            int getPrev(int n) {
                int i1 = find10(n);
                if (i1 < 0) {
                    return n << countLeading0(n);
                }
                int i2 = count0(n, i1 - 1);
                int tail = getTail(n << i2, i1);
                int appender = (1 << i1) ^ tail;
                int i3 = i1 + 2;
                return ((n >>> i3) << i3) | appender;
            }

            int findO1(int n) {
                for (int i = 0; i < 31; i++) {
                    if (getBit(n, i) == 1 && getBit(n, i + 1) == 0) return i;
                }
                throw new IllegalStateException("Can't find 01 in " + Integer.toBinaryString(n));
            }

            int find10(int n) {
                for (int i = 0; i < 31; i++) {
                    if (getBit(n, i) == 0 && getBit(n, i + 1) == 1) return i;
                }
                return -1;
            }

            int findRightmost1(int n) {
                for (int i = 0; i < 32; i++) {
                    if (getBit(n, i) == 1) return i;
                }
                throw new IllegalStateException("Can't find first 1 in " + Integer.toBinaryString(n));
            }

            int count0(int n, int start) {
                int res = 0;
                for (int i = start; i >= 0; i--) {
                    if (getBit(n, i) == 1) {
                        return res;
                    }
                    res++;
                }
                return res;
            }

            int countLeading0(int n) {
                int res = 0;
                for (int i = 31; i >= 0; i--) {
                    if (getBit(n, i) == 1) {
                        return res;
                    }
                    res++;
                }
                return res;
            }
        },

        FROM_BOOK_BIT_MANIPULATION {
            @Override
            int getNext(int n) {
                // compute c0 and c1
                int c = n;
                int c0 = 0;
                int c1 = 0;
                while (((c & 1) == 0) && (c != 0)) {
                    c0++;
                    c >>= 1;
                }

                while ((c & 1) == 1) {
                    c1++;
                    c >>= 1;
                }

                // Error: if n == 11 .. 1100 ... 00, then there is no bigger number with the same number of 1s.
                if (((c0 + c1) == 31) || ((c0 + c1) == 0)) {
                    return -1;
                }

                int p = c0 + c1; // position of rightmost non-trailing zero
                n |= (1 << p); // flip rightmost non-trailing zero
                //noinspection PointlessBitwiseExpression
                n &= ~((1 << p) - 1); // clear all bits to the right of p
                n |= (1 << (c1 - 1)) - 1; // insert (c1-1) ones on the right
                return n;
            }

            @Override
            int getPrev(int n) {
                // To implement getPrev, we follow a very similar approach.
                // 1. Compute c0 and c1. Note that c1 is the number of trailing ones,
                // and c0 is the size of the block of zeros immediately to the left of the trailing ones.
                // 2. Flip the rightmost non-trailing one to a zero. This will be at position p = c1 + c0.
                // 3. Clear all bits to the right of bit p.
                // 4. Insert c1 + 1 ones immediately to the right of position p.

                int temp = n;
                int c0 = 0;
                int c1 = 0;
                while ((temp & 1) == 1) {
                    c1++;
                    temp >>= 1;
                }
                if (temp == 0) {
                    return -1;
                }
                while (((temp & 1) == 0) && (temp != 0)) {
                    c0++;
                    temp >>= 1;
                }
                int p = c0 + c1; // position of rightmost non-trailing one
                n &= ((~0) << (p + 1)); // clears from bit p onwards
                int mask = (1 << (c1 + 1)) - 1; // Sequence of (cl+l) ones
                n |= mask << (c0 - 1);
                return n;
            }
        },

        FROM_BOOK_ARITHMETIC {
            @Override
            int getNext(int n) {
                int temp = n;
                int c0 = 0;
                int c1 = 0;
                while (((temp & 1) == 0) && (temp != 0)) {
                    c0++;
                    temp >>= 1;
                }
                while ((temp & 1) == 1) {
                    c1++;
                    temp >>= 1;
                }
                if (((c0 + c1) == 31) || ((c0 + c1) == 0)) {
                    return -1;
                }
                return n + (1 << c0) + (1 << (c1 - 1)) - 1;
            }

            @Override
            int getPrev(int n) {
                int temp = n;
                int c0 = 0;
                int c1 = 0;
                while ((temp & 1) == 1) {
                    c1++;
                    temp >>= 1;
                }
                if (temp == 0) {
                    return -1;
                }
                while (((temp & 1) == 0) && (temp != 0)) {
                    c0++;
                    temp >>= 1;
                }
                return n - (1 << c1) - (1 << (c0 - 1)) + 1;
            }
        };

        abstract int getNext(int n);

        abstract int getPrev(int n);

        static int getCount(int n) {
            int res = 0;
            for (int i = 0; i < 32; i++) {
                res += getBit(n, i);
            }
            return res;
        }

        static int getBit(int n, int i) {
            return (n >>> i) & 1;
        }

        static int getTail(int n, int i) {
            return n & (Integer.MAX_VALUE >>> (31 - i));
        }
    }
}
