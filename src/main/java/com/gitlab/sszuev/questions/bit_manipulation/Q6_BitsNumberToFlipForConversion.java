package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 * 5.6: Conversion:
 * Write a function to determine the number of bits you would need to flip to convert integer A to integer B.
 * EXAMPLE
 * Input: 29 (or: 11101), 15 (or: 01111)
 * Output: 2
 * Hints: #336, #369
 *
 * <ul>
 * <li>#336: How would you figure out how many bits are different between two numbers?</li>
 * <li>#369: Think about what an XOR indicates. If you do {@code a XOR b}, where does the result have {@code 1}s?
 * Where does it have {@code O}s?</li>
 * </ul>
 * <p>
 * Created by @ssz on 27.06.2020.
 */
public class Q6_BitsNumberToFlipForConversion {

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test1(Data data) {
        Assertions.assertEquals(2, data.getBitDiff(29, 15));
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test2(Data data) {
        Assertions.assertEquals(5, data.getBitDiff(2354, 3432));
    }

    enum Data {
        MY {
            @Override
            int getBitDiff(int a, int b) {
                int x = a ^ b;
                int res = 0;
                for (int i = 0; i < 32; i++) {
                    if (getBit(x, i)) res++;
                }
                return res;
            }
        },
        FROM_BOOK {
            @Override
            int getBitDiff(int a, int b) {
                // The operation c = c & (c -1) will clear the least significant bit in c
                int res = 0;
                for (int c = a ^ b; c != 0; c = c & (c - 1)) {
                    res++;
                }
                return res;
            }
        };

        abstract int getBitDiff(int a, int b);

        static boolean getBit(int num, int i) {
            return ((num >> i) & 1) != 0;
        }
    }
}
