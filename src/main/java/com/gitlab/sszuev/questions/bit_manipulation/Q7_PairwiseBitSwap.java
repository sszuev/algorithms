package com.gitlab.sszuev.questions.bit_manipulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Pairwise Swap: Write a program to swap odd and even bits in an integer with as few instructions as possible
 * (e.g., bit {@code 0} and bit {@code 1} are swapped, bit {@code 2} and bit {@code 3} are swapped, and so on).
 * Hints: #145, #248, #328, #355
 * <p>
 * <ul>
 * <li>#145: Swapping each pair means moving the even bits to the left and the odd bits to the right.
 * Can you break this problem into parts?</li>
 * <li>#248: Can you create a number that represents just the even bits? Then can you shift the even bits over by one?</li>
 * <li>#328: The value {@code 1010} in binary is {@code 10} in decimal or {@code OxA} in hex.
 * What will a sequence of {@code 101010...} be in hex?
 * That is, how do you represent an alternating sequence of {@code 1}s and {@code O}s with {@code 1}s in the odd places?
 * How do you do this for the reverse ({@code 1}s in the even spots)?</li>
 * <li>#355: Try masks {@code 0xaaaaaaaa} and {@code 0x55555555} to select the even and odd bits.
 * Then try shifting the even and odd bits around to create the right number.</li>
 * </ul>
 * Created by @ssz on 02.11.2019.
 */
public class Q7_PairwiseBitSwap {

    static int pairwiseBitSwap_V1_BF(int x) {
        int mask1 = 0b1010_1010_1010_1010_1010_1010_1010_1010; //  0xaaaaaaaa
        int mask2 = mask1 >>> 1; // 0x55555555
        return ((x & mask1) >>> 1) | ((x & mask2) << 1);
    }

    int pairwiseBitSwap(int x) {
        return pairwiseBitSwap_V1_BF(x);
    }

    @Test
    public void test2123235() {
        test(2123235);
    }

    @Test
    public void test98789723() {
        test(98789723);
    }

    private void test(int ex) {
        String sex = toBinaryString(ex);
        System.out.println("Given: " + ex + " => " + sex);
        int ac = pairwiseBitSwap(ex);
        String sac = toBinaryString(ac);
        System.out.println("Actual: " + ac + " => " + sac);
        for (int i = 0; i < sex.length(); i += 2) {
            Assertions.assertEquals(sex.charAt(i), sac.charAt(i + 1));
            Assertions.assertEquals(sex.charAt(i + 1), sac.charAt(i));
        }
    }

    private static String toBinaryString(int v) {
        String res = Integer.toBinaryString(v);
        if (res.length() == 32) return res;
        return IntStream.rangeClosed(1, 32 - res.length()).mapToObj(x -> "0").collect(Collectors.joining()) + res;
    }

}
