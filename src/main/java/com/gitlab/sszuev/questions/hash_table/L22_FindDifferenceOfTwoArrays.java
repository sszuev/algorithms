package com.gitlab.sszuev.questions.hash_table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Given two 0-indexed integer arrays nums1 and nums2, return a list answer of size 2 where:
 * <ul>
 * <li>answer[0] is a list of all distinct integers in nums1 which are not present in nums2.</li>
 * <li>answer[1] is a list of all distinct integers in nums2 which are not present in nums1.</li>
 * </ul>
 * Note that the integers in the lists may be returned in any order.
 * <p>
 * Constraints:
 * 1) 1 <= nums1.length, nums2.length <= 1000
 * 2) -1000 <= nums1[i], nums2[i] <= 1000
 *
 * @see <a href="https://leetcode.com/problems/find-the-difference-of-two-arrays">2215. Find the Difference of Two Arrays</a>
 */
public class L22_FindDifferenceOfTwoArrays {
    public List<List<Integer>> findDifference(int[] nums1, int[] nums2) {
        return difference(nums1, nums2);
    }

    public static List<List<Integer>> difference(int[] nums1, int[] nums2) {
        List<List<Integer>> res = new ArrayList<>();
        res.add(new ArrayList<>());
        res.add(new ArrayList<>());
        Map<Integer, Boolean> withNum1 = new HashMap<>(2 * nums1.length);
        Set<Integer> withNum2 = new HashSet<>(2 * nums2.length);
        for (int n : nums1) {
            withNum1.put(n, false);
        }
        for (int n : nums2) {
            if (!withNum1.containsKey(n)) {
                if (!withNum2.contains(n)) {
                    res.get(1).add(n);
                }
            }
            withNum2.add(n);
        }
        for (int n : nums1) {
            if (!withNum2.contains(n)) {
                if (!withNum1.get(n)) {
                    res.get(0).add(n);
                    withNum1.put(n, true);
                }
            }
        }
        return res;
    }

    @Test
    void test1_2_3_and_2_4_6() {
        Assertions.assertEquals(
                List.of(List.of(1, 3), List.of(4, 6)),
                findDifference(new int[]{1, 2, 3}, new int[]{2, 4, 6})
                        .stream()
                        .peek(it -> it.sort(Integer::compareTo))
                        .collect(Collectors.toList())
        );
    }

    @Test
    void test1_2_3_3_and_1_1_2_2() {
        Assertions.assertEquals(
                List.of(List.of(3), List.of()),
                findDifference(new int[]{1, 2, 3, 3}, new int[]{1, 1, 2, 2})
        );
    }
}
