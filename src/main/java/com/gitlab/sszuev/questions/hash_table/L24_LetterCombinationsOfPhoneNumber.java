package com.gitlab.sszuev.questions.hash_table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent. Return the answer in any order.
 * <p>
 * A mapping of digits to letters (just like on the telephone buttons) is {@link L24_LetterCombinationsOfPhoneNumber#MAPPING}.
 * Note that 1 does not map to any letters.
 * <p>
 * Constraints:
 * 1) 0 <= digits.length <= 4
 * 2) digits[i] is a digit in the range ['2', '9'].
 *
 * @see <a href="https://leetcode.com/problems/letter-combinations-of-a-phone-number">17. Letter Combinations of a Phone Number</a>
 */
public class L24_LetterCombinationsOfPhoneNumber {

    public List<String> letterCombinations(String digits) {
        return findLetterCombinations(digits(digits));
    }

    private static final Map<String, List<String>> MAPPING = Map.of(
            "1", List.of(),
            "2", List.of("a", "b", "c"),
            "3", List.of("d", "e", "f"),
            "4", List.of("g", "h", "i"),
            "5", List.of("j", "k", "l"),
            "6", List.of("m", "n", "o"),
            "7", List.of("p", "q", "r", "s"),
            "8", List.of("t", "u", "v"),
            "9", List.of("w", "x", "y", "z")
    );

    private static List<String> findLetterCombinations(List<String> digits) {
        if (digits.isEmpty()) {
            return List.of();
        }

        List<List<String>> res1 = new ArrayList<>();
        res1.add(new ArrayList<>());
        List<List<String>> res2 = new ArrayList<>();
        List<List<String>> tmp;

        for (String digit : digits) {
            List<String> letters = MAPPING.get(digit);
            if (letters == null) {
                throw new IllegalArgumentException("Invalid digit: " + digit);
            }

            res2.clear();
            for (List<String> prev : res1) {
                for (String letter : letters) {
                    List<String> n = new ArrayList<>(prev);
                    n.add(letter);
                    res2.add(n);
                }
            }
            tmp = res1;
            res1 = res2;
            res2 = tmp;
        }

        return join(res1);
    }

    private static List<String> digits(String digits) {
        char[] chars = digits.toCharArray();
        List<String> res = new ArrayList<>();
        for (char ch : chars) {
            res.add(String.valueOf(ch));
        }
        return res;
    }

    private static List<String> join(List<List<String>> init) {
        List<String> res = new ArrayList<>();
        init.forEach(it -> res.add(String.join("", it)));
        return res;
    }


    @Test
    void testEmpty() {
        List<String> expected = List.of();
        List<String> actual = letterCombinations("");
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test2() {
        List<String> expected = List.of("a", "b", "c");
        List<String> actual = letterCombinations("2");
        Assertions.assertEquals(expected, actual.stream().sorted().collect(Collectors.toList()));
    }

    @Test
    void test23() {
        List<String> expected = List.of("ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf");
        List<String> actual = letterCombinations("23");
        Assertions.assertEquals(expected, actual.stream().sorted().collect(Collectors.toList()));
    }

    @Test
    void test234() {
        List<String> expected = List.of(
                "adg", "adh", "adi", "aeg", "aeh", "aei", "afg", "afh", "afi", "bdg", "bdh", "bdi", "beg",
                "beh", "bei", "bfg", "bfh", "bfi", "cdg", "cdh", "cdi", "ceg", "ceh", "cei", "cfg", "cfh", "cfi"
        );
        List<String> actual = letterCombinations("234");
        Assertions.assertEquals(expected, actual.stream().sorted().collect(Collectors.toList()));
    }
}
