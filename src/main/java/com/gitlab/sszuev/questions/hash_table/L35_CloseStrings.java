package com.gitlab.sszuev.questions.hash_table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * Two strings are considered close if you can attain one from the other using the following operations:
 * <p>
 * Operation 1: Swap any two existing characters.For example, {@code abcde -> aecdb}
 * <p>
 * Operation 2: Transform every occurrence of one existing character into another existing character,
 * and do the same with the other character.
 * For example, {@code aacabb -> bbcbaa} (all {@code a}'s turn into {@code b}'s, and all {@code b}'s turn into {@code a}'s)
 * <p>
 * You can use the operations on either string as many times as necessary.
 * <p>
 * Given two strings, word1 and word2, return true if word1 and word2 are close, and false otherwise
 * <p>
 * Constraints: 1 <= word1.length, word2.length <= 10^5; word1 and word2 contain only lowercase English letters.
 *
 * @see <a href="https://leetcode.com/problems/determine-if-two-strings-are-close">1657. Determine if Two Strings Are Close</a>
 */
public class L35_CloseStrings {
    public boolean closeStrings(String word1, String word2) {
        return areStringsClose(word1, word2);
    }

    public static boolean areStringsClose(String word1, String word2) {
        if (word1.length() != word2.length()) {
            return false;
        }
        int[] map1 = calcFrequencyMap(word1);
        int[] map2 = new int[map1.length];
        if (!calcFrequencyMap(word2, map2, map1)) {
            return false;
        }
        Arrays.sort(map1);
        Arrays.sort(map2);
        return Arrays.equals(map1, map2);
    }

    private static int[] calcFrequencyMap(String word) {
        int[] res = new int[26];
        for (char b : word.toCharArray()) {
            res[b - 97]++;
        }
        return res;
    }

    private static boolean calcFrequencyMap(String word, int[] res, int[] first) {
        for (char b : word.toCharArray()) {
            int i = b - 97;
            if (first[i] == 0) {
                return false;
            }
            res[i]++;
        }
        return true;
    }

    @Test
    void test1() {
        boolean actual = closeStrings("abc", "bca");
        Assertions.assertTrue(actual);
    }

    @Test
    void test2() {
        boolean actual = closeStrings("a", "aa");
        Assertions.assertFalse(actual);
    }

    @Test
    void test3() {
        boolean actual = closeStrings("cabbba", "abbccc");
        Assertions.assertTrue(actual);
    }

    @Test
    void test4() {
        boolean actual = closeStrings("aaabbbbccddeeeeefffff", "aaaaabbcccdddeeeeffff");
        Assertions.assertFalse(actual);
    }

    @Test
    void test5() {
        boolean actual = closeStrings("cabbba", "abbccx");
        Assertions.assertFalse(actual);
    }
}
