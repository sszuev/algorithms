package com.gitlab.sszuev.questions.hash_table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Given a 0-indexed n x n integer matrix grid,
 * return the number of pairs (ri, cj) such that row ri and column cj are equal.
 * A row and column pair is considered equal
 * if they contain the same elements in the same order (i.e., an equal array).
 * <p>
 * Constraints: `n == grid.length == grid[i].length`; `1 <= n <= 200`; `1 <= grid[i][j] <= 10^5`
 *
 * @see <a href="https://leetcode.com/problems/equal-row-and-column-pairs/">2352. Equal Row and Column Pairs</a>
 */
public class L45_EqualRowAndColumnPairs {

    public int equalPairs(int[][] grid) {
        return calcEqualPairs(grid);
    }

    private static int calcEqualPairs(int[][] grid) {
        int res = 0;
        Map<List<Integer>, Integer> rows = new HashMap<>();
        for (int[] row : grid) {
            List<Integer> x = Arrays.stream(row).boxed().collect(Collectors.toUnmodifiableList());
            rows.compute(x, (k, v) -> v == null ? 1 : v + 1);
        }
        for (int i = 0; i < grid.length; i++) {
            List<Integer> column = new ArrayList<>();
            for (int[] ints : grid) {
                column.add(ints[i]);
            }
            Integer x = rows.get(column);
            if (x != null) {
                res += x;
            }
        }
        return res;
    }

    @Test
    void test1() {
        int actual = equalPairs(new int[][]{{3, 2, 1}, {1, 7, 6}, {2, 7, 7}});
        Assertions.assertEquals(1, actual);
    }

    @Test
    void test2() {
        int actual = equalPairs(new int[][]{{3, 1, 2, 2}, {1, 4, 4, 5}, {2, 4, 2, 2}, {2, 4, 2, 2}});
        Assertions.assertEquals(3, actual);
    }
}
