package com.gitlab.sszuev.questions.hash_table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * You are given an integer array `nums` and an integer `k`.
 * In one operation, you can pick two numbers from the array whose sum equals `k` and remove them from the array.
 * Return the maximum number of operations you can perform on the array.
 * <p>
 * Constraints: `1 <= nums.length <= 10^5`; `1 <= nums[i] <= 10^9`; `1 <= k <= 10^9`
 *
 * @see <a href="https://leetcode.com/problems/max-number-of-k-sum-pairs/">1679. Max Number of K-Sum Pairs</a>
 */
public class L53_MaxNumberOfKSumPairs {

    public int maxOperations(int[] nums, int k) {
        return calcMaxOperations(nums, k);
    }

    private static int calcMaxOperations(int[] nums, int k) {
        Map<Integer, Integer> numSet = new HashMap<>();
        for (int n : nums) {
            numSet.compute(n, (key, v) -> v == null ? 1 : ++v);
        }
        int res = 0;
        for (int n : nums) {
            Integer count1 = numSet.get(n);
            if (count1 <= 0) {
                continue;
            }
            numSet.put(n, count1 - 1);
            Integer count2 = numSet.get(k - n);
            if (count2 == null || count2 <= 0) {
                numSet.put(n, count1);
                continue;
            }
            res++;
            numSet.put(k - n, count2 - 1);
        }
        return res;
    }

    @Test
    void test1() {
        int actual = maxOperations(new int[]{1, 2, 3, 4}, 5);
        Assertions.assertEquals(2, actual);
    }

    @Test
    void test2() {
        int actual = maxOperations(new int[]{3, 1, 3, 4, 3}, 6);
        Assertions.assertEquals(1, actual);
    }
}
