package com.gitlab.sszuev.questions.hash_table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Given an array of integers `arr`,
 * return `true` if the number of occurrences of each value in the array is unique or false otherwise
 *
 * @see <a href="https://leetcode.com/problems/unique-number-of-occurrences">1207. Unique Number of Occurrences</a>
 */
public class L90_UniqueNumberOfOccurrences {
    public boolean uniqueOccurrences(int[] arr) {
        return calcUniqueOccurrences(arr);
    }

    private static boolean calcUniqueOccurrences(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int j : arr) {
            if (map.containsKey(j)) {
                map.put(j, map.get(j) + 1);
            } else {
                map.put(j, 1);
            }
        }
        Set<Integer> set = new HashSet<>();
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (set.add(entry.getValue())) {
                continue;
            }
            return false;
        }
        return true;
    }

    @Test
    void test1() {
        // Input: arr = [1,2,2,1,1,3]
        // Output: true
        int[] arr = {1, 2, 2, 1, 1, 3};
        Assertions.assertTrue(uniqueOccurrences(arr));
    }

    @Test
    void test2() {
        // Input: arr = [1,2]
        // Output: false
        int[] arr = {1, 2};
        Assertions.assertFalse(uniqueOccurrences(arr));
    }

    @Test
    void test3() {
        // Input: arr = [-3,0,1,-3,1,1,1,-3,10,0]
        // Output: true
        int[] arr = {-3, 0, 1, -3, 1, 1, 1, -3, 10, 0};
        Assertions.assertTrue(uniqueOccurrences(arr));
    }

}
