package com.gitlab.sszuev.questions.hash_table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Given an array nums of size n, return the majority element.
 * The majority element is the element that appears more than ⌊n / 2⌋ times.
 * You may assume that the majority element always exists in the array.
 * <p>
 * Constraints:
 * - `n == nums.length`
 * - `1 <= n <= 5 * 10^4`
 * - `-10^9 <= nums[i] <= 10^9`
 *
 * @see <a href="https://leetcode.com/problems/majority-element">169. Majority Element</a>
 */
public class L97_MajorityElement {
    public int majorityElement(int[] nums) {
        return calcMajorityElement(nums);
    }

    private static int calcMajorityElement(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            } else {
                map.put(num, 1);
            }
        }
        int item = 0;
        int max = 0;
        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            if (e.getValue() > max) {
                max = e.getValue();
                item = e.getKey();
            }
        }
        return item;
    }

    @Test
    void test1() {
        int actual = majorityElement(new int[]{3, 2, 3});
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test2() {
        int actual = majorityElement(new int[]{2, 2, 1, 1, 1, 2, 2});
        Assertions.assertEquals(2, actual);
    }
}
