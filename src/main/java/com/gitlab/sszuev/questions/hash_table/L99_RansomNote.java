package com.gitlab.sszuev.questions.hash_table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Given two strings `ransomNote` and `magazine`,
 * return `true` if `ransomNote` can be constructed by using the letters from magazine and `false` otherwise.
 * Each letter in `magazine` can only be used once in ransomNote.
 * <p>
 * Constraints:
 * - `1 <= ransomNote.length, magazine.length <= 10^5`
 * - `ransomNote` and `magazine` consist of lowercase English letters
 *
 * @see <a href="https://leetcode.com/problems/ransom-note">383. Ransom Note</a>
 */
public class L99_RansomNote {
    public boolean canConstruct(String ransomNote, String magazine) {
        return calcCanConstruct(ransomNote, magazine);
    }

    private static boolean calcCanConstruct(String ransomNote, String magazine) {
        Map<Character, Integer> letters = new HashMap<>();
        for (char c : magazine.toCharArray()) {
            if (letters.containsKey(c)) {
                letters.put(c, letters.get(c) + 1);
            } else {
                letters.put(c, 1);
            }
        }
        for (char c: ransomNote.toCharArray()) {
            if (letters.containsKey(c)) {
                int count = letters.get(c) - 1;
                if (count == 0) {
                    letters.remove(c);
                } else {
                    letters.put(c, count);
                }
            } else {
                return false;
            }
        }
        return true;
    }

    @Test
    void test1() {
        Assertions.assertFalse(canConstruct("a", "b"));
    }

    @Test
    void test2() {
        Assertions.assertFalse(canConstruct("aa", "ab"));
    }

    @Test
    void test3() {
        Assertions.assertTrue(canConstruct("aa", "aab"));
    }
}
