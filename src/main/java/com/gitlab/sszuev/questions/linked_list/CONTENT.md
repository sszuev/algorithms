[HOME](../../../../../../../../README.md)

- **[T1](T1_FindMiddle.java)**: How to find the middle element of a linked list in single pass
- **[T2](T2_CheckForCycle.java)**: How to find if a linked list contains a loop
- **[T3](T3_Reverse.java)**: How to reverse a linked list using recursion and iteration
- **[T4](T4_RemoveDuplicates.java)**: Remove duplicates from an unsorted linked list (with/without temporary buffer)
- **[T5](T5_SinglyLinkedListLength.java)**: How to find length of a singly linked list using Loop and recursion
- **[T6](T6_ArbitraryNode.java)**: How to find the k-th node from the end of a linked list
- **[T7](T7_NumberRepresentedByLinkedList.java)**: Add two numbers represented by linked lists
- **[L4](L4_AddTwoNumbers.java)**: (leetcode): Add Two Numbers

- **[Q3](Q3_DeleteMiddle.java)**: Delete Middle Node: Implement an algorithm to delete a node in the middle 
(i.e., any node but the first and last node, not necessarily the exact middle) of a singly linked list, given only access to that node. 
EXAMPLE
Input: the node `c` from the linked list `a -> b -> c -> d -> e -> f`
Result: nothing is returned, but the new linked list looks like `a -> b -> d -> e -> f`  
  
  <details>
    <summary>Hints</summary>  

    - ***73***: Picture the list `1 -> 5 -> 9 -> 12`. Removing `9` would make it look like `1 -> 5 -> 12`. 
You only have access to the `9` node. Can you make it look like the correct answer?

  </details>

- **[Q6](Q6_IsPalindrome.java)**: Palindrome: Implement a function to check if a linked list is a palindrome.

  <details>
    <summary>Hints</summary>  

  - ***5***: A palindrome is something which is the same when written forwards and backwards. 
What if you reversed the linked list?
  - ***13***: Try using a stack.
  - ***29***: Assume you have the length of the linked list. Can you implement this recursively?
  - ***61***: In the recursive approach (we have the length of the list), the middle is the base case: 
`isPermutation(middle)` is true. The node `x` to the immediate left of the middle. 
What can that node do to check if `x -> middle -> y` forms a palindrome? 
Now suppose that checks out. What about the previous node `a`? 
If `x -> middle -> y` is a palindrome, how can it check that `a -> x -> middle -> y -> b` is a palindrome?
  - ***101***: Go back to the previous hint. Remember: there are ways to return multiple values. You can do this with a new class.

  </details>

- **[L23](L23_RemoveMiddleItem.java)**: Delete the Middle Node of a Linked List

- **[L51](L51_OddEvenLinkedList.java)**: Odd Even Linked List

- **[L74](L74_MaximumTwinSumOfLinkedList.java)**: Maximum Twin Sum of a Linked List

- **[L91](L91_ReverseLinkedList.java)**: Reverse Linked List

- **[L102](L102_LinkedListCycle.java)**: Linked List Cycle