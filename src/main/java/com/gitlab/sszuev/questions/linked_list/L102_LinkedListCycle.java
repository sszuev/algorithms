package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Given `head`, the head of a linked list, determine if the linked list has a cycle in it.
 * There is a cycle in a linked list if there is some node in the list that can be reached again
 * by continuously following the next pointer.
 * Internally, `pos` is used to denote the index of the node that tail's next pointer is connected to.
 * Note that `pos` is not passed as a parameter.
 * Return `true` if there is a cycle in the linked list. Otherwise, return `false`.
 *
 * Constraints:
 *     The number of the nodes in the list is in the range `[0, 10^4]`.
 *     `-10^5 <= Node.val <= 10^5`
 *     `pos` is -1 or a valid index in the linked-list.
 *
 * @see <a href="https://leetcode.com/problems/linked-list-cycle">141. Linked List Cycle</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L102_LinkedListCycle {
    public boolean hasCycle(ListNode head) {
        return calcHasCycle(head);
    }

    private static boolean calcHasCycle(ListNode head) {
        if (head == null) {
            return false;
        }
        ListNode slow = head;
        ListNode fast = head.next;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                return true;
            }
        }
        return false;
    }

    @Test
    void test1() {
        // Input: head = [3,2,0,-4], pos = 1
        // Output: true
        ListNode head = new ListNode(3);
        ListNode cycle = head.add(2);
        cycle.add(0).add(-4).add(cycle);
        Assertions.assertTrue(hasCycle(head));
    }

    @Test
    void test2() {
        // Input: head = [1,2], pos = 0
        // Output: true
        ListNode head = new ListNode(1);
        ListNode cycle = head.add(2);
        cycle.add(head);
        Assertions.assertTrue(hasCycle(head));
    }

    @Test
    void test3() {
        // Input: head = [1], pos = -1
        // Output: false
        ListNode head = new ListNode(1);
        Assertions.assertFalse(hasCycle(head));
    }

    @SuppressWarnings("unused")
    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        ListNode add(int val) {
            ListNode res = new ListNode(val);
            next = res;
            return res;
        }

        ListNode add(ListNode next) {
            this.next = next;
            return next;
        }

        public List<Integer> toList() {
            ListNode node = this;
            List<Integer> res = new ArrayList<>();
            while (node != null) {
                res.add(node.val);
                node = node.next;
            }
            return res;
        }

        @Override
        public String toString() {
            return "[" + val + "]" + next;
        }
    }
}
