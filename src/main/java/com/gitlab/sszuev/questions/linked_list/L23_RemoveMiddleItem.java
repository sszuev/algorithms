package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You are given the head of a linked list. Delete the middle node, and return the head of the modified linked list.
 * The middle node of a linked list of size n is the ⌊n / 2⌋th node from the start using 0-based indexing,
 * where ⌊x⌋ denotes the largest integer less than or equal to x.
 * For n = 1, 2, 3, 4, and 5, the middle nodes are 0, 1, 1, 2, and 2, respectively.
 * <p>
 * Constraints:
 * 1) The number of nodes in the list is in the range [1, 10^5].
 * 2) 1 <= Node.val <= 10^5
 *
 * @see <a href="https://leetcode.com/problems/delete-the-middle-node-of-a-linked-list">2095. Delete the Middle Node of a Linked List</a>
 */
public class L23_RemoveMiddleItem {

    @SuppressWarnings("ClassEscapesDefinedScope")
    public ListNode deleteMiddle(ListNode head) {
        return removeMiddleItem(head);
    }

    private static ListNode removeMiddleItem(ListNode head) {
        ListNode before = findBeforeMiddle(head);
        if (before == null) {
            return null;
        }
        ListNode current = before.next;
        before.next = current == null ? null : current.next;
        return head;
    }

    private static ListNode findBeforeMiddle(ListNode head) {
        ListNode node = head;
        ListNode res = head;
        ListNode prev = null;
        int index = 0;
        while (node.next != null) {
            if ((index++) % 2 == 0) {
                prev = res;
                res = res.next;
            }
            node = node.next;
        }
        return prev;
    }

    @Test
    void test1_2_3_4() {
        ListNode list = new ListNode(1);
        list.add(2).add(3).add(4);
        String actual = deleteMiddle(list).toString();
        String expected = "[1][2][4]null";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test2_1() {
        ListNode list = new ListNode(2);
        list.add(1);
        String actual = deleteMiddle(list).toString();
        String expected = "[2]null";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test1() {
        ListNode list = new ListNode(1);
        ListNode actual = deleteMiddle(list);
        Assertions.assertNull(actual);
    }

    @Test
    void test1_3_4_7_1_2_6() {
        ListNode list = new ListNode(1);
        list.add(3).add(4).add(7).add(1).add(2).add(6);
        String actual = deleteMiddle(list).toString();
        String expected = "[1][3][4][1][2][6]null";
        Assertions.assertEquals(expected, actual);
    }

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        ListNode add(int val) {
            ListNode res = new ListNode(val);
            next = res;
            return res;
        }

        @Override
        public String toString() {
            return "[" + val + "]" + next;
        }
    }

}
