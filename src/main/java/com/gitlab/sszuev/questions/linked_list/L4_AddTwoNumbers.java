package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;

/**
 * You are given two non-empty linked lists representing two non-negative integers.
 * The digits are stored in reverse order, and each of their nodes contains a single digit.
 * Add the two numbers and return the sum as a linked list.
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 *
 * @see <a href="https://leetcode.com/problems/add-two-numbers">2. Add Two Numbers</a>
 */
public class L4_AddTwoNumbers {

    public ListNode addTwoNumbers(ListNode left, ListNode right) {
        ListNode current;
        ListNode prev = null;
        ListNode res = null;
        int remainder = 0;
        while (left != null || right != null) {
            int leftVal = left == null ? 0 : left.val;
            int rightVal = right == null ? 0 : right.val;
            int sum = leftVal + rightVal + remainder;
            int resValue;
            if (sum >= 10) {
                remainder = 1;
                resValue = sum - 10;
            } else {
                remainder = 0;
                resValue = sum;
            }
            current = new ListNode(resValue);
            if (res == null) {
                res = current;
            }
            if (prev != null) {
                prev.next = current;
            }
            prev = current;
            left = left == null ? null : left.next;
            right = right == null ? null : right.next;
        }
        if (remainder != 0) {
            current = new ListNode(1);
            prev.next = current;
        }
        return Objects.requireNonNull(res);
    }

    @Test
    public void test342add465() {
        // 342 + 465 = 807 : 2->4->3 + 5->6->4 = 7->0->8
        ListNode res = addTwoNumbers(ListNode.of("342"), ListNode.of("465"));
        Assertions.assertEquals(ListNode.of("807"), res);
    }

    @Test
    public void test0add0() {
        // 0 + 0 = 0
        ListNode res = addTwoNumbers(ListNode.of("0"), ListNode.of("0"));
        Assertions.assertEquals(ListNode.of("0"), res);
    }

    @Test
    public void test9999999add9999() {
        // 9999999 + 9999 = 10009998
        ListNode res = addTwoNumbers(ListNode.of("9999999"), ListNode.of("9999"));
        Assertions.assertEquals(ListNode.of("10009998"), res);
    }

    public static class ListNode {
        final int val;
        ListNode next;

        public ListNode(int value) {
            this.val = value;
        }

        public static ListNode of(String n) {
            if (!n.matches("\\d+")) {
                throw new IllegalArgumentException();
            }
            char[] chars = n.toCharArray();
            ListNode current = null;
            ListNode prev = null;
            for (char x : chars) {
                current = new ListNode(Integer.parseInt(String.valueOf(x)));
                if (prev != null) {
                    current.next = prev;
                }
                prev = current;
            }
            if (current == null) {
                throw new IllegalStateException();
            }
            return current;
        }

        @Override
        public String toString() {
            if (next == null) return String.valueOf(val);
            return val + "[" + next + "]";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ListNode)) return false;

            ListNode listNode = (ListNode) o;

            if (val != listNode.val) return false;
            return Objects.equals(next, listNode.next);
        }

        @Override
        public int hashCode() {
            int result = val;
            result = 31 * result + (next != null ? next.hashCode() : 0);
            return result;
        }
    }
}
