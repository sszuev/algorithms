package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Given the head of a singly linked list,
 * group all the nodes with odd indices together followed by the nodes with even indices, and return the reordered list.
 * The first node is considered odd, and the second node is even, and so on.
 * Note that the relative order inside both the even and odd groups should remain as it was in the input.
 * You must solve the problem in O(1) extra space complexity and O(n) time complexity.
 * <p>
 * Constraints: The number of nodes in the linked list is in the range `[0, 10^4]`; `-10^6 <= Node.val <= 10^6`
 *
 * @see <a href="https://leetcode.com/problems/odd-even-linked-list">328. Odd Even Linked List</a>
 */
public class L51_OddEvenLinkedList {

    @SuppressWarnings("ClassEscapesDefinedScope")
    public ListNode oddEvenList(ListNode head) {
        return reorderList(head);
    }

    private static ListNode reorderList(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode newList = new ListNode(head.val);

        ListNode res = newList;

        ListNode current = head;
        while (current != null) {
            current = current.next;
            if (current != null) {
                current = current.next;
            }
            if (current != null) {
                res.next = new ListNode(current.val);
                res = res.next;
            }
        }
        current = head.next;
        while (current != null) {
            res.next = new ListNode(current.val);
            res = res.next;
            current = current.next;
            if (current != null) {
                current = current.next;
            }
        }
        return newList;
    }

    @Test
    void test1() {
        // [2,1,3,5,6,4,7]
        ListNode given = new ListNode(2);
        given.add(1).add(3).add(5).add(6).add(4).add(7);
        ListNode actual = oddEvenList(given);
        Assertions.assertEquals(List.of(2, 3, 6, 7, 1, 5, 4), actual.toList());
    }

    @Test
    void test2() {
        // [1,2,3,4,5]
        ListNode given = new ListNode(1);
        given.add(2).add(3).add(4).add(5);
        ListNode actual = oddEvenList(given);
        Assertions.assertEquals(List.of(1, 3, 5, 2, 4), actual.toList());
    }

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode() {}
     * ListNode(int val) { this.val = val; }
     * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     * }
     */
    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        ListNode add(int val) {
            ListNode res = new ListNode(val);
            next = res;
            return res;
        }

        public List<Integer> toList() {
            ListNode node = this;
            List<Integer> res = new ArrayList<>();
            while (node != null) {
                res.add(node.val);
                node = node.next;
            }
            return res;
        }

        @Override
        public String toString() {
            return "[" + val + "]" + next;
        }
    }
}