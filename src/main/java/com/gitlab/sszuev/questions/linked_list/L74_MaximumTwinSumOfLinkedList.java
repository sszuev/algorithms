package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * In a linked list of size `n`, where `n` is even,
 * the `i`th node (0-indexed) of the linked list is known as the twin of the `(n-1-i)`th node, if `0 <= i <= (n / 2) - 1`.
 * For example, if `n = 4`, then node `0` is the twin of node `3`, and node `1` is the twin of node `2`.
 * These are the only nodes with twins for `n = 4`.
 * The twin sum is defined as the sum of a node and its twin.
 * Given the head of a linked list with even length, return the maximum twin sum of the linked list.
 * <p>
 * Constraints:
 * - The number of nodes in the list is an even integer in the range `[2, 10^5]`.
 * - `1 <= Node.val <= 10^5`
 *
 * @see <a href="https://leetcode.com/problems/maximum-twin-sum-of-a-linked-list/">2130. Maximum Twin Sum of a Linked List</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L74_MaximumTwinSumOfLinkedList {

    public int pairSum(ListNode head) {
        return calcPairSum(head);
    }

    public static int calcPairSum(ListNode head) {
        ListNode head1 = head;

        ListNode p1 = head1;
        ListNode p2 = head1;
        while (true) {
            ListNode a = p1.next;
            ListNode b = p2.next;
            if (b.next != null) {
                b = b.next;
            } else {
                break;
            }
            p1 = a;
            p2 = b;
        }

        ListNode p3 = p1.next;
        p1.next = null;
        ListNode head2 = null;
        while (p3 != null) {
            ListNode p = p3;
            p3 = p3.next;
            p.next = head2;
            head2 = p;
        }

        int sum = 0;
        while (head1 != null) {
            sum = Math.max(head1.val + head2.val, sum);
            head1 = head1.next;
            head2 = head2.next;
        }
        return sum;
    }

    @Test
    void test1() {
        // 5,4,2,1
        ListNode head = new ListNode(5);
        head.add(4).add(2).add(1);
        Assertions.assertEquals(6, pairSum(head));
    }

    @Test
    void test2() {
        // 4,2,2,3
        ListNode head = new ListNode(4);
        head.add(2).add(2).add(3);
        Assertions.assertEquals(7, pairSum(head));
    }

    @Test
    void test3() {
        // 1,100000
        ListNode head = new ListNode(1);
        head.add(100000);
        Assertions.assertEquals(100001, pairSum(head));
    }

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        ListNode add(int val) {
            ListNode res = new ListNode(val);
            next = res;
            return res;
        }

        public List<Integer> toList() {
            ListNode node = this;
            List<Integer> res = new ArrayList<>();
            while (node != null) {
                res.add(node.val);
                node = node.next;
            }
            return res;
        }

        @Override
        public String toString() {
            return "[" + val + "]" + next;
        }

    }
}
