package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Given the head of a singly linked list, reverse the list, and return the reversed list.
 * <p>
 * Constraints:
 * - The number of nodes in the list is the range `[0, 5000]`.
 * - `-5000 <= Node.val <= 5000`
 *
 * @see <a href="https://leetcode.com/problems/reverse-linked-list">206. Reverse Linked List</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L91_ReverseLinkedList {

    public ListNode reverseList(ListNode head) {
        return doReverseList(head);
    }

    private static ListNode doReverseList(ListNode head) {
        ListNode node = head;
        ListNode prev = null;
        while (node != null) {
            ListNode next = node.next;
            node.next = prev;
            prev = node;
            node = next;
        }
        return prev;
    }

    @Test
    void test1() {
        // Input: head = [1,2,3,4,5]
        // Output: [5,4,3,2,1]
        ListNode head = new ListNode(1);
        head.add(2).add(3).add(4).add(5);
        ListNode res = reverseList(head);
        Assertions.assertEquals(List.of(5, 4, 3, 2, 1), res.toList());
    }

    @Test
    void test2() {
        // Input: head = [1,2]
        // Output: [2,1]
        ListNode head = new ListNode(1);
        head.add(2);
        ListNode res = reverseList(head);
        Assertions.assertEquals(List.of(2, 1), res.toList());
    }

    @SuppressWarnings("unused")
    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        ListNode add(int val) {
            ListNode res = new ListNode(val);
            next = res;
            return res;
        }

        public List<Integer> toList() {
            ListNode node = this;
            List<Integer> res = new ArrayList<>();
            while (node != null) {
                res.add(node.val);
                node = node.next;
            }
            return res;
        }

        @Override
        public String toString() {
            return "[" + val + "]" + next;
        }
    }

}
