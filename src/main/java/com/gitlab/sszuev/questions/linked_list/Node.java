package com.gitlab.sszuev.questions.linked_list;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by @ssz on 07.10.2020.
 */
public class Node<E> implements Iterable<E> {
    E data;
    Node<E> next;

    public static <X> Node<X> of(X v) {
        Node<X> res = new Node<>();
        res.setValue(v);
        return res;
    }

    @SafeVarargs
    public static <X> Node<X> create(X... values) {
        return create(Node::of, values);
    }

    @SafeVarargs
    public static <N extends Node<X>, X> N create(Function<X, N> create, X... values) {
        if (values.length == 0) {
            throw new IllegalArgumentException();
        }
        N res = create.apply(values[0]);
        N n = res;
        for (int i = 1; i < values.length; i++) {
            N x = create.apply(values[i]);
            n.setNext(x);
            n = x;
        }
        return res;
    }

    public static <X> String printList(Node<X> node) {
        return node.stream().map(String::valueOf).collect(Collectors.joining(" "));
    }

    public static <X> boolean equals(Node<X> a, Node<X> b) {
        while (a != null && b != null) {
            if (!Objects.equals(a.value(), b.value())) {
                return false;
            }
            a = a.next();
            b = b.next();
        }
        return a == null && b == null;
    }

    public Node<E> next() {
        return next;
    }

    public E value() {
        return data;
    }

    void setValue(E e) {
        this.data = Objects.requireNonNull(e);
    }

    void setNext(Node<E> next) {
        this.next = Objects.requireNonNull(next);
    }

    public Node<E> last() {
        Node<E> res = this;
        Node<E> next = res.next;
        while (next != null) {
            res = next;
            next = next.next;
        }
        return res;
    }

    public Node<E> reverse() {
        Node<E> head = null;
        Node<E> node = this;
        while (node != null) {
            Node<E> tmp = of(node.value());
            if (head != null)
                tmp.setNext(head);
            head = tmp;
            node = node.next();
        }
        return head;
    }

    public long size() {
        long res = 0;
        Node<E> node = this;
        while (node != null) {
            node = node.next();
            res++;
        }
        return res;
    }

    @Override
    public String toString() {
        return next == null ? String.format("(%s)", data) : String.format("(%s)[%s]", data, next);
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<>() {
            private Node<E> future = Node.this;
            private Node<E> current = Node.this;
            private Node<E> previous = Node.this;

            @Override
            public boolean hasNext() {
                if (future == null) {
                    return false;
                }
                this.previous = current;
                this.current = future;
                this.future = future.next;
                return true;
            }

            @Override
            public E next() {
                if (current == null)
                    throw new NoSuchElementException();
                return current.data;
            }

            @Override
            public void remove() {
                if (previous == null) {
                    throw new IllegalStateException();
                }
                // del current
                this.previous.next = this.future;
                this.current = this.previous;
            }
        };
    }

    public Stream<E> stream() {
        return StreamSupport.stream(spliterator(), false);
    }

    public List<E> asList() {
        return stream().collect(Collectors.toList());
    }
}
