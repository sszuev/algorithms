package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiConsumer;

/**
 * Delete Middle Node:
 * <p>
 * Implement an algorithm to delete a node in the middle
 * (i.e., any node but the first and last node, not necessarily the exact middle) of a singly linked list,
 * given only access to that node.
 * EXAMPLE
 * Input: the node {@code c} from the linked list {@code a -> b -> c -> d -> e -> f}
 * Result: nothing is returned, but the new linked list looks like {@code a -> b -> d -> e -> f}
 * <p>
 * Hints: #72
 * Picture the list {@code 1 -> 5 -> 9 -> 12}.
 * Removing {@code 9} would make it look like {@code 1 -> 5 -> 12}.
 * You only have access to the {@code 9} node.
 * Can you make it look like the correct answer?
 * <p>
 * Created by @ssz on 12.07.2020.
 */
public class Q3_DeleteMiddle {

    @Test
    public void testDeleteWithKnownHead() {
        doTestDelete(Q3_DeleteMiddle::deleteWithHead);
    }

    @Test
    public void testDeleteDirect() {
        doTestDelete((head, value) -> {
            Node<String> node = null;
            Node<String> n = head;
            while (n != null) {
                if (Objects.equals(n.value(), value)) {
                    node = n;
                    break;
                }
                n = n.next();
            }
            Assertions.assertNotNull(node);
            deleteFromBook(node);
        });
    }

    private void doTestDelete(BiConsumer<Node<String>, String> doDelete) {
        Node<String> list = Node.create("a", "b", "c", "d", "e", "f");
        System.out.println(list.asList());
        Assertions.assertEquals(Arrays.asList("a", "b", "c", "d", "e", "f"), list.asList());
        doDelete.accept(list, "c");
        System.out.println(list.asList());
        Assertions.assertEquals(Arrays.asList("a", "b", "d", "e", "f"), list.asList());
        doDelete.accept(list, "d");
        System.out.println(list.asList());
        Assertions.assertEquals(Arrays.asList("a", "b", "e", "f"), list.asList());
    }

    private static <X> void deleteWithHead(final Node<X> node, X some) {
        Node<X> next = node;
        Node<X> prev;
        while (next != null) {
            prev = next;
            next = next.next();
            if (Objects.equals(some, next.value())) {
                Node<X> nn = next.next();
                if (nn != null) {
                    prev.setNext(nn);
                }
                return;
            }
        }
    }

    private static <X> void deleteFromBook(Node<X> node) {
        Node<X> next = node.next();
        if (next == null) {
            return;
        }
        node.setValue(next.value());
        node.setNext(next.next());
    }
}
