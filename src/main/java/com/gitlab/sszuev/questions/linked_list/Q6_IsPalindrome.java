package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * Palindrome: Implement a function to check if a linked list is a palindrome.
 * <p>
 * Hints: #5, #13, #29, #61, #101
 * <ul>
 * <li>#5: A palindrome is something which is the same when written forwards and backwards. What if you reversed the linked list?</li>
 * <li>#13: Try using a stack.</li>
 * <li>#29: Assume you have the length of the linked list. Can you implement this recursively?</li>
 * <li>#61: In the recursive approach (we have the length of the list), the middle is the base case:
 * {@code isPermutation(middle)} is {@code true}. The node {@code x} to the immediate left of the middle.
 * What can that node do to check if {@code x -> middle -> y} forms a palindrome?
 * Now suppose that checks out. What about the previous node {@code a}?
 * If {@code x -> middle -> y} is a palindrome, how can it check that {@code a -> x -> middle -> y -> b} is a palindrome?</li>
 * <li>#101: Go back to the previous hint. Remember: there are ways to return multiple values. You can do this with a new class.</li>
 * </ul>
 * <p>
 * Created by @ssz on 19.06.2020.
 */
public class Q6_IsPalindrome {

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void testFalse1(Solution data) {
        doTest(data, false, "a", "b", "c", "a", "d", "d", "e", "e", "d", "c", "b", "a");
    }

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void testTrue2(Solution data) {
        doTest(data, true, "a", "b", "c", "d", "d", "c", "b", "a");
    }

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void testTrue3(Solution data) {
        doTest(data, true, "a", "b", "c", "d", "3", "d", "c", "b", "a");
    }

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void testFalse4(Solution data) {
        doTest(data, false, "a", "b", "c", "d", "3", "d", "c", "b", "a", "4");
    }

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void testFalse5(Solution data) {
        doTest(data, false, "a", "b", "c");
    }

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void testTrue6(Solution data) {
        doTest(data, true, "a", "b", "a");
    }

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void testTrue7(Solution data) {
        doTest(data, true, "A");
    }

    private void doTest(Solution data, boolean expected, String... values) {
        Node<String> list = Node.create(values);
        Assertions.assertEquals(expected, data.isPalindrome(list), "Wrong: " + list.asList());
    }

    enum Solution {
        MY {
            @Override
            <E> boolean isPalindrome(Node<E> list) {
                List<E> buffer = list.stream().collect(Collectors.toList()); // O(N) space + O(N) time
                for (int i = 0, j = buffer.size() - 1; i <= j; i++, j--) { // O(N/2) time
                    if (!buffer.get(i).equals(buffer.get(j))) {
                        return false;
                    }
                }
                return true;
            }
        },
        SOLUTION_1_REVERSE {
            @Override
            <E> boolean isPalindrome(Node<E> list) {
                Node<E> reversed = list.reverse(); // space + time = O(N)
                return Node.equals(list, reversed); // time = O(N)
            }
        },

        SOLUTION_2_STACK { // P230

            @Override
            <E> boolean isPalindrome(Node<E> head) { // space O(N/2), time O(N)
                Node<E> fast = head;
                Node<E> slow = head;

                Stack<E> stack = new Stack<>();

                // Push elements from first half of linked list onto stack.
                // When fast runner (which is moving at 2x speed) reaches the end of the linked list,
                // then we know we're at the middle.
                while (fast != null && fast.next() != null) {
                    stack.push(slow.value());
                    slow = slow.next();
                    fast = fast.next().next();
                }

                // Has odd number of elements, so skip the middle element
                if (fast != null) {
                    slow = slow.next();
                }

                while (slow != null) {
                    E top = stack.pop();

                    // If values are different, then it's not a palindrome
                    if (!Objects.equals(top, slow.value())) {
                        return false;
                    }
                    slow = slow.next();
                }
                return true;
            }
        },

        SOLUTION_3_RECURSIVE {
            @Override
            <E> boolean isPalindrome(Node<E> list) {
                return isPalindromeRecurse(list, list.size()).res;
            }

            <E> R<E> isPalindromeRecurse(Node<E> head, long length) { // space O(N/2), time O(N)
                if (head == null || length <= 0) { // Even number of nodes
                    return new R<>(head, true);
                } else if (length == 1) { // Odd number of nodes
                    return new R<>(head.next(), true);
                }

                // Recurse on sublist:
                R<E> res = isPalindromeRecurse(head.next(), length - 2);
                // If child calls are not a palindrome, pass back up a failure
                if (!res.res || res.node == null) {
                    return res;
                }
                // Check if matches corresponding node on other side
                boolean eq = Objects.equals(head.value(), res.node.value());
                // Return corresponding node
                Node<E> node = res.node.next();
                return new R<>(node, eq);
            }

            class R<X> {
                final Node<X> node;
                final boolean res;

                private R(Node<X> node, boolean res) {
                    this.node = node;
                    this.res = res;
                }
            }
        },

        ;

        abstract <E> boolean isPalindrome(Node<E> list);
    }

}
