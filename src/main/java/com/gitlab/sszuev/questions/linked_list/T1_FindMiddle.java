package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * <b>How to Find Middle Element of Linked List in Java in Single Pass</b>
 *
 * @see <a href='https://javarevisited.blogspot.com/2012/12/how-to-find-middle-element-of-linked-list-one-pass.html'>How to Find Middle Element of Linked List in Java in Single Pass</a>
 */
public class T1_FindMiddle {

    @Test
    public void test1() {
        Node<Integer> list = Node.create(1, 2, 3, 4, 5);
        Node<Integer> middle = findMiddle(list);
        Assertions.assertNotNull(middle);
        Assertions.assertEquals(3, middle.value());
    }

    @Test
    public void test2() {
        Node<String> list = Node.create("A", "B", "C");
        Node<String> middle = findMiddle(list);
        Assertions.assertNotNull(middle);
        Assertions.assertEquals("B", middle.value());
    }

    @Test
    public void test3() {
        Node<String> list = Node.create("A", "B", "C", "d");
        Node<String> middle = findMiddle(list);
        Assertions.assertNotNull(middle);
        Assertions.assertEquals("C", middle.value());
    }

    @Test
    public void test4() {
        Node<String> list = Node.create("A", "B");
        Node<String> middle = findMiddle(list);
        Assertions.assertNotNull(middle);
        Assertions.assertEquals("B", middle.value());
    }

    @Test
    public void test5() {
        Node<String> list = Node.create("A");
        Node<String> middle = findMiddle(list);
        Assertions.assertNotNull(middle);
        Assertions.assertEquals("A", middle.value());
    }

    public static <X> Node<X> findMiddle(Node<X> head) {
        Node<X> node = head;
        Node<X> res = head;
        int length = 0;
        while (node.next() != null) {
            length++;
            if (length % 2 == 0) {
                res = res.next();
            }
            node = node.next();
        }
        if (length % 2 == 1) {
            res = res.next();
        }
        return res;
    }
}
