package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * <b>How to Find If Linked List Contains Loop</b>
 *
 * @see <a href='https://javarevisited.blogspot.com/2013/05/find-if-linked-list-contains-loops-cycle-cyclic-circular-check.html'>How to Find If Linked List Contains Loop or Cycle in Java - Coding Interview Question With Solution</a>
 */
public class T2_CheckForCycle {

    @Test
    public void testNonCyclic() {
        Node<Integer> list = Node.create(4, 3, 42, 5);
        Assertions.assertFalse(isCyclic(list));
    }

    @Test
    public void testIsCyclic() {
        Node<String> head = Node.create("a", "b", "c", "d");
        Node<String> last = head.last();
        Assertions.assertNotNull(last);
        last.next = head;
        Assertions.assertEquals("a", head.value());
        Assertions.assertEquals("d", last.value());

        Assertions.assertTrue(isCyclic(head));
    }

    /*
     * If singly LinkedList contains Cycle then following would be true:
     * slow and fast will point to same node, i.e. they meet;
     * on the other hand if fast will point to null or next node of fast will point to null then LinkedList does not contains cycle.
     */
    public static boolean isCyclic(Node<?> head) {
        Node<?> fast = head;
        Node<?> slow = head;

        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
            // if fast and slow pointers are meeting then LinkedList is cyclic
            if (fast == slow) {
                return true;
            }
        }
        return false;
    }

}
