package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;

/**
 * <b>How to reverse a linked list in Java using Recursion and Iteration</b>
 *
 * @see <a href='https://javarevisited.blogspot.com/2017/03/how-to-reverse-linked-list-in-java-using-iteration-and-recursion.html'>How to reverse a linked list in Java using Recursion and Iteration</a>
 */
public class T3_Reverse {

    @Test
    public void testReverseIteratively() {
        Node<Integer> before = Node.create(4, 3, 42, 5);
        System.out.println("Before: " + before);
        Node<Integer> after = reverseIteratively(before);
        System.out.println("After: " + after);
        Assertions.assertEquals("5 42 3 4", Node.printList(after));
    }

    @Test
    public void testReverseRecursively() {
        Node<String> before = Node.create("z", "y", "x");
        System.out.println("Before: " + before);
        Node<String> after = reverseRecursively(before);
        System.out.println("After: " + after);
        Assertions.assertEquals("x y z", Node.printList(after));
    }

    /**
     * Reverses linked list using 3 pointers approach in {@code O(n)} time.
     * It basically creates a new list by reversing direction, a
     * nd subsequently insert the element at the start of the list.
     *
     * @param head {@link Node}
     * @param <X>  anything
     */
    public static <X> Node<X> reverseIteratively(Node<X> head) {
        Node<X> current = Objects.requireNonNull(head);
        Node<X> previous = null;
        Node<X> forward;

        // traversing linked list until there is no more element
        while (current.next != null) {
            // Saving reference of next node, since we are changing current node
            forward = current.next;

            // Inserting node at start of new list
            current.next = previous;
            previous = current;
            // Advancing to next node
            current = forward;
        }
        head = current;
        head.next = previous;
        return head;
    }

    /**
     * Reverses a singly linked list using recursion.
     * In recursion Stack is used to store data.
     * Traverse linked list till we find the tail, that would be new head for reversed linked list.
     *
     * @param head {@link Node}
     * @param <X>  anything
     */
    public static <X> Node<X> reverseRecursively(Node<X> head) {
        Node<X> res;
        // base case - tail of original linked list
        if (Objects.requireNonNull(head).next == null) {
            return head;
        }
        res = reverseRecursively(head.next);

        // reverse the link e.g. C->D->null will be null
        head.next.next = head;
        head.next = null;
        return res;
    }

}
