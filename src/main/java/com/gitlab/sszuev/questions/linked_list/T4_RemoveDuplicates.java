package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * <b>Remove duplicates from an unsorted linked list</b>
 * <p/>
 * Write a {@code removeDuplicates()} function which takes a list and deletes any duplicate nodes from the list.
 * The list is not sorted.
 * For example if the linked list is {@code 12->11->12->21->41->43->21}
 * then {@code removeDuplicates()} should convert the list to {@code 12->11->21->41->43}.
 * <p/>
 * <b>METHOD 1 (Using two loops)</b>
 * This is the simple way where two loops are used.
 * Outer loop is used to pick the elements one by one and inner loop compares the picked element with rest of the elements.
 * <p/>
 * <b>METHOD 2 (Use Sorting)</b>
 * In general, Merge Sort is the best-suited sorting algorithm for sorting linked lists efficiently.
 * 1) Sort the elements using Merge Sort. We will soon be writing a post about sorting a linked list. {@code O(n*log(n))}
 * 2) Remove duplicates in linear time using the algorithm for removing duplicates in sorted Linked List. {@code O(n)}
 * Please note that this method does not preserve the original order of elements.
 * Time Complexity: {@code O(n*log(n))}
 * <p/>
 * <b>METHOD 3 (Use Hashing)</b>
 * We traverse the link list from head to end.
 * For every newly encountered element, we check whether it is in the hash table:
 * if yes, we remove it; otherwise we put it in the hash table.
 *
 * @see <a href='https://www.geeksforgeeks.org/remove-duplicates-from-an-unsorted-linked-list/'>Remove duplicates from an unsorted linked list</a>
 */
public class T4_RemoveDuplicates {

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testMethod1(Data data) {
        Node<Integer> list = Node.create(10, 12, 11, 11, 12, 11, 10);
        System.out.println("Before: " + list);
        data.removeDuplicates(list);
        System.out.println("After: " + list);
        Assertions.assertEquals("10 12 11", Node.printList(list));
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testMethod3(Data data) {
        Node<String> list = Node.create("a", "b", "a", "b", "b", "a", "c");
        System.out.println("Before: " + list);
        data.removeDuplicates(list);
        System.out.println("After: " + list);
        Assertions.assertEquals("a b c", Node.printList(list));
    }

    enum Data {
        REMOVE_DUPLICATES_WITH_SET {
            @Override
            <X> void removeDuplicates(Node<X> head) {
                Node<X> current = Objects.requireNonNull(head);
                Node<X> prev = null;
                Set<X> set = new HashSet<>();
                while (current != null) {
                    if (set.add(current.data)) {
                        prev = current;
                    } else {
                        //noinspection ConstantConditions
                        prev.next = current.next;
                    }
                    current = current.next;
                }
            }
        },
        REMOVE_DUPLICATES_WITH_TWO_LOOPS {
            @Override
            <X> void removeDuplicates(Node<X> head) {
                Node<X> outer = Objects.requireNonNull(head);
                while (outer != null && outer.next() != null) {
                    Node<X> inner = outer;
                    while (inner.next() != null) {
                        if (Objects.equals(outer.data, inner.next.data)) {
                            inner.next = inner.next.next;
                        } else {
                            inner = inner.next;
                        }
                    }
                    outer = outer.next;
                }
            }
        },
        ;

        abstract <X> void removeDuplicates(Node<X> head);
    }
}
