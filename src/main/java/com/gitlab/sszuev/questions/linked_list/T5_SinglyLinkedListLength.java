package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * <b>How to find length of a Singly Linked list using Loop and Recursion</b>
 *
 * @see <a href='https://javarevisited.blogspot.com/2016/05/how-do-you-find-length-of-singly-linked.html'>How do you find length of a Singly Linked list using Loop and Recursion</a>
 */
public class T5_SinglyLinkedListLength {

    public static int lengthIterative(Node<?> head) {
        int res = 0;
        Node<?> n = head;
        while (n != null) {
            res++;
            n = n.next();
        }
        return res;
    }

    public static int lengthRecursive(Node<?> head) {
        if (head == null) {
            return 0;
        }
        return 1 + lengthIterative(head.next());
    }

    @Test
    public void testIterative() {
        Node<Integer> list = Node.create(1, 2, 3, 4, 5, 6);
        System.out.println(list);
        Assertions.assertEquals(6, lengthIterative(list));
    }

    @Test
    public void testRecursive() {
        Node<String> list = Node.create("a", "B", "b");
        System.out.println(list);
        Assertions.assertEquals(3, lengthRecursive(list));
    }

}
