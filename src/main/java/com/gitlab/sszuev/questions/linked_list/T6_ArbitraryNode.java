package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * <b>How to find the k-th Node from the end of a linked list</b>
 * Here, we'll use the tortoise and hare algorithm to solve this problem.
 * This is the same algorithm which we have used to find the middle element of linked list in one pass.
 *
 * @see <a href='https://javarevisited.blogspot.com/2016/07/how-to-find-3rd-element-from-end-in-linked-list-java.html'>How to find the 3rd (kth) Node from end or tail in a linked list in Java</a>
 */
public class T6_ArbitraryNode {

    @Test
    public void testXXX() {
        SinglyLinkedList list = new SinglyLinkedList();
        Assertions.assertTrue(list.isEmpty());
        list.append("1");
        list.append("2");
        list.append("3");
        list.append("4");

        System.out.println("linked list : " + list);
        Assertions.assertFalse(list.isEmpty());
        Assertions.assertEquals(4, list.length());

        String s;
        s = list.getLastNode(1);
        System.out.println("The first node from last: " + s);
        Assertions.assertEquals("4", s);

        s = list.getLastNode(2);
        System.out.println("The second node from the end: " + s);
        Assertions.assertEquals("3", s);

        s = list.getLastNode(3);
        System.out.println("The third node from the tail: " + s);
        Assertions.assertEquals("2", s);
    }

    static class SinglyLinkedList {
        static class Node {
            private Node next;
            private final String data;

            public Node(String data) {
                this.data = data;
            }

            @Override
            public String toString() {
                return data;
            }
        }

        private Node head; // Head is the first node in linked list

        /**
         * checks if linked list is empty
         *
         * @return true if linked list is empty i.e. no node
         */
        public boolean isEmpty() {
            return length() == 0;
        }

        /**
         * Appends a node at the tail of this linked list.
         *
         * @param data {@code String}
         */
        public void append(String data) {
            if (head == null) {
                head = new Node(data);
                return;
            }
            tail().next = new Node(data);
        }

        /**
         * Returns the last node or tail of this linked list.
         *
         * @return last node
         */
        private Node tail() {
            Node tail = head;
            // Find last element of linked list known as tail
            while (tail.next != null) {
                tail = tail.next;
            }
            return tail;
        }

        /**
         * method to get the length of linked list
         *
         * @return length i.e. number of nodes in linked list
         */
        public int length() {
            int length = 0;
            Node current = head;
            while (current != null) {
                length++;
                current = current.next;
            }
            return length;
        }

        /**
         * to get the nth node from end
         *
         * @param n int
         * @return nth node from last
         */
        public String getLastNode(int n) {
            Node fast = head;
            Node slow = head;
            int start = 1;

            while (fast.next != null) {
                fast = fast.next;
                start++;

                if (start > n) {
                    slow = slow.next;
                }
            }
            return slow.data;
        }

        @Override
        public String toString() {
            StringBuilder res = new StringBuilder();
            Node current = head;
            while (current != null) {
                res.append(current).append("-->");
                current = current.next;
            }
            if (res.length() >= 3) {
                res.delete(res.length() - 3, res.length());
            }
            return res.toString();
        }
    }
}
