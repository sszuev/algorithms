package com.gitlab.sszuev.questions.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Random;
import java.util.function.BinaryOperator;

/**
 * <b>Add two numbers represented by linked lists</b>
 * <p>
 * Given two numbers represented by two linked lists, write a function that returns the sum list.
 * The sum list is linked list representation of the addition of two input numbers.
 * It is not allowed to modify the lists.
 * Also, not allowed to use explicit extra space (Hint: Use Recursion).
 * <p>
 * Example
 * <pre>
 * Input:
 *  First List: 5->6->3  // represents number 563
 *  Second List: 8->4->2 //  represents number 842
 * Output:
 *  Resultant list: 1->4->0->5  // represents number 1405
 * </pre>
 *
 * @see <a href='https://www.geeksforgeeks.org/sum-of-two-linked-lists/'>Add two numbers represented by linked lists | Set 2</a>
 */
public class T7_NumberRepresentedByLinkedList {

    private static void doTest(long n, long m, BinaryOperator<Node> add) {
        int[] arr1 = toArray(n);
        int[] arr2 = toArray(m);
        Node left = Node.create(arr1);
        Node right = Node.create(arr2);
        Node res = add.apply(left, right);
        System.out.println(left + " + " + right + " = " + res);
        long actual = Long.parseLong(res.toString().replace(" ", ""));
        Assertions.assertEquals(n + m, actual);
    }

    private static int[] toArray(long n) {
        Deque<Integer> res = new LinkedList<>();
        while (n > 0) {
            res.push((int) (n % 10));
            n = n / 10;
        }
        return res.stream().mapToInt(x -> x).toArray();
    }

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void testFixed(Solutions solution) {
        doTest(991, 18, solution);
        doTest(3894, 187, solution);
        doTest(3, 2323423, solution);
    }

    @ParameterizedTest
    @EnumSource(Solutions.class)
    public void testRandom(Solutions solution) {
        Random r = new Random();
        long a = Math.abs(r.nextInt());
        long b = Math.abs(r.nextInt());
        doTest(a, b, solution);
    }

    enum Solutions implements BinaryOperator<Node> {
        MY_SOLUTION {
            @Override
            public Node apply(Node left, Node right) {
                int sizeLeft = Node.size(left);
                int sizeRight = Node.size(right);
                int min = Math.min(sizeLeft, sizeRight);
                Object[] res = new Object[2];
                add(left, right, sizeLeft - min, sizeRight - min, res);
                int r = (int) res[1];
                Node n = (Node) res[0];
                return r == 1 ? Node.create(1, n) : n;
            }

            private void add(Node left, Node right, int leftIndex, int rightIndex, Object[] res) {
                if (left == null && right == null) {
                    res[0] = res[1] = null;
                    return;
                }
                Node nextLeft = left;
                int valueLeft = 0;
                Node nextRight = right;
                int valueRight = 0;
                if (leftIndex > 0 || rightIndex <= 0) {
                    nextLeft = left.next;
                    valueLeft = left.val;
                }
                if (rightIndex > 0 || leftIndex <= 0) {
                    nextRight = right.next;
                    valueRight = right.val;
                }
                if (leftIndex > 0) {
                    leftIndex--;
                }
                if (rightIndex > 0) {
                    rightIndex--;
                }
                add(nextLeft, nextRight, leftIndex, rightIndex, res);
                Node current = (Node) res[0];
                int reminder = res[1] == null ? 0 : (int) res[1];
                int sum = valueLeft + valueRight + reminder;
                int val = sum % 10;
                reminder = sum != val ? 1 : 0;
                current = Node.create(val, current);
                res[0] = current;
                res[1] = reminder;
            }
        },

        FROM_INTERNET {
            @Override
            public Node apply(Node left, Node right) {
                return new OriginalSolution().addLists(left, right);
            }

            class OriginalSolution {
                private Node result;
                private Node current;
                private int carry;

                // Adds two linked lists of same size represented by
                // head1 and head2 and returns head of the resultant
                // linked list. Carry is propagated while returning
                // from the recursion
                private void addSameSize(Node n, Node m) {
                    // Since the function assumes linked lists are of same size, check any of the two head pointers
                    if (n == null)
                        return;

                    // Recursively add remaining nodes and get the carry
                    addSameSize(n.next, m.next);
                    // add digits of current nodes and propagated carry
                    int sum = n.val + m.val + carry;
                    carry = sum / 10;
                    sum = sum % 10;

                    // Push this to result list
                    result = Node.create(sum, result);
                }

                // This function is called after the smaller list is added to the bigger lists's sublist of same size.
                // Once the right sublist is added,
                // the carry must be added to the left side of larger list to get the final result.
                private void propagateCarry(Node head1) {
                    if (head1 == current) {
                        return;
                    }
                    // If diff. number of nodes are not traversed, add carry
                    propagateCarry(head1.next);
                    int sum = carry + head1.val;
                    carry = sum / 10;
                    sum %= 10;
                    // add this node to the front of the result
                    result = Node.create(sum, result);
                }

                // The main function that adds two linked lists represented by head1 and head2.
                // The sum of two lists is stored in a list referred by result
                public Node addLists(Node head1, Node head2) {
                    if (head1 == null) {
                        result = head2;
                        return result;
                    }
                    if (head2 == null) {
                        result = head1;
                        return result;
                    }

                    int size1 = Node.size(head1);
                    int size2 = Node.size(head2);

                    // Add same size lists
                    if (size1 == size2) {
                        addSameSize(head1, head2);
                    } else {
                        // First list should always be larger than second list.
                        // If not, swap pointers
                        if (size1 < size2) {
                            Node temp = head1;
                            head1 = head2;
                            head2 = temp;
                        }
                        int diff = Math.abs(size1 - size2);

                        // move diff. number of nodes in first list
                        Node temp = head1;
                        while (diff-- >= 0) {
                            current = temp;
                            temp = temp.next;
                        }

                        // get addition of same size lists
                        addSameSize(current, head2);

                        // get addition of remaining first list and carry
                        propagateCarry(head1);
                    }
                    // if some carry is still there, add a new node to
                    // the front of the result list. e.g. 999 and 87
                    if (carry > 0) {
                        result = Node.create(carry, result);
                    }
                    return result;
                }
            }
        },
    }

    static class Node {
        private final int val;
        private final Node next;

        private Node(int val, Node next) {
            this.val = val;
            this.next = next;
        }

        public static Node create(int... values) {
            Node head = null;
            for (int i = values.length - 1; i >= 0; --i) {
                head = create(values[i], head);
            }
            return head;
        }

        public static Node create(int val, Node next) {
            if (val < 0 || val > 9) {
                throw new IllegalArgumentException("Wrong node: " + val);
            }
            return new Node(val, next);
        }

        static int size(Node head) {
            int count = 0;
            while (head != null) {
                count++;
                head = head.next;
            }
            return count;
        }

        @Override
        public String toString() {
            String res = String.valueOf(val);
            if (next != null) {
                return res + " " + next.toString();
            }
            return res;
        }


    }
}
