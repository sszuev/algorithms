[HOME](../../../../../../../../README.md)

- **[T1](T1_IntegerSwapping.java)**: How to swap two Integers without using temporary variable
- **[T2](T2_RectangleIntersection.java)**: How to Check if two Rectangles overlap
- **[T3](T3_VendingMachine/VendingMachineFactory.java)**: Vending Machine
- **[H4](H4_BirthdayCakeCandles.java)**: Birthday Cake Candles
- **[L1](L1_RomanToInteger.java)**: Roman to Integer
- **[L2](L2_IntegerToRoman.java)**: Integer to Roman
- **[L12](L12_SolutionForUnknownEquation.java)**: Find Positive Integer Solution for a Given Equation 