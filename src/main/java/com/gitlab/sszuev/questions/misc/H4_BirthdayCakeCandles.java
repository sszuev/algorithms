package com.gitlab.sszuev.questions.misc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @see <a href="https://www.hackerrank.com/challenges/birthday-cake-candles">Hackerrank: Birthday Cake Candles</a>
 */
public class H4_BirthdayCakeCandles {

    public static int countTallestCandles(List<Integer> candles) {
        if (candles.isEmpty() || candles.size() > 100_000) {
            throw new IllegalArgumentException();
        }
        NavigableMap<Integer, AtomicInteger> res = new TreeMap<>();
        for (Integer candle : candles) {
            if (candle < 1 || candle > 10_000_000) {
                throw new IllegalArgumentException();
            }
            res.computeIfAbsent(candle, x -> new AtomicInteger()).incrementAndGet();
        }
        return res.lastEntry().getValue().get();
    }

    @Test
    public void test4413() {
        int res = countTallestCandles(List.of(4, 4, 1, 3));
        Assertions.assertEquals(2, res);
    }
}
