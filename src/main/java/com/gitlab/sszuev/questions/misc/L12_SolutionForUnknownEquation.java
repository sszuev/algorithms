package com.gitlab.sszuev.questions.misc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Given a callable function {@code f(x, y)} with a hidden formula and a value {@code z},
 * reverse engineer the formula and return all positive integer pairs {@code x} and {@code y}
 * where {@code f(x,y) == z}.
 * You may return the pairs in any order.
 * While the exact formula is hidden, the function is monotonically increasing, i.e.:
 * <pre>{@code
 *     f(x, y) < f(x + 1, y)
 *     f(x, y) < f(x, y + 1)
 * }</pre>
 * The function interface is defined like this:
 * <pre>{@code
 * interface CustomFunction {
 *   // Returns some positive integer f(x, y)
 *   // for two positive integers x and y based on a formula.
 *   int f(int x, int y);
 * };
 * }</pre>
 * <p>
 * We will judge your solution as follows:
 * 1) The judge has a list of 9 hidden implementations of CustomFunction, along with a way to generate an answer key of all valid pairs for a specific z.
 * 2) The judge will receive two inputs: a function_id (to determine which implementation to test your code with), and the target z.
 * 3) The judge will call your findSolution and compare your results with the answer key.
 * 4) If your results match the answer key, your solution will be Accepted.
 * <p>
 * <p>
 * 1) {@code 1 <= function_id <= 9}
 * 2) {@code 1 <= z <= 100}
 * 3) It is guaranteed that the solutions of {@code f(x, y) == z} will be in the range {@code 1 <= x, y <= 1000}.
 * 4) It is also guaranteed that {@code f(x, y)} will fit in 32-bit signed integer if {@code 1 <= x, y <= 1000}.
 *
 * @see <a href="https://leetcode.com/problems/find-positive-integer-solution-for-a-given-equation">1237. Find Positive Integer Solution for a Given Equation</a>
 */
public class L12_SolutionForUnknownEquation {

    public static List<List<Integer>> findSolution(CustomFunction customfunction, int z) {
        List<List<Integer>> res = new ArrayList<>();
        for (int x = 1; x <= 1000; x++) {
            int min = 1;
            int max = 1000;
            while (min < max) {
                int m = (min + max) / 2;
                if (customfunction.f(x, m) >= z) {
                    max = m;
                } else {
                    min = m + 1;
                }
            }
            if (customfunction.f(x, min) == z) {
                res.add(List.of(x, min));
            }
        }
        return res;
    }

    @Test
    public void testXaY() {
        Set<List<Integer>> actual = new HashSet<>(findSolution(Integer::sum, 5));
        // [[1,4],[2,3],[3,2],[4,1]]
        Assertions.assertEquals(Set.of(List.of(1, 4), List.of(2, 3), List.of(3, 2), List.of(4, 1)), actual);
    }

    @Test
    public void testXmY() {
        Set<List<Integer>> actual = new HashSet<>(findSolution((x, y) -> x * y, 5));
        // [[1,5],[5,1]]
        Assertions.assertEquals(Set.of(List.of(1, 5), List.of(5, 1)), actual);
    }

    public interface CustomFunction {
        int f(int x, int y);
    }
}
