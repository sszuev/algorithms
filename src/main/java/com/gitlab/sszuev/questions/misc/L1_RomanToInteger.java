package com.gitlab.sszuev.questions.misc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 * <pre>{@code
 * Symbol       Value
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 * }
 * </pre>
 * For example, 2 is written as II in Roman numeral, just two ones added together.
 * 12 is written as XII, which is simply X + II.
 * The number 27 is written as XXVII, which is XX + V + II.
 * Roman numerals are usually written largest to smallest from left to right.
 * However, the numeral for four is not IIII.
 * Instead, the number four is written as IV.
 * Because the one is before the five we subtract it making four.
 * The same principle applies to the number nine, which is written as IX.
 * There are six instances where subtraction is used:
 *
 * <ul>
 * <li>I can be placed before V (5) and X (10) to make 4 and 9.</li>
 * <li>X can be placed before L (50) and C (100) to make 40 and 90.</li>
 * <li>C can be placed before D (500) and M (1000) to make 400 and 900.</li>
 * </ul>
 *
 * @see <a href="https://leetcode.com/problems/roman-to-integer">13. Roman to Integer</a>
 */
public class L1_RomanToInteger {

    public static long romanToInteger(String roman) {
        List<Integer> res = new ArrayList<>();
        char[] chars = roman.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == 'I') {
                if (i != chars.length - 1) {
                    if (chars[i + 1] == 'V') {
                        res.add(4);
                        i++;
                        continue;
                    } else if (chars[i + 1] == 'X') {
                        res.add(9);
                        i++;
                        continue;
                    }
                }
                res.add(1);
            } else if (c == 'X') {
                if (i != chars.length - 1) {
                    if (chars[i + 1] == 'L') {
                        res.add(40);
                        i++;
                        continue;
                    } else if (chars[i + 1] == 'C') {
                        res.add(90);
                        i++;
                        continue;
                    }
                }
                res.add(10);
            } else if (c == 'C') {
                if (i != chars.length - 1) {
                    if (chars[i + 1] == 'D') {
                        res.add(400);
                        i++;
                        continue;
                    } else if (chars[i + 1] == 'M') {
                        res.add(900);
                        i++;
                        continue;
                    }
                }
                res.add(100);
            } else if (c == 'V') {
                res.add(5);
            } else if (c == 'L') {
                res.add(50);
            } else if (c == 'D') {
                res.add(500);
            } else if (c == 'M') {
                res.add(1000);
            } else {
                throw new IllegalArgumentException("Wrong input: '" + c + "'");
            }
        }
        AtomicInteger arabic = new AtomicInteger();
        res.forEach(arabic::getAndAdd);
        return arabic.get();
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test(Data data) {
        long actual = romanToInteger(data.name());
        Assertions.assertEquals(data.arabic, actual);
    }

    public enum Data {
        III(3),
        LVIII(58),
        MCMXCIV(1994),
        ;
        private final long arabic;

        Data(long arabic) {
            this.arabic = arabic;
        }
    }
}
