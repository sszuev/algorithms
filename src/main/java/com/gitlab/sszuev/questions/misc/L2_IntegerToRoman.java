package com.gitlab.sszuev.questions.misc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.List;

/**
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 * <pre>{@code
 * Symbol       Value
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 * }
 * </pre>
 * For example, 2 is written as II in Roman numeral, just two ones added together.
 * 12 is written as XII, which is simply X + II.
 * The number 27 is written as XXVII, which is XX + V + II.
 * Roman numerals are usually written largest to smallest from left to right.
 * However, the numeral for four is not IIII.
 * Instead, the number four is written as IV.
 * Because the one is before the five we subtract it making four.
 * The same principle applies to the number nine, which is written as IX.
 * There are six instances where subtraction is used:
 *
 * <ul>
 * <li>I can be placed before V (5) and X (10) to make 4 and 9.</li>
 * <li>X can be placed before L (50) and C (100) to make 40 and 90.</li>
 * <li>C can be placed before D (500) and M (1000) to make 400 and 900.</li>
 * </ul>
 *
 * @see <a href="https://leetcode.com/problems/integer-to-roman/">12. Integer to Roman</a>
 */
public class L2_IntegerToRoman {
    //   I             1
    //  IV             4
    //   V             5
    //  IX             9
    //   X             10
    //  XL             40
    //   L             50
    //  XC             90
    //   C             100
    //  CD             400
    //   D             500
    //  CM             900
    //   M             1000

    private static final List<String> ROMAN_DIGITS = List.of("I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M");
    private static final List<Integer> ARABIC_NUMBERS = List.of(1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000);
    public static String intToRoman(int arabic) {
        StringBuilder res = new StringBuilder();
        int n = arabic;
        int index = ROMAN_DIGITS.size() - 1;
        while (n > 0) {
            String lastRomanDigit = ROMAN_DIGITS.get(index);
            int lastArabicNumber = ARABIC_NUMBERS.get(index--);
            while (n >= lastArabicNumber) {
                n -= lastArabicNumber;
                res.append(lastRomanDigit);
            }
        }
        return res.toString();
    }

    @ParameterizedTest
    @EnumSource(Data.class)
    public void test(Data data) {
        String actual = intToRoman(data.arabic);
        Assertions.assertEquals(data.name(), actual);
    }

    public enum Data {
        III(3),
        LVIII(58),
        MCMXCIV(1994),
        ;
        private final int arabic;

        Data(int arabic) {
            this.arabic = arabic;
        }
    }
}
