package com.gitlab.sszuev.questions.misc;

/**
 * <b>Как определить, пересекаются ли два прямоугольника?</b>
 *
 * @see <a href='https://javarevisited.blogspot.com/2016/10/how-to-check-if-two-rectangle-overlap-in-java-algorithm.html'>How to Check if two Rectangles Overlap in Java</a>
 */
public class T2_RectangleIntersection {
    public static void main(String[] args) {
        Point l1 = new Point(0, 10);
        Point r1 = new Point(10, 0);
        Point l2 = new Point(5, 5);
        Point r2 = new Point(15, 0);

        Rectangle first = new Rectangle(l1, r1);
        Rectangle second = new Rectangle(l2, r2);

        System.out.println(first.isOverLapping(second) ? "Yes" : "No");
    }

    static class Point {
        int x;
        int y;

        public Point(int x, int y) {
            super();
            this.x = x;
            this.y = y;
        }
    }

    static class Rectangle {

        private final Point topLeft;
        private final Point bottomRight;

        public Rectangle(Point topLeft, Point bottomRight) {
            this.topLeft = topLeft;
            this.bottomRight = bottomRight;
        }

        /**
         * Java method to check if two rectangle are intersecting to each other.
         *
         * @param other {@link Rectangle}
         * @return true if two rectangle overlap with each other
         */
        public boolean isOverLapping(Rectangle other) {
            // R1 is below R1
            return this.topLeft.x <= other.bottomRight.x // R1 is right to R2
                    && this.bottomRight.x >= other.topLeft.x // R1 is left to R2
                    && this.topLeft.y >= other.bottomRight.y // R1 is above R2
                    && this.bottomRight.y <= other.topLeft.y;
        }
    }
}


