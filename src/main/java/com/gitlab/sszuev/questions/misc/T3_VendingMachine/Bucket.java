package com.gitlab.sszuev.questions.misc.T3_VendingMachine;

/**
 * A parameterized class to hold two objects. It's kind of Pair class.
 * Created by @ssz on 29.09.2020.
 */
public class Bucket<K, V> {
    private final K first;
    private final V second;

    public Bucket(K first, V second) {
        this.first = first;
        this.second = second;
    }

    public K getFirst() {
        return first;
    }

    public V getSecond() {
        return second;
    }
}
