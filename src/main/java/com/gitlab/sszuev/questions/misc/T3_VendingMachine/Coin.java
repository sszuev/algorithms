package com.gitlab.sszuev.questions.misc.T3_VendingMachine;

/**
 * Another Java Enum to represent Coins supported by Vending Machine.
 * Created by @ssz on 29.09.2020.
 */
public enum Coin {
    PENNY(1), NICKLE(5), DIME(10), QUARTER(25);

    private final int denomination;

    Coin(int denomination) {
        this.denomination = denomination;
    }

    public int getDenomination() {
        return denomination;
    }
}
