package com.gitlab.sszuev.questions.misc.T3_VendingMachine;

import java.util.HashMap;
import java.util.Map;

/**
 * Java class to represent an Inventory, used for creating case and item inventory inside Vending Machine.
 * Created by @ssz on 29.09.2020.
 */
public class Inventory<X> {
    private final Map<X, Integer> inventory = new HashMap<>();

    public int getQuantity(X item) {
        Integer value = inventory.get(item);
        return value == null ? 0 : value;
    }

    public void add(X item) {
        int count = inventory.get(item);
        inventory.put(item, count + 1);
    }

    public void deduct(X item) {
        if (!hasItem(item)) {
            return;
        }
        int count = inventory.get(item);
        inventory.put(item, count - 1);
    }

    public boolean hasItem(X item) {
        return getQuantity(item) > 0;
    }

    public void clear() {
        inventory.clear();
    }

    public void put(X item, int quantity) {
        inventory.put(item, quantity);
    }

    @Override
    public String toString() {
        return String.format("Inventory{%s}", inventory);
    }
}
