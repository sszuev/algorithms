package com.gitlab.sszuev.questions.misc.T3_VendingMachine;

/**
 * Java Enum to represent Item served by Vending Machine.
 * Created by @ssz on 29.09.2020.
 */
public enum Item {
    COKE("Coke", 25), PEPSI("Pepsi", 35), SODA("Soda", 45);

    private final String name;
    private final int price;

    Item(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public long getPrice() {
        return price;
    }
}
