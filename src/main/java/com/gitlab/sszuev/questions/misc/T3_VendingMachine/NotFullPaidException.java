package com.gitlab.sszuev.questions.misc.T3_VendingMachine;

/**
 * An Exception thrown by Vending Machine when a user tries to collect an item, without paying the full amount.
 * Created by @ssz on 29.09.2020.
 */
public class NotFullPaidException extends RuntimeException {
    private final String message;
    private final long remaining;

    public NotFullPaidException(String message, long remaining) {
        this.message = message;
        this.remaining = remaining;
    }

    public long getRemaining() {
        return remaining;
    }

    @Override
    public String getMessage() {
        return message + remaining;
    }
}
