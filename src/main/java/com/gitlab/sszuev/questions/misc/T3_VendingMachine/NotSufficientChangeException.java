package com.gitlab.sszuev.questions.misc.T3_VendingMachine;

/**
 * Vending Machine throws this exception to indicate that it doesn't have sufficient change to complete this request.
 * Created by @ssz on 29.09.2020.
 */
public class NotSufficientChangeException extends RuntimeException {
    public NotSufficientChangeException(String msg) {
        super(msg);
    }
}
