package com.gitlab.sszuev.questions.misc.T3_VendingMachine;

/**
 * Vending Machine throws this exception if the user request for a product which is sold out.
 * Created by @ssz on 29.09.2020.
 */
public class SoldOutException extends RuntimeException {

    public SoldOutException(String msg) {
        super(msg);
    }
}
