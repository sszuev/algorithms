package com.gitlab.sszuev.questions.misc.T3_VendingMachine;

import java.util.List;

/**
 * It defines the public API of vending machine, usually all high-level functionality should go in this class.
 * Created by @ssz on 29.09.2020.
 */
public interface VendingMachine {
    long selectItemAndGetPrice(Item item);

    void insertCoin(Coin coin);

    List<Coin> refund();

    Bucket<Item, List<Coin>> collectItemAndChange();

    void reset();
}
