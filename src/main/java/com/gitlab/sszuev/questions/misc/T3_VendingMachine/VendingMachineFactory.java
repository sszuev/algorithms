package com.gitlab.sszuev.questions.misc.T3_VendingMachine;

import java.util.List;

/**
 * A Factory class to create different kinds of Vending Machine.
 *
 * @see <a href='https://javarevisited.blogspot.com/2016/06/design-vending-machine-in-java.html'>Design a Vending Machine</a>
 */
public class VendingMachineFactory {
    public static VendingMachine createVendingMachine() {
        return new VendingMachineImpl();
    }

    static class Test {

        public static void main(String... xxx) {
            VendingMachineImpl machine = (VendingMachineImpl) createVendingMachine();
            machine.printStats();

            System.out.println(machine.selectItemAndGetPrice(Item.SODA));
            machine.insertCoin(Coin.PENNY);
            machine.insertCoin(Coin.QUARTER);
            machine.insertCoin(Coin.QUARTER);
            Bucket<Item, List<Coin>> res = machine.collectItemAndChange();
            System.out.println(res.getFirst());
            System.out.println(res.getSecond());
            System.out.println(machine.refund());
            machine.reset();
        }
    }

}


