[HOME](../../../../../../../../README.md)

- **[Q7](Q7_PermutationWithoutDuplicates.java)**: Permutations without Duplicates:
  Write a method to compute all permutations of a string of unique characters.
  <details>
    <summary>Hints</summary>

    - ***150***: Approach 1: Suppose you had all permutations of `abc`.
      How can you use that to get all permutations of `abcd`?
    - ***185***: Approach 1: The permutations of `abc` represent all ways of ordering abc.
      Now, we want to create all orderings of `abcd`. Take a specific ordering of `abcd`, such as `bdca`.
      This `bdca` string represents an ordering of `abc`, too: remove the `d` and you get `bca`.
      Given the string `bca`, can you create all the "related" orderings that include `d`, too?
    - ***200***: Approach 1: Given a string such as `bca`, you can create all permutations of `abcd`
      that have `a`, `b`, `c` in the order `bca` by inserting `d` into each possible
      location: `dbca`, `bdca`, `bcda`, `bcad`.
      Given all permutations of `abc`, can you then create all permutations of `abcd`?
    - ***267***: Approach 1: You can create all permutations of `abcd` by computing all permutations of `abc`
      and then inserting `d` into each possible location within those.
    - ***278***: Approach 2: If you had all permutations of two-character substrings,
      could you generate all permutations of three-character substrings?
    - ***309***: Approach 2: To generate a permutation of `abcd`, you need to pick an initial character.
      It can be `a`, `b`, `c`, or `d`. You can then permute the remaining characters.
      How can you use this approach to generate all permutations of the full string?
    - ***335***: Approach 2: To generate all permutations of `abcd`,
      pick each character (`a`, `b`, `c`, or `d`) as a starting character.
      Permute the remaining characters and prepend the starting character. How do you permute the remaining characters?
      With a recursive process that follows the same logic.
    - ***356***: Approach 2: You can implement this approach by having the recursive function pass back the list of the
      strings,
      and then you prepend the starting character to it. Or, you can push down a prefix to the recursive calls.

  </details>


- **[Q8](Q8_PermutationWithDuplicates.java)**: Permutations with Duplicates:
  Write a method to compute all permutations of a string whose characters are not necessarily unique.
  <details>
    <summary>Hints</summary>  

    - ***161***: You could handle this by just checking to see if there are duplicates before printing them (or adding
      them to a list).
      You can do this with a hash table. In what case might this be okay? In what case might it not be a very good
      solution?
    - ***190***: If you haven't solved 8.7 yet, do that one first.
    - ***222***: Try getting the count of each character. For example, `ABCAAC` has 3 `A`s, 2 `C`s, and 1 `B`.
    - ***255***: To get all permutations with 3 `A`s, 2 `C`s, and 1 `B`, you need to first pick a starting
      character: `A`, `B`, or `C`.
      If it's an `A`, then you need all permutations with 2 `A`s, 2 `C`s, and 1 `B`.

  </details>


- **[Q14](Q14_BooleanExpressionEvaluationWithGivenResult.java)**: Boolean Evaluation:
  Given a boolean expression consisting of the symbols `0`(_FALSE_), `1` (_TRUE_), `&`(_AND_), `|`(_OR_), `^`(_XOR_),
  and a desired boolean result value `result` (_TRUE_ or _FALSE_),
  implement a function to count the number of ways of parenthesizing the expression
  such that it evaluates to `result`.
  EXAMPLES:
    * `countEval("1^0|0|1", false) = 2`
    * `countEval("0&0&0&1^1|0", true) = 10`
  <details>
    <summary>Hints (from book)</summary>  

    - ***148***: Can we just try all possibilities? What would this look like?
    - ***168***: We can think about each possibility as each place where we can put parentheses.
      This means around each operator, such that the expression is split at the operator. What is the base case?
    - ***197***: The base case is when we have a single value, `1` or `O`.
    - ***305***: If your code looks really lengthy, with a lot of if's (for each possible operator, "target"
      boolean result, and left/right side), think about the relationship between the different parts.
      Try to simplify your code. It should not need a ton of complicated if-statements.
      For example, consider expressions of the form <LEFT>OR<RIGHT> versus <LEFT>AND<RIGHT>.
      Both may need to know the number of ways that the <LEFT> evaluates to `true`. See what code you can reuse.
    - ***327***: Look at your recursion. Do you have repeated calls anywhere? Can you memoize it?

  </details>

- **[L5](L5_ClimbingStairs.java)**: (leetcode) Climbing stairs:
  You are climbing a staircase.
  It takes n steps to reach the top.
  Each time you can either climb 1 or 2 steps.
  In how many distinct ways can you climb to the top?

- **[L6](L6_TribonacciNumber.java)**: (leetcode) N-th Tribonacci Number
  The Tribonacci sequence `Tn` is defined as follows: `T0 = 0, T1 = 1, T2 = 1, Tn+3 = Tn + Tn+1 + Tn+2` for `n >= 0`

- **[L29](L29_UniquePath.java)**: (leetcode) Unique path (a robot on an m x n grid)

- **[L41](L41_MinCostClimbingStairs.java)**: (leetcode) Min Cost Climbing Stairs

- **[L49](L49_LongestCommonSubsequence.java)**: (leetcode) Longest Common Subsequence

- **[L52](L52_HouseRobber.java)**: (leetcode) House Robber

- **[L54](L54_CombinationSumIII.java)**: (leetcode) Combination Sum III

- **[L56](L56_StringEditDistance.java)**: (leetcode) Edit Distance

- **[L88](L88_DominoAndTrominoTiling.java)**: (leetcode) Domino and Tromino Tiling

- **[L92](L92_PascalsTriangle.java)**: (leetcode) Pascal's Triangle

- **[L93](L93_BestTimeToBuyAndSellStock.java)**: (leetcode) Best Time to Buy and Sell Stock

- **[L94](L94_DivisorGame.java)**: (leetcode) Divisor Game

[HOME](../../../../../../../../README.md)