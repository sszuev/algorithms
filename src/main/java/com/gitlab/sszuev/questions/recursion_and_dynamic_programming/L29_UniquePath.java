package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * There is a robot on an {@code m x n} grid.
 * The robot is initially located at the top-left corner (i.e., {@code grid[0][0]}).
 * The robot tries to move to the bottom-right corner (i.e., {@code grid[m - 1][n - 1]}).
 * The robot can only move either down or right at any point in time.
 * <p>
 * Given the two integers m and n,
 * return the number of possible unique paths that the robot can take to reach the bottom-right corner.
 * <p>
 * The test cases are generated so that the answer will be less than or equal to {@code 2 * 10^9}.
 * Constraints: {@code 1 <= m, n <= 100}
 *
 * @see <a href="https://leetcode.com/problems/unique-paths">62. Unique Paths</a>
 */
public class L29_UniquePath {
    public int uniquePaths(int m, int n) {
        return calcUniquePath(m, n, new HashMap<>());
    }

    private static int calcUniquePath(int m, int n, Map<Set<Integer>, Integer> cache) {
        if (m == 0 || n == 0) {
            return 1;
        }
        if (m == 1) {
            return 1;
        }
        if (n == 1) {
            return 1;
        }
        // f(m, n) = f(m - 1, n) + f(m, n - 1)
        int left = computeIfAbsent(cache, pair(m - 1, n), x -> calcUniquePath(m - 1, n, cache));
        int right = computeIfAbsent(cache, pair(m, n - 1), x -> calcUniquePath(m, n - 1, cache));
        return left + right;
    }

    private static <X, V> V computeIfAbsent(Map<X, V> map, X key, Function<X, V> value) {
        V res = map.get(key);
        if (res != null) {
            return res;
        }
        res = value.apply(key);
        map.put(key, res);
        return res;
    }

    private static Set<Integer> pair(int x, int y) {
        if (x == y) {
            return Set.of(x);
        }
        return Set.of(x, y);
    }

    @Test
    void test1() {
        int actual = uniquePaths(3, 7);
        Assertions.assertEquals(28, actual);
    }

    @Test
    void test2() {
        int actual = uniquePaths(2, 3);
        Assertions.assertEquals(3, actual);
    }
}
