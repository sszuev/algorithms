package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You are given an integer array `cost` where `cost[i]` is the cost of ith step on a staircase.
 * Once you pay the cost, you can either climb one or two steps.
 * You can either start from the step with index 0, or the step with index 1.
 * Return the minimum cost to reach the top of the floor.
 * <p>
 * Constraints: `2 <= cost.length <= 1000`, `0 <= cost[i] <= 999`
 *
 * @see <a href="https://leetcode.com/problems/min-cost-climbing-stairs/">746. Min Cost Climbing Stairs</a>
 */
public class L41_MinCostClimbingStairs {
    public int minCostClimbingStairs(int[] cost) {
        return calcMinCostClimbingStairs(cost);
    }

    private static int calcMinCostClimbingStairs(int[] cost) {
        // d(n-2) = cost(n-2)
        // d(n-1) = cost(n-1)
        // d(i-2) = cost(i-2) + min{d(i-1), d(i)}

        int n = cost.length;

        int di = cost[n - 1];
        int di1 = cost[n - 2];
        int di2 = -1;

        for (int i = n; i >= 3; i--) {
            di2 = Math.min(di, di1) + cost[i - 3];
            di = di1;
            di1 = di2;
        }
        return Math.min(di, di1);
    }

    @Test
    void test1() {
        int actual = minCostClimbingStairs(new int[]{10, 15, 20});
        Assertions.assertEquals(15, actual);
    }

    @Test
    void test2() {
        int actual = minCostClimbingStairs(new int[]{1, 100, 1, 1, 1, 100, 1, 1, 100, 1});
        Assertions.assertEquals(6, actual);
    }
}
