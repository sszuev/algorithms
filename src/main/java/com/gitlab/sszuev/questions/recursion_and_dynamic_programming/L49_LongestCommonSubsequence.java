package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Given two strings text1 and text2, return the length of their longest common subsequence.
 * If there is no common subsequence, return 0.
 * A subsequence of a string is a new string generated from the original string with some characters (can be none)
 * deleted without changing the relative order of the remaining characters.
 * For example, "ace" is a subsequence of "abcde".
 * A common subsequence of two strings is a subsequence that is common to both strings.
 * <p>
 * Constraints: `1 <= text1.length, text2.length <= 1000`; text1 and text2 consist of only lowercase English characters.
 *
 * @see <a href="https://leetcode.com/problems/longest-common-subsequence/">1143. Longest Common Subsequence</a>
 */
public class L49_LongestCommonSubsequence {

    public int longestCommonSubsequence(String text1, String text2) {
        return lcsLength(text1.toCharArray(), text2.toCharArray(), new Pair(0, 0), new HashMap<>());
    }

    private static int lcsLength(char[] a, char[] b, Pair start, Map<Pair, Integer> cache) {
        if (start.i > a.length - 1 || start.j > b.length - 1) {
            return 0;
        }
        if (a[start.i] == b[start.j]) {
            Pair k = new Pair(start.i + 1, start.j + 1);
            Integer res = cache.get(k);
            if (res == null) {
                res = lcsLength(a, b, k, cache);
                cache.put(k, res);
            }
            return 1 + res;
        }
        Pair k1 = new Pair(start.i + 1, start.j);
        Pair k2 = new Pair(start.i, start.j + 1);
        Integer res1 = cache.get(k1);
        if (res1 == null) {
            res1 = lcsLength(a, b, k1, cache);
            cache.put(k1, res1);
        }
        Integer res2 = cache.get(k2);
        if (res2 == null) {
            res2 = lcsLength(a, b, k2, cache);
            cache.put(k2, res2);
        }
        return Math.max(res1, res2);
    }

    static class Pair {
        final int i; final int j;
        final int h;

        Pair(int i, int j) {
            this.i = i;
            this.j = j;
            int h = i;
            h = 31 * h + j;
            this.h = h;
        }

        @Override
        public boolean equals(Object o) {
            Pair pair = (Pair) o;
            return i == pair.i && j == pair.j;
        }

        @Override
        public int hashCode() {
            return h;
        }
    }

    @Test
    void test1() {
        int actual = longestCommonSubsequence("abcde", "ace");
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test2() {
        int actual = longestCommonSubsequence("abc", "abc");
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test3() {
        int actual = longestCommonSubsequence("abc", "def");
        Assertions.assertEquals(0, actual);
    }

    @Test
    void test4() {
        int actual = longestCommonSubsequence("nematode knowledge", "empty bottle");
        Assertions.assertEquals(7, actual);
    }
}
