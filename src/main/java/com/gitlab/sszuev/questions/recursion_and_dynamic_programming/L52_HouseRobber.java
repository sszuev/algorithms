package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

/**
 * You are a professional robber planning to rob houses along a street.
 * Each house has a certain amount of money stashed,
 * the only constraint stopping you from robbing each of them is that
 * adjacent houses have security systems connected,
 * and it will automatically contact the police if two adjacent houses were broken into on the same night.
 * Given an integer array nums representing the amount of
 * money of each house,
 * return the maximum amount of money you can rob tonight without alerting the police.
 * <p>
 * Constraints: `1 <= nums.length <= 100`; `0 <= nums[i] <= 400`
 *
 * @see <a href="https://leetcode.com/problems/house-robber">198. House Robber</a>
 */
public class L52_HouseRobber {

    public int rob(int[] nums) {
        int right = 0;
        int left = 0;
        for (int num : nums) {
            int newLeft = left + num;
            int newRight = Math.max(left, right);
            right = newLeft;
            left = newRight;
        }
        return Math.max(right, left);
    }

    private static int calcMaxRobberyBenefit(int[] nums, int length, Map<Integer, Integer> cache) {
        if (length <= 0) {
            return 0;
        }
        if (length == 1) {
            return nums[0];
        }
        if (length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        int leftLength = length - 2;
        int rightLength = length - 3;
        int left;
        if (cache.containsKey(leftLength)) {
            left = cache.get(leftLength);
        } else {
            left = calcMaxRobberyBenefit(nums, leftLength, cache);
            cache.put(leftLength, left);
        }
        int right;
        if (cache.containsKey(rightLength)) {
            right = cache.get(rightLength);
        } else {
            right = calcMaxRobberyBenefit(nums, rightLength, cache);
            cache.put(rightLength, right);
        }
        return Math.max(left + nums[length - 1], right + nums[length - 2]);
    }

    @Test
    void test1() {
        int[] given = new int[]{1, 2, 3, 1};
        Assertions.assertEquals(4, rob(given));
    }

    @Test
    void test2() {
        int[] given = new int[]{2, 7, 9, 3, 1};
        Assertions.assertEquals(12, rob(given));
    }
}
