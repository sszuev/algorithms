package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Find all valid combinations of `k` numbers that sum up to `n` such that the following conditions are true:
 * 1) only numbers 1 through 9 are used; 2) each number is used at most once.
 * Return a list of all possible valid combinations.
 * The list must not contain the same combination twice, and the combinations may be returned in any order.
 * <p>
 * Constraints: `2 <= k <= 9`; `1 <= n <= 60`
 *
 * @see <a href="https://leetcode.com/problems/combination-sum-iii">216. Combination Sum III</a>
 */
public class L54_CombinationSumIII {

    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> res = new ArrayList<>();
        calcCombinations(k, n, 0, 1, new ArrayList<>(), res);
        return res;
    }

    private static void calcCombinations(int k, int n, int sum, int start, List<Integer> solution, List<List<Integer>> res) {
        if (solution.size() == k) {
            if (sum == n) {
                res.add(new ArrayList<>(solution));
            }
            return;
        }
        for (int i = start; i <= 9; i++) {
            solution.add(i);
            calcCombinations(k, n, sum + i, i + 1, solution, res);
            solution.remove(solution.size() - 1);
        }
    }

    @Test
    void test1() {
        List<List<Integer>> actual = combinationSum3(3, 9);
        test(actual, Set.of(Set.of(1, 2, 6), Set.of(1, 3, 5), Set.of(2, 3, 4)), 3);
    }

    @Test
    void test2() {
        List<List<Integer>> actual = combinationSum3(3, 7);
        test(actual, Set.of(Set.of(1,2,4)), 3);
    }

    @Test
    void test3() {
        List<List<Integer>> actual = combinationSum3(4, 1);
        test(actual, Set.of(), 4);
    }

    private void test(List<List<Integer>> actual, Set<Set<Integer>> expected, int k) {
        Set<Set<Integer>> res = new HashSet<>();
        actual.forEach(integers -> {
            Assertions.assertEquals(k, integers.size());
            Set<Integer> inner = new HashSet<>(integers);
            Assertions.assertEquals(k, inner.size());
            res.add(inner);
        });
        Assertions.assertEquals(expected, res);
    }
}
