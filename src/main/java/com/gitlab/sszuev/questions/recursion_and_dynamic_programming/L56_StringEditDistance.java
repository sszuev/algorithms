package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Given two strings word1 and word2, return the minimum number of operations required to convert word1 to word2.
 * You have the following three operations permitted on a word:
 * 1) Insert a character
 * 2) Delete a character
 * 3) Replace a character
 * <p>
 * Constraints:
 * `0 <= word1.length, word2.length <= 500`;
 * word1 and word2 consist of lowercase English letters.
 *
 * @see <a href="https://leetcode.com/problems/edit-distance/description">72. Edit Distance</a>
 */
public class L56_StringEditDistance {
    public int minDistance(String word1, String word2) {
        return calcMinDistance(word1, word2);
    }

    private static int calcMinDistance(String word1, String word2) {
        // insert:  f(i, j - 1) + 1
        // delete:  f(i - 1, j) + 1
        // replace: f(i - 1, j - 1) + {1,0}
        // f(i, j) = min(insert, delete, replace)
        // f(i, 0) = i
        // f(0, j) = j
        return f(word1.toCharArray(), word2.toCharArray(), word1.length(), word2.length(), new HashMap<>());
    }

    private static int f(char[] word1, char[] word2, int i, int j, Map<IntPair, Integer> cache) {
        if (i == 0) {
            return j;
        }
        if (j == 0) {
            return i;
        }
        int insert = withCache(word1, word2, i, j - 1, cache) + 1;
        int delete = withCache(word1, word2, i - 1, j, cache) + 1;
        int replace = withCache(word1, word2, i - 1, j - 1, cache) + (word1[i - 1] == word2[j - 1] ? 0 : 1);
        return Math.min(insert, Math.min(delete, replace));
    }

    private static int withCache(char[] word1, char[] word2, int i, int j, Map<IntPair, Integer> cache) {
        IntPair key = new IntPair(i, j);
        Integer res = cache.get(key);
        if (res == null) {
            res = f(word1, word2, i, j, cache);
            cache.put(key, res);
        }
        return res;
    }

    private static class IntPair {
        final int i;
        final int j;
        final int h;

        private IntPair(int i, int j) {
            this.i = i;
            this.j = j;
            int h = i;
            h = 31 * h + j;
            this.h = h;
        }

        @Override
        public final boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof IntPair)) return false;

            IntPair intPair = (IntPair) o;
            return i == intPair.i && j == intPair.j;
        }

        @Override
        public int hashCode() {
            return h;
        }
    }

    @Test
    void test1() {
        int actual = minDistance("horse", "ros");
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test2() {
        int actual = minDistance("intention", "execution");
        Assertions.assertEquals(5, actual);
    }
}
