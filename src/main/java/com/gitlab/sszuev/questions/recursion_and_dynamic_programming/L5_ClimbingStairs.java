package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You are climbing a staircase. It takes n steps to reach the top.
 * Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
 * <p>
 * Example 1:
 * <pre>{@code
 * Input: n = 2
 * Output: 2
 * Explanation: There are two ways to climb to the top.
 * 1. 1 step + 1 step
 * 2. 2 steps
 * }</pre>
 * Example 2:
 * <pre>{@code
 * Input: n = 3
 * Output: 3
 * Explanation: There are three ways to climb to the top.
 * 1. 1 step + 1 step + 1 step
 * 2. 1 step + 2 steps
 * 3. 2 steps + 1 step
 * }</pre>
 * <p>
 * {@code 1 <= n <= 45}
 *
 * @see <a href="https://leetcode.com/problems/climbing-stairs/">70. Climbing Stairs</a>
 */
public class L5_ClimbingStairs {

    // f(n) = f(n-2) + f(n-1); f(1) = 1; f(2) = 2;
    public static int climbStairs(int n) {
        if (n < 1 || n > 45) {
            throw new IllegalArgumentException();
        }
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }
        int res = 0;
        int fNm1 = 2; // f(n-1)
        int fNm2 = 1; // f(n-2)
        for (int i = 3; i <= n; i++) {
            res = fNm1 + fNm2;
            fNm2 = fNm1;
            fNm1 = res;
        }
        return res;
    }

    @Test
    public void testN2() {
        Assertions.assertEquals(2, climbStairs(2));
    }

    @Test
    public void testN3() {
        Assertions.assertEquals(3, climbStairs(3));
    }

    @Test
    public void testN4() {
        Assertions.assertEquals(5, climbStairs(4));
    }
}
