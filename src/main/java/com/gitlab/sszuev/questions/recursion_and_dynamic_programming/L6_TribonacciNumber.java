package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * The Tribonacci sequence Tn is defined as follows:
 * {@code T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0}
 * Given n, return the value of Tn.
 * <p>
 * {@code 0 <= n <= 37}; The answer is guaranteed to fit within a 32-bit integer, i.e., {@code answer <= 2^31 - 1}.
 *
 * @see <a href="https://leetcode.com/problems/n-th-tribonacci-number">1137. N-th Tribonacci Number</a>
 */
public class L6_TribonacciNumber {

    public static int tribonacci(int n) {
        if (n < 0 || n > 37) {
            throw new IllegalArgumentException();
        }
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 1;
        }
        int res = 0;
        // Tn = Tn-3 + Tn-2 + Tn-1
        int tNm3 = 0;
        int tNm2 = 1;
        int tNm1 = 1;
        for (int i = 3; i <= n; i++) {
            res = tNm3 + tNm2 + tNm1;
            tNm3 = tNm2;
            tNm2 = tNm1;
            tNm1 = res;
        }
        return res;
    }

    @Test
    public void testN4() {
        Assertions.assertEquals(4, tribonacci(4));
    }

    @Test
    public void testN25() {
        Assertions.assertEquals(1389537, tribonacci(25));
    }
}
