package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You have two types of tiles: a `2 x 1` domino shape and a tromino shape. You may rotate these shapes.
 * Given an integer `n`, return the number of ways to tile an `2 x n` board.
 * Since the answer may be very large, return it modulo `10^9 + 7`.
 * In a tiling, every square must be covered by a tile.
 * Two tilings are different if and only if there are two 4-directionally adjacent cells on the board such
 * that exactly one of the tilings has both squares occupied by a tile.
 * <p>
 * Constraints:
 * `1 <= n <= 1000`
 *
 * @see <a href="https://leetcode.com/problems/domino-and-tromino-tiling">790. Domino and Tromino Tiling</a>
 */
public class L88_DominoAndTrominoTiling {
    public int numTilings(int n) {
        return calcNumTilings(n);
    }

    private static int calcNumTilings(int n) {
        // A[N] = A[N-1] * 2 + A[N-3]
        long a1 = 1;
        long a2 = 2;
        long a3 = 5;
        long a4 = -1;
        if (n == 1) {
            return (int) a1;
        }
        if (n == 2) {
            return (int) a2;
        }
        if (n == 3) {
            return (int) a3;
        }
        for (int i = 4; i <= n; i++) {
            a4 = (a3 * 2 + a1) % 1_000_000_007;
            a1 = a2;
            a2 = a3;
            a3 = a4;
        }
        return (int) a4;
    }

    @Test
    void test1() {
        Assertions.assertEquals(5, numTilings(3));
    }

    @Test
    void test2() {
        Assertions.assertEquals(11, numTilings(4));
    }

    @Test
    void test3() {
        Assertions.assertEquals(24, numTilings(5));
    }
}
