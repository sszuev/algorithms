package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Given an integer numRows, return the first numRows of Pascal's triangle.
 * In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:
 *
 * @see <a href="https://leetcode.com/problems/pascals-triangle">118. Pascal's Triangle</a>
 */
public class L92_PascalsTriangle {

    public List<List<Integer>> generate(int numRows) {
        return calc(numRows);
    }

    private static List<List<Integer>> calc(int numRows) {
        if (numRows == 0) {
            return List.of();
        }
        if (numRows == 1) {
            return List.of(List.of(1));
        }
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> level = List.of(1, 1);
        res.add(List.of(1));
        res.add(level);
        for (int i = 1; i < numRows - 1; i++) {
            List<Integer> next = next(level);
            res.add(next);
            level = next;
        }
        return res;
    }

    private static List<Integer> next(List<Integer> level) {
        List<Integer> next = new ArrayList<>();
        next.add(1);
        for (int j = 0; j < level.size() - 1; j++) {
            Integer x = level.get(j);
            Integer y = level.get(j + 1);
            next.add(x + y);
        }
        next.add(1);
        return next;
    }

    @Test
    void test1() {
        // Input: numRows = 5
        // Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
        List<List<Integer>> expected = List.of(
                List.of(1),
                List.of(1, 1),
                List.of(1, 2, 1),
                List.of(1, 3, 3, 1),
                List.of(1, 4, 6, 4, 1));
        List<List<Integer>> res = generate(5);
        Assertions.assertEquals(expected, res);
    }
}
