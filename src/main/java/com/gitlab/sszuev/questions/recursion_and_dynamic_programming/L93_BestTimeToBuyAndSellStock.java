package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You are given an array prices where `prices[i]` is the price of a given stock on the ith day.
 * You want to maximize your profit by choosing a single day to buy one stock and choosing
 * a different day in the future to sell that stock.
 * Return the maximum profit you can achieve from this transaction.
 * If you cannot achieve any profit, return 0.
 * <p>
 * Constraints:
 * - `1 <= prices.length <= 10^5`
 * - `0 <= prices[i] <= 10^4`
 *
 * @see <a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock">121. Best Time to Buy and Sell Stock</a>
 */
public class L93_BestTimeToBuyAndSellStock {
    public int maxProfit(int[] prices) {
        return calcMaxProfit(prices);
    }

    private static int calcMaxProfit(int[] prices) {
        int maxProfit = 0;
        int min = prices[0];
        for (int i = 1; i < prices.length; i++) {
            int price = prices[i];
            min = Math.min(min, price);
            maxProfit = Math.max(maxProfit, price - min);
        }
        return maxProfit;
    }

    @Test
    void test1() {
        int[] given = new int[]{7, 1, 5, 3, 6, 4};
        int res = maxProfit(given);
        Assertions.assertEquals(5, res);
    }

    @Test
    void test2() {
        int[] given = new int[]{7,6,4,3,1};
        int res = maxProfit(given);
        Assertions.assertEquals(0, res);
    }
}
