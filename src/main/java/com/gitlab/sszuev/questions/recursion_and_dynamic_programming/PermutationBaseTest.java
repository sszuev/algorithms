package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by @ssz on 06.01.2021.
 */
abstract class PermutationBaseTest {

    public static Solution[] data() {
        throw new UnsupportedOperationException();
    }

    static void insert(String s, char c, Collection<String> res) {
        for (int i = 0; i < s.length(); i++) {
            res.add(new StringBuilder(s).insert(i, c).toString());
        }
        res.add(s + c);
    }

    static Map<Character, Long> toMap(String s) {
        return s.chars().mapToObj(x -> (char) x)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testABC(Solution data) {
        Set<String> expected = Stream.of("abc", "bac", "acb", "cab", "bca", "cba").collect(Collectors.toSet());
        Collection<String> actual = data.getPermutations("abc");
        Assertions.assertEquals(expected.size(), actual.size());
        Assertions.assertEquals(expected, actual instanceof Set ? actual : new HashSet<>(actual));
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testABCD(Solution data) {
        test(data, "abcd", 24); // 4!
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testABCDE(Solution data) {
        test(data, "abcde", 120); // 5!
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testABCDEF(Solution data) {
        test(data, "abcdef", 720); // 6!
    }

    void test(Solution data, String s, long length) {
        Collection<String> actual = data.getPermutations(s);
        Assertions.assertEquals(length, actual.size());
    }

    interface Solution {
        Collection<String> getPermutations(String s);
    }
}
