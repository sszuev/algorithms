package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Boolean Evaluation:
 * Given a boolean expression consisting of the symbols {@code 0}(FALSE), {@code 1}(TRUE), {@code &}(AND), {@code |}(OR), {@code ^}(XOR),
 * and a desired boolean result value {@code result},
 * implement a function to count the number of ways of parenthesizing the expression
 * such that it evaluates to {@code result}.
 * EXAMPLES:
 * <ul>
 * <li>{@code countEval("1^0|0|1", false) = 2} (i.e. {@code (1^((0|0)|1)) == (1^(0|(0|1))) == false})</li>
 * <li>{@code countEval("0&0&0&1^1|0", true) = 10}</li>
 * </ul>
 * Hints: #148, #168, #197, #305, #327
 * <ul>
 * <li>#148: Can we just try all possibilities? What would this look like?</li>
 * <li>#168: We can think about each possibility as each place where we can put parentheses.
 * This means around each operator, such that the expression is split at the operator. What is the base case?</li>
 * <li>#197: The base case is when we have a single value, {@code 1} or {@code O}.</li>
 * <li>#305: If your code looks really lengthy, with a lot of if's (for each possible operator, "target"
 * boolean result, and left/right side), think about the relationship between the different
 * parts. Try to simplify your code. It should not need a ton of complicated if-statements.
 * For example, consider expressions of the form <LEFT>OR<RIGHT> versus <LEFT>AND<RIGHT>.
 * Both may need to know the number of ways that the <LEFT> evaluates to {@code true}. See what code you can reuse. </li>
 * <li>#327: Look at your recursion. Do you have repeated calls anywhere? Can you memoize it?</li>
 * </ul>
 * <p>
 * <p>
 * Created by @ssz on 07.11.2019.
 */
public class Q14_BooleanExpressionEvaluationWithGivenResult {

    static Stream<Solution> solutions() {
        return Stream.of(new OriginalFromBookSolution(), new MySolution());
    }

    interface Solution {
        int countEval(String expression, boolean result);

        String name();
    }

    @DisplayName("0&1 => false")
    @ParameterizedTest
    @MethodSource("solutions")
    public void test0And1ToFALSE(Solution solution) {
        test(solution, "0&1", false, 1);
    }

    @DisplayName("0&0|1 => false")
    @ParameterizedTest
    @MethodSource("solutions")
    public void test0And0Or1ToFALSE(Solution solution) {
        test(solution, "0&0|1", false, 1);
    }

    @DisplayName("0^0&0^1|1 => false")
    @ParameterizedTest
    @MethodSource("solutions")
    public void test0Xor0And0Xor1Or1ToFALSE(Solution solution) {
        // 0^0&0^1|1
        test(solution, "0^0&0^1|1", false, 4);
    }

    @DisplayName("1^0|0|1 => false")
    @ParameterizedTest
    @MethodSource("solutions")
    public void test1Xor0Or0Or1ToFALSE(Solution solution) {
        // 1^0|0|1
        test(solution, "1^0|0|1", false, 2);
    }

    @DisplayName("1^0|0|1 => true")
    @ParameterizedTest
    @MethodSource("solutions")
    public void test1Xor0Or0Or1ToTRUE(Solution solution) {
        // 1^0|0|1
        test(solution, "1^0|0|1", true, 3);
    }

    @DisplayName("0&0&0&1^1|0 => true")
    @ParameterizedTest
    @MethodSource("solutions")
    public void test0And0And0And1Xor1Or0ToTRUE(Solution solution) {
        // 0&0&0&1^1|0
        test(solution, "0&0&0&1^1|0", true, 10);
    }

    static void test(Solution solution, String expression, boolean result, int expectedCount) {
        System.out.println("==".repeat(42));
        System.out.println("Given expression: " + expression + " => " + result);
        MySolution.Expr expr = MySolution.Expr.parse(expression);
        System.out.println("Direct (no brackets) evaluation: " + expr + " => " + expr.eval());
        int actualCount = solution.countEval(expression, result);
        System.out.println(solution.name() + " : " + actualCount);
        Assertions.assertEquals(expectedCount, actualCount);
    }

    static class OriginalFromBookSolution implements Solution {
        @Override
        public String name() {
            return "FROM_BOOK";
        }

        @Override
        public String toString() {
            return name();
        }

        @Override
        public int countEval(String expression, boolean result) { // 381 page
            if (expression.length() == 0) return 0;
            if (expression.length() == 1) return stringToBool(expression) == result ? 1 : 0;

            int ways = 0;
            for (int i = 1; i < expression.length(); i += 2) {
                char c = expression.charAt(i);
                String left = expression.substring(0, i);
                String right = expression.substring(i + 1);

                int leftTrue = countEval(left, true);
                int leftFalse = countEval(left, false);
                int rightTrue = countEval(right, true);
                int rightFalse = countEval(right, false);

                int total = (leftTrue + leftFalse) * (rightTrue + rightFalse);

                int totalTrue = 0;
                if (c == '^') { // one true and one false
                    totalTrue = leftTrue * rightFalse + leftFalse * rightTrue;
                } else if (c == '&') { // both true
                    totalTrue = leftTrue * rightTrue;
                } else if (c == '|') {
                    totalTrue = leftTrue * rightTrue + leftFalse * rightTrue + leftTrue * rightFalse;
                }
                int subWays = result ? totalTrue : (total - totalTrue);
                ways += subWays;
            }
            return ways;
        }

        private boolean stringToBool(String s) {
            return "1".equals(s);
        }
    }

    static class MySolution implements Solution {
        @Override
        public String name() {
            return "MY_SOLUTION";
        }

        @Override
        public String toString() {
            return name();
        }

        @Override
        public int countEval(String expression, boolean result) {
            return countEval(Expr.parse(expression), Type.of(result));
        }

        public int countEval(Expr expr, Type expected) {
            if (expr.size() <= 3) { // no need parenthesis
                return expr.eval() == expected ? 1 : 0;
            }
            int res = 0;
            for (int i = 1; i < expr.size(); i += 2) {
                Expr leftExpr = expr.subTo(i - 1);
                Op op = expr.getOp(i);
                Expr rightExpr = expr.subFrom(i + 1);

                for (Type left : Type.values()) {
                    for (Type right : Type.values()) {
                        if (op.apply(left, right) != expected) {
                            continue;
                        }
                        int l = countEval(leftExpr, left);
                        int r = countEval(rightExpr, right);
                        int v = l * r;
                        res += v;
                    }
                }
            }
            return res;
        }

        enum Bracket implements Expr.Part {
            LEFT {
                @Override
                public String toString() {
                    return "(";
                }
            },
            RIGHT {
                @Override
                public String toString() {
                    return ")";
                }
            },
            ;

            public static Bracket of(char x) {
                switch (x) {
                    case '(':
                        return LEFT;
                    case ')':
                        return RIGHT;
                    default:
                        return null;
                }
            }
        }

        @SuppressWarnings("unused")
        enum Type implements Expr.Part {
            TRUE(true),
            FALSE(false),
            ;

            Type(boolean b) {
                this.value = b;
            }

            private final boolean value;

            public boolean toBoolean() {
                return value;
            }

            public int toInt() {
                return value ? 1 : 0;
            }

            public Type inverse() {
                return TRUE == this ? FALSE : TRUE;
            }

            public static Type of(boolean b) {
                return b ? TRUE : FALSE;
            }

            public static Type of(int i) {
                return i == 1 ? TRUE : FALSE;
            }

            public static Type of(char x) {
                switch (x) {
                    case '1':
                        return TRUE;
                    case '0':
                        return FALSE;
                    default:
                        return null;
                }
            }

            @Override
            public boolean isOperand() {
                return true;
            }
        }

        enum Op implements Expr.Part {
            AND {
                @Override
                public Type apply(Type left, Type right) {
                    return Type.of(left.toInt() & right.toInt());
                }

                @Override
                public String toString() {
                    return "&";
                }
            },
            OR {
                @Override
                public Type apply(Type left, Type right) {
                    return Type.of(left.toInt() | right.toInt());
                }

                @Override
                public String toString() {
                    return "|";
                }
            },
            XOR {
                @Override
                public Type apply(Type left, Type right) {
                    return Type.of(left.toInt() ^ right.toInt());
                }

                @Override
                public String toString() {
                    return "^";
                }
            },
            ;

            public abstract Type apply(Type left, Type right);

            public static Op of(char x) {
                switch (x) {
                    case '&':
                        return Op.AND;
                    case '|':
                        return Op.OR;
                    case '^':
                        return Op.XOR;
                    default:
                        return null;
                }
            }

            @Override
            public boolean isOperation() {
                return true;
            }
        }

        static class Expr {
            private final List<Part> list;

            Expr(List<Part> expr) {
                this.list = expr;
            }

            public long size() {
                return list.size();
            }

            public Expr subFrom(int from) {
                return subExpr(from, list.size());
            }

            public Expr subTo(int to) {
                return subExpr(0, to + 1);
            }

            public Expr subExpr(int include, int exclude) {
                return new Expr(list.subList(include, exclude));
            }

            public Op getOp(int index) {
                return (Op) list.get(index);
            }

            public static Expr parse(String txt) {
                List<Part> res = new ArrayList<>();
                for (char c : txt.toCharArray()) {
                    Expr.Part t = Bracket.of(c);
                    if (t != null) {
                        res.add(t);
                        continue;
                    }
                    t = Type.of(c);
                    if (t != null) {
                        res.add(t);
                        continue;
                    }
                    t = Op.of(c);
                    if (t == null)
                        throw new IllegalArgumentException();
                    if (res.get(res.size() - 1) instanceof Op)
                        throw new IllegalArgumentException();
                    res.add(t);
                }
                return new Expr(res);
            }

            @Override
            public String toString() {
                return list.stream().map(Object::toString).collect(Collectors.joining(" ", "[", "]"));
            }

            public Type eval() {
                if (list.size() == 1) {
                    Part p = list.get(0);
                    if (p instanceof Type) {
                        return (Type) p;
                    }
                    throw new IllegalStateException();
                }
                int i = list.indexOf(Bracket.LEFT);
                if (i != -1) {
                    int j = list.lastIndexOf(Bracket.RIGHT);
                    if (j == -1)
                        throw new IllegalStateException();
                    List<Part> list = new ArrayList<>(this.list.subList(0, i));
                    list.add(new Expr(this.list.subList(i + 1, j)).eval());
                    list.addAll(this.list.subList(j + 1, this.list.size()));
                    return new Expr(list).eval();
                }
                Op op1 = (Op) list.get(1);
                Type left = (Type) list.get(0);
                Type right = (Type) list.get(2);
                Type res = op1.apply(left, right);
                if (list.size() == 3) {
                    return res;
                }
                Op op2 = (Op) list.get(3);
                return op2.apply(res, new Expr(list.subList(4, list.size())).eval());
            }

            @SuppressWarnings("unused")
            interface Part {
                default boolean isOperand() {
                    return false;
                }

                default boolean isOperation() {
                    return false;
                }
            }
        }

    }

}
