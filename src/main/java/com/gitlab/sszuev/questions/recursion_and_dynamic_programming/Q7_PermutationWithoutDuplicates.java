package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 8.7: Permutations without Dups: Write a method to compute all permutations of a string of unique characters.
 * Hints: #150, #185, #200, #267, #278, #309, #335, #356
 * <p>
 * #150: Approach 1: Suppose you had all permutations of {@code abc}.
 * How can you use that to get all permutations of {@code abcd}?
 * <p>
 * #185: Approach 1: The permutations of {@code abc} represent all ways of ordering abc.
 * Now, we want to create all orderings of {@code abcd}.
 * Take a specific ordering of {@code abcd}, such as {@code bdca}.
 * This {@code bdca} string represents an ordering of {@code abc}, too:
 * remove the {@code d} and you get {@code bca}.
 * Given the string {@code bca}, can you create all the "related" orderings that include {@code d}, too?
 * <p>
 * #200: Approach 1: Given a string such as {@code bca},
 * you can create all permutations of {@code abcd}
 * that have {@code {a, b, c}} in the order {@code bca} by inserting {@code d} into each possible location:
 * {@code dbca}, {@code bdca}, {@code bcda}, {@code bcad}.
 * Given all permutations of {@code abc}, can you then create all permutations of {@code abcd}?
 * <p>
 * #267: Approach 1: You can create all permutations of {@code abcd} by computing all permutations of {@code abc}
 * and then inserting {@code d} into each possible location within those.
 * <p>
 * #278: Approach 2: If you had all permutations of two-character substrings,
 * could you generate all permutations of three-character substrings?
 * <p>
 * #309: Approach 2: To generate a permutation of {@code abcd}, you need to pick an initial character.
 * It can be {@code a}, {@code b}, {@code c}, or {@code d}.
 * You can then permute the remaining characters.
 * How can you use this approach to generate all permutations of the full string?
 * <p>
 * #335: Approach 2: To generate all permutations of {@code abcd},
 * pick each character ({@code a}, {@code b}, {@code c}, or {@code d}) as a starting character.
 * Permute the remaining characters and prepend the starting character.
 * How do you permute the remaining characters?
 * With a recursive process that follows the same logic.
 * <p>
 * #356: Approach 2: You can implement this approach by having the recursive function pass back the list of the strings,
 * and then you prepend the starting character to it. Or, you can push down a prefix to the recursive calls.
 * <p>
 * <p>
 * Created by @ssz on 05.07.2020.
 */
public class Q7_PermutationWithoutDuplicates extends PermutationBaseTest {

    public static Solution[] data() {
        return ESolution.values();
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testABCAAC(Solution data) {
        test(data, "ABCAAC", 720);
    }

    enum ESolution implements Solution {
        MY_UPDATED {
            @Override
            public List<String> getPermutations(String s) {
                if (s.length() == 1) {
                    return Collections.singletonList(s);
                }
                if (s.length() == 2) {
                    return Arrays.asList(s, new StringBuilder(s).reverse().toString());
                }
                List<String> res = new ArrayList<>();
                char c = s.charAt(0);
                getPermutations(s.substring(1))
                        .forEach(x -> insert(x, c, res));
                return res;
            }
        },
        APPROACH_1 {
            @Override
            public List<String> getPermutations(String str) {
                List<String> res = new ArrayList<>();
                if (str.isEmpty()) { // base case
                    res.add("");
                    return res;
                }
                char first = str.charAt(0); // get the first char
                String remainder = str.substring(1);  // remove the first char
                List<String> words = getPermutations(remainder);
                for (String word : words) {
                    for (int j = 0; j <= word.length(); j++) {
                        String s = insertCharAt(word, first, j);
                        res.add(s);
                    }
                }
                return res;
            }

            private String insertCharAt(String word, char c, int i) {
                String start = word.substring(0, i);
                String end = word.substring(i);
                return start + c + end;
            }
        },
        APPROACH_2 {
            @Override
            public List<String> getPermutations(String str) {
                int len = str.length();
                List<String> res = new ArrayList<>();
                if (len == 0) {
                    res.add(""); // Be sure to return empty string!
                    return res;
                }
                for (int i = 0; i < len; i++) {
                    // Remove char i and find permutations of remaining chars.
                    String before = str.substring(0, i);
                    String after = str.substring(i + 1, len);
                    List<String> partials = getPermutations(before + after);
                    // Prepend char i to each permutation
                    for (String s : partials) {
                        res.add(str.charAt(i) + s);
                    }
                }
                return res;
            }
        },
        APPROACH_21 {
            @Override
            public List<String> getPermutations(String s) {
                List<String> res = new ArrayList<>();
                getPerms("", s, res);
                return res;
            }

            private void getPerms(String prefix, String remainder, Collection<String> result) {
                int len = remainder.length();
                if (len == 0) result.add(prefix);
                for (int i = 0; i < len; i++) {
                    String before = remainder.substring(0, i);
                    String after = remainder.substring(i + 1, len);
                    char c = remainder.charAt(i);
                    getPerms(prefix + c, before + after, result);
                }
            }
        }

    }
}
