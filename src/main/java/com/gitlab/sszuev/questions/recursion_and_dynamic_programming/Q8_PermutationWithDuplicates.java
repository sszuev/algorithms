package com.gitlab.sszuev.questions.recursion_and_dynamic_programming;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 8.8: Permutations with Dups:
 * Write a method to compute all permutations of a string whose characters are not necessarily unique.
 * The list of permutations should not have duplicates.
 * <p>
 * Hints: #161, #190, #222, #255
 * <p>
 * #161: You could handle this by just checking to see if
 * there are duplicates before printing them (or adding them to a list).
 * You can do this with a hash table.
 * In what case might this be okay? In what case might it not be a very good solution?
 * <p>
 * #190: If you haven't solved 8.7 yet, do that one first.
 * <p>
 * #222: Try getting the count of each character.
 * For example, {@code ABCAAC} has 3 {@code A}s, 2 {@code C}s, and 1 {@code B}.
 * <p>
 * #255: To get all permutations with 3 {@code A}s, 2 {@code C}s, and 1 {@code B},
 * you need to first pick a starting character: {@code A}, {@code B}, or {@code C}.
 * If it's an {@code A}, then you need all permutations with 2 {@code A}s, 2 {@code C}s, and 1 {@code B}.
 * <p>
 * Created by @ssz on 09.07.2020.
 */
public class Q8_PermutationWithDuplicates extends PermutationBaseTest {

    public static Solution[] data() {
        return ESolution.values();
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testABCAAC(Solution data) {
        // 6! / 3! / 2! = 60
        test(data, "ABCAAC", 60);
    }

    enum ESolution implements Solution {
        MY_SOLUTION_WITH_SET {
            @Override
            public Set<String> getPermutations(String s) {
                if (s.isEmpty()) {
                    return Collections.emptySet();
                }
                if (s.length() == 1) {
                    return Collections.singleton(s);
                }
                char c = s.charAt(0);
                Set<String> res = new HashSet<>();
                getPermutations(s.substring(1)).forEach(x -> insert(x, c, res));
                return res;
            }

        },

        FROM_BOOK {
            @Override
            public Collection<String> getPermutations(String s) {
                List<String> res = new ArrayList<>();
                printPerms(toMap(s), "", s.length(), res);
                return res;
            }

            void printPerms(Map<Character, Long> map, String prefix, int remaining, List<String> result) {
                /* Base case. Permutation has been completed. */
                if (remaining == 0) {
                    result.add(prefix);
                    return;
                }
                for (Character c : map.keySet()) {
                    long count = map.get(c);
                    if (count > 0) {
                        map.put(c, count - 1);
                        printPerms(map, prefix + c, remaining - 1, result);
                        map.put(c, count);
                    }
                }
            }
        },

        NEXT_TRY {
            @Override
            public Collection<String> getPermutations(String s) {
                return getPermutations(toMap(s));
            }

            private Collection<String> getPermutations(Map<Character, Long> map) {
                if (map.isEmpty()) {
                    return Collections.emptyList();
                }
                Character c = map.entrySet().stream().filter(x -> x.getValue() > 0)
                        .map(Map.Entry::getKey).findFirst().orElse(null);
                if (c == null) {
                    return Collections.emptyList();
                }
                Map<Character, Long> nextMap = new HashMap<>(map);
                nextMap.computeIfPresent(c, (x, v) -> v == 1 ? null : v - 1);
                if (nextMap.isEmpty()) {
                    return Collections.singletonList(String.valueOf(c));
                }
                Set<String> res = new HashSet<>(); // not enough efficient
                getPermutations(nextMap)
                        .forEach(x -> insert(x, c, res));
                return res;
            }

            void insert(String s, char c, Collection<String> res) {
                if (s.isEmpty()) {
                    res.add(String.valueOf(c));
                    return;
                }
                String t;
                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == c) {
                        continue;
                    }
                    t = s.substring(0, i) + c + s.substring(i);
                    res.add(t);
                }
                t = s + c;
                res.add(t);
            }
        },

        NEXT_TRY_2 {
            @Override
            public Collection<String> getPermutations(String s) {
                List<String> res = new ArrayList<>();
                collectPermutations(toMap(s), "", res);
                return res;
            }

            void collectPermutations(Map<Character, Long> map, String str, Collection<String> res) {
                if (map.isEmpty()) {
                    res.add(str);
                    return;
                }
                for (Character c : map.keySet()) {
                    Map<Character, Long> nextMap = new HashMap<>(map);
                    nextMap.computeIfPresent(c, (x, v) -> v == 1 ? null : v - 1);
                    collectPermutations(nextMap, str + c, res);
                }
            }
        },
    }
}
