[HOME](../../../../../../../../README.md)

- **[T1](T1_BubbleSort.java)**: Bubble (+ Improved) sort
- **[T2](T2_IterativeQuickSort.java)**: Iterative Quicksort
- **[T3](T3_InsertSort.java)**: Insert sort
- **[T4](T4_MergeSort.java)**: Merge sort
- **[T5](T5_BucketSort.java)**: Bucket sort
- **[T6](T6_CountingSort.java)**: Counting sort
- **[T7](T7_RadixSort.java)**: Radix sort

- **[Q1](Q1_MergeTwoSortedArrays.java)**: Sorted Merge: You are given two sorted arrays, A and B, where A has a large
  enough buffer at the end to hold B.
  Write a method to merge B into A in sorted order.

  <details>
    <summary>Hints</summary>  

    - ***332***: Try moving from the end of the array to the beginning.

  </details>  

- **[Q5](Q5_IndexOfInSparseSortedArray.java)**: Sparse Search: Given a sorted array of strings that is interspersed with
  empty strings,
  write a method to find the location of a given string.
  EXAMPLE
  Input: `ball`, `{"at", "", "", "", "ball", "", "", "car", "", "", "dad", "", ""}`
  Output: `4`

  <details>
    <summary>Hints</summary>  

    - ***256***: Try modifying binary search to handle this.

  </details>  

- **[L9](L9_KthLargestElementInArray.java)**: Kth Largest Element in an Array.
  Given an integer array nums and an integer k, return the kth largest element in the array. Note that it is the kth
  largest element in the sorted order,
  not the kth distinct element.

- **[L18](L18_KokoEatingBananas.java)**: (leetcode) Koko Eating Bananas

- **[L38](L38_GuessNumberHigherOrLower.java)**: (leetcode) Guess Number Higher or Lower

- **[L76](L76_SuccessfulPairsOfSpellsAndPotions.java)**: (leetcode) Successful Pairs of Spells and Potions

- **[L86](L86_FindPeakElement.java)**: (leetcode) Find Peak Element