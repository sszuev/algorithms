package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Koko loves to eat bananas. There are n piles of bananas, the ith pile has piles[i] bananas.
 * The guards have gone and will come back in h hours.
 * Koko can decide her bananas-per-hour eating speed of k.
 * Each hour, she chooses some pile of bananas and eats k bananas from that pile.
 * If the pile has less than k bananas, she eats all of them instead and will not eat any more bananas during this hour.
 * Koko likes to eat slowly but still wants to finish eating all the bananas before the guards return.
 * Return the minimum integer k such that she can eat all the bananas within h hours.
 * <p>
 * Constraints:
 * 1) 1 <= piles.length <= 10^4;
 * 2) piles.length <= h <= 10^9;
 * 3) 1 <= piles[i] <= 10^9;
 *
 * @see <a href="https://leetcode.com/problems/koko-eating-bananas">875. Koko Eating Bananas</a>
 */
public class L18_KokoEatingBananas {

    public int minEatingSpeed(int[] piles, int h) {
        return findMinEatingSpeed(piles, h);
    }

    public static int findMinEatingSpeed(int[] piles, int h) {
        int max = 0;
        double sum = 0;
        for (int num : piles) {
            sum += num;
            max = Math.max(max, num);
        }
        int speed = (int) Math.ceil(sum / h);
        int speedMin = 0;
        int speedMax = max;
        int speedPrev = speed;
        while (speedMin <= speedMax) {
            int x = calcH(piles, speed);
            if (x == h) {
                if (speedPrev == speed) {
                    return speed;
                }
                if (speedMin + 1 == speedMax) {
                    return speed;
                }
                speed -= 1;
                if (calcH(piles, speed) != h) {
                    return speed + 1;
                }
            }
            if (speedMin + 1 == speedMax) {
                return speedMax;
            }
            speedPrev = speed;
            if (x < 0 || x > h) {
                speedMin = speed;
                speed = (int) Math.ceil((speed + speedMax) / 2.0);
            } else {
                speedMax = speed;
                speed = (int) ((speed + speedMin) / 2.0);
            }
            if (speed == 0) {
                return 1;
            }
        }
        throw new IllegalStateException();
    }

    public static int calcH(int[] piles, int k) {
        int hours = 0;
        for (int pile : piles) {
            int div = pile / k;
            hours += div;
            if (pile % k != 0) {
                hours++;
            }
        }
        return hours;
    }

    @Test
    public void test1() {
        Assertions.assertEquals(4, minEatingSpeed(new int[]{3, 6, 7, 11}, 8));
    }

    @Test
    public void test2() {
        Assertions.assertEquals(30, minEatingSpeed(new int[]{30, 11, 23, 4, 20}, 5));
    }

    @Test
    public void test3() {
        Assertions.assertEquals(23, minEatingSpeed(new int[]{30, 11, 23, 4, 20}, 6));
    }

    @Test
    public void test4() {
        Assertions.assertEquals(2, minEatingSpeed(new int[]{312884470}, 312884469));
    }

    @Test
    public void test5() {
        Assertions.assertEquals(1, minEatingSpeed(new int[]{312884470}, 968709470));
    }

    @Test
    public void test6() {
        Assertions.assertEquals(142857143, minEatingSpeed(new int[]{1, 1, 1, 999999999}, 10));
    }

    @Test
    public void test7() {
        Assertions.assertEquals(3, minEatingSpeed(new int[]{805306368, 805306368, 805306368}, 1000000000));
    }

}
