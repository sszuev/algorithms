package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * We are playing the Guess Game. The game is as follows:
 * I pick a number from 1 to n. You have to guess which number I picked.
 * Every time you guess wrong, I will tell you whether the number I picked is higher or lower than your guess.
 * You call a pre-defined API {@code int guess(int num)}, which returns three possible results:
 * <p>
 * -1: Your guess is higher than the number I picked (i.e. num > pick).
 * <p>
 * 1: Your guess is lower than the number I picked (i.e. num < pick).
 * <p>
 * 0: your guess is equal to the number I picked (i.e. num == pick).
 * <p>
 * Return the number that I picked.
 * <p>
 * Constraints: {@code 1 <= n <= 2^31 - 1}, {@code 1 <= pick <= n}
 *
 * @see <a href="https://leetcode.com/problems/guess-number-higher-or-lower">374. Guess Number Higher or Lower</a>
 */
public class L38_GuessNumberHigherOrLower {

    @Test
    void test1() {
        Assertions.assertEquals(6, new Solution(6).guessNumber(10));
    }

    @Test
    void test2() {
        Assertions.assertEquals(1, new Solution(1).guessNumber(1));
    }

    @Test
    void test3() {
        Assertions.assertEquals(1, new Solution(1).guessNumber(2));
    }

    @Test
    void test4() {
        Assertions.assertEquals(1702766719, new Solution(1702766719).guessNumber(2126753390));
    }

    static class Solution {
        final int pick;

        Solution(int pick) {
            this.pick = pick;
        }

        final int guess(int num) {
            return Integer.compare(pick, num);
        }

        public int guessNumber(int n) {
            long min = 1;
            long max = Integer.MAX_VALUE;

            while (true) {
                int res = guess(n);
                if (res == 0) {
                    return n;
                }
                if (res > 0) {
                    // lower than the number pick
                    min = n;
                } else {
                    max = n;
                }
                n = (int) ((max + min) / 2);
            }
        }
    }
}
