package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * You are given two positive integer arrays spells and potions, of length `n` and `m` respectively,
 * where `spells[i]` represents the strength of the `i`th spell and `potions[j]` represents the strength of the `j`th potion.
 * You are also given an integer `success`.
 * A spell and potion pair is considered successful if the product of their strengths is at least `success`.
 * Return an integer array pairs of length `n` where `pairs[i]` is the number of potions that will form a
 * successful pair with the ith spell.
 * <p>
 * Constraints:
 * - `n == spells.length`
 * - `m == potions.length`
 * - `1 <= n, m <= 10^5`
 * - `1 <= spells[i]`, `potions[i] <= 10^5`
 * - `1 <= success <= 10^10`
 *
 * @see <a href="https://leetcode.com/problems/successful-pairs-of-spells-and-potions">2300. Successful Pairs of Spells and Potions</a>
 */
public class L76_SuccessfulPairsOfSpellsAndPotions {

    public int[] successfulPairs(int[] spells, int[] potions, long success) {
        return calcSuccessfulPairs1(spells, potions, success);
    }

    public static int[] calcSuccessfulPairs1(int[] spells, int[] potions, long success) {
        Arrays.sort(potions);
        int n = spells.length;
        int m = potions.length;
        int[] res = new int[n];

        for (int i = 0; i < n; i++) {
            int spell = spells[i];
            long minPotion = (success + spell - 1) / spell;
            int idx = lowerBound(potions, minPotion);
            res[i] = m - idx;
        }
        return res;
    }

    private static int lowerBound(int[] arr, long target) {
        int left = 0, right = arr.length;
        while (left < right) {
            int mid = (left + right) / 2;
            if (arr[mid] < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }

    public static int[] calcSuccessfulPairs2(int[] spells, int[] potions, long success) {
        Arrays.sort(potions);
        int[] res = new int[spells.length];
        double[] _potions = new double[potions.length];
        for (int i = 0; i < potions.length; i++) {
            _potions[i] = potions[i];
        }
        for (int i = 0; i < spells.length; i++) {
            double x = (double) success / spells[i];
            int q = Arrays.binarySearch(_potions, x);
            if (q >= 0) {
                int g = q;
                while (g != 0 && _potions[g - 1] == x) {
                    g--;
                }
                res[i] = potions.length - g;
            } else {
                res[i] = potions.length + q + 1;
            }
        }
        return res;
    }

    @Test
    void test1() {
        // Input: spells = [5,1,3], potions = [1,2,3,4,5], success = 7
        // Output: [4,0,3]
        int[] actual = successfulPairs(new int[]{5, 1, 3}, new int[]{1, 2, 3, 4, 5}, 7);
        Assertions.assertArrayEquals(new int[]{4, 0, 3}, actual);
    }

    @Test
    void test2() {
        // Input: spells = [3,1,2], potions = [8,5,8], success = 16
        // Output: [2,0,2]
        int[] actual = successfulPairs(new int[]{3, 1, 2}, new int[]{8, 5, 8}, 16);
        Assertions.assertArrayEquals(new int[]{2, 0, 2}, actual);
    }

    @Test
    void test3() {
        int[] actual = successfulPairs(
                new int[]{
                        40, 11, 24, 28, 40, 22, 26, 38, 28, 10, 31, 16, 10,
                        37, 13, 21, 9, 22, 21, 18, 34, 2, 40, 40, 6, 16, 9,
                        14, 14, 15, 37, 15, 32, 4, 27, 20, 24, 12, 26, 39,
                        32, 39, 20, 19, 22, 33, 2, 22, 9, 18, 12, 5
                }, new int[]{
                        31, 40, 29, 19, 27, 16, 25, 8, 33, 25, 36, 21, 7,
                        27, 40, 24, 18, 26, 32, 25, 22, 21, 38, 22, 37, 34,
                        15, 36, 21, 22, 37, 14, 31, 20, 36, 27, 28, 32, 21,
                        26, 33, 37, 27, 39, 19, 36, 20, 23, 25, 39, 40
                }, 600);
        int[] expected = new int[]{
                48, 0, 32, 37, 48, 22, 33, 47, 37, 0, 43, 6, 0, 46, 0, 21, 0,
                22, 21, 14, 46, 0, 48, 48, 0, 6, 0, 0, 0, 3, 46, 3, 45, 0, 34,
                20, 32, 0, 33, 47, 45, 47, 20, 18, 22, 45, 0, 22, 0, 14, 0, 0
        };
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void test4() {
        successfulPairs(
                new int[]{
                        36, 36, 22, 11, 35, 21, 4, 25, 30, 35, 31, 10,
                        8, 39, 7, 22, 18, 9, 23, 30, 9, 37, 22, 7, 36,
                        40, 17, 37, 38, 27, 6, 15, 1, 15, 7, 31, 36, 29,
                        9, 15, 3, 37, 15, 17, 25, 35, 9, 21, 5, 17, 25,
                        8, 18, 25, 7, 19, 4, 33, 9, 5, 29, 13, 9, 18,
                        5, 10, 31, 6, 7, 24, 13, 11, 8, 19, 2
                }, new int[]{
                        30, 11, 5, 20, 19, 36, 39, 24, 20, 37, 33, 22, 32,
                        28, 36, 24, 40, 27, 36, 37, 38, 23, 39, 11, 40, 19,
                        37, 32, 25, 29, 28, 37, 31, 36, 32, 40, 38, 22, 17,
                        38, 20, 33, 29, 17, 36, 33, 35, 25, 28, 18, 17, 19,
                        40, 27, 40, 28, 40, 40, 40, 39, 17, 34, 36, 11, 22,
                        29, 22, 35, 35, 22, 18, 34
                }, 135);
    }
}
