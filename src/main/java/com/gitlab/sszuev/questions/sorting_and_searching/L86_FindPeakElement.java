package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * A peak element is an element that is strictly greater than its neighbors.
 * Given a 0-indexed integer array nums, find a peak element, and return its index.
 * If the array contains multiple peaks, return the index to any of the peaks.
 * You may imagine that `nums[-1] = nums[n] = -∞`.
 * In other words, an element is always considered to be strictly greater than a neighbor
 * that is outside the array.
 * You must write an algorithm that runs in `O(log n)` time.
 * <p>
 * Constraints:
 * - `1 <= nums.length <= 1000`
 * - `-2^31 <= nums[i] <= 2^31 - 1`
 * - `nums[i] != nums[i + 1]` for all valid i.
 *
 * @see <a href="https://leetcode.com/problems/find-peak-element">162. Find Peak Element</a>
 */
public class L86_FindPeakElement {

    public int findPeakElement(int[] nums) {
        return binarySearch(nums);
    }

    private static int binarySearch(int[] arr) {
        if (arr.length == 0) return -1;
        if (arr.length == 1) return 0;
        int low = 0;
        int high = arr.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (isLocalMaximum(arr, mid)) {
                return mid;
            }
            if (arr[mid] < arr[mid + 1]) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }

    private static boolean isLocalMaximum(int[] arr, int index) {
        if (index == 0) {
            return arr[1] < arr[0];
        }
        if (index == arr.length - 1) {
            return arr[arr.length - 2] < arr[arr.length - 1];
        }
        return arr[index] > arr[index - 1] && arr[index] > arr[index + 1];
    }

    @Test
    void test1() {
        Assertions.assertEquals(2, findPeakElement(new int[]{1, 2, 3, 1}));
    }

    @Test
    void test2() {
        int actual = findPeakElement(new int[]{1, 2, 1, 3, 5, 6, 4});
        Assertions.assertTrue(1 == actual || 5 == actual);
    }

    @Test
    void test3() {
        Assertions.assertEquals(0, findPeakElement(new int[]{1}));
    }
}
