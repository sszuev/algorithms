package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.PriorityQueue;
import java.util.Random;

/**
 * Given an integer array nums and an integer k, return the kth largest element in the array.
 * Note that it is the kth largest element in the sorted order, not the kth distinct element.
 * Can you solve it without sorting?
 * <p>
 * Constraints: {@code 1 <= k <= nums.length <= 10^5}, {@code -10^4 <= nums[i] <= 10^4}
 *
 * @see <a href="https://leetcode.com/problems/kth-largest-element-in-an-array/">215. Kth Largest Element in an Array</a>
 */
@SuppressWarnings("unused")
public class L9_KthLargestElementInArray {

    public int findKthLargest(int[] nums, int k) {
        return minHeapAlgorithm(nums, nums.length - k);
    }

    public int selectLargestKth(int[] nums, int k, Algorithm algorithm) {
        if (algorithm == Algorithm.QUICK_SELECT) {
            return quickSelectAlgorithm(nums, nums.length - k);
        }
        if (algorithm == Algorithm.MIN_HEAP) {
            return minHeapAlgorithm(nums, nums.length - k);
        }
        throw new IllegalStateException();
    }

    public static int minHeapAlgorithm(int[] array, int k) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int i = 0; i < array.length - k; i++) {
            minHeap.offer(array[i]);
        }

        for (int i = array.length - k; i < array.length; i++) {
            if (array[i] > minHeap.element()) {
                minHeap.poll();
                minHeap.offer(array[i]);
            }
        }

        return minHeap.element();
    }

    public static int quickSelectAlgorithm(int[] array, int k) {
        return quickSelectIterative(array, 0, array.length - 1, k);
    }

    public static int quickSelectIterative(int[] array, int left, int right, int k) {
        Random random = new Random();
        while (true) {
            if (left == right) {
                return array[left];
            }
            int pivotalIndex = left + random.nextInt(right - left + 1);
            pivotalIndex = partition(array, left, right, pivotalIndex);
            if (k == pivotalIndex) {
                return array[pivotalIndex];
            } else if (k < pivotalIndex) {
                right = pivotalIndex - 1;
            } else {
                left = pivotalIndex + 1;
            }
        }
    }

    private static int partition(int[] array, int left, int right, int pivotalIndex) {
        int pivot = array[pivotalIndex];
        swap(array, pivotalIndex, right);
        int stored_index = left;
        for (int i = left; i < right; i++) {
            if (array[i] < pivot) {
                swap(array, i, stored_index);
                stored_index++;
            }
        }
        swap(array, right, stored_index);
        return stored_index;
    }

    private static void swap(int[] array, int i, int j) {
        int x = array[i];
        array[i] = array[j];
        array[j] = x;
    }

    @ParameterizedTest
    @EnumSource(Algorithm.class)
    public void test3_2_1_5_6_4(Algorithm algorithm) {
        Assertions.assertEquals(5,
                selectLargestKth(new int[]{3, 2, 1, 5, 6, 4}, 2, algorithm)
        );
    }

    @ParameterizedTest
    @EnumSource(Algorithm.class)
    public void test3_2_3_1_2_4_5_5_6(Algorithm algorithm) {
        Assertions.assertEquals(4,
                selectLargestKth(new int[]{3, 2, 3, 1, 2, 4, 5, 5, 6}, 4, algorithm)
        );
    }

    @ParameterizedTest
    @EnumSource(Algorithm.class)
    public void test_XXX(Algorithm algorithm) {
        Assertions.assertEquals(32,
                selectLargestKth(new int[]{3, 32, 42, 1, 5, 7, 2, 32, 33, 4, 21, 25, 16, 7, 8, 9}, 4, algorithm)
        );
    }

    public enum Algorithm {
        MIN_HEAP,
        QUICK_SELECT,
    }
}
