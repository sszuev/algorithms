package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Sorted Merge:
 * You are given two sorted arrays, A and B, where A has a large enough buffer at the end to hold B.
 * Write a method to merge B into A in sorted order.
 * <p>
 * Hints: #332 Try moving from the end of the array to the beginning.
 * <p>
 * Created by @ssz on 13.06.2020.
 */
public class Q1_MergeTwoSortedArrays {

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void test1(Solution data) {
        test(new int[]{3, 4, 4, 6, 7, 9, 9, 9, 10}, new int[]{1, 2, 6, 7}, data);
    }

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void test2(Solution data) {
        test(new int[]{1, 2, 6, 7}, new int[]{3, 4, 4, 6, 7, 9, 9, 9, 10}, data);
    }

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void test3(Solution data) {
        test(new int[]{1, 2, 100, 101, 101, 101, 102}, new int[]{-3, -3, 0, 2, 3, 4, 99, 103}, data);
    }

    private void test(int[] a, int[] b, Solution data) {
        int[] actual = data.merge(a, b);
        int[] expected = IntStream.concat(IntStream.of(a), IntStream.of(b)).sorted().toArray();
        Assertions.assertArrayEquals(expected, actual, "Wrong res: " + Arrays.toString(actual));
    }

    enum Solution {
        MY {
            @Override
            public int[] merge(int[] a, int[] b) {
                int[] array = new int[a.length + b.length];
                int i = 0;
                int j = 0;
                int index = 0;
                while (index < array.length && i < a.length && j < b.length) {
                    array[index++] = a[i] < b[j] ? a[i++] : b[j++];
                }
                int[] ta = b.length < a.length ? a : b;
                int ti = b.length < a.length ? i : j;
                for (int k = ti; k < ta.length; k++) {
                    array[index++] = ta[k];
                }
                return array;
            }
        },
        FROM_BOOK {
            @Override
            public int[] merge(int[] a, int[] b) {
                int[] array = Arrays.copyOf(a, a.length + b.length);
                mergeFromBook(array, b, a.length, b.length);
                return array;
            }

            /*
             * Since we know that A has enough buffer at the end, we won't need to allocate additional space.
             * Our logic should involve simply comparing elements of A and B and inserting them in order,
             * until we've exhausted all elements in A and in B.
             * The only issue with this is that if we insert an element into the front of A,
             * then we'll have to shift the existing elements backwards to make room for it.
             * It's better to insert elements into the back of the array, where there's empty space.
             * The code below does just that. It works from the back of A and B, moving the largest elements to the back of A
             */
            public void mergeFromBook(int[] a, int[] b, int lastA, int lastB) {
                int indexA = lastA - 1; // index of last element in array a
                int indexB = lastB - 1; // index of last element in array b
                int indexMerged = lastB + lastA - 1; // end of merged array

                // merge a and b starting from the last element in each
                while (indexB >= 0) {
                    // end of a is > than end of b
                    if (indexA >= 0 && a[indexA] > b[indexB]) {
                        a[indexMerged] = a[indexA];
                        indexA--;
                    } else {
                        a[indexMerged] = b[indexB];
                        indexB--;
                    }
                    indexMerged--;
                }
            }
        },
        ;

        public abstract int[] merge(int[] a, int[] b);
    }
}
