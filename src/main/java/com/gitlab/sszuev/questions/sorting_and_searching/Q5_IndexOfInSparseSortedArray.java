package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 * Sparse Search:
 * Given a sorted array of strings that is interspersed with empty strings,
 * write a method to find the location of a given string.
 * EXAMPLE
 * Input: {@code ball}, {@code {"at", "", "", "", "ball", "", "", "car", "", "", "dad", "", ""}}
 * Output: 4
 *
 * Hints: #256 Try modifying binary search to handle this.
 * <p>
 * Created by @ssz on 12.06.2020.
 */
public class Q5_IndexOfInSparseSortedArray {

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void test1(Solution solution) {
        String x = "ball";
        String[] data = {"at", "", "", "", x, "", "", "car", "", "", "dad", "", ""};
        Assertions.assertEquals(4, solution.indexOf(data, x));
    }

    @ParameterizedTest
    @EnumSource(Solution.class)
    public void test2(Solution solution) {
        String x = "ball";
        String[] data = {"at", "aw", x, "car", "ccc", "dad", "rdf", "www", "z"};
        Assertions.assertEquals(2, solution.indexOf(data, x));
    }

    enum Solution {
        BRUTE_FORCE {
            @Override
            int indexOf(String[] data, String x) {
                for (int i = 0; i < data.length; i++) {
                    String s = data[i];
                    if (s.isEmpty()) continue;
                    int value = x.compareTo(s);
                    if (value < 0) break;
                    if (value == 0) return i;
                }
                return -1;
            }
        },

        BINARY_SEARCH {
            @Override
            int indexOf(String[] data, String x) {
                int low = 0;
                int high = data.length - 1;
                do {
                    int m = (high + low) / 2;
                    String s = data[m];
                    if (s.isEmpty()) {
                        if (data[low].isEmpty()) {
                            low++;
                        }
                        high--;
                        continue;
                    }
                    int value = x.compareTo(s);
                    if (value == 0) {
                        return m;
                    }
                    if (value < 0) {
                        high = m - 1;
                    } else {
                        low = m + 1;
                    }
                } while (low < high);
                return -1;
            }
        },

        BINARY_SEARCH_FROM_BOOK {
            @Override
            int indexOf(String[] data, String x) {
                int low = 0;
                int high = data.length - 1;
                do {
                    int m = (low + high) / 2;
                    String s = data[m];
                    if (s.isEmpty()) {
                        int left = m - 1;
                        int right = m + 1;
                        do {
                            if (left < low && right > high) return -1;
                            if (left >= low && !data[left].isEmpty()) {
                                m = left;
                                break;
                            }
                            if (right <= high && !data[right].isEmpty()) {
                                m = right;
                                break;
                            }
                            left--;
                            right++;
                        } while (true);
                        s = data[m];
                    }
                    int value = x.compareTo(s);
                    if (value == 0) {
                        return m;
                    }
                    if (value < 0) {
                        high = m - 1;
                    } else {
                        low = m + 1;
                    }
                } while (low < high);
                return -1;
            }
        }
        ;
        abstract int indexOf(String[] data, String x);
    }
}
