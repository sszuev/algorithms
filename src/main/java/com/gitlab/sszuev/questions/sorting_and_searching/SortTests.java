package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.IntStream;

/**
 * Created by @ssz on 04.10.2020.
 */
abstract class SortTests {

    static void testSortRandomIntArray(Consumer<int[]> doSort, int n) {
        testSortRandomIntArray(doSort, n, 100);
    }

    static void testSortRandomIntArray(Consumer<int[]> doSort, int length, int max) {
        testSortIntArray(doSort, getRandomArray(length, max));
    }

    static void testSortIntArray(Consumer<int[]> doSort, int... test) {
        System.out.println("Random int array before sorting : " + Arrays.toString(test));
        doSort.accept(test);
        System.out.println("                   Sorted array : " + Arrays.toString(test));
        SortTests.testIsSorted(test);
    }


    static void testIsSorted(int[] test) {
        int[] expected = Arrays.copyOf(test, test.length);
        Arrays.sort(expected);
        Assertions.assertTrue(Arrays.equals(expected, test));
    }

    static <X extends Comparable<X>> void testIsSorted(X[] test) {
        X[] expected = Arrays.copyOf(test, test.length);
        Arrays.sort(expected);
        Assertions.assertTrue(Arrays.equals(expected, test));
    }

    public static int[] getRandomArray(int length, int max) {
        return IntStream.generate(() -> (int) (Math.random() * max)).limit(length).toArray();
    }
}
