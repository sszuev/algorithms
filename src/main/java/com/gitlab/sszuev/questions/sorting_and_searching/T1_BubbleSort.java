package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Test;

/**
 * <b>Bubble Sort Algorithm</b>
 * <p>
 * Bubble sort Worst case performance, {@code O(n^2)}
 * Bubble sort Best case performance, {@code O(n)}
 * Bubble sort Average case performance, {@code O(n^2)}
 *
 * @see <a href='https://javarevisited.blogspot.com/2014/08/bubble-sort-algorithm-in-java-with.html'>Bubble Sort Algorithm</a>
 */
public class T1_BubbleSort {

    @Test
    public void testRandom9() {
        testRandom(9);
    }

    @Test
    public void testRandom7() {
        testRandom(7);
    }

    @Test
    public void testRandomClassicBubbleSort9() {
        SortTests.testSortRandomIntArray(T1_BubbleSort::bubbleSort, 9);
    }


    private void testRandom(int n) {
        SortTests.testSortRandomIntArray(T1_BubbleSort::bubbleSortImproved, n);
    }

    public static void bubbleSort(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            for (int j = numbers.length - 1; j > i; j--) {
                if (numbers[j] < numbers[j - 1]) {
                    swap(numbers, j, j - 1);
                }
            }
        }
    }

    public static void bubbleSortImproved(int[] number) {
        boolean swapped = true;
        int last = number.length - 2;

        // only continue if swapping of number has occurred
        while (swapped) {
            swapped = false;
            for (int i = 0; i <= last; i++) {
                if (number[i] > number[i + 1]) {
                    // pair is out of order, swap them
                    swap(number, i, i + 1);
                    swapped = true; // swapping occurred
                }
            }
            // after each pass largest element moved to end of array
            last--;
        }

    }

    private static void swap(int[] array, int from, int to) {
        int temp = array[from];
        array[from] = array[to];
        array[to] = temp;
    }

}
