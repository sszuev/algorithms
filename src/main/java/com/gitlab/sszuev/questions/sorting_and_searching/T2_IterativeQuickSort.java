package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Test;

import java.util.Deque;
import java.util.LinkedList;

/**
 * <b>Quick Sort</b>
 * The quicksort algorithm is one of the important sorting algorithms.
 * Similar to merge sort, quicksort also uses divide-and-conquer.
 *
 * @see <a href='https://javarevisited.blogspot.com/2016/09/iterative-quicksort-example-in-java-without-recursion.html'>Iterative QuickSort (without Recursion)</a>
 */
public class T2_IterativeQuickSort {

    @Test
    public void testRandom9() {
        testRandom(9);
    }

    @Test
    public void testRandom7() {
        testRandom(7);
    }

    private void testRandom(int n) {
        SortTests.testSortRandomIntArray(T2_IterativeQuickSort::quickSort, n);
    }

    public static void quickSort(int[] numbers) {
        Deque<Integer> stack = new LinkedList<>();
        stack.push(0);
        stack.push(numbers.length);

        while (!stack.isEmpty()) {
            int end = stack.pop();
            int start = stack.pop();
            if (end - start < 2) {
                continue;
            }
            int p = (start + end) / 2;
            p = partition(numbers, p, start, end);

            stack.push(p + 1);
            stack.push(end);

            stack.push(start);
            stack.push(p);
        }
    }

    private static int partition(int[] input, int position, int start, int end) {
        int l = start;
        int h = end - 2;
        int piv = input[position];
        swap(input, position, end - 1);

        while (l < h) {
            if (input[l] < piv) {
                l++;
            } else if (input[h] >= piv) {
                h--;
            } else {
                swap(input, l, h);
            }
        }
        int idx = h;
        if (input[h] < piv) {
            idx++;
        }
        swap(input, end - 1, idx);
        return idx;
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

}
