package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * <b>Insert Sort</b>
 * <p>
 * Insertion sort is a perfect sorting algorithm to insert a new value into an already sorted array.
 * That's why the best-case complexity of insertion sort is O(n),
 * in which case you can just insert a new number in the already sorted list of integers.
 * Another thing to keep in mind is the size of the list,
 * insertion sort is very good for small list or array,
 * but not so for a large list, where QuickSort, MergeSort, and HeapSort rules.
 *
 * @see <a href='https://www.java67.com/2014/09/insertion-sort-in-java-with-example.html'>Insertion Sort Algorithm in Java with Example</a>
 * @see <a href='https://en.wikipedia.org/wiki/Insertion_sort'>Insertion sort</a>
 */
public class T3_InsertSort {

    @Test
    public void testRandom9() {
        testRandom(9);
    }

    @Test
    public void testRandom7() {
        testRandom(7);
    }

    @Test
    public void testStrings() {
        // Sorting String array using Insertion Sort in Java
        String[] cities = {"London", "Paris", "Tokyo", "NewYork", "Chicago"};
        System.out.println("String array before sorting : " + Arrays.toString(cities));
        insertionSort(cities);
        System.out.println("String array after sorting : " + Arrays.toString(cities));
        SortTests.testIsSorted(cities);
    }

    private void testRandom(int n) {
        SortTests.testSortRandomIntArray(T3_InsertSort::insertionSort, n);
    }

    /*
     * Java implementation of insertion sort algorithm to sort an integer array.
     */
    public static void insertionSort(int[] array) {
        // insertion sort starts from second element
        for (int i = 1; i < array.length; i++) {
            int numberToInsert = array[i];

            int compareIndex = i;
            while (compareIndex > 0 && array[compareIndex - 1] > numberToInsert) {
                array[compareIndex] = array[compareIndex - 1]; // shifting element
                compareIndex--; // moving backwards, towards index 0
            }
            // compareIndex now denotes proper place for number to be sorted
            array[compareIndex] = numberToInsert;
        }
    }

    /*
     * Method to Sort String array using insertion sort in Java.
     * This can also sort any object array which implements Comparable interface.
     */
    public static <X extends Comparable<X>> void insertionSort(X[] objArray) {
        // insertion sort starts from second element
        for (int i = 1; i < objArray.length; i++) {
            X objectToSort = objArray[i];
            int j = i;
            while (j > 0 && objArray[j - 1].compareTo(objectToSort) > 0) {
                objArray[j] = objArray[j - 1];
                j--;
            }
            objArray[j] = objectToSort;
        }
    }

}
