package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Test;

/**
 * <b>Merge Sort</b>
 *
 * @see <a href='https://www.java67.com/2018/03/mergesort-in-java-algorithm-example-and.html'>Merge Sort in Java - Algorithm Example</a>
 */
public class T4_MergeSort {

    @Test
    public void testRandom9() {
        testRandom(9);
    }

    @Test
    public void testRandom7() {
        testRandom(7);
    }

    @Test
    public void testFixed() {
        SortTests.testSortIntArray(T4_MergeSort::mergeSort, 87, 57, 370, 110, 90, 610, 2, 710, 140, 203, 150);
    }

    private void testRandom(int n) {
        SortTests.testSortRandomIntArray(T4_MergeSort::mergeSort, n);
    }

    /**
     * Java function to sort given array using merge sort algorithm
     *
     * @param input {@code Array}
     */
    public static void mergeSort(int[] input) {
        mergeSort(input, 0, input.length - 1);
    }

    /**
     * A Java method to implement MergeSort algorithm using recursion
     *
     * @param input integer array to be sorted
     * @param start index of first element in array
     * @param end   index of last element in array
     */
    private static void mergeSort(int[] input, int start, int end) {
        // break problem into smaller structurally identical problems
        int mid = (start + end) / 2;
        if (start < end) {
            mergeSort(input, start, mid);
            mergeSort(input, mid + 1, end);
        }

        // merge solved pieces to get solution to original problem
        int i = 0, first = start, last = mid + 1;
        int[] tmp = new int[end - start + 1];

        while (first <= mid && last <= end) {
            tmp[i++] = input[first] < input[last] ? input[first++] : input[last++];
        }
        while (first <= mid) {
            tmp[i++] = input[first++];
        }
        while (last <= end) {
            tmp[i++] = input[last++];
        }
        i = 0;
        while (start <= end) {
            input[start++] = tmp[i++];
        }
    }

}
