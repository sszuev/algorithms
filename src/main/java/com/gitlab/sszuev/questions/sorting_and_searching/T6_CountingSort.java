package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Test;

/**
 * <b>Counting Sort</b>
 * One of the few linear sorting algorithms or {@code O(n)} sorting algorithm.
 * It is an integer-based sorting algorithm.
 * Time Complexity of the Counting Sort is {@code O(n + k)} in the best case,
 * average case and worst case, where {@code n} is the size of the input array
 * and {@code k} is the maximum value in the array.
 * <p>
 * There is a key difference between Bucket Sort and Counting sort,
 * for example, Bucket sort uses a hash function to distribute values;
 * Counting sort, on the other hand, creates a counter for each value hence it is called Counting Sort algorithm.
 * The counting sort is a stable sort like multiple keys with the same value
 * are placed in the sorted array in the same order that they appear in the original input array.
 * The counting sort is not a comparison based algorithm. It's actually a non-comparison sorting algorithm.
 *
 * @see <a href='https://en.wikipedia.org/wiki/Counting_sort'>wiki</a>
 * @see <a href='https://en.wikipedia.org/wiki/Sorting_algorithm#Stability'>wiki:stability</a>
 * @see <a href='https://en.wikipedia.org/wiki/Sorting_algorithm#Comparison_of_algorithms'>wiki: comparison and non-comparison sorts</a>
 * @see <a href='https://www.java67.com/2017/06/counting-sort-in-java-example.html'>Counting Sort</a>
 */
public class T6_CountingSort {

    @Test
    public void testRandom20() {
        SortTests.testSortRandomIntArray(ints -> countingSort(ints, 10), 20, 10);
    }

    @Test
    public void testRandom177() {
        SortTests.testSortRandomIntArray(ints -> countingSort(ints, 100), 177, 100);
    }

    public static void countingSort(int[] input, int k) {
        // create buckets
        int[] counter = new int[k + 1];
        // fill buckets
        for (int i : input) {
            counter[i]++;
        }
        // sort array
        int index = 0;
        for (int i = 0; i < counter.length; i++) {
            while (0 < counter[i]) {
                input[index++] = i;
                counter[i]--;
            }
        }
    }
}
