package com.gitlab.sszuev.questions.sorting_and_searching;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Radix Sort</b>
 * <p>
 * Time Complexity of radix sort in the best case, average case, and worst case is {@code O(k * n)}
 * where {@code k} is the length of the longest number, and {@code n} is the size of the input array.
 * Note: if {@code k} is greater than {@code log(n)} then a {@code n * log(n)} algorithm would be a better fit.
 * In reality, we can always change the Radix to make {@code k} less than {@code log(n)}.
 *
 * @see <a href='https://www.java67.com/2018/03/how-to-implement-radix-sort-in-java.html'>How to implement Radix Sort</a>
 */
public class T7_RadixSort {

    @Test
    public void testRandom9() {
        testRandom(9);
    }

    @Test
    public void testRandom7() {
        testRandom(7);
    }

    private void testRandom(int n) {
        SortTests.testSortRandomIntArray(T7_RadixSort::radixSort, n);
    }

    public static void radixSort(int[] input) {
        final int radix = 10;

        // declare and initialize bucket[]
        @SuppressWarnings("unchecked") List<Integer>[] bucket = new List[radix];
        for (int i = 0; i < bucket.length; i++) {
            bucket[i] = new ArrayList<>();
        }

        // sort
        boolean maxLength = false;
        int tmp;
        int placement = 1;
        while (!maxLength) {
            maxLength = true;

            // split input between lists
            for (int i : input) {
                tmp = i / placement;
                bucket[tmp % radix].add(i);
                if (maxLength && tmp > 0) {
                    maxLength = false;
                }
            }

            // empty lists into input array
            int a = 0;
            for (int b = 0; b < radix; b++) {
                for (Integer i : bucket[b]) {
                    input[a++] = i;
                }
                bucket[b].clear();
            }
            // move to next digit
            placement *= radix;
        }
    }

}
