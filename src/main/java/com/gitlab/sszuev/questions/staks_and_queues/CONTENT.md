[HOME](../../../../../../../../README.md)

- **[Q1](Q1_ThreeStacksFromSingleArray.java)**: Three in One: Describe how you could use a single array to implement three stacks:
  <details>
    <summary>Hints</summary>
  
    - ***2***: A stack is simply a data structure in which the most recently added elements are removed first. 
Can you simulate a single stack using an array? Remember that there are many possible solutions, and there are tradeoffs of each.
    - ***12***: We could simulate three stacks in an array by just allocating the first third of the array to the first stack, 
the second third to the second stack, and the final third to the third stack. 
One might actually be much bigger than the others, though. Can we be more flexible with the divisions?
    - ***38***: If you want to allow for flexible divisions, you can shift stacks around. 
Can you ensure that all available capacity is used?
    - ***58***: Try thinking about the array as circular, such that the end of the array "wraps around" to the start of the array.

  </details> 

- **[Q4](Q4_QueueBasedOnTwoStacks.java)**: Queue via Stacks: Implement a MyQueue class which implements a queue using two stacks.
  <details>
    <summary>Hints</summary>
  
    - ***98***: The major difference between a queue and a stack is the order of elements. 
A queue removes the oldest item and a stack removes the newest item. 
How could you remove the oldest item from a stack if you only had access to the newest item?
    - ***114***: We can remove the oldest item from a stack by repeatedly removing the newest item
(inserting those into the temporary stack) until we get down to one element. 
Then, after we've retrieved the newest item, putting all the elements back. 
The issue with this is that doing several pops in a row will require `0(N)` work each time. 
Can we optimize for scenarios where we might do several pops in a row?

  </details>

- **[L15](L15_RemovingStarsFromString.java)**: (leetcode) Removing Stars From a String

- **[L26](L26_NumberOfRecentCalls.java)**: (leetcode) Number of Recent Calls

- **[L30](L30_DailyTemperatures.java)**: (leetcode; topics: Array, Stack, Monotonic Stack) Daily Temperatures

- **[L37](L37_AsteroidCollision.java)**: (leetcode; topics: Array, Stack, Simulation) Asteroid collision

- **[L55](L55_OnlineStockSpan.java)**: (leetcode; Monotonic Stack) Online Stock Span

- **[L56](L56_Dota2Senate.java)**: (leetcode) Dota2 Senate

- **[L60](L60_BestTimeToBuyAndSellStockWithTransactionFee.java)**: (leetcode) Best Time to Buy and Sell Stock with Transaction Fee

- **[L73](L73_MaximumSubsequenceScore.java)**: (leetcode) Maximum Subsequence Score

- **[L81](L81_TotalCostToHireKWorkers.java)**: (leetcode) Total Cost to Hire K Workers

- **[L101](L101_ValidParentheses.java)**: (leetcode) Valid Parentheses

[HOME](../../../../../../../../README.md)