package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Given a string `s` containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
 * An input string is valid if:
 * - Open brackets must be closed by the same type of brackets.
 * - Open brackets must be closed in the correct order.
 * - Every close bracket has a corresponding open bracket of the same type.
 * <p>
 * Constraints:
 * - `1 <= s.length <= 10^4`
 * - `s consists of parentheses only '()[]{}'`
 *
 * @see <a href="https://leetcode.com/problems/valid-parentheses">20. Valid Parentheses</a>
 */
public class L101_ValidParentheses {
    public boolean isValid(String s) {
        return calcIsValid(s);
    }

    private static boolean calcIsValid(String s) {
        if (s.length() % 2 != 0) return false;
        Deque<Character> stack = new ArrayDeque<>();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == '(' || ch == '{' || ch == '[') {
                stack.add(ch);
            } else if (ch == ')' || ch == '}' || ch == ']') {
                Character open = stack.pollLast();
                if (open == null) {
                    return false;
                }
                if (open == '(' && ch == ')') {
                    continue;
                }
                if (open == '{' && ch == '}') {
                    continue;
                }
                if (open == '[' && ch == ']') {
                    continue;
                }
                return false;
            } else {
                throw new IllegalArgumentException();
            }
        }
        return stack.isEmpty();
    }

    @Test
    void test1() {
        Assertions.assertTrue(isValid("()"));
    }

    @Test
    void test2() {
        Assertions.assertTrue(isValid("()[]{}"));
    }

    @Test
    void test3() {
        Assertions.assertTrue(isValid("([])"));
    }

    @Test
    void test4() {
        Assertions.assertFalse(isValid("(]"));
    }

    @Test
    void test5() {
        Assertions.assertFalse(isValid("(("));
    }

    @Test
    void test6() {
        Assertions.assertFalse(isValid("){"));
    }
}
