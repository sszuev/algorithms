package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You are given a string {@code s}, which contains stars {@code *}.
 * In one operation, you can:
 * 1) choose a star in {@code s};
 * 2) remove the closest non-star character to its left, as well as remove the star itself.
 * Return the string after all stars have been removed.
 * <p>
 * Note:
 * The input will be generated such that the operation is always possible.
 * It can be shown that the resulting string will always be unique.
 * <p>
 * Constraints: {@code 1 <= s.length <= 10^5}; {@code s} consists of lowercase English letters and stars {@code *}.
 * The operation above can be performed on {@code s}.
 *
 * @see <a href="https://leetcode.com/problems/removing-stars-from-a-string">2390. Removing Stars From a String</a>
 */
public class L15_RemovingStarsFromString {
    public String removeStars(String s) {
        return doRemoveStars(s);
    }

    private static String doRemoveStars(String s) {
        char[] chars = s.toCharArray();
        StringBuilder res = new StringBuilder();
        for (char ch : chars) {
            if (ch == '*') {
                res.deleteCharAt(res.length() - 1);
            } else {
                res.append(ch);
            }
        }
        return res.toString();
    }

    @Test
    public void testLeet_ss_cod_s_e() {
        Assertions.assertEquals(
                "lecoe",
                removeStars("leet**cod*e")
        );
    }

    @Test
    public void testErase_sssss() {
        Assertions.assertEquals(
                "",
                removeStars("erase*****")
        );
    }
}
