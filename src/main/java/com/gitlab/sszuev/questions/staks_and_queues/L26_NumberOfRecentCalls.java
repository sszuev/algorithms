package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * You have a {@code RecentCounter} class which counts the number of recent requests within a certain time frame.
 * Implement the RecentCounter class:
 * 1) {@code RecentCounter()} - initializes the counter with zero recent requests.
 * 2) {@code int ping(int t)} - adds a new request at time t, where t represents some time in milliseconds,
 * and returns the number of requests that has happened in the past 3000 milliseconds (including the new request).
 * Specifically, return the number of requests that have happened in the inclusive range [t - 3000, t].
 * It is guaranteed that every call to ping uses a strictly larger value of t than the previous call.
 * <p>
 * Constraints:
 * 1) 1 <= t <= {@code 10^9}
 * 2) Each test case will call ping with strictly increasing values of t.
 * 3) At most 10^4 calls will be made to ping.
 *
 * @see <a href="https://leetcode.com/problems/number-of-recent-calls">933. Number of Recent Calls</a>
 */
public class L26_NumberOfRecentCalls {

    @Test
    void test() {
        RecentCounter counter = new RecentCounter();
        Assertions.assertEquals(1, counter.ping(1));
        Assertions.assertEquals(2, counter.ping(100));
        Assertions.assertEquals(3, counter.ping(3001));
        Assertions.assertEquals(3, counter.ping(3002));
    }


    static class RecentCounter {
        private final Queue<Integer> calls = new ArrayDeque<>();

        public int ping(int t) {
            calls.add(t);
            Integer i;
            while (((i = calls.peek()) != null) && i < t - 3000) {
                calls.remove();
            }
            return calls.size();
        }
    }
}
