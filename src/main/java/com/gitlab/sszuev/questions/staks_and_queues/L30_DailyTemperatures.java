package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Given an array of integers' temperatures represents the daily temperatures,
 * return an array {@code answer} such that {@code answer[i]} is the number of days you have to wait after
 * the {@code i}th day to get a warmer temperature.
 * If there is no future day for which this is possible, keep {@code answer[i] == 0} instead.
 * Constraints: {@code 1 <= temperatures.length <= 10^5}, {@code 30 <= temperatures[i] <= 100}.
 *
 * @see <a href="https://leetcode.com/problems/daily-temperatures">739. Daily Temperatures</a>
 */
public class L30_DailyTemperatures {

    public int[] dailyTemperatures(int[] temperatures) {
        return calcDailyTemperatures(temperatures);
    }

    private static int[] calcDailyTemperatures(int[] temperatures) {
        int[] answer = new int[temperatures.length];
        Deque<Integer> prev = new ArrayDeque<>();
        prev.add(0);
        for (int i = 1; i < temperatures.length; i++) {
            int currentTemperature = temperatures[i];
            while (!prev.isEmpty()) {
                int index = prev.getLast();
                int prevTemperature = temperatures[index];
                if (currentTemperature > prevTemperature) {
                    answer[index] = i - index;
                    prev.removeLast();
                } else {
                    break;
                }
            }
            prev.addLast(i);
        }
        return answer;
    }

    @Test
    public void test1() {
        int[] given = new int[]{73, 74, 75, 71, 69, 72, 76, 73};
        int[] expected = new int[]{1, 1, 4, 2, 1, 1, 0, 0};
        int[] actual = dailyTemperatures(given);
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    public void test2() {
        int[] given = new int[]{30, 40, 50, 60};
        int[] expected = new int[]{1, 1, 1, 0};
        int[] actual = dailyTemperatures(given);
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    public void test3() {
        int[] given = new int[]{30, 60, 90};
        int[] expected = new int[]{1, 1, 0};
        int[] actual = dailyTemperatures(given);
        Assertions.assertArrayEquals(expected, actual);
    }
}
