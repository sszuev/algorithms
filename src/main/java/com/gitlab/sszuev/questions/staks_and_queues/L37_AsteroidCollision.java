package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * We are given an array asteroids of integers representing asteroids in a row.
 * For each asteroid, the absolute value represents its size,
 * and the sign represents its direction (positive meaning right, negative meaning left).
 * Each asteroid moves at the same speed.
 * Find out the state of the asteroids after all collisions.
 * If two asteroids meet, the smaller one will explode.
 * If both are the same size, both will explode.
 * Two asteroids moving in the same direction will never meet.
 * <p>
 * Constraints: `2 <= asteroids.length <= 10^4`, `-1000 <= asteroids[i] <= 1000`, `asteroids[i] != 0`
 *
 * @see <a href="https://leetcode.com/problems/asteroid-collision/">735. Asteroid Collision</a>
 */
public class L37_AsteroidCollision {
    public int[] asteroidCollision(int[] asteroids) {
        return calcFinalState(asteroids);
    }

    private static int[] calcFinalState(int[] asteroids) {
        Deque<Integer> res = new ArrayDeque<>();
        for (int right : asteroids) {
            push(res, right);
        }
        return res.stream().mapToInt(i -> i).toArray();
    }

    private static void push(Deque<Integer> res, int next) {
        if (res.isEmpty()) {
            res.add(next);
            return;
        }
        Integer last = res.pollLast();
        while (last != null) {
            if (last > 0 && next < 0) {
                // collision
                if (Math.abs(last) == Math.abs(next)) {
                    return;
                }
                if (Math.abs(last) > Math.abs(next)) {
                    res.add(last);
                    return;
                } else {
                    last = res.pollLast();
                    if (last == null) {
                        res.add(next);
                        return;
                    }
                }
            } else {
                res.add(last);
                res.add(next);
                return;
            }
        }
    }

    @Test
    void test1() {
        int[] actual = asteroidCollision(new int[]{5, 10, -5});
        Assertions.assertArrayEquals(new int[]{5, 10}, actual);
    }

    @Test
    void test2() {
        int[] actual = asteroidCollision(new int[]{8, -8});
        Assertions.assertArrayEquals(new int[]{}, actual);
    }

    @Test
    void test3() {
        int[] actual = asteroidCollision(new int[]{10, 2, -5});
        Assertions.assertArrayEquals(new int[]{10}, actual);
    }

    @Test
    void test4() {
        int[] actual = asteroidCollision(new int[]{1, -2, -2, -2});
        Assertions.assertArrayEquals(new int[]{-2, -2, -2}, actual);
    }

}
