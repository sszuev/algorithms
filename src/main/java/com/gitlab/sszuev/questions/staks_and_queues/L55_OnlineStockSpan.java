package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Design an algorithm that collects daily price quotes for some stock and returns the span of that stock's price for the current day.
 * The span of the stock's price in one day is the maximum number of consecutive days
 * (starting from that day and going backward)
 * for which the stock price was less than or equal to the price of that day.
 * For example, if the prices of the stock in the last four days is `[7,2,1,2]`
 * and the price of the stock today is `2`, then the span of today is `4` because starting from today,
 * the price of the stock was less than or equal `2` for `4` consecutive days.
 * Also, if the prices of the stock in the last four days is `[7,34,1,2]` and the price of the stock today is `8`,
 * then the span of today is `3` because starting from today,
 * the price of the stock was less than or equal `8` for ~ consecutive days.
 * <p>
 * Implement the `StockSpanner` class:
 * 1) `StockSpanner()` Initializes the object of the class.
 * 2) `int next(int price)` Returns the span of the stock's price given that today's price is price.
 * <p>
 * Constraints:
 * 1) `1 <= price <= 10^5`
 * 2) At most `10^4` calls will be made to next.
 *
 * @see <a href="https://leetcode.com/problems/online-stock-span/">901. Online Stock Span</a>
 */
public class L55_OnlineStockSpan {

    @Test
    void test1() {
        StockSpanner spanner = new StockSpanner();
        Assertions.assertEquals(1, spanner.next(100));
        Assertions.assertEquals(1, spanner.next(80));
        Assertions.assertEquals(1, spanner.next(60));
        Assertions.assertEquals(2, spanner.next(70));
        Assertions.assertEquals(1, spanner.next(60));
        Assertions.assertEquals(4, spanner.next(75));
        Assertions.assertEquals(6, spanner.next(85));
    }

    @Test
    void test2() {
        StockSpanner spanner = new StockSpanner();
        Assertions.assertEquals(1, spanner.next(90));
        Assertions.assertEquals(1, spanner.next(21));
        Assertions.assertEquals(2, spanner.next(21));
        Assertions.assertEquals(3, spanner.next(68));
        Assertions.assertEquals(5, spanner.next(94));
    }

    static class StockSpanner {

        private final Deque<int[]> stack = new ArrayDeque<>();

        public int next(int price) {
            if (stack.isEmpty() || stack.getLast()[0] > price) {
                stack.addLast(new int[]{price, 1});
                return 1;
            }
            int count = 0;
            while (!stack.isEmpty()) {
                int[] prev = stack.removeLast();
                if (prev[0] > price) {
                    stack.addLast(prev);
                    break;
                }
                count += prev[1];
            }
            count += 1;
            stack.addLast(new int[]{price, count});
            return count;
        }

/*
        private final Deque<Integer> stack = new ArrayDeque<>();
        public int next(int price) {
            stack.addLast(price);
            Deque<Integer> last = new ArrayDeque<>();
            int count = 0;
            while (!stack.isEmpty()) {
                Integer prev = stack.removeLast();
                last.addFirst(prev);
                if (prev > price) {
                    break;
                }
                count++;
            }
            stack.addAll(last);
            return count;
        }*/
    }
}
