package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

/**
 * In the world of Dota2, there are two parties: the Radiant and the Dire.
 * The Dota2 senate consists of senators coming from two parties.
 * Now the Senate wants to decide on a change in the Dota2 game.
 * The voting for this change is a round-based procedure.
 * <p>
 * In each round, each senator can exercise one of the two rights:
 * 1) Ban one senator's right: A senator can make another senator lose all his rights in this and all the following rounds.
 * 2) Announce the victory: If this senator found the senators who still have rights to vote are all from the same party,
 * he can announce the victory and decide on the change in the game.
 * <p>
 * Given a string senate representing each senator's party belonging.
 * The character 'R' and 'D' represent the Radiant party and the Dire party.
 * Then if there are n senators, the size of the given string will be n.
 * The round-based procedure starts from the first senator to the last senator in the given order.
 * This procedure will last until the end of voting.
 * All the senators who have lost their rights will be skipped during the procedure.
 * Suppose every senator is smart enough and will play the best strategy for his own party.
 * Predict which party will finally announce the victory and change the Dota2 game.
 * The output should be "Radiant" or "Dire".
 * <p>
 * Constraints: 1) n == senate.length; 2) 1 <= n <= 10^4 3) senate[i] is either 'R' or 'D'
 *
 * @see <a href="https://leetcode.com/problems/dota2-senate/">649. Dota2 Senate</a>
 */
public class L56_Dota2Senate {

    @Test
    void test1() {
        String actual = predictPartyVictory("DRRDRDRDRDDRDRDR");
        Assertions.assertEquals("Radiant", actual);
    }

    @Test
    void test2() {
        String actual = predictPartyVictory("DDRRR");
        Assertions.assertEquals("Dire", actual);
    }

    @Test
    void test3() {
        String actual = predictPartyVictory("RD");
        Assertions.assertEquals("Radiant", actual);
    }

    @Test
    void test4() {
        String actual = predictPartyVictory("RDD");
        Assertions.assertEquals("Dire", actual);
    }

    public String predictPartyVictory(String senate) {
        return calcPartyVictory(senate.chars().boxed().collect(Collectors.toList()));
    }

    private static String calcPartyVictory(List<Integer> senate) {
        int i = 0;
        while (senate.size() > 1) {
            int p = senate.get(i);
            int next = nextIndex(senate, i, other(p));
            if (next == -1) {
                break;
            }
            senate.remove(next);
            if (i >= senate.size() - 1) {
                i = 0;
            } else {
                i++;
            }
        }
        return toStr(senate.get(0));
    }

    private static int other(int x) {
        if (x == 82) {
            return 68;
        }
        if (x == 68) {
            return 82;
        }
        throw new IllegalStateException();
    }

    private static String toStr(int x) {
        if (x == 82) {
            return "Radiant";
        }
        if (x == 68) {
            return "Dire";
        }
        throw new IllegalStateException();
    }

    private static int nextIndex(List<Integer> senate, int from, int find) {
        int i;
        if (from >= senate.size() - 1) {
            i = 0;
        } else {
            i = from + 1;
        }
        while (true) {
            if (find == senate.get(i)) {
                return i;
            }
            if (i >= senate.size() - 1) {
                i = 0;
            } else {
                i++;
            }
            if (i == from) {
                return -1;
            }
        }
    }
}
