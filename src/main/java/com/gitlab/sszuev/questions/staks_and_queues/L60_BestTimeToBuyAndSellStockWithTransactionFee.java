package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * You are given an array `prices` where `prices[i]` is the price of a given stock on the ith day,
 * and an integer `fee` representing a transaction fee.
 * Find the maximum profit you can achieve.
 * You may complete as many transactions as you like,
 * but you need to pay the transaction fee for each transaction.
 * Note:
 * 1) You may not engage in multiple transactions simultaneously (i.e., you must sell the stock before you buy again).
 * 2) The transaction fee is only charged once for each stock purchase and sale.
 * <p>
 * Constraints:
 * 1) `1 <= prices.length <= 5 * 10^4`
 * 2) `1 <= prices[i] < 5 * 10^4`
 * 3) `0 <= fee < 5 * 10^4`
 *
 * @see <a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee">714. Best Time to Buy and Sell Stock with Transaction Fee</a>
 */
@SuppressWarnings("unused")
public class L60_BestTimeToBuyAndSellStockWithTransactionFee {
    public int maxProfit(int[] prices, int fee) {
        return calcMaxProfitIterativeChatGpt(prices, fee);
    }

    private static int calcMaxProfitIterativeChatGpt(int[] prices, int fee) {
        int n = prices.length;
        if (n < 2) {
            return 0;
        }

        int cash = 0;
        int hold = -prices[0];

        for (int i = 1; i < n; i++) {
            cash = Math.max(cash, hold + prices[i] - fee);
            hold = Math.max(hold, cash - prices[i]);
        }
        return cash;
    }

    private static int calcMaxProfitIterative1(int[] prices, int fee) {
        int n = prices.length;
        if (n < 2) {
            return 0;
        }

        int[] dp = new int[n];
        for (int i = 1; i < n; i++) {
            int maxProfitTillNow = 0;
            for (int j = 0; j < i; j++) {
                int profitIfSoldToday = prices[i] - prices[j] - fee + (j > 0 ? dp[j - 1] : 0);
                maxProfitTillNow = Math.max(maxProfitTillNow, profitIfSoldToday);
            }
            dp[i] = Math.max(dp[i - 1], maxProfitTillNow);
        }
        return dp[n - 1];
    }

    private static int calcMaxProfitRecursive(int[] prices, int fee) {
        return calcMaxProfitRecursive(prices, fee, 0, prices.length, new Integer[prices.length]);
    }

    private static int calcMaxProfitRecursive(int[] prices, int fee, int startInclusive, int endExclusive, Integer[] cache) {
        if (endExclusive > prices.length || startInclusive >= endExclusive) {
            return 0;
        }
        if (endExclusive - startInclusive < 2) {
            return 0;
        }
        int max = 0;
        for (int i = startInclusive; i < endExclusive; i++) {
            int left = prices[i] - prices[startInclusive] - fee;
            left = Math.max(left, 0);
            int right = calcMaxProfitRecursiveWithCache(prices, fee, i + 1, endExclusive, cache);
            max = Math.max(max, left + right);
        }
        return max;
    }

    private static int calcMaxProfitRecursiveWithCache(int[] prices, int fee, int i, int j, Integer[] cache) {
        Integer res = cache[i - 1];
        if (res != null) {
            return res;
        }
        res = calcMaxProfitRecursive(prices, fee, i, j, cache);
        cache[i - 1] = res;
        return res;
    }


    @Test
    void test1() {
        int actual = maxProfit(new int[]{1, 3, 2, 8, 4, 9}, 2);
        Assertions.assertEquals(8, actual);
    }

    @Test
    void test2() {
        int actual = maxProfit(new int[]{2, 8, 10}, 1);
        Assertions.assertEquals(7, actual);
    }

    @Test
    void test3() {
        int actual = maxProfit(new int[]{1, 3, 7, 5, 10, 3}, 3);
        Assertions.assertEquals(6, actual);
    }

    @Test
    void test4() {
        int actual = maxProfit(new int[]{1, 4, 6, 2, 8, 3, 10, 14}, 3);
        Assertions.assertEquals(13, actual);
    }

    @Test
    void test5() {
        int actual = maxProfit(new int[]{4, 5, 2, 4, 3, 3, 1, 2, 5, 4}, 1);
        Assertions.assertEquals(4, actual);
    }

    @Test
    void test6() {
        int actual = maxProfit(new int[]{2, 1, 4, 4, 2, 3, 2, 5, 1, 2}, 1);
        Assertions.assertEquals(4, actual);
    }
}
