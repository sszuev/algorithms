package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.PriorityQueue;

/**
 * You are given two 0-indexed integer arrays `nums1` and `nums2` of equal length `n` and a positive integer `k`.
 * You must choose a subsequence of indices from `nums1` of length `k`.
 * For chosen indices `i0, i1, ..., ik - 1`, your score is defined as:
 * The sum of the selected elements from `nums1` multiplied with the minimum of the selected elements from `nums2`.
 * It can defined simply as:`(nums1[i0] + nums1[i1] +...+ nums1[ik - 1]) * min(nums2[i0] , nums2[i1], ... ,nums2[ik - 1])`.
 * Return the maximum possible score.
 * A subsequence of indices of an array is a set that can be derived from the set `{0, 1, ..., n-1}` by deleting some or no elements.
 * <p>
 * Constraints:
 * - `n == nums1.length == nums2.length`
 * - `1 <= n <= 10^5`
 * - `0 <= nums1[i], nums2[j] <= 10^5`
 * - `1 <= k <= n`
 *
 * @see <a href="https://leetcode.com/problems/maximum-subsequence-score/">2542. Maximum Subsequence Score</a>
 */
public class L73_MaximumSubsequenceScore {

    public long maxScore(int[] nums1, int[] nums2, int k) {
        return calcMaxScore2(nums1, nums2, k);
    }

    private static class Pair {
        final int num1;
        final int num2;

        private Pair(int num1, int num2) {
            this.num1 = num1;
            this.num2 = num2;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "num1=" + num1 +
                    ", num2=" + num2 +
                    '}';
        }
    }

    private static long calcMaxScore2(int[] nums1, int[] nums2, int k) {
        int n = nums1.length;
        Pair[] pairs = new Pair[n];
        for (int i = 0; i < n; i++) {
            pairs[i] = new Pair(nums1[i], nums2[i]);
        }
        Arrays.sort(pairs, (a, b) -> b.num2 - a.num2);

        PriorityQueue<Integer> minHeap = new PriorityQueue<>(k);
        long sum = 0;
        long res = 0;

        for (Pair pair : pairs) {
            minHeap.add(pair.num1);
            sum += pair.num1;

            if (minHeap.size() > k) {
                sum -= Objects.requireNonNull(minHeap.poll());
            }
            if (minHeap.size() == k) {
                res = Math.max(res, sum * pair.num2);
            }
        }
        return res;
    }

    private static long calcMaxScore1(int[] nums1, int[] nums2, int k) {
        List<Pair> numbers = new ArrayList<>();
        for (int i = 0; i < nums1.length; i++) {
            numbers.add(new Pair(nums1[i], nums2[i]));
        }
        Comparator<Pair> comparator2 = Comparator.comparing(i -> i.num2);
        Comparator<Pair> comparator1 = Comparator.comparing(i -> i.num1);
        //comparator1 = comparator1.reversed();
        numbers.sort(comparator2);

        long res = 0;
        for (int i = 0; i <= nums1.length - k; i++) {
            Collection<Pair> heap = selectMax(numbers, i + 1, k - 1, comparator1);
            long min = numbers.get(i).num2;
            long sum = heap.stream().mapToInt(p -> p.num1).sum() + numbers.get(i).num1;
            res = Math.max(res, sum * min);
        }
        return res;
    }

    private static <X> Collection<X> selectMax(List<X> nums, int start, int k, Comparator<X> comparator) {
        if (k == 0) {
            return List.of();
        }
        PriorityQueue<X> minHeap = new PriorityQueue<>(k, comparator);
        for (int i = start; i < nums.size(); i++) {
            X x = nums.get(i);
            if (minHeap.size() < k) {
                minHeap.add(x);
            } else {
                if (comparator.compare(x, minHeap.peek()) > 0) {
                    minHeap.poll();
                    minHeap.add(x);
                }
            }
        }
        return minHeap;
    }

    @Test
    void test1() {
        // nums1 = [1,3,3,2], nums2 = [2,1,3,4], k = 3
        long res = maxScore(new int[]{1, 3, 3, 2}, new int[]{2, 1, 3, 4}, 3);
        Assertions.assertEquals(12, res);
    }

    @Test
    void test2() {
        // nums1 = [4,2,3,1,1], nums2 = [7,5,10,9,6], k = 1
        long res = maxScore(new int[]{4, 2, 3, 1, 1}, new int[]{7, 5, 10, 9, 6}, 1);
        Assertions.assertEquals(30, res);
    }

    @Test
    void test3() {
        // nums1 = [2,1,14,12], nums2 = [11,7,13,6], k = 3
        long res = maxScore(new int[]{2, 1, 14, 12}, new int[]{11, 7, 13, 6}, 3);
        Assertions.assertEquals(168, res);
    }

    @Test
    void test4() {
        // nums1 = [23,16,20,7,3], nums2 = [11,7,13,6], k = 3
        long res = maxScore(new int[]{23, 16, 20, 7, 3}, new int[]{19, 21, 22, 22, 12}, 3);
        Assertions.assertEquals(1121, res);
    }
}
