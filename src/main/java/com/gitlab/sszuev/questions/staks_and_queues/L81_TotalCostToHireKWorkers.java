package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * You are given a 0-indexed integer array `costs` where `costs[i]` is the cost of hiring the `i`th worker.
 * You are also given two integers `k` and `candidates`.
 * We want to hire exactly `k` workers according to the following rules:
 * - You will run `k` sessions and hire exactly one worker in each session.
 * - In each hiring session, choose the worker with the lowest cost from either the first `candidates` workers
 * or the last `candidates` workers. Break the tie by the smallest index.
 * For example, if `costs = [3,2,7,7,1,2]` and `candidates = 2`, then in the first hiring session,
 * we will choose the `4`th worker because they have the lowest cost `[3,2,7,7,1,2]`.
 * In the second hiring session, we will choose `1`st worker because they have the same lowest cost as `4`th worker
 * but they have the smallest index `[3,2,7,7,2]`. Please note that the indexing may be changed in the process.
 * - If there are fewer than `candidates` workers remaining, choose the worker with the lowest cost among them.
 * Break the tie by the smallest index.
 * - A worker can only be chosen once.
 * Return the total cost to hire exactly `k` workers.
 * <p>
 * Constraints:
 * - `1 <= costs.length <= 10^5`
 * - `1 <= costs[i] <= 10^5`
 * - `1 <= k, candidates <= costs.length`
 *
 * @see <a href="https://leetcode.com/problems/total-cost-to-hire-k-workers">2462. Total Cost to Hire K Workers</a>
 */
public class L81_TotalCostToHireKWorkers {

    public long totalCost(int[] costs, int k, int candidates) {
        return calcTotalCost(costs, k, candidates);
    }

    private static long calcTotalCost(int[] costs, int k, int candidates) {
        TwoQuery tq = new TwoQuery(costs, candidates);
        long res = 0;
        for (int i = 0; i < k; i++) {
            res += tq.next();
        }
        return res;
    }

    private static class TwoQuery {
        private final Queue<Integer> first;
        private Queue<Integer> second;
        private List<Integer> remaining;

        private TwoQuery(int[] costs, int candidates) {
            first = new PriorityQueue<>();
            if (costs.length > 2 * candidates) {
                second = new PriorityQueue<>();
                remaining = new ArrayList<>();
                for (int i = 0; i < candidates; i++) {
                    first.add(costs[i]);
                }
                for (int i = costs.length - candidates; i < costs.length; i++) {
                    second.add(costs[i]);
                }
                for (int i = candidates; i < costs.length - candidates; i++) {
                    remaining.add(costs[i]);
                }
            } else {
                second = null;
                remaining = null;
                for (int cost : costs) {
                    first.add(cost);
                }
            }
        }

        int next() {
            if (second == null) {
                return first.poll();
            }
            int f = first.peek();
            int s = second.peek();
            int r;
            if (f <= s) {
                first.poll();
                if (!remaining.isEmpty()) {
                    first.add(remaining.remove(0));
                }
                r = f;
            } else {
                second.poll();
                if (!remaining.isEmpty()) {
                    second.add(remaining.remove(remaining.size() - 1));
                }
                r = s;
            }
            if (remaining.isEmpty()) {
                first.addAll(second);
                second = null;
                remaining = null;
            }
            return r;
        }
    }

    @Test
    void test1() {
        long actual = totalCost(new int[]{17,12,10,2,7,2,11,20,8}, 3, 4);
        Assertions.assertEquals(11, actual);
    }

    @Test
    void test2() {
        long actual = totalCost(new int[]{1,2,4,1}, 3, 3);
        Assertions.assertEquals(4, actual);
    }
}
