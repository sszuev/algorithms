package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * <b>Q1:</b> Three in One: Describe how you could use a single array to implement three stacks.
 * <p>
 * Hints: #2, #12, #38, #58
 * <ul>
 * <li>#2: A stack is simply a data structure in which the most recently added elements are removed first.
 * Can you simulate a single stack using an array? Remember that there are many possible solutions, and there are tradeoffs of each.</li>
 * <li>#12: We could simulate three stacks in an array by just allocating the first third of the array to the first stack,
 * the second third to the second stack, and the final third to the third stack.
 * One might actually be much bigger than the others, though. Can we be more flexible with the divisions?</li>
 * <li>#38: If you want to allow for flexible divisions, you can shift stacks around.
 * Can you ensure that all available capacity is used?</li>
 * <li>#58: Try thinking about the array as circular, such that the end of the array "wraps around" to the start of the array.</li>
 * </ul>
 * <p>
 * Created by @ssz on 06.01.2021.
 */
public class Q1_ThreeStacksFromSingleArray {

    @Test
    public void testSimpleFIFO() {
        Stack<Integer> stack = ArrayStack.createSimpleArrayStack(10);
        Assertions.assertTrue(stack.isEmpty());
        testPushPopItemsStack(stack);
        Assertions.assertTrue(stack.isEmpty());
    }

    @Test
    public void testValidateMultiStack() {
        int stacks = 4;
        int length = 100;
        MultiStackFromBook<Integer> ms = new MultiStackFromBook<>(stacks, length);
        for (int id = 0; id < stacks; id++) {
            System.out.println("Test stack: " + id);
            Stack<Integer> stack = ms.getStack(id);
            Assertions.assertTrue(stack.isEmpty());
            testPushPopItemsStack(stack);
            Assertions.assertTrue(stack.isEmpty());

            for (int j = 0; j < length; j++) {
                stack.push(j);
                Assertions.assertFalse(stack.isEmpty());
            }
        }
    }

    @Test
    public void testAdvancedMultiStack() {
        int stacks = 3;
        int length = 20;
        MultiStackFromBook<Integer> ms = new MultiStackFromBook<>(stacks, length);
        for (int j = 0; j < length - 1; j++) {
            ms.getStack(0).push(j);
            ms.getStack(1).push(j);
            ms.getStack(2).push(j);
        }
        Stack<Integer> s = ms.getStack(0);
        s.push(12);
        s.push(12);
        s.push(12);
        try {
            s.push(12);
            Assertions.fail("Can put element");
        } catch (MultiStackFromBook.NoSpaceException e) {
            // expected
        }
        ms.getStack(1).pop();
        s.push(12);
        ms.getStack(2).pop();
        s.push(12);
    }

    private void testPushPopItemsStack(Stack<Integer> empty) {
        empty.push(10);
        Assertions.assertFalse(empty.isEmpty());
        Assertions.assertEquals(10, (int) empty.peek());

        empty.push(2);
        Assertions.assertFalse(empty.isEmpty());
        Assertions.assertEquals(2, (int) empty.peek());
        Assertions.assertFalse(empty.isEmpty());

        empty.push(2);
        Assertions.assertEquals(2, (int) empty.peek());
        Assertions.assertFalse(empty.isEmpty());

        empty.push(3);
        Assertions.assertEquals(3, (int) empty.peek());
        Assertions.assertFalse(empty.isEmpty());

        Assertions.assertEquals(3, (int) empty.pop());
        Assertions.assertFalse(empty.isEmpty());

        Assertions.assertEquals(2, (int) empty.pop());
        Assertions.assertFalse(empty.isEmpty());

        Assertions.assertEquals(2, (int) empty.pop());
        Assertions.assertFalse(empty.isEmpty());

        Assertions.assertEquals(10, (int) empty.pop());
    }

    interface Stack<E> {

        void push(E item);

        E pop();

        E peek();

        boolean isEmpty();
    }

    public static class ArrayStack<E> implements Stack<E> {
        final E[] store;
        final int start, end;
        int index;

        @SuppressWarnings("unchecked")
        ArrayStack(Object[] store, int start, int end) {
            this.store = (E[]) store;
            this.start = start;
            this.end = end;
            this.index = start;
        }

        public static <E> Stack<E> createSimpleArrayStack(int capacity) {
            return new ArrayStack<>(new Object[capacity], 0, capacity - 1);
        }

        @Override
        public void push(E item) {
            checkIndex();
            store[index++] = item;
        }

        protected void checkIndex() {
            if (index > end) {
                throw new ArrayIndexOutOfBoundsException();
            }
        }

        @Override
        public E peek() {
            return store[index - 1];
        }

        @Override
        public E pop() {
            E res = store[--index];
            store[index] = null;
            return res;
        }


        @Override
        public boolean isEmpty() {
            return index == start;
        }
    }

    /**
     * Updated, from the book.
     *
     * @param <E> - anything
     */
    static class MultiStackFromBook<E> {

        final E[] values;
        final StackInfo[] info;

        @SuppressWarnings("unchecked")
        MultiStackFromBook(int numberOfStacks, int defaultSize) {
            this.info = new StackInfo[numberOfStacks];
            int size = defaultSize * numberOfStacks;
            for (int i = 0; i < numberOfStacks; i++) {
                info[i] = new StackInfo(size, defaultSize * i, defaultSize);
            }
            this.values = (E[]) new Object[size];
        }

        public Stack<E> getStack(int id) {
            if (id < 0 || id > info.length)
                throw new IllegalArgumentException();
            return new Stack<>() {
                @Override
                public void push(E item) {
                    MultiStackFromBook.this.push(id, item);
                }

                @Override
                public E pop() {
                    return MultiStackFromBook.this.pop(id);
                }

                @Override
                public E peek() {
                    return MultiStackFromBook.this.peek(id);
                }

                @Override
                public boolean isEmpty() {
                    return info[id].size == 0;
                }
            };
        }

        /**
         * Pushes the value onto stack with the given {@code id},
         * shifting/expanding stacks as necessary.
         *
         * @param id    stack id, starting from {@code 0}
         * @param value value
         * @throws NoSpaceException if no space
         */
        public void push(int id, E value) throws IndexOutOfBoundsException {
            if (numberOfElements() == values.length) {
                throw new NoSpaceException("All stacks are full.");
            }

            StackInfo stack = this.info[id];
            if (stack.isFull()) {
                expand(id);
            }
            stack.size++;
            values[stack.lastElementIndex()] = value;
        }

        /**
         * Removes the value from the stack.
         *
         * @param id stack id, starting from {@code 0}
         * @return value
         */
        public E pop(int id) throws IndexOutOfBoundsException {
            StackInfo stack = this.info[id];
            if (stack.isEmpty()) {
                return null;
            }
            // remove last element
            int i = stack.lastElementIndex();
            E value = values[i];
            values[i] = null; // clear
            stack.size--;
            return value;
        }

        public E peek(int id) {
            StackInfo stack = info[id];
            return values[stack.lastElementIndex()];
        }

        private int numberOfElements() {
            int res = 0;
            for (StackInfo i : info) {
                res += i.size;
            }
            return res;
        }

        /**
         * Expands the stack by shifting over other stacks.
         *
         * @param stackNum stack id, starting from {@code 0}
         */
        private void expand(int stackNum) {
            shift((stackNum + 1) % info.length);
            info[stackNum].capacity++;
        }

        /**
         * Shifts items in stack over by one element.
         * If we have available capacity then we'll end up shrinking the stack by one element;
         * If we don't have available capacity then we need to shift the next stack over too.
         *
         * @param id stack id, starting from {@code 0}
         */
        private void shift(int id) {
            StackInfo stack = info[id];
            // If this stack is at its full capacity then we need to move the next stack over by one element.
            // This stack can now claim the freed index.
            if (stack.size >= stack.capacity) {
                int nextStack = (id + 1) % info.length;
                shift(nextStack);
                stack.capacity++; // claim index that next stack lost
            }
            // shift all elements in stack over by one
            int index = stack.lastCapacityIndex();
            while (stack.isWithinStackCapacity(index)) {
                int i = previousIndex(index);
                values[index] = values[i];
                index = i;
            }
            // adjust stack data
            values[stack.start] = null; // clear item
            stack.start = nextIndex(stack.start); // move start
            stack.capacity--; // shrink capacity
        }

        /**
         * Gets the index after specified, adjusted for wrap around.
         *
         * @param index int
         * @return int
         */
        private int nextIndex(int index) {
            return adjustIndex(index + 1);
        }

        /**
         * Gets the index before specified, adjusted for wrap around.
         *
         * @param index int
         * @return int
         */
        private int previousIndex(int index) {
            return adjustIndex(index - 1);
        }

        /**
         * Adjusts index to be within the range of {@code [0, length - 1]}.
         *
         * @param index the index, int
         * @return int
         */
        private int adjustIndex(int index) {
            return adjustIndex(values.length, index);
        }

        private static int adjustIndex(int max, int index) {
            // -7/5 => 3; 7/5 => 2
            return ((index % max) + max) % max;
        }

        /**
         * It is a simple class that holds a set of data about each stack.
         * It does not hold the actual items in the stack.
         */
        static class StackInfo {
            private int start;
            private int capacity;
            private int size;
            private final int max;

            StackInfo(int max, int start, int capacity) {
                this.max = max;
                this.start = start;
                this.capacity = capacity;
            }

            /**
             * Checks if the index is within the main array boundaries.
             * The stack can be wrap around to the start of the array.
             *
             * @param index int
             * @return boolean
             */
            boolean isWithinStackCapacity(int index) {
                if (index < 0 || index >= max) return false;
                int contiguousIndex = index < start ? index + max : index;
                int end = start + capacity;
                return start <= contiguousIndex && contiguousIndex < end;
            }

            int lastCapacityIndex() {
                return adjustIndex(max, start + capacity - 1);
            }

            int lastElementIndex() {
                return adjustIndex(max, start + size - 1);
            }

            boolean isFull() {
                return size == capacity;
            }

            boolean isEmpty() {
                return size == 0;
            }
        }

        static class NoSpaceException extends IndexOutOfBoundsException {

            public NoSpaceException(String s) {
                super(s);
            }
        }

    }

}
