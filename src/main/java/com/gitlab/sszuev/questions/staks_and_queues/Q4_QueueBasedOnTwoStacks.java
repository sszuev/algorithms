package com.gitlab.sszuev.questions.staks_and_queues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.LinkedList;

/**
 * <b>Q4:</b>: Queue via Stacks: Implement a MyQueue class which implements a queue using two stacks.
 * <p>
 * Hints: #98, #114
 * <ul>
 * <li>#98: The major difference between a queue and a stack is the order of elements.
 * A queue removes the oldest item and a stack removes the newest item.
 * How could you remove the oldest item from a stack if you only had access to the newest item?</li>
 * <li>#114: We can remove the oldest item from a stack by repeatedly removing the newest item
 * (inserting those into the temporary stack) until we get down to one element.
 * Then, after we've retrieved the newest item, putting all the elements back.
 * The issue with this is that doing several pops in a row will require {@code 0(N)} work each time.
 * Can we optimize for scenarios where we might do several pops in a row?</li>
 * </ul>
 * Created by @ssz on 06.01.2021.
 */
public class Q4_QueueBasedOnTwoStacks {

    @ParameterizedTest
    @EnumSource(Data.class)
    public void testStringQueue(Data data) {
        Q<String> list = data.createQueue();
        Assertions.assertTrue(list.isEmpty());

        Assertions.assertTrue(list.add("a"));
        Assertions.assertFalse(list.isEmpty());
        Assertions.assertEquals("a", list.peek());
        Assertions.assertEquals("a", list.remove());
        Assertions.assertTrue(list.isEmpty());

        Assertions.assertTrue(list.add("b"));
        Assertions.assertTrue(list.add("c"));
        Assertions.assertEquals("b", list.peek());
        Assertions.assertTrue(list.add("d"));
        Assertions.assertEquals("b", list.peek());
        Assertions.assertFalse(list.isEmpty());

        Assertions.assertEquals("b", list.remove());
        Assertions.assertFalse(list.isEmpty());
        Assertions.assertEquals("c", list.peek());
        Assertions.assertEquals("c", list.remove());
        Assertions.assertFalse(list.isEmpty());
        Assertions.assertEquals("d", list.peek());
        Assertions.assertEquals("d", list.remove());
        Assertions.assertTrue(list.isEmpty());

        Assertions.assertTrue(list.add("e"));
        Assertions.assertTrue(list.add("f"));
        Assertions.assertEquals("e", list.remove());
        Assertions.assertEquals("f", list.remove());
        Assertions.assertTrue(list.isEmpty());
    }

    interface Q<E> {

        static <X> Q<X> createStandardImpl() {
            class Impl<E> extends LinkedList<E> implements Q<E> {
            }
            return new Impl<>();
        }

        boolean add(E e);

        E remove();

        E peek();

        boolean isEmpty();
    }

    interface S<E> {

        static <X> S<X> createStandardImpl() {
            class Impl<E> extends LinkedList<E> implements S<E> {
            }
            return new Impl<>();
        }

        void push(E item);

        E pop();

        E peek();

        boolean isEmpty();

    }

    enum Data {
        STD_LINKED_LIST {
            @Override
            <E> Q<E> createQueue() {
                return Q.createStandardImpl();
            }
        },

        MY_TWO_STACK_IMPL {
            @Override
            <E> Q<E> createQueue() {
                return new QImpl<>();
            }

            class QImpl<E> implements Q<E> {
                final S<E> head = S.createStandardImpl();
                final S<E> bottom = S.createStandardImpl();

                @Override
                public boolean add(E e) {
                    head.push(e);
                    return true;
                }

                @Override
                public E remove() {
                    return bottom().pop();
                }

                @Override
                public E peek() {
                    return bottom().peek();
                }

                private S<E> bottom() {
                    if (!bottom.isEmpty()) {
                        return bottom;
                    }
                    while (!head.isEmpty()) {
                        bottom.push(head.pop());
                    }
                    return bottom;
                }

                @Override
                public boolean isEmpty() {
                    return head.isEmpty() && bottom.isEmpty();
                }
            }
        };

        abstract <E> Q<E> createQueue();
    }
}
