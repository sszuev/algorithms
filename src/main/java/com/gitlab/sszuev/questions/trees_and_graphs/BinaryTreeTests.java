package com.gitlab.sszuev.questions.trees_and_graphs;

import java.util.stream.Stream;

/**
 * Created by @ssz on 07.10.2020.
 */
abstract class BinaryTreeTests {

    public static Node<String> createSampleStringTree() {
        Node<String> root = Node.of("a");
        root.left = Node.of("b");
        root.right = Node.of("f");
        root.left.left = Node.of("c");
        root.left.right = Node.of("e");
        root.left.left.left = Node.of("d");
        root.right.left = Node.of("g");
        root.right.right = Node.of("h");
        root.right.right.right = Node.of("k");
        return root;
    }

    public static Node<Integer> createSampleIntTree() {
        Node<Integer> root = Node.of(8);

        root.left = Node.of(3);
        root.left.left = Node.of(1);
        root.left.right = Node.of(6);
        root.left.right.left = Node.of(4);
        root.left.right.right = Node.of(7);

        root.right = Node.of(10);

        root.right.right = Node.of(14);
        root.right.right.left = Node.of(12);
        return root;
    }

    static class Node<X> implements MyTreeNode<X> {
        final X value;
        Node<X> left, right;

        Node(X value) {
            this.value = value;
            left = right = null;
        }

        static <X> Node<X> of(X v) {
            return new Node<>(v);
        }

        @Override
        public Stream<MyTreeNode<X>> children() {
            return Stream.of(left, right);
        }

        public X getValue() {
            return value;
        }

        boolean isLeaf() {
            return left == null && right == null;
        }
    }
}
