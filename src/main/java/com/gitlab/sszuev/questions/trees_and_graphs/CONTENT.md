[HOME](../../../../../../../../README.md)

- **[T1](T1_PreOrderTraversal.java)**: Binary Tree: Pre order traversal Algorithms
- **[T2](T2_InOrderTraversal.java)**: Binary Tree: In order traversal Algorithms
- **[T3](T3_PostOrderTraversal.java)**: Binary Tree: Post order traversal Algorithms
- **[T4](T4_PrintTree.java)**: Binary Tree: Print all leaves
- **[T5](T5_CountLeaves.java)**: Binary Tree: Count all leaves
- **[T6](T6_BSTSize.java)**: BST (Binary Search Tree) implementation

- **[Q1](Q1_FindRouteBetweenGraphNodes.java)**: Route Between Nodes:
Given a directed graph, design an algorithm to find out whether there is a route between two nodes.
  <details>
    <summary>Hints</summary>

    - ***127***: Two well-known algorithms can do this. What are the tradeoffs between them?

  </details>
  
- **[Q2](Q2_CreateMinimalHeightTreeFromSortedArray.java)**: Minimal Tree: Given a sorted (increasing order) array with
  unique integer elements, write an algorithm to create a binary search tree with minimal height.
  <details>
    <summary>Hints</summary>

    - ***19***: A minimal binary tree has about the same number of nodes on the left of each node as on the right. Let's
      focus on just the root for now. How would you ensure that about the same number of nodes are on the left of the
      root as on the right?
    - ***73***: You could implement this by finding the "ideal" next element to add and repeatedly calling `insertValue`
      . This will be a bit inefficient, as you would have to repeatedly traverse the tree. Try recursion instead. Can
      you divide this problem into subproblems?
    - ***116***: Imagine we had a `createMinimalTree` method that returns a minimal tree for a given array
      (but for some strange reason doesn't operate on the root of the tree). Could you use this to operate on the root
      of the tree? Could you write the base case for the function? Great! Then that's basically the entire function.

  </details> 

- **[L11](L11_CountNodesEqualToSubTreeAverage.java)**: (leetcode) Count Nodes Equal to Average of Subtree

- **[L16](L16_LeafSimilarTrees.java)**: (leetcode) Leaf-Similar Trees

- **[L17](L17_KeysAndRooms.java)**: (leetcode) Keys and Rooms

- **[L25](L25_BinaryTreeRightSideView.java)**: (leetcode) Binary Tree Right Side View

- **[L27](L27_SearchInBST.java)**: (leetcode) Search in a Binary Search Tree

- **[L28](L28_NearestExitFromEntranceInMaze.java)**: (leetcode) Nearest Exit from Entrance in Maze

- **[L32](L32_HashMapTrie.java)**: (leetcode) Implement Trie (Prefix Tree)

- **[L40](L40_SmallestNumberInInfiniteSet.java)**: (leetcode) Smallest Number in Infinite Set

- **[L42](L42_MaximumDepthOfBinaryTree.java)**: (leetcode) Maximum Depth of Binary Tree

- **[L46](L46_CountGoodNodesInBinaryTree.java)**: (leetcode) Count Good Nodes in Binary Tree

- **[L48](L48_NumberOfProvinces.java)**: (leetcode) Number of provinces

- **[L57](L57_SearchSuggestionsSystem.java)**: (leetcode) Search Suggestions System

- **[L61](L61_LowestCommonAncestorOfBinaryTree.java)**: (leetcode) Lowest Common Ancestor of a Binary Tree

- **[L77](L77_ReorderRoutesToMakeAllPathsLeadToCityZero.java)**: (leetcode) Reorder Routes to Make All Paths Lead to the City Zero

- **[L79](L79_PathSumIII.java)**: (leetcode) Path Sum III

- **[L80](L80_DeleteNodeInBST.java)**: (leetcode) Delete Node in a BST

- **[L82](L82_RottingOranges.java)**: (leetcode) Rotting Oranges

- **[L83](L83_LongestZigZagPathInBinaryTree.java)**: (leetcode) Longest ZigZag Path in a Binary Tree

- **[L84](L84_MaximumLevelSumOfBinaryTree.java)**: (leetcode) Maximum Level Sum of a Binary Tree

- **[L85](L85_EvaluateDivision.java)**: (leetcode) Evaluate Division