package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Given the root of a binary tree,
 * return the number of nodes where the value of the node is equal to the average of the values in its subtree.
 * <p>
 * Notes:
 * 1) The average of n elements is the sum of the n elements divided by n and rounded down to the nearest integer.
 * 2) A subtree of root is a tree consisting of root and all of its descendants.
 * <p>
 * Constraints: 1) The number of nodes in the tree is in the range [1, 1000].
 * 2) 0 <= Node.val <= 1000
 *
 * @see <a href="https://leetcode.com/problems/count-nodes-equal-to-average-of-subtree">2265. Count Nodes Equal to Average of Subtree</a>
 */
@SuppressWarnings("ALL")
public class L11_CountNodesEqualToSubTreeAverage {

    public static int numberOfNodesWhereAverageOfSubtreeEqualValue(TreeNode root) {
        return calcAverage(root);
    }

    private static int calcAverage(TreeNode root) {
        AtomicInteger res = new AtomicInteger();
        calcStats(root, res);
        return res.intValue();
    }

    private static TreeNodeStats calcStats(TreeNode root, AtomicInteger res) {
        int num = 1;
        int sum = root.val;
        if (root.right != null) {
            TreeNodeStats rightStats = calcStats(root.right, res);
            num += rightStats.num;
            sum += rightStats.sum;
        }
        if (root.left != null) {
            TreeNodeStats rightStats = calcStats(root.left, res);
            num += rightStats.num;
            sum += rightStats.sum;
        }
        int av = sum / num;
        if (av == root.val) {
            res.incrementAndGet();
        }
        return new TreeNodeStats(num, sum);
    }

    private static class TreeNodeStats {
        private final int num;
        private final int sum;

        private TreeNodeStats(int num, int sum) {
            this.num = num;
            this.sum = sum;
        }
    }

    @Test
    public void test4_8_5_0_1_null_6() {
        TreeNode tree = TreeNode.of(4)
                .left(
                        TreeNode.of(8)
                                .left(0)
                                .right(1)
                )
                .right(
                        TreeNode.of(5)
                                .right(6)
                );
        Assertions.assertEquals(5, numberOfNodesWhereAverageOfSubtreeEqualValue(tree));
    }

    @Test
    public void test1() {
        TreeNode tree = TreeNode.of(1);
        Assertions.assertEquals(1, numberOfNodesWhereAverageOfSubtreeEqualValue(tree));
    }

    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }
    }
}
