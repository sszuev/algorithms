package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Consider all the leaves of a binary tree, from left to right order, the values of those leaves form a leaf value sequence.
 * <pre>
 * {@code
 *        3
 *       /  \
 *     5     1
 *   /  |   | \
 *  6   2   9  8
 *     / \
 *    7   4
 * }
 * </pre>
 * For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).
 * Two binary trees are considered leaf-similar if their leaf value sequence is the same.
 * Return true if and only if the two given trees with head nodes root1 and root2 are leaf-similar.
 * <p>
 * Constraints:
 * 1) The number of nodes in each tree will be in the range [1, 200].
 * 2) Both of the given trees will have values in the range [0, 200].
 *
 * @see <a href="https://leetcode.com/problems/leaf-similar-trees">872. Leaf-Similar Trees</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L16_LeafSimilarTrees {

    public boolean leafSimilar(TreeNode root1, TreeNode root2) {
        return isLeafSimilar(root1, root2);
    }

    private static boolean isLeafSimilar(TreeNode root1, TreeNode root2) {
        Deque<TreeNode> stack1 = new ArrayDeque<>();
        Deque<TreeNode> stack2 = new ArrayDeque<>();
        stack1.addFirst(root1);
        stack2.addFirst(root2);

        TreeNode leaf1;
        TreeNode leaf2;
        while (true) {
            leaf1 = findLeaf(stack1);
            leaf2 = findLeaf(stack2);
            if (leaf1 == null && leaf2 == null) {
                break;
            }
            if (leaf1 == null || leaf2 == null) {
                return false;
            }
            if (leaf1.val != leaf2.val) {
                return false;
            }
        }
        return true;
    }

    private static TreeNode findLeaf(Deque<TreeNode> stack) {
        while (!stack.isEmpty()) {
            TreeNode node = stack.removeFirst();
            if (isLeaf(node)) {
                return node;
            }
            if (node.left != null) {
                stack.addFirst(node.left);
            }
            if (node.right != null) {
                stack.addFirst(node.right);
            }
        }
        return null;
    }

    private static boolean isLeaf(TreeNode node) {
        return node.left == null && node.right == null;
    }

    @Test
    public void test1() {
        // *        3
        // *       /  \
        // *     5     1
        // *   /  |   | \
        // *  6   2   9  8
        // *     / \
        // *    7   4
        TreeNode root1 = TreeNode.of(3)
                .left(TreeNode.of(5)
                        .left(6)
                        .right(TreeNode.of(2)
                                .left(7)
                                .right(4)
                        )
                )
                .right(TreeNode.of(1)
                        .left(9)
                        .right(8)
                );

        // *        3
        // *       /  \
        // *     5     1
        // *   /  |   | \
        // *  6   7   4  2
        // *            / \
        // *           9   8
        TreeNode root2 = TreeNode.of(3)
                .left(TreeNode.of(5)
                        .left(6)
                        .right(7)
                )
                .right(TreeNode.of(1)
                        .left(4)
                        .right(TreeNode.of(2)
                                .left(9)
                                .right(8)
                        )
                );

        Assertions.assertTrue(leafSimilar(root1, root2));
    }

    @Test
    public void test2() {
        TreeNode root1 = TreeNode.of(1).left(2).right(3);
        TreeNode root2 = TreeNode.of(1).left(3).right(2);
        Assertions.assertFalse(leafSimilar(root1, root2));
    }

    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }
    }
}
