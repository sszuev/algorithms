package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * There are n rooms labeled from {@code 0} to {@code n - 1} and all the rooms are locked except for room {@code 0}.
 * Your goal is to visit all the rooms. However, you cannot enter a locked room without having its key.
 * When you visit a room, you may find a set of distinct keys in it.
 * Each key has a number on it, denoting which room it unlocks,
 * and you can take all of them with you to unlock the other rooms.
 * <p>
 * Given an array rooms where {@code rooms[i]} is the set of keys that you can obtain if you visited room {@code i},
 * return true if you can visit all the rooms, or false otherwise.
 * Constraints:
 * 1) n == rooms.length;
 * 2) 2 <= n <= 1000
 * 3) 0 <= rooms[i].length <= 1000
 * 4) 1 <= sum(rooms[i].length) <= 3000
 * 5) 0 <= rooms[i][j] < n
 * All the values of rooms[i] are unique.
 *
 * @see <a href="https://leetcode.com/problems/keys-and-rooms</a>
 */
public class L17_KeysAndRooms {
    public boolean canVisitAllRooms(List<List<Integer>> rooms) {
        return visitAllRooms(rooms);
    }

    public static boolean visitAllRooms(List<List<Integer>> rooms) {
        Set<Integer> seen = new HashSet<>();
        Deque<Integer> queue = new ArrayDeque<>();
        queue.add(0);
        while (!queue.isEmpty()) {
            Integer current = queue.removeFirst();
            if (!seen.add(current)) {
                continue;
            }
            List<Integer> next = rooms.size() > current ? rooms.get(current) : null;
            if (next == null) {
                continue;
            }
            queue.addAll(next);
        }
        return seen.size() == rooms.size();
    }

    @Test
    public void test1() {
        // [[1],[2],[3],[]]
        Assertions.assertTrue(canVisitAllRooms(List.of(List.of(1), List.of(2), List.of(3), List.of())));
    }

    @Test
    public void test2() {
        // [[1,3],[3,0,1],[2],[0]]
        Assertions.assertFalse(canVisitAllRooms(List.of(List.of(1, 3), List.of(3, 0, 1), List.of(2), List.of(0))));
    }
}
