package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Given the root of a binary tree, imagine yourself standing on the right side of it,
 * return the values of the nodes you can see ordered from top to bottom.
 * OR:
 * Give the root of the binary tree, return the values of the right most node at each level of the tree from top to bottom.
 *
 * @see <a href="https://leetcode.com/problems/binary-tree-right-side-view">199. Binary Tree Right Side View</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L25_BinaryTreeRightSideView {

    public List<Integer> rightSideView(TreeNode root) {
        if (root == null) {
            return List.of();
        }
        return findRightmostNodeValues(root);
    }

    private List<Integer> findRightmostNodeValues(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        Map<TreeNode, Integer> levels = new LinkedHashMap<>();
        levels.put(root, 0);
        while (!levels.isEmpty()) {
            Iterator<TreeNode> it = levels.keySet().iterator();
            TreeNode node = it.next();
            TreeNode next = it.hasNext() ? it.next() : null;
            Integer level = levels.remove(node);
            Integer nextLevel = level + 1;
            if (next == null || !Objects.equals(levels.get(next), level)) {
                // next level
                res.add(node.val);
            }
            if (node.left != null) {
                levels.put(node.left, nextLevel);
            }
            if (node.right != null) {
                levels.put(node.right, nextLevel);
            }
        }
        return res;
    }

    @Test
    void test_1_2_3_null_5_null_4_null() {
        TreeNode root = TreeNode.of(1)
                .left(
                        TreeNode.of(2)
                                .right(5)
                ).right(
                        TreeNode.of(3)
                                .right(4)
                );

        Assertions.assertEquals(List.of(1, 3, 4), rightSideView(root));
    }

    @Test
    void test_1_null_3() {
        TreeNode root = TreeNode.of(1)
                .right(3);

        Assertions.assertEquals(List.of(1, 3), rightSideView(root));
    }

    @Test
    void test_null() {
        Assertions.assertEquals(List.of(), rightSideView(null));
    }

    @Test
    void test_1_2_3_null_5_3_6_null_5_4_null() {
        TreeNode root = TreeNode.of(1)
                .left(
                        TreeNode.of(2)
                                .right(
                                        TreeNode.of(5)
                                                .left(4)
                                )
                ).right(
                        TreeNode.of(3)
                                .left(6)
                );

        Assertions.assertEquals(List.of(1, 3, 6, 4), rightSideView(root));
    }

    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }
    }
}
