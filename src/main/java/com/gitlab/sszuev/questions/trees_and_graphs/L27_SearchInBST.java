package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * You are given the root of a binary search tree (BST) and an integer val. *
 * Find the node in the BST that the node's value equals val and return the subtree rooted with that node.
 * If such a node does not exist, return null.
 * <p>
 * Constraints:
 * 1) The number of nodes in the tree is in the range [1, 5000].
 * 2) 1 <= Node.val <= 10^7
 * 3) root is a binary search tree.
 * 4) 1 <= val <= 10^7
 *
 * @see <a href="https://leetcode.com/problems/search-in-a-binary-search-tree">700. Search in a Binary Search Tree</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L27_SearchInBST {

    public TreeNode searchBST(TreeNode root, int val) {
        return findBST(root, val);
    }

    public static TreeNode findBST(TreeNode root, int val) {
        Deque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode node = queue.removeFirst();
            if (node.val == val) {
                return node;
            }
            if (node.right != null && node.val < val) {
                queue.add(node.right);
            }
            if (node.left != null && node.val > val) {
                queue.add(node.left);
            }
        }
        return null;
    }

    @Test
    void testSearch2_in_4_2_7_1_3() {
        TreeNode root = TreeNode.of(4)
                .left(
                        TreeNode.of(2)
                                .left(1)
                                .right(3)
                ).right(7);
        int actual = searchBST(root, 2).val;
        Assertions.assertEquals(2, actual);
    }

    @Test
    void testSearch5_in_4_2_7_1_3() {
        TreeNode root = TreeNode.of(4)
                .left(
                        TreeNode.of(2)
                                .left(1)
                                .right(3)
                ).right(7);
        TreeNode actual = searchBST(root, 5);
        Assertions.assertNull(actual);
    }

    @SuppressWarnings("SameParameterValue")
    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }
    }
}
