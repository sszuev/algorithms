package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * You are given an m x n matrix maze (0-indexed) with empty cells (represented as '.') and walls (represented as '+').
 * You are also given the entrance of the maze,
 * where entrance = [row, col] denotes the row and column of the cell you are initially standing at.
 * <p>
 * In one step, you can move one cell up, down, left, or right.
 * You cannot step into a cell with a wall, and you cannot step outside the maze.
 * Your goal is to find the nearest exit from the entrance.
 * An exit is defined as an empty cell that is at the border of the maze.
 * The entrance does not count as an exit.
 * <p>
 * Return the number of steps in the shortest path from the entrance to the nearest exit, or -1 if no such path exists.
 *
 * @see <a href="https://leetcode.com/problems/nearest-exit-from-entrance-in-maze">1926. Nearest Exit from Entrance in Maze</a>
 */
public class L28_NearestExitFromEntranceInMaze {

    public int nearestExit(char[][] maze, int[] entrance) {
        return findNearestExit(maze, List.of(entrance[0], entrance[1]));
    }

    private static int findNearestExit(char[][] maze, List<Integer> entrance) {
        Deque<List<Integer>> queue = new ArrayDeque<>();
        queue.add(entrance);
        Map<List<Integer>, Integer> paths = new HashMap<>();
        paths.put(entrance, 0);
        Set<List<Integer>> seen = new HashSet<>();
        seen.add(entrance);
        boolean first = true;
        while (!queue.isEmpty()) {
            List<Integer> point = queue.removeFirst();
            int path = Objects.requireNonNull(paths.get(point));
            if (first) {
                first = false;
            } else {
                if (isBound(maze, point)) {
                    return path;
                }
            }
            int nextPath = path + 1;
            findPossibleSteps(maze, point).forEach(p -> {
                if (seen.add(p)) {
                    queue.add(p);
                    paths.put(p, nextPath);
                }
            });
        }
        return -1;
    }

    private static Set<List<Integer>> findPossibleSteps(char[][] maze, List<Integer> point) {
        int x = point.get(0);
        int y = point.get(1);
        Set<List<Integer>> res = new HashSet<>();
        if (x != 0) {
            // up
            if (maze[x - 1][y] == '.') {
                res.add(List.of(x - 1, y));
            }
        }
        if (x != maze.length - 1) {
            // down
            if (maze[x + 1][y] == '.') {
                res.add(List.of(x + 1, y));
            }
        }
        char[] row = maze[x];
        if (y != 0) {
            // left
            if (row[y - 1] == '.') {
                res.add(List.of(x, y - 1));
            }
        }
        if (y != row.length - 1) {
            // right
            if (row[y + 1] == '.') {
                res.add(List.of(x, y + 1));
            }
        }
        return res;
    }

    private static boolean isBound(char[][] maze, List<Integer> point) {
        int x = point.get(0);
        int y = point.get(1);
        if (x == 0 || y == 0 || x == maze.length - 1) {
            return true;
        }
        char[] row = maze[x];
        return y == row.length - 1;
    }

    @Test
    public void test1() {
        int actual = nearestExit(new char[][]{{'+', '+', '.', '+'}, {'.', '.', '.', '+'}, {'+', '+', '+', '.'}}, new int[]{1, 2});
        Assertions.assertEquals(1, actual);
    }

    @Test
    public void test2() {
        int actual = nearestExit(new char[][]{{'+', '+', '+'}, {'.', '.', '.'}, {'+', '+', '+'}}, new int[]{1, 0});
        Assertions.assertEquals(2, actual);
    }

    @Test
    public void test3() {
        int actual = nearestExit(new char[][]{{'.', '+'}}, new int[]{0, 0});
        Assertions.assertEquals(-1, actual);
    }

    @Test
    public void test4() {
        char[][] maze = {
                {'+', '.', '+', '+', '+', '+', '+'},
                {'+', '.', '+', '.', '.', '.', '+'},
                {'+', '.', '+', '.', '+', '.', '+'},
                {'+', '.', '.', '.', '+', '.', '+'},
                {'+', '+', '+', '+', '+', '.', '+'}
        };
        int[] entrance = new int[]{0, 1};
        int actual = nearestExit(maze, entrance);
        Assertions.assertEquals(12, actual);
    }

}
