package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * A trie (pronounced as "try") or prefix tree is a tree data structure used
 * to efficiently store and retrieve keys in a dataset of strings.
 * There are various applications of this data structure, such as autocomplete and spellchecker.
 * <pre>
 * {@code
 * interface Trie {
 *     // Inserts the string word into the trie.
 *     void insert(String word);
 *     // Returns true if the string word is in the trie (i.e., was inserted before), and false otherwise.
 *     boolean search(String word);
 *     // Returns true if there is a previously inserted string word that has the prefix prefix, and false otherwise.
 *     boolean startsWith(String prefix);
 * }
 * }
 * </pre>
 *
 * @see <a href="https://leetcode.com/problems/implement-trie-prefix-tree">208. Implement Trie (Prefix Tree)</a>
 */
public class L32_HashMapTrie {

    @Test
    public void test() {
        Trie trie = new Trie();
        trie.insert("apple");
        Assertions.assertTrue(trie.search("apple"));
        Assertions.assertFalse(trie.search("app"));
        Assertions.assertTrue(trie.startsWith("app"));
        trie.insert("app");
        Assertions.assertTrue(trie.search("app"));
    }

    static class Trie {
        private final Set<String> words = new HashSet<>();
        private final Set<String> prefixes = new HashSet<>();

        // Inserts the string word into the trie.
        public void insert(String word) {
            words.add(word);
            char[] chars = word.toCharArray();
            StringBuilder sb = new StringBuilder();
            for (char ch : chars) {
                sb.append(ch);
                prefixes.add(sb.toString());
            }
        }

        // Returns true if the string word is in the trie (i.e., was inserted before), and false otherwise.
        public boolean search(String word) {
            return words.contains(word);
        }

        // Returns true if there is a previously inserted string word that has the prefix prefix, and false otherwise.
        public boolean startsWith(String prefix) {
            return prefixes.contains(prefix);
        }
    }
}
