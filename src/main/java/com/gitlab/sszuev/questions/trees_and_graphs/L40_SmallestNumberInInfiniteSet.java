package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * You have a set which contains all positive integers {@code [1, 2, 3, 4, 5, ...]}.
 * Implement the {@code SmallestInfiniteSet} class:
 * <p>
 * {@code SmallestInfiniteSet()} Initializes the {@code SmallestInfiniteSet} object to contain all positive integers.
 * <p>
 * {@code int popSmallest()} Removes and returns the smallest integer contained in the infinite set.
 * <p>
 * {@code void addBack(int num)} Adds a positive integer num back into the infinite set, if it is not already in the infinite set.
 * <p>
 * Constraints:
 * <p>
 * {@code 1 <= num <= 1000}
 * <p>
 * At most, 1000 calls will be made in total to `popSmallest` and `addBack`.
 *
 * @see <a href="https://leetcode.com/problems/smallest-number-in-infinite-set">2336. Smallest Number in Infinite Set</a>
 */
public class L40_SmallestNumberInInfiniteSet {

    @Test
    void test1() {
        SmallestInfiniteSet res = new SmallestInfiniteSet();
        res.addBack(2);
        Assertions.assertEquals(1, res.popSmallest());
        Assertions.assertEquals(2, res.popSmallest());
        Assertions.assertEquals(3, res.popSmallest());
        res.addBack(1);
        Assertions.assertEquals(1, res.popSmallest());
        Assertions.assertEquals(4, res.popSmallest());
        Assertions.assertEquals(5, res.popSmallest());
    }

    @Test
    void test2() {
        SmallestInfiniteSet res = new SmallestInfiniteSet();

        Assertions.assertEquals(1, res.popSmallest());
        res.addBack(608);
        Assertions.assertEquals(2, res.popSmallest());
        res.addBack(4);
        Assertions.assertEquals(3, res.popSmallest());
        res.addBack(384);
        res.addBack(975);
        res.addBack(698);
    }

    static class SmallestInfiniteSet {
        NavigableSet<Integer> set = new TreeSet<>();
        Set<Integer> seen = new HashSet<>();

        public SmallestInfiniteSet() {
            set.add(1);
        }

        public int popSmallest() {
            int res = set.first();
            set.remove(res);
            seen.add(res);
            int next = res + 1;
            if (!seen.contains(next)) {
                set.add(next);
            }
            return res;
        }

        public void addBack(int num) {
            set.add(num);
            seen.remove(num);
        }
    }
}
