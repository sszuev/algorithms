package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

/**
 * Given the root of a binary tree, return its maximum depth.
 * A binary tree's maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
 * <p>
 * Constraints: the number of nodes in the tree is in the range `[0, 10^4]`; `-100 <= Node.val <= 100`
 *
 * @see <a href="https://leetcode.com/problems/maximum-depth-of-binary-tree/">104. Maximum Depth of Binary Tree</a>
 */
public class L42_MaximumDepthOfBinaryTree {

    @SuppressWarnings("ClassEscapesDefinedScope")
    public int maxDepth(TreeNode root) {
        return calcMaxDepth(root);
    }

    private static int calcMaxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Deque<TreeNode> queue = new ArrayDeque<>();
        Map<TreeNode, Integer> paths = new HashMap<>();
        queue.add(root);
        paths.put(root, 1);
        TreeNode current = null;
        while (!queue.isEmpty()) {
            current = queue.removeFirst();
            TreeNode left = current.left;
            TreeNode right = current.right;
            int depth = paths.get(current) + 1;
            if (left != null) {
                queue.add(left);
                paths.put(left, depth);
            }
            if (right != null) {
                queue.add(right);
                paths.put(right, depth);
            }
        }
        return paths.get(current);
    }

    @Test
    void test1() {
        TreeNode root = TreeNode.of(3)
                .left(9)
                .right(TreeNode.of(20)
                        .left(15)
                        .right(7)
                );
        int actual = maxDepth(root);
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test2() {
        TreeNode root = TreeNode.of(1).right(2);
        int actual = maxDepth(root);
        Assertions.assertEquals(2, actual);
    }

    @Test
    void test3(){
        TreeNode root = TreeNode.of(1);
        int actual = maxDepth(root);
        Assertions.assertEquals(1, actual);
    }

    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }
    }
}
