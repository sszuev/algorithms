package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

/**
 * Given a binary tree root,
 * a node X in the tree is named good if in the path from root to X there are no nodes with a value greater than X.
 * Return the number of good nodes in the binary tree.
 * <p>
 * Constraints: the number of nodes in the binary tree is in the range [1, 10^5], each node's value is between [-10^4, 10^4].
 *
 * @see <a href="https://leetcode.com/problems/count-good-nodes-in-binary-tree">1448. Count Good Nodes in Binary Tree/a>
 */
public class L46_CountGoodNodesInBinaryTree {

    @SuppressWarnings("ClassEscapesDefinedScope")
    public int goodNodes(TreeNode root) {
        return countGoodNodes(root);
    }

    private static int countGoodNodes(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int res = 0;
        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.add(root);
        Map<TreeNode, Integer> maxInPath = new HashMap<>();
        maxInPath.put(root, Integer.MIN_VALUE);
        while (!stack.isEmpty()) {
            TreeNode current = stack.removeFirst();
            Integer max = maxInPath.get(current);
            if (max <= current.val) {
                // good
                res++;
            }
            if (current.right != null) {
                stack.addFirst(current.right);
                maxInPath.put(current.right, Math.max(max, current.val));
            }
            if (current.left != null) {
                stack.addFirst(current.left);
                maxInPath.put(current.left, Math.max(max, current.val));
            }
        }
        return res;
    }

    @Test
    void test1() {
        TreeNode root = TreeNode.of(3)
                .left(
                        TreeNode.of(1)
                                .left(3))
                .right(
                        TreeNode.of(4)
                                .left(1)
                                .right(5)
                );
        int actual = goodNodes(root);
        Assertions.assertEquals(4, actual);
    }

    @Test
    void test2() {
        TreeNode root = TreeNode.of(3)
                .left(
                        TreeNode.of(3)
                                .left(4)
                                .right(2)
                );
        int actual = goodNodes(root);
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test3() {
        TreeNode root = TreeNode.of(9).right(TreeNode.of(3).left(6));
        int actual = goodNodes(root);
        Assertions.assertEquals(1, actual);
    }

    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }
    }
}
