package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

/**
 * There are `n` cities.
 * Some of them are connected, while some are not.
 * If city `a` is connected directly with city `b`,
 * and city `b` is connected directly with city ~, then city `a` is connected indirectly with city `c`.
 * A province is a group of directly or indirectly connected cities and no other cities outside of the group.
 * You are given an `n x n` matrix `isConnected` where `isConnected[i][j] = 1`
 * if the `i`th city and the `j`th city are directly connected, and `isConnected[i][j] = 0` otherwise.
 * Return the total number of provinces.
 * <p>
 * Constraints:
 * `1 <= n <= 200`;
 * `n == isConnected.length`;
 * `n == isConnected[i].length`;
 * `isConnected[i][j] is 1 or 0`;
 * `isConnected[i][i] == 1`;
 * `isConnected[i][j] == isConnected[j][i]`
 *
 * @see <a href="https://leetcode.com/problems/number-of-provinces/">547. Number of Provinces</a>
 */
public class L48_NumberOfProvinces {

    public int findCircleNum(int[][] isConnected) {
        return calcCircleNum(isConnected);
    }

    private static int calcCircleNum(int[][] isConnected) {
        Set<Integer> roots = new HashSet<>();
        for (int i = 0; i < isConnected.length; i++) {
            roots.add(i);
        }
        for (int i = 0; i < isConnected.length; i++) {
            if (!roots.contains(i)) {
                continue;
            }
            Deque<Integer> queue = new ArrayDeque<>();
            queue.add(i);
            Set<Integer> seen = new HashSet<>();
            while (!queue.isEmpty()) {
                int j = queue.removeFirst();
                if (!seen.add(j)) {
                    continue;
                }
                for (int k = 0; k < isConnected[j].length; k++) {
                    if (seen.contains(k)) {
                        continue;
                    }
                    if (k != j && (isConnected[j][k] == 1 || isConnected[k][j] == 1)) {
                        // connected
                        roots.remove(k);
                        queue.add(k);
                    }
                }
            }
        }
        return roots.size();
    }

    @Test
    void test1() {
        int res = findCircleNum(new int[][]{{1, 1, 0}, {1, 1, 0}, {0, 0, 1}});
        Assertions.assertEquals(2, res);
    }

    @Test
    void test2() {
        int res = findCircleNum(new int[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}});
        Assertions.assertEquals(3, res);
    }

    @Test
    void test3() {
        int res = findCircleNum(new int[][]{
                {1, 0, 0, 0, 0, 0},
                {0, 1, 0, 1, 0, 0},
                {0, 0, 1, 0, 1, 1},
                {0, 1, 0, 1, 0, 0},
                {0, 0, 1, 0, 1, 1},
                {0, 0, 1, 0, 1, 1},
        });
        Assertions.assertEquals(3, res);
    }

}
