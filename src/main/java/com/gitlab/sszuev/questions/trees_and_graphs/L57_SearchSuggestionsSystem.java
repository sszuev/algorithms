package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * You are given an array of strings `products` and a string `searchWord`.
 * Design a system that suggests at most three product names from `products` after each character of `searchWord` is typed.
 * Suggested products should have common prefix with `searchWord`.
 * If there are more than three products with a common prefix return the three lexicographically minimums products.
 * Return a list of lists of the suggested `products` after each character of `searchWord` is typed.
 * <p>
 * Constraints:
 * 1) `1 <= products.length <= 1000`
 * 2) `1 <= products[i].length <= 3000`
 * 3) `1 <= sum(products[i].length) <= 2 * 10^4`
 * 4) All the strings of products are unique
 * 5) `products[i]` consists of lowercase English letters
 * 6) `1 <= searchWord.length <= 1000`
 * 7) `searchWord` consists of lowercase English letters
 *
 * @see <a href="https://leetcode.com/problems/search-suggestions-system">1268. Search Suggestions System</a>
 */
public class L57_SearchSuggestionsSystem {

    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        return selectSuggestedProducts(products, searchWord);
    }

    private static List<List<String>> selectSuggestedProducts(String[] products, String searchWord) {
        char[] chars = searchWord.toCharArray();
        StringBuilder sb = new StringBuilder();
        Map<String, Set<String>> res = new LinkedHashMap<>();
        for (char ch : chars) {
            sb.append(ch);
            res.put(sb.toString(), new TreeSet<>());
        }
        for (String product : products) {
            res.forEach((key, strings) -> {
                if (product.startsWith(key)) {
                    strings.add(product);
                }
            });
        }
        List<List<String>> suggestedProducts = new ArrayList<>();
        res.forEach((s, strings) -> suggestedProducts.add(strings.stream().limit(3).collect(Collectors.toList())));
        return suggestedProducts;
    }

    @Test
    void test1() {
        List<List<String>> res = suggestedProducts(new String[]{"mobile", "mouse", "moneypot", "monitor", "mousepad"}, "mouse");
        // [["mobile","moneypot","monitor"],["mobile","moneypot","monitor"],["mouse","mousepad"],["mouse","mousepad"],["mouse","mousepad"]]
        Assertions.assertEquals(
                List.of(
                        List.of("mobile", "moneypot", "monitor"),
                        List.of("mobile", "moneypot", "monitor"),
                        List.of("mouse", "mousepad"),
                        List.of("mouse", "mousepad"),
                        List.of("mouse", "mousepad")
                ),
                res
        );
    }

    @Test
    void test2() {
        List<List<String>> res = suggestedProducts(new String[]{"havana"}, "havana");
        // [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
        Assertions.assertEquals(
                List.of(
                        List.of("havana"),
                        List.of("havana"),
                        List.of("havana"),
                        List.of("havana"),
                        List.of("havana"),
                        List.of("havana")
                ),
                res
        );
    }
}
