package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.
 * According to the definition of LCA on Wikipedia:
 * “The lowest common ancestor is defined between two nodes p and q as the lowest node in T
 * that has both p and q as descendants (where we allow a node to be a descendant of itself).”
 * <p>
 * Constraints:
 * 1) The number of nodes in the tree is in the range [2, 10^5].
 * 2) -10^9 <= Node.val <= 10^9
 * 3) All Node.val are unique.
 * 4) p != q
 * 5) p and q will exist in the tree.
 *
 * @see <a href="https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/">236. Lowest Common Ancestor of a Binary Tree</a>
 */
public class L61_LowestCommonAncestorOfBinaryTree {

    @SuppressWarnings("ClassEscapesDefinedScope")
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        return findLCA(root, p, q);
    }

    private static TreeNode findLCA(TreeNode root, TreeNode p, TreeNode q) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        Map<TreeNode, TreeNode> parents = new HashMap<>();
        stack.add(root);
        parents.put(root, null);
        boolean foundQ = false;
        boolean foundP = false;
        while (!stack.isEmpty()) {
            TreeNode current = stack.removeFirst();
            if (current.equals(q)) {
                foundQ = true;
            }
            if (current.equals(p)) {
                foundP = true;
            }
            if (foundQ && foundP) {
                break;
            }
            if (current.left != null) {
                parents.put(current.left, current);
                stack.addFirst(current.left);
            }
            if (current.right != null) {
                parents.put(current.right, current);
                stack.addFirst(current.right);
            }
        }
        if (!foundQ || !foundP) {
            throw new IllegalStateException("Can't find q or p");
        }
        Set<TreeNode> pathQ = new LinkedHashSet<>();
        TreeNode currentForQ = q;
        while (currentForQ != null) {
            pathQ.add(currentForQ);
            currentForQ = parents.get(currentForQ);
        }
        TreeNode currentForP = p;
        while (currentForP != null) {
            if (pathQ.contains(currentForP)) {
                return currentForP;
            }
            currentForP = parents.get(currentForP);
        }
        throw new IllegalStateException("Can't find solution");
    }

    @Test
    void test1() {
        TreeNode p = TreeNode.of(5);
        TreeNode q = TreeNode.of(1);
        TreeNode root = TreeNode.of(3)
                .left(
                        p
                                .left(TreeNode.of(6))
                                .right(TreeNode.of(2)
                                        .left(7)
                                        .right(4))
                )
                .right(
                        q
                                .left(0)
                                .right(8)
                );

        TreeNode actual = lowestCommonAncestor(root, p, q);
        Assertions.assertEquals(3, actual.val);
    }

    @Test
    void test2() {
        TreeNode p = TreeNode.of(5);
        TreeNode q = TreeNode.of(4);
        TreeNode root = TreeNode.of(3)
                .left(
                        p
                                .left(TreeNode.of(6))
                                .right(TreeNode.of(2)
                                        .left(7)
                                        .right(q))
                )
                .right(
                        TreeNode.of(1)
                                .left(0)
                                .right(8)
                );
        TreeNode actual = lowestCommonAncestor(root, p, q);
        Assertions.assertEquals(5, actual.val);
    }

    @Test
    void test3() {
        TreeNode p = TreeNode.of(1);
        TreeNode q = TreeNode.of(2);
        @SuppressWarnings("UnnecessaryLocalVariable") TreeNode root = p;
        root.left(q);
        TreeNode actual = lowestCommonAncestor(root, p, q);
        Assertions.assertEquals(1, actual.val);
    }

    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }
    }
}
