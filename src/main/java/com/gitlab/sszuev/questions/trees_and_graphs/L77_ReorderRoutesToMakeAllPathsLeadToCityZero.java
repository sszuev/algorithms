package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * There are `n` cities numbered from `0` to `n - 1` and `n - 1` roads such that there is only one way to
 * travel between two different cities (this network form a tree).
 * Last year, The ministry of transport decided to orient the roads in one direction because they are too narrow.
 * Roads are represented by `connections` where `connections[i] = [ai, bi]` represents a road from city `ai` to city `bi`.
 * This year, there will be a big event in the capital (city `0`), and many people want to travel to this city.
 * Your task consists of reorienting some roads such that each city can visit the city `0`.
 * Return the minimum number of edges changed.
 * It's guaranteed that each city can reach city `0` after reorder.
 * <p>
 * Constraints:
 * - `2 <= n <= 5 * 10^4`
 * `connections.length == n - 1`
 * `connections[i].length == 2`
 * `0 <= ai, bi <= n - 1`
 * `ai != bi`
 *
 * @see <a href="https://leetcode.com/problems/reorder-routes-to-make-all-paths-lead-to-the-city-zero">1466. Reorder Routes to Make All Paths Lead to the City Zero</a>
 */
public class L77_ReorderRoutesToMakeAllPathsLeadToCityZero {
    public int minReorder(int n, int[][] connections) {
        return calcMinReorder(connections);
    }

    private static int calcMinReorder(int[][] connections) {
        Map<Integer, List<int[]>> cities = new HashMap<>();
        for (int[] connection : connections) {
            cities.computeIfAbsent(connection[0], x -> new ArrayList<>()).add(connection);
            cities.computeIfAbsent(connection[1], x -> new ArrayList<>()).add(connection);
        }
        AtomicInteger res = new AtomicInteger(0);
        visit(0, cities, new HashSet<>(), res);
        return res.intValue();
    }

    private static void visit(int city, Map<Integer, List<int[]>> cities, Set<int[]> seen, AtomicInteger res) {
        List<int[]> roads = Objects.requireNonNull(cities.get(city));
        Set<Integer> neighbors = new HashSet<>();
        for (int[] road : roads) {
            if (!seen.add(road)) {
                continue;
            }
            if (road[1] != city) {
                res.incrementAndGet();
                neighbors.add(road[1]);
            } else {
                neighbors.add(road[0]);
            }
            seen.add(road);
        }
        for (int neighbor : neighbors) {
            visit(neighbor, cities, seen, res);
        }
    }

    @Test
    void test1() {
        // Input: n = 6, connections = [[0,1],[1,3],[2,3],[4,0],[4,5]]
        // Output: 3
        int actual = minReorder(6, new int[][]{{0, 1}, {1, 3}, {2, 3}, {4, 0}, {4, 5}});
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test2() {
        // Input: n = 5, connections = [[1,0],[1,2],[3,2],[3,4]]
        // Output: 2
        int actual = minReorder(5, new int[][]{{1, 0}, {1, 2}, {3, 2}, {3, 4}});
        Assertions.assertEquals(2, actual);
    }
}
