package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Given the `root` of a binary tree and an integer `targetSum`,
 * return the number of paths where the sum of the values along the path equals `targetSum`.
 * The path does not need to start or end at the root or a leaf,
 * but it must go downwards (i.e., traveling only from parent nodes to child nodes).
 * <p>
 * Constraints:
 * - The number of nodes in the tree is in the range `[0, 1000]`.
 * - `-10^9 <= Node.val <= 10^9`
 * - `-1000 <= targetSum <= 1000`
 *
 * @see <a href="https://leetcode.com/problems/path-sum-iii">437. Path Sum III</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L79_PathSumIII {

    public int pathSum(TreeNode root, int targetSum) {
        if (root == null) return 0;
        return calcPathSum(root, targetSum);
    }

    private static int calcPathSum(TreeNode root, int targetSum) {
        Map<TreeNode, List<TreeNode>> paths = new HashMap<>();
        paths.computeIfAbsent(root, t -> new ArrayList<>()).add(root);
        AtomicInteger res = new AtomicInteger(0);
        if (root.val == targetSum) {
            res.incrementAndGet();
        }
        traverse(root, paths, targetSum, res);
        return res.intValue();
    }

    private static void traverse(TreeNode node, Map<TreeNode, List<TreeNode>> paths, int targetSum, AtomicInteger res) {
        List<TreeNode> rootPath = paths.get(node);
        if (node.left != null) {
            List<TreeNode> leftPath = new ArrayList<>(rootPath);
            leftPath.add(node.left);
            paths.put(node.left, leftPath);
            isPathSum(leftPath, targetSum, res);
        }
        if (node.right != null) {
            List<TreeNode> rightPath = new ArrayList<>(rootPath);
            rightPath.add(node.right);
            paths.put(node.right, rightPath);
            isPathSum(rightPath, targetSum, res);
        }
        if (node.left != null) {
            traverse(node.left, paths, targetSum, res);
        }
        if (node.right != null) {
            traverse(node.right, paths, targetSum, res);
        }
    }

    private static void isPathSum(List<TreeNode> path, int targetSum, AtomicInteger res) {
        long sum = 0;
        for (int i = path.size() - 1; i >= 0; i--) {
            sum += path.get(i).val;
            if (sum == targetSum) {
                res.incrementAndGet();
            }
        }
    }

    @Test
    void test1() {
        TreeNode root = TreeNode.of(10)
                .left(TreeNode.of(5)
                        .left(TreeNode.of(3)
                                .left(3)
                                .right(-2))
                        .right(TreeNode.of(2)
                                .right(1))
                )
                .right(TreeNode.of(-3)
                        .right(11));
        var actual = pathSum(root, 8);
        Assertions.assertEquals(3, actual);
    }

    @Test
    void test2() {
        var root = TreeNode.of(1);
        Assertions.assertEquals(1, pathSum(root, 1));
    }

    @Test
    void test3() {
        var root = TreeNode.of(0)
                .right(1)
                .left(1);
        Assertions.assertEquals(4, pathSum(root, 1));
    }

    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }

        @Override
        public String toString() {
            return "TreeNode{" +
                    "val=" + val +
                    '}';
        }
    }
}
