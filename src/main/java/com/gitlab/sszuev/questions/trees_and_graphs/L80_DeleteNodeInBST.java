package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;

/**
 * Given a root node reference of a BST and a key,
 * delete the node with the given key in the BST.
 * Return the root node reference (possibly updated) of the BST.
 * Basically, the deletion can be divided into two stages:
 * 1) Search for a node to remove.
 * 2) If the node is found, delete the node.
 *
 * @see <a href="https://leetcode.com/problems/delete-node-in-a-bst/">450. Delete Node in a BST</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L80_DeleteNodeInBST {

    public TreeNode deleteNode(TreeNode root, int key) {
        return doDeleteNode(root, key);
    }

    private TreeNode doDeleteNode(TreeNode root, int val) {
        if (root == null) {
            return null;
        }
        TreeNode found = root;
        TreeNode parent = null;
        boolean isLeft = false;
        while (found.val != val) {
            parent = found;
            if (found.val > val) {
                isLeft = true;
                found = found.left;
            } else {
                isLeft = false;
                found = found.right;
            }
            if (found == null) {
                return root;
            }
        }
        // 1. no children
        if (found.left == null && found.right == null) {
            if (parent == null) {
                // attempt to delete root with no children
                return null;
            }
            if (isLeft) {
                parent.left = null;
            } else {
                parent.right = null;
            }
            return root;
        }
        // 2. single child
        if (found.left == null || found.right == null) {
            TreeNode node = found.left != null ? found.left : found.right;
            if (parent == null) {
                return node;
            }
            if (isLeft) {
                parent.left = node;
            } else {
                parent.right = node;
            }
            return root;
        }
        // 3. both left and right
        TreeNode successor = found.right;
        TreeNode successorParent = found;
        while (successor.left != null) {
            successorParent = successor;
            successor = successor.left;
        }
        doDeleteNode(successorParent, successor.val);
        found.val = successor.val;
        return root;
    }

    @Test
    void test1() {
        // [5|[3|[2|null|null]|[4|null|null]]|[6|null|[7|null|null]]]
        TreeNode root = TreeNode.of(5)
                .left(TreeNode.of(3)
                        .left(2)
                        .right(4))
                .right(TreeNode.of(6)
                        .right(7));

        TreeNode expected = TreeNode.of(5)
                .left(TreeNode.of(4)
                        .left(2))
                .right(TreeNode.of(6)
                        .right(7));

        TreeNode actual = deleteNode(root, 3);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test2() {
        // [5|[3|[2|null|null]|[4|null|null]]|[6|null|[7|null|null]]]
        TreeNode root = TreeNode.of(5)
                .left(TreeNode.of(3)
                        .left(2)
                        .right(4))
                .right(TreeNode.of(6)
                        .right(7));

        TreeNode actual = deleteNode(root, 0);
        String expected = "[5|[3|[2|null|null]|[4|null|null]]|[6|null|[7|null|null]]]";
        Assertions.assertEquals(expected, actual.toString());
    }

    @Test
    void test3() {
        // [5|[3|[2|null|null]|[4|null|null]]|[6|null|[7|null|null]]]
        TreeNode root = TreeNode.of(5)
                .left(TreeNode.of(3)
                        .left(2)
                        .right(4))
                .right(TreeNode.of(6)
                        .right(7));

        TreeNode expected = TreeNode.of(6)
                .left(TreeNode.of(3)
                        .left(2)
                        .right(4))
                .right(7);

        TreeNode actual = deleteNode(root, 5);
        Assertions.assertEquals(expected, actual);
    }

    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }

        @Override
        public String toString() {
            return "[" + val + "|" + left + "|" + right + "]";
        }

        @Override
        public final boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof TreeNode)) return false;

            TreeNode treeNode = (TreeNode) o;
            return val == treeNode.val && Objects.equals(left, treeNode.left) && Objects.equals(right, treeNode.right);
        }

        @Override
        public int hashCode() {
            int result = val;
            result = 31 * result + Objects.hashCode(left);
            result = 31 * result + Objects.hashCode(right);
            return result;
        }
    }
}
