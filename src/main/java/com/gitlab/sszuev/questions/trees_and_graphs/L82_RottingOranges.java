package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * You are given an `m x n` grid where each cell can have one of three values:
 * - 0 representing an empty cell,
 * - 1 representing a fresh orange, or
 * - 2 representing a rotten orange.
 * Every minute, any fresh orange that is 4-directionally adjacent to a rotten orange becomes rotten.
 * Return the minimum number of minutes that must elapse until no cell has a fresh orange.
 * If this is impossible, return -1.
 * <p>
 * Constraints:
 * - `m == grid.length`
 * - `n == grid[i].length`
 * - `1 <= m, n <= 10`
 * - `grid[i][j] is 0, 1, or 2`
 *
 * @see <a href="https://leetcode.com/problems/rotting-oranges">994. Rotting Oranges</a>
 */
public class L82_RottingOranges {

    public int orangesRotting(int[][] grid) {
        return calcOrangesRotting(grid);
    }

    private static int calcOrangesRotting(int[][] grid) {
        List<Point> initRotten = new ArrayList<>();
        Set<Point> allFresh = new HashSet<>();
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 2) {
                    initRotten.add(get(grid, i, j));
                } else if (grid[i][j] == 1) {
                    allFresh.add(get(grid, i, j));
                }
            }
        }
        if (allFresh.isEmpty()) {
            return 0;
        }
        if (initRotten.isEmpty()) {
            return -1;
        }
        Queue<List<Point>> queue = new LinkedList<>();
        queue.add(initRotten);
        Set<Point> seen = new HashSet<>();
        int res = -1;
        while (!queue.isEmpty()) {
            res++;
            List<Point> thisRotten = queue.remove();
            List<Point> nextRotten = new ArrayList<>();
            for (Point rotten : thisRotten) {
                if (!seen.add(rotten)) {
                    continue;
                }
                findNextPoints(grid, rotten).forEach(point -> {
                    if (point.value() == 1) {
                        point.value(2);
                        allFresh.remove(point);
                        nextRotten.add(point);
                    }
                });
            }
            if (!nextRotten.isEmpty()) {
                queue.add(nextRotten);
            }
        }
        if (!allFresh.isEmpty()) {
            return -1;
        }
        return res;
    }

    private static List<Point> findNextPoints(int[][] grid, Point current) {
        Point p1 = get(grid, current.i - 1, current.j);
        Point p2 = get(grid, current.i + 1, current.j);
        Point p3 = get(grid, current.i, current.j - 1);
        Point p4 = get(grid, current.i, current.j + 1);
        List<Point> points = new ArrayList<>();
        if (p1 != null) {
            points.add(p1);
        }
        if (p2 != null) {
            points.add(p2);
        }
        if (p3 != null) {
            points.add(p3);
        }
        if (p4 != null) {
            points.add(p4);
        }
        return points;
    }

    private static Point get(int[][] grid, int i, int j) {
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length) {
            return null;
        }
        return new Point(i, j, grid);
    }

    private static class Point {
        private final int i;
        private final int j;
        private final int[][] grid;

        private Point(int i, int j, int[][] grid) {
            this.i = i;
            this.j = j;
            this.grid = grid;
        }

        int value() {
            return grid[i][j];
        }

        void value(int value) {
            grid[i][j] = value;
        }

        @Override
        public final boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Point)) return false;

            Point point = (Point) o;
            return i == point.i && j == point.j;
        }

        @Override
        public int hashCode() {
            int result = i;
            result = 31 * result + j;
            return result;
        }

        @Override
        public String toString() {
            return "Point{" +
                    "i=" + i +
                    ", j=" + j +
                    ", value=" + value() +
                    "}";
        }
    }

    @Test
    void test1() {
        // Input: grid = [[2,1,1],[1,1,0],[0,1,1]]
        // Output: 4
        int actual = orangesRotting(new int[][]{{2, 1, 1}, {1, 1, 0}, {0, 1, 1}});
        Assertions.assertEquals(4, actual);
    }

    @Test
    void test2() {
        // Input: grid = [[2,1,1],[0,1,1],[1,0,1]]
        // Output: -1
        int actual = orangesRotting(new int[][]{{2, 1, 1}, {0, 1, 1}, {1, 0, 1}});
        Assertions.assertEquals(-1, actual);
    }

    @Test
    void test3() {
        // Input: grid = [[0,2]]
        // Output: 0
        int actual = orangesRotting(new int[][]{{0, 2}});
        Assertions.assertEquals(0, actual);
    }
}
