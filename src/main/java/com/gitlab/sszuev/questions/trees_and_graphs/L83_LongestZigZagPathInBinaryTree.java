package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * You are given the root of a binary tree.
 * A ZigZag path for a binary tree is defined as follow:
 * - Choose any node in the binary tree and a direction (right or left).
 * - If the current direction is right, move to the right child of the current node; otherwise, move to the left child.
 * - Change the direction from right to left or from left to right.
 * - Repeat the second and third steps until you can't move in the tree.
 * Zigzag length is defined as the number of nodes visited - 1. (A single node has a length of 0).
 * Return the longest ZigZag path contained in that tree.
 * <p>
 * Constraints:
 * - The number of nodes in the tree is in the range `[1, 5 * 10^4]`.
 * - `1 <= Node.val <= 100`
 *
 * @see <a href="https://leetcode.com/problems/longest-zigzag-path-in-a-binary-tree">1372. Longest ZigZag Path in a Binary Tree</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L83_LongestZigZagPathInBinaryTree {

    public int longestZigZag(TreeNode root) {
        return calcLongestZigZag2(root);
    }
    private static int calcLongestZigZag2(TreeNode root) {
        AtomicInteger longestZigZag = new AtomicInteger(0);
        maxZigZag(root, 0, true, longestZigZag);
        return longestZigZag.intValue();
    }

    private static void maxZigZag(TreeNode node, int a, boolean direction, AtomicInteger res) {
        if (node == null) {
            return;
        }
        res.set(Math.max(a, res.get()));
        if (direction) {
            maxZigZag(node.left, a + 1, false, res);
            maxZigZag(node.right, 1, true, res);
        } else {
            maxZigZag(node.right, a + 1, true, res);
            maxZigZag(node.left, 1, false, res);
        }
    }

    private static int calcLongestZigZag1(TreeNode root) {
        Queue<TreeNode> nodes = new ArrayDeque<>();
        nodes.add(root);
        int res = 0;
        Map<Key, Integer> cache = new HashMap<>();
        while (!nodes.isEmpty()) {
            TreeNode current = nodes.remove();
            res = Math.max(res, calcZigZag(current, true, cache));
            res = Math.max(res, calcZigZag(current, false, cache));
            if (current.left != null) {
                nodes.add(current.left);
            }
            if (current.right != null) {
                nodes.add(current.right);
            }
        }
        return res;
    }

    private static int calcZigZag(TreeNode node, boolean direction, Map<Key, Integer> cache) {
        Integer res = cache.get(new Key(node, direction));
        if (res != null) {
            return res;
        }
        res = 0;
        if (direction) {
            if (node.right != null) {
                res += 1;
                Integer path = cache.get(new Key(node.right, false));
                if (path == null) {
                    path = calcZigZag(node.right, false, cache);
                }
                res += path;
            }
        } else {
            if (node.left != null) {
                res += 1;
                Integer path = cache.get(new Key(node.left, true));
                if (path == null) {
                    path = calcZigZag(node.left, true, cache);
                }
                res += path;
            }
        }
        return res;
    }

    private static class Key {
        @Override
        public final boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Key)) return false;

            Key key = (Key) o;
            return direction == key.direction && node.equals(key.node);
        }

        @Override
        public int hashCode() {
            int result = node.hashCode();
            result = 31 * result + Boolean.hashCode(direction);
            return result;
        }

        private final TreeNode node;
        private final boolean direction;

        private Key(TreeNode node, boolean direction) {
            this.node = node;
            this.direction = direction;
        }
    }

    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }

        @Override
        public String toString() {
            return "TreeNode{" +
                    "val=" + val +
                    '}';
        }
    }

    @Test
    void test1() {
        TreeNode root = TreeNode.of(1)
                .right(TreeNode.of(1)
                        .right(TreeNode.of(1)
                                .left(TreeNode.of(1)
                                        .right(TreeNode.of(1)
                                                .right(1)
                                        )

                                )
                                .right(1)
                        )
                        .left(1)
                );
        Assertions.assertEquals(3, longestZigZag(root));
    }

    @Test
    void test2() {
        TreeNode root = TreeNode.of(1)
                .right(1)
                .left(TreeNode.of(1)
                        .right(TreeNode.of(1)
                                .right(1)
                                .left(TreeNode.of(1)
                                        .right(1))
                        )
                );
        Assertions.assertEquals(4, longestZigZag(root));
    }
}
