package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * Given the root of a binary tree, the level of its root is 1, the level of its children is 2, and so on.
 * Return the smallest level x such that the sum of all the values of nodes at level x is maximal.
 * <p>
 * Constraints:
 * - The number of nodes in the tree is in the range `[1, 10^4]`.
 * - `-10^5 <= Node.val <= 10^5`
 *
 * @see <a href="https://leetcode.com/problems/maximum-level-sum-of-a-binary-tree">1161. Maximum Level Sum of a Binary Tree</a>
 */
@SuppressWarnings("ClassEscapesDefinedScope")
public class L84_MaximumLevelSumOfBinaryTree {

    public int maxLevelSum(TreeNode root) {
        return calcMaxSum(root);
    }

    static int calcMaxSum(TreeNode root) {
        if (root == null) return 0;
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        Map<TreeNode, List<TreeNode>> paths = new HashMap<>();
        paths.put(root, List.of());
        int level = 1;
        int levelSum = 0;
        List<Integer> sums = new ArrayList<>();
        while (!queue.isEmpty()) {
            TreeNode node = queue.remove();
            List<TreeNode> list = paths.get(node);
            if (list.size() >= level) {
                // next level
                sums.add(levelSum);
                levelSum = 0;
                level++;
            }
            levelSum += node.val;

            if (node.left != null) {
                List<TreeNode> p = paths.computeIfAbsent(node.left, it -> new ArrayList<>());
                p.add(node);
                p.addAll(list);
                queue.add(node.left);
            }
            if (node.right != null) {
                List<TreeNode> p = paths.computeIfAbsent(node.right, it -> new ArrayList<>());
                p.add(node);
                p.addAll(list);
                queue.add(node.right);
            }
        }
        sums.add(levelSum);
        int _level = 1;
        int max = Integer.MIN_VALUE;
        int foundMaxLevel = 0;
        for (int sum : sums) {
            if (sum > max) {
                foundMaxLevel = _level;
                max = sum;
            }
            _level++;
        }
        return foundMaxLevel;
    }

    private static class TreeNode {
        final int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        static TreeNode of(int val) {
            return new TreeNode(val);
        }

        TreeNode left(int left) {
            return left(new TreeNode(left));
        }

        TreeNode right(int right) {
            return right(new TreeNode(right));
        }

        TreeNode left(TreeNode left) {
            this.left = left;
            return this;
        }

        TreeNode right(TreeNode right) {
            this.right = right;
            return this;
        }

        @Override
        public String toString() {
            return "TreeNode{" +
                    "val=" + val +
                    '}';
        }
    }

    @Test
    void test1() {
        TreeNode root = new TreeNode(1)
                .left(TreeNode.of(7)
                        .left(7)
                        .right(-8))
                .right(0);

        int actual = maxLevelSum(root);
        Assertions.assertEquals(2, actual);
    }

    @Test
    void test2() {
        TreeNode root = new TreeNode(989)
                .right(TreeNode.of(10250)
                        .left(98693)
                        .right(TreeNode.of(-89388)
                                .right(-32127)));
        int actual = maxLevelSum(root);
        Assertions.assertEquals(2, actual);
    }

    @Test
    void test3() {
        TreeNode root = new TreeNode(-100)
                .left(TreeNode.of(-200)
                        .left(-20)
                        .right(-5))
                .right(TreeNode.of(-300)
                        .left(-10));
        int actual = maxLevelSum(root);
        Assertions.assertEquals(3, actual);
    }
}
