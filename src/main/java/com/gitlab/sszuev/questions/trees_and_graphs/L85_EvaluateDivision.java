package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;

/**
 * You are given an array of variable pairs equations and an array of real numbers values,
 * where `equations[i] = [Ai, Bi]` and `values[i]` represent the equation `Ai / Bi = values[i]`.
 * Each `Ai` or `Bi` is a string that represents a single variable.
 * You are also given some `queries`, where `queries[j] = [Cj, Dj]` represents the `j`th query
 * where you must find the answer for `Cj / Dj = ?`.
 * Return the answers to all queries. If a single answer cannot be determined, return `-1.0`.
 * Note: The input is always valid.
 * You may assume that evaluating the queries will not result in division by zero and that there is no contradiction.
 * Note: The variables that do not occur in the list of equations are undefined,
 * so the answer cannot be determined for them.
 * <p>
 * Constraints:
 * - `1 <= equations.length <= 20`
 * - `equations[i].length == 2`
 * - `1 <= Ai.length, Bi.length <= 5`
 * - `values.length == equations.length`
 * - `0.0 < values[i] <= 20.0`
 * - `1 <= queries.length <= 20`
 * - `queries[i].length == 2`
 * - `1 <= Cj.length, Dj.length <= 5`
 * - `Ai, Bi, Cj, Dj` consist of lower case English letters and digits.
 *
 * @see <a href="https://leetcode.com/problems/evaluate-division">399. Evaluate Division</a>
 */
public class L85_EvaluateDivision {
    public double[] calcEquation(List<List<String>> equations, double[] values, List<List<String>> queries) {
        return doCalcEquation(equations, values, queries);
    }

    private static double[] doCalcEquation(List<List<String>> equations, double[] values, List<List<String>> queries) {
        Map<String, Map<String, Double>> graph = new HashMap<>();
        for (int i = 0; i < equations.size(); i++) {
            List<String> equation = equations.get(i);
            double value = values[i];
            String a = equation.get(0);
            String b = equation.get(1);
            graph.computeIfAbsent(a, k -> new HashMap<>()).put(b, value);
            graph.computeIfAbsent(b, k -> new HashMap<>()).put(a, 1 / value);
        }

        for (List<String> query : queries) {
            String a = query.get(0);
            String b = query.get(1);
            Queue<String> queue = new ArrayDeque<>();
            queue.add(a);
            Set<String> seen = new HashSet<>();
            while (!queue.isEmpty()) {
                String f = queue.remove();
                if (!seen.add(f)) {
                    continue;
                }
                Map<String, Double> dst = graph.get(f);
                if (dst == null) {
                    continue;
                }
                Map<String, Double> newNodes = new HashMap<>();
                dst.forEach((node, d1) -> {
                    Map<String, Double> children = Objects.requireNonNull(graph.get(node));
                    children.forEach((ch, d2) -> newNodes.put(ch, d1 * d2));
                    queue.add(node);
                });
                dst.putAll(newNodes);
                newNodes.forEach((s, d) -> graph.computeIfAbsent(s, s1 -> new HashMap<>()).put(f, 1 / d));
            }
        }

        double[] res = new double[queries.size()];
        for (int i = 0; i < queries.size(); i++) {
            List<String> query = queries.get(i);
            String a = query.get(0);
            String b = query.get(1);
            Map<String, Double> found = graph.get(a);
            if (found == null) {
                res[i] = -1;
                continue;
            }
            Double r = found.get(b);
            if (r == null) {
                res[i] = -1;
                continue;
            }
            res[i] = r;
        }
        return res;
    }

    @Test
    void test1() {
        // Input: equations = [["a","b"],["b","c"]], values = [2.0,3.0], queries = [["a","c"],["b","a"],["a","e"],["a","a"],["x","x"]]
        // Output: [6.00000,0.50000,-1.00000,1.00000,-1.00000]

        double[] actual = calcEquation(
                List.of(List.of("a", "b"), List.of("b", "c")),
                new double[]{2, 3},
                List.of(List.of("a", "c"), List.of("b", "a"), List.of("a", "e"), List.of("a", "a"), List.of("x", "x"))
        );
        Assertions.assertArrayEquals(new double[]{6.00000, 0.50000, -1.00000, 1.00000, -1.00000}, actual, 0.00001);
    }

    @Test
    void test2() {
        // Input: equations = [["a","b"],["b","c"],["bc","cd"]], values = [1.5,2.5,5.0],
        // queries = [["a","c"],["c","b"],["bc","cd"],["cd","bc"]]
        // Output: [3.75000,0.40000,5.00000,0.20000]
        double[] actual = calcEquation(
                List.of(List.of("a", "b"), List.of("b", "c"), List.of("bc", "cd")),
                new double[]{1.5, 2.5, 5.0},
                List.of(List.of("a", "c"), List.of("c", "b"), List.of("bc", "cd"), List.of("cd", "bc"))
        );
        Assertions.assertArrayEquals(new double[]{3.75000, 0.40000, 5.00000, 0.20000}, actual, 0.00001);
    }

    @Test
    void test3() {
        // Input: equations = [["a","b"]], values = [0.5], queries = [["a","b"],["b","a"],["a","c"],["x","y"]]
        // Output: [0.50000,2.00000,-1.00000,-1.00000]
        double[] actual = calcEquation(
                List.of(List.of("a", "b")),
                new double[]{0.5},
                List.of(List.of("a", "b"), List.of("b", "a"), List.of("a", "c"), List.of("x", "y"))
        );
        Assertions.assertArrayEquals(new double[]{0.50000, 2.00000, -1.00000, -1.00000}, actual, 0.00001);
    }

    @Test
    void test4() {
        double[] actual = calcEquation(
                // [["a","b"],["b","c"],["d","e"],["a","d"]]
                List.of(List.of("a", "b"), List.of("b", "c"), List.of("d", "e"), List.of("a", "d")),
                new double[]{1.0, 2.0, 3.0, 4.0},
                List.of(List.of("c", "e"))
        );
        Assertions.assertArrayEquals(new double[]{6.0}, actual, 0.00001);
    }
}
