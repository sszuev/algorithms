package com.gitlab.sszuev.questions.trees_and_graphs;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created by @ssz on 19.01.2021.
 */
public interface MyTreeNode<X> {
    MyTreeNode<?> EMPTY = new MyTreeNode<>() {
        @Override
        public Stream<MyTreeNode<Object>> children() {
            return Stream.empty();
        }

        @Override
        public Object getValue() {
            return null;
        }
    };

    @SuppressWarnings("unchecked")
    static <X> MyTreeNode<X> createEmpty() {
        return (MyTreeNode<X>) EMPTY;
    }

    Stream<MyTreeNode<X>> children();

    X getValue();

    default void preOrder(Consumer<MyTreeNode<X>> v) {
        v.accept(this);
        children().filter(Objects::nonNull).forEach(x -> x.preOrder(v));
    }

    default void postOrder(Consumer<MyTreeNode<X>> v) {
        children().filter(Objects::nonNull).forEach(x -> x.postOrder(v));
        v.accept(this);
    }

    default long size() {
        AtomicLong res = new AtomicLong();
        postOrder(xTreeNode -> res.incrementAndGet());
        return res.get();
    }
}
