package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Stream;

/**
 * <b>Q1:</b> Route Between Nodes:
 * Given a directed graph, design an algorithm to find out whether there is a route between two nodes.
 * <p>
 * Hints: #127
 * <ul>
 * <li>#127: Two well-known algorithms can do this. What are the tradeoffs between them?</li>
 * </ul>
 * Created by @ssz on 01.11.2019.
 */
public class Q1_FindRouteBetweenGraphNodes {

    static <X> boolean hasPath_V1_BF(Node<X> a, Node<X> b) { // BFS
        if (a == b) return true;
        LinkedList<Node<X>> queue1 = new LinkedList<>();
        LinkedList<Node<X>> queue2 = new LinkedList<>();
        Set<Node<X>> found1 = new HashSet<>();
        Set<Node<X>> found2 = new HashSet<>();
        queue1.add(a.getRoot()); // O(N)
        queue2.add(b.getRoot()); // O(N)
        while (!queue1.isEmpty() || !queue2.isEmpty()) { // O(N)
            Node<X> node1 = process(queue1);
            if (node1 != null) found1.add(node1);
            if (found2.contains(node1)) {
                return true;
            }
            Node<X> node2 = process(queue2);
            if (node2 != null) found2.add(node2);
            if (found1.contains(node2)) {
                return true;
            }
        }
        return false;
    }

    private static <X> Node<X> process(Queue<Node<X>> queue) {
        if (queue.isEmpty()) return null;
        Node<X> n = queue.poll();
        n.children().forEach(queue::add);
        return n;
    }

    static <X> boolean hasPath(Node<X> a, Node<X> b) {
        return hasPath_V1_BF(a, b);
    }

    @Test
    public void testTrue() {
        Node<Integer> root = Node.createRoot(1).addChild(2).addChild(3).add(4).add(5).add(6).addChild(7).addChild(8).getRoot();
        System.out.println(root);
        Node<Integer> node1 = root.descendants().skip(2).findAny().orElseThrow(AssertionError::new);
        System.out.println(node1);
        Node<Integer> node2 = root.descendants().skip(4).findAny().orElseThrow(AssertionError::new);
        System.out.println(node2);

        Assertions.assertTrue(hasPath(root, node1));
        Assertions.assertTrue(hasPath(node1, root));
        Assertions.assertTrue(hasPath(root, node2));
        Assertions.assertTrue(hasPath(node2, root));
        Assertions.assertTrue(hasPath(node2, node1));
        Assertions.assertTrue(hasPath(node1, node2));
    }

    @Test
    public void testFalse() {
        Node<Integer> root1 = Node.createRoot(1).addChild(2).addChild(3).add(1).add(6).addChild(7).addChild(8).getRoot();
        Node<Integer> root2 = Node.createRoot(2).add(4).addChild(4).add(2).addChild(5).addChild(8).getRoot();
        System.out.println(root1);
        System.out.println(root2);

        Node<Integer> node1 = root1.descendants().skip(2).findAny().orElseThrow(AssertionError::new);
        System.out.println(node1);
        Node<Integer> node2 = root2.descendants().skip(2).findAny().orElseThrow(AssertionError::new);
        System.out.println(node2);

        Assertions.assertFalse(hasPath(root1, node2));
        Assertions.assertFalse(hasPath(root2, node1));
        Assertions.assertFalse(hasPath(root2, root1));
        Assertions.assertFalse(hasPath(node2, node1));
    }

    static class Node<X> {
        final X id;
        final Node<X> parent;
        final List<Node<X>> children = new ArrayList<>();

        Node(X value, Node<X> parent) {
            this.id = value;
            this.parent = parent;
        }

        Stream<Node<X>> children() {
            return children.stream();
        }

        Stream<Node<X>> descendants() { // DFS
            return children().flatMap(x -> Stream.concat(Stream.of(x), x.descendants()));
        }

        Node<X> add(X value) {
            addChild(value);
            return this;
        }

        Node<X> addChild(X value) {
            Node<X> res = new Node<>(value, this);
            this.children.add(res);
            return res;
        }

        Node<X> getRoot() {
            if (parent == null)
                return this;
            return parent.getRoot();
        }

        @SuppressWarnings("SameParameterValue")
        static <X> Node<X> createRoot(X value) {
            return new Node<>(value, null);
        }

        @Override
        public String toString() {
            return String.format("Node%s=>%s", id, children);
        }
    }

}
