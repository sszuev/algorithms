package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * <b>Q4:</b> Minimal Tree: Given a sorted (increasing order) array with unique integer elements,
 * write an algorithm to create a binary search tree with minimal height.
 * <p>
 * Hints: #19, #73, #116
 * <ul>
 * <li>#19: A minimal binary tree has about the same number of nodes on the left of each node as on the right.
 * Let's focus on just the root for now.
 * How would you ensure that about the same number of nodes are on the left of the root as on the right? </li>
 * <li>#73: You could implement this by finding the "ideal" next element to add and repeatedly calling {@code insertValue}.
 * This will be a bit inefficient, as you would have to repeatedly traverse the tree.
 * Try recursion instead. Can you divide this problem into subproblems?</li>
 * <li>#116: Imagine we had a {@code createMinimalTree} method that returns a minimal tree for a given array
 * (but for some strange reason doesn't operate on the root of the tree).
 * Could you use this to operate on the root of the tree? Could you write the base case for the function?
 * Great! Then that's basically the entire function.</li>
 * </ul>
 * Created by @ssz on 24.06.2020.
 */
public class Q2_CreateMinimalHeightTreeFromSortedArray {

    private static final Random R = new Random();

    @Test
    public void testFromRandomArray() {
        testFromArray(generateArray(330, 224));
    }

    @Test
    public void testFromGivenArray1() {
        testFromArray(new int[]{0, 4, 16, 22, 28, 40, 52, 58, 77, 77, 77, 80, 81, 82, 96, 101, 101, 106, 107, 108, 111});
    }

    @Test
    public void testFromGivenArray2() {
        testFromArray(new int[]{0, 4, 5, 6, 9, 9, 9, 10, 10, 11, 11, 12, 15, 15, 16, 16, 16, 17, 18, 20, 21, 26, 26, 26});
    }

    private void testFromArray(int[] array) {
        System.out.println(IntStream.rangeClosed(0, array.length).mapToObj(x -> "").collect(Collectors.joining("-")));
        System.out.println(Arrays.toString(array));
        TreeUtils.BiNode<Integer> res = fromArray(array);
        System.out.println(TreeUtils.print(res));
        Assertions.assertEquals(array.length, res.size());
        Assertions.assertTrue(TreeUtils.isBST(res));
        Assertions.assertTrue(TreeUtils.isIntBST(res));
        AtomicLong sum = new AtomicLong();
        res.preOrder(x -> sum.addAndGet(x.getValue()));
        Assertions.assertEquals(Arrays.stream(array).sum(), sum.intValue());
    }

    @SuppressWarnings("SameParameterValue")
    private static int[] generateArray(int max, int size) {
        return IntStream.generate(() -> R.nextInt(max)).limit(size).sorted().toArray();
    }

    private static TreeUtils.BiNode<Integer> fromArray(int[] array) {
        return fromArray(array, 0, array.length - 1);
    }

    private static TreeUtils.BiNode<Integer> fromArray(int[] array, int min, int max) {
        if (max == min) {
            return TreeUtils.of(array[min]);
        }
        if (max - min == 1) {
            return TreeUtils.of(array[max], TreeUtils.of(array[min]), null);
        }
        int i = (max + min) / 2;
        while (i < max && array[i] == array[i + 1]) {
            i++;
        }
        if (i >= max) {
            return TreeUtils.of(array[i], fromArray(array, min, i - 1), null);
        }
        return TreeUtils.of(array[i], fromArray(array, min, i - 1), fromArray(array, i + 1, max));
    }

}
