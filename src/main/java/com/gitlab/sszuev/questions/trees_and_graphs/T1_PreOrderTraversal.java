package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * <b>Pre order traversal Algorithms</b>
 * Created by @ssz on 29.11.2020.
 *
 * @see <a href='https://www.java67.com/2016/07/binary-tree-preorder-traversal-in-java-without-recursion.html'>Нow to implement Binary Tree PreOrder Traversal without Recursion</a>
 * @see <a href='https://javarevisited.blogspot.com/2016/07/binary-tree-preorder-traversal-in-java-using-recursion-iteration-example.html'>Binary Tree PreOrder Traversal - Recursion and Iteration Example</a>
 */
public class T1_PreOrderTraversal extends TraversalAlgorithms {

    public static <X> void preOrderRecursively(BinaryTreeTests.Node<X> node, Consumer<X> op) {
        if (node == null) return;
        op.accept(node.getValue());
        preOrderRecursively(node.left, op);
        preOrderRecursively(node.right, op);
    }

    public static <X> void preOrderIteratively(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        Deque<BinaryTreeTests.Node<X>> nodes = new LinkedList<>();
        BinaryTreeTests.Node<X> current = root;
        while (current != null) {
            op.accept(current.getValue());
            BinaryTreeTests.Node<X> left = current.left;
            BinaryTreeTests.Node<X> right = current.right;
            if (left != null) {
                current = left;
            }
            if (right != null) {
                nodes.addLast(right);
            }
            if (left == null) {
                current = nodes.isEmpty() ? null : nodes.removeLast();
            }
        }
    }

    public static <X> void preOrderIterativelyFromBook(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        Deque<BinaryTreeTests.Node<X>> nodes = new LinkedList<>();
        nodes.push(root);
        while (!nodes.isEmpty()) {
            BinaryTreeTests.Node<X> current = nodes.pop();
            op.accept(current.getValue());
            if (current.right != null) {
                nodes.push(current.right);
            }
            if (current.left != null) {
                nodes.push(current.left);
            }
        }
    }

    @Override
    <X> void processIteratively(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        preOrderIteratively(root, op);
    }

    @Override
    <X> void processRecursively(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        preOrderRecursively(root, op);
    }

    @Override
    String getExpectedForFirstTestData() {
        return "[a, b, c, d, e, f, g, h, k]";
    }

    @Override
    List<Integer> getExpectedForSecondTestData() {
        return Arrays.asList(8, 3, 1, 6, 4, 7, 10, 14, 12);
    }

    @Test
    public final void testIterativeAlgorithmFromBook() {
        processAlgorithmForFirstData(T1_PreOrderTraversal::preOrderIterativelyFromBook);
    }
}
