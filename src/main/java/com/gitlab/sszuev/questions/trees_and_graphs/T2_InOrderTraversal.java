package com.gitlab.sszuev.questions.trees_and_graphs;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * <b>In order traversal Algorithms</b>
 * The InOrder traversal is also known as the <b>left-node-right</b> or <b>left-root-right</b> traversal or <b>LNR</b> traversal algorithm.
 * It prints all nodes of the tree in sorted order if given binary tree is binary search tree.
 * <p>
 * Here are the exact steps to implement in-order traversal in a binary tree without recursion
 * <ul>
 * <li>1) Start with {@code current = root}</li>
 * <li>2) loop, until {@code stack} is empty or {@code current} becomes {@code null}</li>
 * <li>3) if the {@code current} is not {@code null} push {@code current} into the {@code stack} and {@code current = current.left}</li>
 * <li>4) if the {@code current} is {@code null} then pop from {@code stack}, print its value and {@code current = current.right}</li>
 * </ul>
 *
 * @see <a href='https://www.java67.com/2016/08/binary-tree-inorder-traversal-in-java.html'>Binary tree InOrder traversal using Recursion</a>
 * @see <a href='https://www.java67.com/2016/09/inorder-traversal-of-binary-tree-java-example.html'>The InOrder traversal of Binary tree without Recursion</a>
 */
public class T2_InOrderTraversal extends TraversalAlgorithms {

    @Override
    String getExpectedForFirstTestData() {
        return "[d, c, b, e, a, g, f, h, k]";
    }

    @Override
    List<Integer> getExpectedForSecondTestData() {
        return Arrays.asList(1, 3, 4, 6, 7, 8, 10, 12, 14);
    }

    public static <X> void inOrderIteratively(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        Deque<BinaryTreeTests.Node<X>> nodes = new LinkedList<>();
        BinaryTreeTests.Node<X> current = root;
        while (!nodes.isEmpty() || current != null) {
            if (current == null) {
                current = nodes.removeLast();
                op.accept(current.getValue());
                current = current.right;
            } else {
                nodes.addLast(current);
                current = current.left;
            }
        }
    }

    public static <X> void inOrderRecursively(BinaryTreeTests.Node<X> node, Consumer<X> op) {
        if (node == null) return;
        inOrderRecursively(node.left, op);
        op.accept(node.getValue());
        inOrderRecursively(node.right, op);
    }

    @Override
    <X> void processIteratively(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        inOrderIteratively(root, op);
    }

    @Override
    <X> void processRecursively(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        inOrderRecursively(root, op);
    }
}
