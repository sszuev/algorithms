package com.gitlab.sszuev.questions.trees_and_graphs;

import java.util.*;
import java.util.function.Consumer;

/**
 * <b>Post order traversal Algorithms</b>
 * The post order traversal is also a <b>depth-first algorithm</b> or <b>DFS</b>.
 *
 * @see <a href='https://www.java67.com/2017/05/binary-tree-post-order-traversal-in-java-without-recursion.html'>Post Order Traversal Without Recursion</a>
 * @see <a href='https://www.java67.com/2016/10/binary-tree-post-order-traversal-in.html'>Post order traversal Algorithms</a>
 */
public class T3_PostOrderTraversal extends TraversalAlgorithms {

    @Override
    String getExpectedForFirstTestData() {
        return "[d, c, e, b, g, k, h, f, a]";
    }

    @Override
    List<Integer> getExpectedForSecondTestData() {
        return Arrays.asList(1, 4, 7, 6, 3, 12, 14, 10, 8);
    }

    public static <X> void postOrderIteratively(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        Deque<BinaryTreeTests.Node<X>> nodes = new LinkedList<>();
        nodes.push(root);

        BinaryTreeTests.Node<X> prev = root;
        while (!nodes.isEmpty()) {
            BinaryTreeTests.Node<X> current = nodes.pop();
            if (current.isLeaf() || Objects.equals(prev, current.right) || Objects.equals(prev, current.left)) {
                prev = current;
                op.accept(current.getValue());
                continue;
            }
            nodes.push(current);
            if (current.right != null) {
                nodes.push(current.right);
            }
            if (current.left != null) {
                nodes.push(current.left);
            }
        }
    }

    public static <X> void postOrderRecursively(BinaryTreeTests.Node<X> node, Consumer<X> op) {
        if (node == null) return;
        postOrderRecursively(node.left, op);
        postOrderRecursively(node.right, op);
        op.accept(node.getValue());
    }

    @Override
    <X> void processIteratively(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        postOrderIteratively(root, op);
    }

    @Override
    <X> void processRecursively(BinaryTreeTests.Node<X> root, Consumer<X> op) {
        postOrderRecursively(root, op);
    }
}
