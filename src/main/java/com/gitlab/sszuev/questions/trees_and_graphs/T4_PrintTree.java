package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @see <a href='https://www.java67.com/2016/09/how-to-print-all-leaf-nodes-of-binary-tree-in-java.html'>How to Print all leaf Nodes of a Binary - Coding Interview Questions</a>
 */
public class T4_PrintTree {

    @Test
    public void testXXX() throws IOException {
        BinaryTreeTests.Node<?> n = BinaryTreeTests.createSampleStringTree();
        Assertions.assertNotNull(n);

        Charset charset = StandardCharsets.UTF_8;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (PrintStream out = new PrintStream(baos, true, charset.name())) {
            printLeaves(n, out);
        }
        String content = baos.toString(charset);
        System.out.println(content);
        Assertions.assertNotNull(content);
        Assertions.assertEquals("d e g k", content.trim());
    }

    public static void printLeaves(BinaryTreeTests.Node<?> node, PrintStream out) {
        if (node == null) {
            return;
        }
        if (node.isLeaf()) {
            out.printf("%s ", node.value);
        }
        printLeaves(node.left, out);
        printLeaves(node.right, out);
    }
}
