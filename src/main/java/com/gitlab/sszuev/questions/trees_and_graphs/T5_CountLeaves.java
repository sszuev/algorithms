package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Deque;
import java.util.LinkedList;

/**
 * <b>How to Count Number of Leaf Nodes in a Binary Tree</b>
 *
 * <a href='https://javarevisited.blogspot.com/2016/12/how-to-count-number-of-leaf-nodes-in-java-recursive-iterative-algorithm.html'>How to Count Number of Leaf Nodes in a Binary Tree in Java - Iterative and Recursive Solution</a>
 */
public class T5_CountLeaves {

    @Test
    public void testRecursiveWay() {
        BinaryTreeTests.Node<?> tree = BinaryTreeTests.createSampleStringTree();
        Assertions.assertEquals(4, countLeavesRecursively(tree));
    }

    @Test
    public void testIterativeWay() {
        BinaryTreeTests.Node<?> tree = BinaryTreeTests.createSampleStringTree();
        Assertions.assertEquals(4, countLeavesIteratively(tree));
    }

    public static int countLeavesRecursively(BinaryTreeTests.Node<?> node) {
        if (node == null)
            return 0;

        if (node.isLeaf()) {
            return 1;
        }
        return countLeavesRecursively(node.left) + countLeavesRecursively(node.right);
    }

    public static int countLeavesIteratively(BinaryTreeTests.Node<?> node) {
        if (node == null) {
            return 0;
        }
        Deque<BinaryTreeTests.Node<?>> stack = new LinkedList<>();
        stack.push(node);
        int count = 0;
        while (!stack.isEmpty()) {
            BinaryTreeTests.Node<?> n = stack.pop();
            if (n.left != null)
                stack.push(n.left);
            if (n.right != null)
                stack.push(n.right);
            if (n.isLeaf())
                count++;
        }
        return count;
    }

}
