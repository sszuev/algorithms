package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * <b>How to Implement Binary Search Tree?</b> WTF ?
 *
 * @see <a href='https://javarevisited.blogspot.com/2015/10/how-to-implement-binary-search-tree-in-java-example.html#axzz4wnEtnNB3'>How to Implement Binary Search Tree in Java?</a>
 */
public class T6_BSTSize {

    @Test
    public void testXXX() {
        BST tree = createSample();
        Assertions.assertNotNull(tree.getRoot());
        Assertions.assertFalse(tree.isEmpty());
        Assertions.assertEquals(14, tree.size());

        tree.clear();
        Assertions.assertTrue(tree.isEmpty());
        Assertions.assertEquals(0, tree.size());
    }

    public static BST createSample() {
        BST res = new BST();
        res.root = BST.of(7);

        res.root.left = BST.of(5);
        res.root.right = BST.of(12);

        res.root.left.left = BST.of(3);
        res.root.left.right = BST.of(6);
        res.root.right.left = BST.of(9);
        res.root.right.right = BST.of(15);

        res.root.left.left.left = BST.of(1);
        res.root.left.left.right = BST.of(4);

        res.root.right.left.left = BST.of(8);
        res.root.right.left.right = BST.of(10);
        res.root.right.right.left = BST.of(13);
        res.root.right.right.right = BST.of(17);

        res.root.left.left.left.right = BST.of(2);
        return res;
    }

    /**
     * Java Program to implement a binary search tree.
     * A binary search tree is a sorted binary tree,
     * where value of a node is greater than or equal to its left the child and less than or equal to its right child.
     */
    @SuppressWarnings("FieldCanBeLocal")
    public static class BST {

        private Node root;

        public BST() {
            root = null;
        }

        public Node getRoot() {
            return root;
        }

        /**
         * Java function to check if binary tree is empty or not
         * Time Complexity of this solution is constant O(1) for
         * best, average and worst case.
         *
         * @return true if binary search tree is empty
         */
        public boolean isEmpty() {
            return null == root;
        }

        /**
         * Java function to return number of nodes in this binary search tree.
         * Time complexity of this method is O(n)
         *
         * @return size of this binary search tree
         */
        public int size() {
            Node current = root;
            int size = 0;
            Deque<Node> stack = new ArrayDeque<>();
            while (!stack.isEmpty() || current != null) {
                if (current != null) {
                    stack.push(current);
                    current = current.left;
                } else {
                    size++;
                    current = stack.pop();
                    current = current.right;
                }
            }
            return size;
        }

        /**
         * Java function to clear the binary search tree.
         * Time complexity of this method is O(1)
         */
        public void clear() {
            root = null;
        }

        @SuppressWarnings("unused")
        public static class Node {
            private final int data;
            private Node left, right;

            private Node(int value) {
                data = value;
            }
        }

        public static Node of(int d) {
            return new Node(d);
        }

    }

}
