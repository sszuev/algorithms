package com.gitlab.sszuev.questions.trees_and_graphs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Created by @ssz on 20.10.2020.
 */
abstract class TraversalAlgorithms {

    static <X> List<X> toList(BinaryTreeTests.Node<X> root, BiConsumer<BinaryTreeTests.Node<X>, Consumer<X>> traverse) {
        List<X> res = new ArrayList<>();
        traverse.accept(root, res::add);
        return res;
    }

    abstract <X> void processIteratively(BinaryTreeTests.Node<X> root, Consumer<X> op);

    abstract <X> void processRecursively(BinaryTreeTests.Node<X> root, Consumer<X> op);

    abstract String getExpectedForFirstTestData();

    abstract List<Integer> getExpectedForSecondTestData();

    BinaryTreeTests.Node<String> getFirstTestData() {
        return BinaryTreeTests.createSampleStringTree();
    }

    BinaryTreeTests.Node<Integer> getSecondTestData() {
        return BinaryTreeTests.createSampleIntTree();
    }

    @Test
    public final void testIterativeAlgorithm() {
        processAlgorithmForFirstData(this::processIteratively);
    }

    final void processAlgorithmForFirstData(BiConsumer<BinaryTreeTests.Node<String>, Consumer<String>> f) {
        BinaryTreeTests.Node<String> tree = getFirstTestData();
        String expected = getExpectedForFirstTestData();

        List<String> res = toList(tree, f);
        System.out.println(res);
        Assertions.assertEquals(expected, res.toString());
        res = toList(tree, f);
        Assertions.assertEquals(expected, res.toString());
    }

    @Test
    public final void testRecursiveAlgorithm() {
        BiConsumer<BinaryTreeTests.Node<Integer>, Consumer<Integer>> f = this::processRecursively;
        BinaryTreeTests.Node<Integer> tree = getSecondTestData();
        List<Integer> expected = getExpectedForSecondTestData();

        List<Integer> res = toList(tree, f);
        System.out.println(res);
        Assertions.assertEquals(expected, res);
        res = toList(tree, f);
        Assertions.assertEquals(expected, res);
    }

}
