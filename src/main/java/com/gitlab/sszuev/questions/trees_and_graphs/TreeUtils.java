package com.gitlab.sszuev.questions.trees_and_graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by @ssz on 19.01.2021.
 */
public class TreeUtils {

    static String print(MyTreeNode<?> root) {
        return doPrint(root).toString();
    }

    private static <X> StringBlock doPrint(MyTreeNode<X> root) {
        StringBlock res = new StringBlock();
        List<MyTreeNode<X>> children = root.children().collect(Collectors.toList());
        if (!children.stream().allMatch(Objects::isNull))
            children.stream().map(x -> x == null ? MyTreeNode.createEmpty() : x).forEach(x -> res.addBlock(doPrint(x)));
        res.addLine(Optional.ofNullable(root.getValue()).map(Object::toString).orElse(null));
        return res;
    }

    private static String withSpaces(String str, @SuppressWarnings("SameParameterValue") String space, int length) {
        if (str == null) {
            return withSpaces(space, length);
        }
        if (str.length() == length) {
            return str;
        }
        if (str.length() > length) {
            throw new IllegalArgumentException();
        }
        int left = (length - str.length()) / 2;
        int right = length - left - str.length();
        return withSpaces(space, left) + str + withSpaces(space, right);
    }

    private static String withSpaces(String space, int length) {
        return IntStream.rangeClosed(0, length).mapToObj(x -> "").collect(Collectors.joining(space));
    }

    public static <X> BiNode<X> of(X v, BiNode<X> left, BiNode<X> right) {
        return new BiNode<>(left, right, v);
    }

    public static <X> BiNode<X> of(X v) {
        return new BiNode<>(null, null, v);
    }

    public static <X extends Comparable<X>> boolean isBST(BiNode<X> node) {
        return isBST(node, null, null);
    }

    private static <X extends Comparable<X>> boolean isBST(BiNode<X> root, BiNode<X> min, BiNode<X> max) {
        if (root == null)
            return true;
        X data = root.getValue();
        if ((min != null && data.compareTo(min.data) <= 0) || (max != null && data.compareTo(max.data) > 0)) {
            return false;
        }
        return isBST(root.left, min, root) && isBST(root.right, root, max);
    }

    public static boolean isIntBST(BiNode<Integer> root) {
        return isIntBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private static boolean isIntBST(BiNode<Integer> root, Integer min, Integer max) {
        if (root == null)
            return true;
        if (root.data <= min || root.data > max) {
            return false;
        }
        return isIntBST(root.left, min, root.data) && isIntBST(root.right, root.data, max);
    }

    private static class StringBlock {
        private static final String SPACE = " ";
        private static final String TAB = SPACE + SPACE;
        private static final String NIL = "x";
        private final List<String> lines = new ArrayList<>();

        void addLine(String v) {
            String s;
            if (lines.isEmpty()) {
                s = SPACE + (v == null ? NIL : v) + SPACE;
            } else {
                s = withSpaces(v, SPACE, length());
            }
            lines.add(0, s);
        }

        void addBlock(StringBlock other) {
            int l2 = other.length();
            int s = Math.max(lines.size(), other.lines.size());
            other.formatBlock(s, l2);
            if (lines.isEmpty()) {
                this.lines.addAll(other.lines);
                return;
            }
            int l1 = length();
            formatBlock(s, l1);
            for (int i = 0; i < s; i++) {
                lines.set(i, lines.get(i) + TAB + other.lines.get(i));
            }
        }

        private void formatBlock(int size, int length) {
            int s = lines.size();
            for (int i = 0; i < s; i++) {
                String t = lines.get(i);
                lines.set(i, withSpaces(t, SPACE, length));
            }
            for (int i = 0; i < size - s; i++) {
                lines.add(withSpaces(SPACE, length));
            }
        }

        private int length() {
            return lines.stream().mapToInt(String::length).max().orElse(0);
        }

        @Override
        public String toString() {
            return String.join("\n", lines);
        }
    }

    public static class BiNode<X> implements MyTreeNode<X> {
        private final BiNode<X> left, right;
        private final X data;

        public BiNode(BiNode<X> left, BiNode<X> right, X value) {
            this.left = left;
            this.right = right;
            this.data = Objects.requireNonNull(value);
        }

        @Override
        public String toString() {
            if (left == null && right == null) {
                return "(" + data + ")";
            }
            return String.format("(%s)[%s, %s]", data, left, right);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BiNode<?> biNode = (BiNode<?>) o;
            return Objects.equals(left, biNode.left) &&
                    Objects.equals(right, biNode.right) &&
                    data.equals(biNode.data);
        }

        @Override
        public int hashCode() {
            return Objects.hash(left, right, data);
        }

        @Override
        public Stream<MyTreeNode<X>> children() {
            return Stream.of(left, right);
        }

        @Override
        public X getValue() {
            return data;
        }
    }

}
